Changelog
=========

4.3.0
-----
 * Improved placement of "Add Cloud" button
 * Auto-Click "Add Cloud" when TagTool is empty
 * Show edit buttons (add/rename/delete element) right from the start
 * `server.py` handles signals correctly if PID 1 (when run in docker)
 * Gitlab CI configured to not publish internal version tags
 * Gitlab CI configured to publish images on Docker Hub
 * Moving from `gevent` to `eventlet` web server
 * Demo mode uses additional hand cursor to visualize simulated clicks
   (can be disabled in settings)

4.2.2
-----
 * Added changelog
 * Added MIT license
 * Improved copyright notice

4.2.1
-----
First public release
