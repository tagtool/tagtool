[![build status](/../../../badges/master/pipeline.svg)](/../commits/master)
[![coverage report](/../../../badges/master/coverage.svg)](/../commits/master)

TagTool
=======
TagTool allows you to create as many tag clouds as you like. You can then
link elements inside and between these clouds. It can be used to quickly
approach many knowledge management tasks. You can also change the display
style of the clouds to be lists of URLs.

Here are some example use-cases:
 * You are a team of co-workers and want to track who has which skills to
   provide the team members an easy and fast way to find specialists in the
   team for whatever task they need help with.  
   In this case you would create two clouds "Person" and "Skill", add
   all your co-workers and skills, and finally link them.
 * You want to manage research papers in your team.  
   In that case you could add a cloud called "Paper" and set its display
   style to link list (so you can attach the PDF link to the paper's title).
   You can then add any clouds that help you to categorize the papers
   (e.g. "Topic", "Conference", etc.).
 * You can use the tool to manage your bookmarks.  
   In this case you would create a cloud called "Bookmark" and set its
   display style to link list. You can then add Clouds like "Tag",
   "Topic", or what ever suits your needs.

TagTool also allows you to protect your instance with a password.

Here are the things that TagTool can NOT do for you:
 * Links are unlabeled and undirected. There is no differentiation
   between types of links or their direction. The semantic of the links
   must either be communicated to all users or it must be clear from the
   context.  
   While this can seem like a flaw in design it is actually a powerful
   core concept of TagTool as it makes usage much faster and simpler.
 * Granular access controls.  
   You can set a password to protect you instance and optionally allow
   read-only access for people without password. There is, however, no
   user-management: everyone with the URL (and password if set) can do
   everything.

![Demo screenshot](/../../raw/public-resources/screenshot.png)

There is a live [DEMO](https://demo.letstag.it/) of TagTool.  
During reset there are short downtimes and frontend errors are to be
expected.


Setup with Docker (recommended)
-------------------------------
Create an empty data directory and a log directory

    mkdir -p /tags/myTagTool/data
    mkdir /tags/myTagTool/log

Then copy the default settings file and edit it to your needs (unchanged
settings can be deleted safely). Don't change the port number as the
docker container assumes that port `8080` is used.

    cp default_settings.py /tags/myTagTool/settings.py
    nano /tags/myTagTool/settings.py

Then start up the docker images by calling

    docker run -p 80:8080 -d --restart unless-stopped \
               -v /tags/myTagTool/data:/var/www/tags/data \
               -v /tags/myTagTool/settings.py:/var/www/tags/settings.py \
               -v /tags/myTagTool/log:/var/www/tags/log \
               tagtool/tagtool:latest


Setup bare metal
----------------
Make sure you have the dependencies installed:
 * python 3.5 or higher
 * nodejs and npm
 * the needed python libraries `python3 -m pip install -r requirements.txt`
 * require-js `npm install -g requirejs`

Then copy the default settings file and edit it to your needs (unchanged
settings can be deleted safely):

    cp default_settings.py settings.py
    nano settings.py

To start the server call

    python3 server.py

This server is production ready but performance can presumably be
improved by serving static files via nginx, apache, or similar. If you
decide to do so, you can improve startup times by calling
`python3 server.py -p` once (and after each update) which will render
all static files to `/static`. You can then make your webserver serve
all files relative to that directory, but proxy the following requests
to the python server:
 * GET/POST/PUT/DELETE `/api/**`
 * POST `/login.html`
 * GET `/logout`
