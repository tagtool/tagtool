import logging
log = logging.getLogger(__name__)

import hashlib
import datetime
from functools import wraps
from flask import request

import util


def auth_token(password, salt):
    s = (password + salt).encode('utf-8')
    return hashlib.sha256(s).hexdigest().lower()


def add_auth_to_url(url, password, salt=None):
    auth = password if salt is None else auth_token(password, salt)
    return util.add_params_to_url(url, {'auth': auth})


def add_password_to_url(url, password):
    return util.add_params_to_url(url, {'password': password})


def get_request_auth(cookie=True, auth=True, password=True):
    pw = request.cookies.get('TagToolAuth', None) if cookie else None
    pw = request.values.get('auth', pw) if auth else pw
    return request.values.get('password', pw) if password else pw


def set_auth_cookie(response, password, salt):
    response.set_cookie(
        'TagToolAuth', auth_token(password, salt),
        expires=datetime.datetime.utcnow()+datetime.timedelta(days=365),
        httponly=True)
    return response


def remove_auth_cookie(response):
    response.set_cookie('TagToolAuth', '', expires=0, httponly=True)
    return response


def auth_cookie_set(response):
    for header in response.headers.getlist('Set-Cookie'):
        if header.startswith('TagToolAuth='):
            return True
    return False


def auth_request(password, salt=None, use_auth=True):
    if password is None:
        return True
    pw = get_request_auth(auth=False, cookie=False)
    if pw is not None:
        return password == pw
    if use_auth:
        if salt is None:
            raise TypeError('Specify a salt if allowing authorization by auth')
        pw = get_request_auth(cookie=False, password=False)
        if pw is not None:
            return password == pw or pw.lower() == auth_token(password, salt)
    if salt is not None:
        pw = get_request_auth(auth=False, password=False)
        return auth_token(password, salt) == pw
    return False
