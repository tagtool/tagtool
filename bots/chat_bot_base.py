import random
import re
from threading import Thread
from abc import ABCMeta, abstractmethod
import hashlib
from collections import OrderedDict

from .parser import understand
from .template_matcher import prepare_templates
from .template_matcher import match_templates
from .store_listener import StoreListener
import auth_util

from models.errors import ElementExistsError
from models.errors import ElementMissingError
from models.errors import CategoryExistsError
from models.errors import CategoryMissingError
from models.errors import LinkExistsError
from models.errors import LinkMissingError
from models.errors import StoreError
from models.store import Store
import models.db_diff

from util import flatten

from logging import getLogger

log = getLogger(__name__)


def optionally_add_update_msg(func):
    def inner(*args, **kwargs):
        chat_bot_base = args[0]
        update_msg = kwargs.pop('update_msg', True)
        msg, md_msg = func(*args, **kwargs)
        if update_msg:
            if msg is not None:
                assert md_msg is not None
                return (chat_bot_base.update_msg() + msg,
                        chat_bot_base.update_msg() + md_msg)
            else:
                return msg, md_msg
        else:
            return msg, md_msg

    return inner


# methods decorated with optionally_emitting take an additional
# keyword-parameter `emit`. If emit=True (default) the string returned will be
# emitted else it's returned as string.
def optionally_emitting(func):
    def inner(*args, **kwargs):
        chat_bot_base = args[0]
        emit = kwargs.pop('emit', True)
        msg, md_msg = func(*args, **kwargs)
        if emit and msg:
            chat_bot_base.emit(msg, md_msg)
        else:
            return msg, md_msg

    return inner


def markdown_escape(arg_desc):
    """Apply appropriate markdown escape functions to the arguments first.

The decorated method must be of a class that implements the following
methods:
  * escape_md_elem(str: elem, str: cat) -> str: escaped_elem
  * escape_md_cat(str: cat) -> str: escaped_cat


:param arg_desc: Description of the decorated func's arguments.
It's a list of strings with the same length as the func's number of args.
The string can be:
  * "cat": will call escape_md_cat on that argument
  * "catelem": will assume the argument to be a 2-tuple/-list and escape
               the first as category and the second as element of that cat
  * "elemcat": like catelem but in reverse order
  * "elem <int>": will call escape_md_elem(arg, args[<int>]) on that argument.

Example:
@markdown_escape(['cat', 'elem 0'])
def _remove_from_category(self, category, element):
    do_something(category, element)

is equivalent to:
def _remove_from_category(self, category, element):
    esc_category = self.escape_md_cat(category)
    esc_element = self.escape_md_elem(element, category)
    do_something(esc_category, esc_element)
"""
    def decorator(func):
        def inner(self, *args, **kwargs):
            assert len(args) == len(arg_desc)
            orig_args = list(args)
            args = list(args)
            for i in range(len(args)):
                if arg_desc[i] == 'cat':
                    args[i] = self.escape_md_cat(args[i])
                elif arg_desc[i].startswith('elem '):
                    split = arg_desc[i].split()
                    assert len(split) == 2
                    cat_idx = int(split[1])
                    args[i] = self.escape_md_elem(args[i], orig_args[cat_idx])
                elif arg_desc[i] == 'catelem':
                    cat, elem = args[i]
                    elem = self.escape_md_elem(elem, cat)
                    cat = self.escape_md_cat(cat)
                    args[i] = [cat, elem]
                elif arg_desc[i] == 'elemcat':
                    elem, cat = args[i]
                    elem = self.escape_md_elem(elem, cat)
                    cat = self.escape_md_cat(cat)
                    args[i] = [elem, cat]
                else:
                    raise ValueError('Unknown arg_desc %s' % arg_desc[i])
            return func(self, *orig_args, **kwargs), func(self, *args, **kwargs)
        return inner
    return decorator


def no_markdown_escape(func):
    def inner(*args, **kwargs):
        res = func(*args, **kwargs)
        if isinstance(res, (tuple, list)):
            assert len(res) == 2
            return tuple(res)
        elif isinstance(res, str):
            return res, res
        elif res is None:
            return None, None
        else:
            ValueError('Expect msg to be a string, 2-tuple/-list, or None, '
                       'but got %s' % res)
    return inner


def manage_impl_creations(diffs_func):
    def decorator(func):
        def _implicit_creations(diffs, raw_diff):
            if raw_diff is None:
                raw_diff = models.db_diff.DB_Diff()
            if not isinstance(raw_diff, models.db_diff.DB_Diff):
                raw_diff = models.db_diff.DB_Diff(raw_diff=raw_diff)
            res = {'categories': set(raw_diff['insertions']['categories']),
                   'elements': set(map(tuple,
                                       raw_diff['insertions']['elements']))}
            for diff in diffs:
                if not isinstance(diff, models.db_diff.HighLevelDB_Diff):
                    if isinstance(diff, models.db_diff.DB_Diff):
                        diff = models.db_diff.HighLevelDB_Diff(db_diff=diff)
                    else:
                        diff = models.db_diff.HighLevelDB_Diff(raw_diff=diff)
                res['categories'] -= set(diff['insertions']['categories'] +
                                         [x[1] for x in
                                          diff['renamings']['categories']])
                res['elements'] -= set(map(tuple,
                                           diff['insertions']['elements'] +
                                           [x[1] for x in
                                            diff['renamings']['elements']]))
            res['elements'] = map(list, res['elements'])
            return {k: list(v) for k, v in res.items()}

        def inner(*args, **kwargs):
            raw_diff = kwargs.pop('raw_diff', None)
            impl_creations = kwargs.pop('impl_creations', None)
            if impl_creations is None:
                # args[1:] to cut of self
                diffs = diffs_func(*args[1:], **kwargs)
                impl_creations = _implicit_creations(diffs, raw_diff)
            return func(impl_creations=impl_creations, *args, **kwargs)
        return inner
    return decorator


class ChatBotBase(object):
    UNKNOWN_CMD_STRING = ("Sorry, I don't understand that.\n"
                          "Try `@{0} help` to read, what I can do for you.")
    DISABLED_CMD_STRING = ("Sorry this action isn't available from chat.\n"
                           "Please visit the website at {url}.")
    UNIMPLEMENTED_CMD_STRING = ("Sorry this action isn't available from chat.\n"
                                "It will be coming soon. Until then "
                                "please visit the website at {url}.")
    HELP_STRING = """This is {0}.

I'll assist you in operating the TagTool at {url}.\
 Besides accepting commands I'll also inform you about updates of the Clouds.
I can understand these commands:
 - @{0} help
   (type `@{0} help` to see this help)
 - @{0} add "<c>" "<e>"
   (e.g. `@{0} add Person "John Doe"` to add "John Doe" to the Cloud "Person".)
 - @{0} add link from "<c1>" "<e1>" to "<c2>" "<e2>"
   (e.g. `@{0} add link from Person "John Doe" to Person "Jane Doe"` to add a
    link between the Persons "John Doe" and "Jane Doe". Notice that the order
    doesn't matter at all.)
 - @{0} remove <arguments>
   (see `@{0} add ...` command for possible arguments)
 - @{0} rename "<c>" "<e1>" to "<e2>"
   (e.g. `@{0} rename Person "John Doe" to "Jane Doe"` to rename "John Doe"
   inside of the Cloud "Person" to "Jane Doe".)
 - @{0} what's linked with <c> <e>
   (e.g. `@{0} what's linked with Skill "HTML5"` to see the links of "HTML5")
 - @{0} find <query>
   (e.g. `@{0} find "HTML5"` to search all categories. If a match is found I
   will also list match's links)

If you need to escape `"` use `\\"`. E.g. if you would like to add the cloud\
 `T"est` write `@{0} add "T\\"est"`."""

    _SUCCESS_MESSAGES = [
        "Yes master!",  # first msg is used to respond to force mode messages
        "Alright.", "I did so.", "For sure.", "As you please.", "Job's done!",
        "I did as you asked.", "Done.", "OK."
    ]

    _MESSAGES = {
        'updates': 'Updates from {name} at {url}:\n',
        'new_cat': 'A new cloud "{}" was created.',
        'new_cat_impl_creation_idxs': None,
        'new_elem': '"{}" was added to the cloud "{}"{}.',
        'new_elem_impl_creation_idxs': (1,),
        'new_link': '"{}"{} "{}"{} and "{}"{} "{}"{} were linked.',
        'new_link_impl_creation_idxs': range(4),
        'remove_cat': 'The cloud "{}" (with all it\'s items and their links) '
                      'was removed.',
        'remove_cat_impl_creation_idxs': None,
        'remove_elem': '"{}" (with all it\'s links) was removed from the cloud '
                       '"{}".',
        'remove_elem_impl_creation_idxs': None,
        'remove_link': '"{}"{} "{}"{} and "{}"{} "{}"{} were unlinked.',
        'remove_link_impl_creation_idxs': range(4),
        'rename_elem_in_cat': 'The item "{}" "{}" was renamed to "{}".',
        'rename_elem_in_cat_impl_creation_idxs': None,
        'rename_elem': 'The item "{}" "{}" was moved to "{}"{} "{}"{}.',
        'rename_elem_impl_creation_idxs': (2, 3),
        'rename_cat': 'The category "{}" was renamed to "{}".',
        'rename_cat_impl_creation_idxs': None
    }

    _FRAGMENTS = {
        'new': ' (new)'
    }

    _LIST_MESSAGES = {
        'head': {
            'cat': 'The current categories are:\n',
            'elem': 'The category "{}" contains:\n',
            'link': 'These things are linked with "{}" "{}":\n',
            'search': 'Found "{}" "{}", linked with:\n',
            'search_many': 'Found possible matches:\n',
            'template': 'You might be looking for:\n',
        },
        'empty': {
            'cat': 'There are no categories.',
            'elem': 'The category "{}" is empty.',
            'link': 'Nothing is linked with "{}" "{}"',
            'search': "Sorry. I couldn't find {}.",
        },
        'body': {
            'cat': '  * "{}"',
            'elem': '  * "{}"',
            'link': '  * "{}" "{}"',
            'catelem': '  * "{}" "{}"'
        }
    }

    __metaclass__ = ABCMeta

    def __init__(self, store, settings, name, url, allow_cat_actions=False,
                 digest_minutes=None, password=None, salt=None,
                 query_templates=None):
        self.store = store
        self.settings = settings
        self.running = False
        self.name = name
        self.password = password
        self.salt = salt
        self.query_templates = query_templates or []
        prepare_templates(self.query_templates)
        self.raw_url = url
        self.cat_enabled = allow_cat_actions
        self._listener = StoreListener(store, {
            'add_category': self._add_category,
            'remove_category': self._remove_category,
            'rename_category': self._rename_category,
            'add_to_category': self._add_to_category,
            'remove_from_category': self._remove_from_category,
            'rename_element': self._rename_element,
            'add_link': self._add_link,
            'remove_link': self._remove_link,
            'diff_action': self._diff_action,
            'diff_actions': self._diff_actions
        }, digest_minutes=digest_minutes)
        self.inDigestMode = bool(digest_minutes)
        self.thread = Thread(target=self._listen)
        self.before_start()
        # corresponding stop is called from within thread
        self._start_store_listener()
        self.thread.start()
        self.after_start(self.thread)

    def before_start(self):
        """Override this hook to perform set up tasks before entering the
        listening loop."""
        pass

    def _start_store_listener(self):
        self._listener.register()

    def after_start(self, thread):
        """Override this hook to perform tasks after starting the listening
        loop."""
        pass

    def _stop_store_listener(self):
        self._listener.unregister()

    def after_run(self):
        """Override this hook to perform clean up tasks after listening loop
        terminated."""
        pass

    def _listen(self):
        self.running = True
        if self.listen_is_implemented():
            self.listen()
            self._stop_store_listener()
            self.after_run()

    def listen(self):
        """Set up connection and listen for messages.

        This method is run in a own thread. If you don't need an extra thread
        for listening, just don't override this method.

        Check self.running in the loop. If set to False you may terminate the
        loop."""
        pass

    def stop(self):
        self.running = False
        if not self.listen_is_implemented():
            self._stop_store_listener()
            self.after_run()

    def url(self):
        if self.password is not None:
            return auth_util.add_auth_to_url(self.raw_url,
                                             self.password,
                                             self.salt)
        else:
            return self.raw_url

    def update_msg(self):
        return ChatBotBase._MESSAGES['updates'].format(name=self.name,
                                                       url=self.url())

    def _no_md_emit(self, message, *args, **kwargs):
        self.emit(message, message, *args, **kwargs)

    @abstractmethod
    def emit(self, message, md_message, *args, **kwargs):
        """Is called, when a message should be send to chat.

        :type message: str
        :type md_message: str"""
        pass

    def _diff_handler(self, diff):
        pass

    def _match_templates(self, message, *args, **kwargs):
        matches = match_templates(message, self.query_templates, self.store)
        if not matches:
            log.debug('Template matcher returned no results.')
            return
        log.info('Template matcher returned: %s', matches)
        response_md = response = ChatBotBase._LIST_MESSAGES['head']['template']
        response += '\n'.join(
            map(lambda x: ChatBotBase._LIST_MESSAGES['body']['elem']
                                     .format(x[1]),
                matches))
        response_md += '\n'.join(
            map(lambda x: ChatBotBase._LIST_MESSAGES['body']['elem']
                                     .format(self.escape_md_elem(x[1], x[0])),
                matches))
        self.emit(response, response_md, *args, **kwargs)

    def receive(self, message, *args, **kwargs):
        """Call this, when a message is received.

        :type message: unicode
        :param private=False
            If set to True the message is treated as a message in a private
            chat. This means, that the mention before the command is optional.

        *args and **kwargs are passed to emit call if one happens"""

        # ------------------Understand the message------------------------------
        def shorten_msg(msg_):
            lines = msg_.split('\n')
            return (lines[0] + '[...]') if len(lines) > 1 else msg_

        private = kwargs.pop('private', False)
        name_re = r'^(?:@?{}(?:(?:\s*:\s*)|(?:\s+)))'.format(self.name)
        if private:
            m = re.match(name_re + r'?(.*)$', message)
            assert m
            msg = m.groups()[0]
        else:
            m = re.match(name_re + r'(.*)$', message)
            if not m:
                log.debug('Message was not addressed to me: %s' %
                          shorten_msg(message))
                self._match_templates(message, *args, **kwargs)
                return
            msg = m.groups()[0]

        cmd, target, call_args, force = understand(msg)

        if cmd == -1:
            cmd = 'unknown'
        elif cmd in ('add', 'remove'):
            cmd += '_' + target
            if target == 'cat' and not self.cat_enabled:
                cmd = 'forbidden'
            elif target == 'elem':
                call_args = call_args[0]
        elif cmd == 'rename':
            cmd += '_' + target
            if target == 'cat' and not self.cat_enabled:
                cmd = 'forbidden'
            elif target == 'elem':
                if isinstance(call_args[1], str):
                    call_args = call_args[0] + [call_args[0][0], call_args[1]]
                else:
                    call_args = call_args[0] + call_args[1]
        elif cmd == 'list':
            call_args = [target] + call_args
        elif cmd in ('help', 'search'):
            pass
        else:
            log.error('Unknown command: %s' % cmd)

        # -----------------------react to message-------------------------------
        call_kwargs = {}
        call_mapping = {
            'help':
                lambda: self._no_md_emit(
                    ChatBotBase.HELP_STRING.format(self.name, url=self.url()),
                    *args, **kwargs),
            'unknown': lambda: self._no_md_emit(
                ChatBotBase.UNKNOWN_CMD_STRING.format(self.name),
                *args, **kwargs),
            'forbidden': lambda: self._no_md_emit(
                ChatBotBase.DISABLED_CMD_STRING.format(url=self.url()),
                *args, **kwargs),
            'not_implemented': lambda: self._no_md_emit(
                ChatBotBase.UNIMPLEMENTED_CMD_STRING.format(url=self.url()),
                *args, **kwargs),
            'add_elem': self.store.add_to_category,
            'add_cat': self.store.add_category,
            'add_link': self.store.add_link,
            'remove_elem': self.store.remove_from_category,
            'remove_cat': self.store.remove_category,
            'remove_link': self.store.remove_link,
            'rename_elem': self.store.rename_element,
            'rename_cat': self.store.rename_category,
            'list': lambda *l_args, **l_kwargs: self._no_md_emit(
                self._nice_print_list(*l_args, **l_kwargs), *args, **kwargs),
            'search': lambda query: self._no_md_emit(self._search(query),
                                                     *args, **kwargs)
        }

        def error_emit(err):
            self._no_md_emit("Sorry. I'm afraid I can't do this...\n" +
                             str(err), *args, **kwargs)

        def try_force_emit(err):
            self._no_md_emit(
                "Sorry. I'm afraid I can't do this...\n" + str(err) +
                "\nTo implicitly create missing things try "
                "`" + message + "!`.",
                *args, **kwargs)

        try:
            if cmd in ('add_elem', 'add_cat', 'add_link',
                       'remove_elem', 'remove_cat', 'remove_link'):
                call_kwargs = {'strict': True}
            if cmd in ('forbidden', 'unknown', 'help', 'not_implemented'):
                call_args = []
                call_kwargs = {}
            log.debug('Calling: %s, %s, %s' % (cmd, call_args, call_kwargs))
            call_mapping[cmd](*call_args, **call_kwargs)
            if self.inDigestMode:
                self._listener.force_digest_flush()
        except StoreError as e:
            log.debug('Call failed: ' + e.__repr__())
            if (e.strict_fixable and (not isinstance(e, CategoryMissingError) or
                                      self.cat_enabled)):
                if force:
                    call_kwargs = {'strict': False}
                    log.debug('Calling again (creative now): %s, %s, %s' %
                              (cmd, call_args, call_kwargs))
                    call_mapping[cmd](*call_args, **call_kwargs)
                    self._listener.force_digest_flush()
                else:
                    try_force_emit(e)
            else:
                error_emit(e)

    def _nice_print_list(self, target, *args):
        """
        :raises:
            StoreError: e.g. if you want to list a non-existing category
        """
        assert target in (None, 'cat', 'elem')
        if target is None:
            cats = self.store.get_all_categories()
            if cats:
                msg = ChatBotBase._LIST_MESSAGES['head']['cat'].format(
                    url=self.url())
                msg += '\n'.join(map(lambda x:
                                     ChatBotBase._LIST_MESSAGES['body'][
                                         'cat'].format(x), cats))
            else:
                msg = ChatBotBase._LIST_MESSAGES['empty']['cat'].format(
                    url=self.url())
        elif target == 'cat':
            assert isinstance(args, (list, tuple))
            assert len(args) == 1
            assert isinstance(args[0], str)
            elems = self.store.get_all_category_members(args[0])
            if elems:
                msg = ChatBotBase._LIST_MESSAGES['head']['elem'] \
                    .format(args[0], url=self.url())
                msg += '\n'.join(map(
                    lambda x: ChatBotBase._LIST_MESSAGES['body']['elem'].format(
                        x[1]), elems))
            else:
                msg = ChatBotBase._LIST_MESSAGES['empty']['elem'].format(
                    args[0], url=self.url())
        else:  # target == 'elem'
            assert isinstance(args, (list, tuple))
            assert len(args) == 1
            assert isinstance(args[0], (list, tuple))
            assert len(args[0]) == 2
            assert isinstance(args[0][0], str)
            assert isinstance(args[0][1], str)
            links = self.store.get_all_links(args[0])
            if links:
                msg = ChatBotBase._LIST_MESSAGES['head']['link'] \
                    .format(*args[0], url=self.url())
                for e1, e2 in links:
                    e = e2 if e1 == list(args[0]) else e1
                    msg += '\n'+ChatBotBase._LIST_MESSAGES['body']['link']\
                        .format(*e)
            else:
                msg = ChatBotBase._LIST_MESSAGES['empty']['link'].format(
                    *args[0], url=self.url())

        return msg

    def _search(self, query):
        res = self.store.search_element(query)
        if len(res) == 0:
            msg = ChatBotBase._LIST_MESSAGES['empty']['search'].format(query)
        elif len(res) == 1:
            msg = ChatBotBase._LIST_MESSAGES['head']['search'].format(*res[0])
            linked = self.store.get_all_linked(res[0])
            msg += '\n'.join(
                [ChatBotBase._LIST_MESSAGES['body']['catelem'].format(*l)
                 for l in linked]
            )
        else:
            msg = ChatBotBase._LIST_MESSAGES['head']['search_many']
            msg += '\n'.join(
                [ChatBotBase._LIST_MESSAGES['body']['catelem'].format(*r)
                 for r in res]
            )
        return msg

    # where the magic happens
    def listen_is_implemented(self):
        """Return True if the current class has overridden the listen method."""
        return self.__class__.listen is not ChatBotBase.listen

    def _full_md_escape(self, str_):
        return re.subn(r'([\\`\*_{}\[\]()#+\-.!~>])', r'\\\1', str_)[0]

    URL_REPLACE = {' ': '%20', '\t': '%09', '\n': '%0A', '\v': '%0B',
                   '\f': '%0C', '\r': '%0D'}
    URL_REPLACE_PATTERN = re.compile('|'.join(URL_REPLACE.keys()))

    def escape_md_elem(self, elem, cat):
        if self.settings.get()['display-type'].get(cat, None) == 'links':
            # cat is link cat
            match = Store.LINK_RE.match(elem)
            if not match:
                return self._full_md_escape(elem)
            title, url = match.groups()
            url_esc = ChatBotBase.URL_REPLACE_PATTERN.sub(
                lambda x: ChatBotBase.URL_REPLACE[x.group()], url)
            return "[{}]({})".format(self._full_md_escape(title), url_esc)
        else:
            return self._full_md_escape(elem)

    def escape_md_cat(self, cat):
        return self._full_md_escape(cat)

    # Store listener callbacks -------------------------------------------------
    @staticmethod
    def _format_msg(key, items, new):
        fmt_args = items
        impl_creation_idxs = ChatBotBase._MESSAGES[key+'_impl_creation_idxs']
        if impl_creation_idxs is not None:
            if not new or new is True:
                new = [bool(new)]*len(items)
            for offset, idx in enumerate(impl_creation_idxs):
                new_fmt_arg = ChatBotBase._FRAGMENTS['new'] if new[idx] else ''
                fmt_args = (fmt_args[:idx + offset + 1] + [new_fmt_arg] +
                            fmt_args[idx + offset + 1:])
        return ChatBotBase._MESSAGES[key].format(*fmt_args)

    @optionally_emitting
    @optionally_add_update_msg
    @markdown_escape(['cat'])
    @manage_impl_creations(lambda c: [{'insertions': {'categories': [c]}}])
    def _add_category(self, category, impl_creations=None):
        return ChatBotBase._format_msg(
            'new_cat', [category], [category in impl_creations['categories']])

    @optionally_emitting
    @optionally_add_update_msg
    @markdown_escape(['cat'])
    @manage_impl_creations(lambda c: [{'insertions': {'categories': [c]}}])
    def _remove_category(self, category, impl_creations=None):
        return ChatBotBase._format_msg(
            'remove_cat', [category],
            [category in impl_creations['categories']])

    @optionally_emitting
    @optionally_add_update_msg
    @markdown_escape(['cat', 'cat'])
    @manage_impl_creations(lambda c1, c2:
                           [{'renamings': {'categories': [[c1, c2]]}}])
    def _rename_category(self, old_name, new_name, impl_creations=None):
        return ChatBotBase._format_msg(
            'rename_cat', [old_name, new_name],
            [old_name in impl_creations['categories'],
             new_name in impl_creations['categories']])

    @optionally_emitting
    @optionally_add_update_msg
    @markdown_escape(['cat', 'elem 0'])
    @manage_impl_creations(lambda c, e:
                           [{'insertions': {'elements': [[c, e]]}}])
    def _add_to_category(self, category, element, impl_creations=None):
        return ChatBotBase._format_msg(
            'new_elem', [element, category],
            [[category, element] in impl_creations['elements'],
             category in impl_creations['categories']])

    @optionally_emitting
    @optionally_add_update_msg
    @markdown_escape(['cat', 'elem 0'])
    @manage_impl_creations(lambda c, e:
                           [{'deletions': {'elements': [[c, e]]}}])
    def _remove_from_category(self, category, element, impl_creations=None):
        return ChatBotBase._format_msg(
            'remove_elem', [element, category],
            [[category, element] in impl_creations['elements'],
             category in impl_creations['categories']])

    @optionally_emitting
    @optionally_add_update_msg
    @markdown_escape(['cat', 'elem 0', 'cat', 'elem 2'])
    @manage_impl_creations(lambda c1, e1, c2, e2:
                           [{'renamings': {'elements': [[[c1, e1], [c2, e2]]]}}])
    def _rename_element(self, old_cat, old_name, new_cat, new_name,
                        impl_creations=None):
        if old_cat == new_cat:
            return ChatBotBase._format_msg(
                'rename_elem_in_cat', [old_cat, old_name, new_name],
                [old_cat in impl_creations['categories'],
                 [old_cat, old_name] in impl_creations['elements'],
                 [old_cat, new_name] in impl_creations['elements']])
        else:
            return ChatBotBase._format_msg(
                'rename_elem', [old_cat, old_name, new_cat, new_name],
                [old_cat in impl_creations['categories'],
                 [old_cat, old_name] in impl_creations['elements'],
                 new_cat in impl_creations['categories'],
                 [new_cat, new_name] in impl_creations['elements']])

    @optionally_emitting
    @optionally_add_update_msg
    @markdown_escape(['catelem', 'catelem'])
    @manage_impl_creations(lambda e1, e2, **_:
                           [{'insertions': {'links': [[e1, e2]]}}])
    def _add_link(self, element1, element2, impl_creations=None):
        if element2 < element1:
            element1, element2 = element2, element1
        return ChatBotBase._format_msg(
            'new_link', element1 + element2,
            [element1[0] in impl_creations['categories'],
             element1 in impl_creations['elements'],
             element2[0] in impl_creations['categories'],
             element2 in impl_creations['elements']])

    @optionally_emitting
    @optionally_add_update_msg
    @markdown_escape(['catelem', 'catelem'])
    @manage_impl_creations(lambda e1, e2, **_:
                           [{'deletions': {'links': [[e1, e2]]}}])
    def _remove_link(self, element1, element2, impl_creations=None):
        if element2 < element1:
            element1, element2 = element2, element1
        return ChatBotBase._format_msg(
            'remove_link', element1 + element2,
            [element1[0] in impl_creations['categories'],
             element1 in impl_creations['elements'],
             element2[0] in impl_creations['categories'],
             element2 in impl_creations['elements']])

    @optionally_emitting
    @optionally_add_update_msg
    @no_markdown_escape
    @manage_impl_creations(lambda diff: [diff])
    def _diff_action(self, diff, impl_creations=None):
        msgs = []
        md_msgs = []
        for action, obj, func in \
                (('insertions', 'categories', self._add_category),
                 ('insertions', 'elements', self._add_to_category),
                 ('insertions', 'links', self._add_link),
                 ('renamings', 'categories', self._rename_category),
                 ('renamings', 'elements', self._rename_element),
                 ('deletions', 'links', self._remove_link),
                 ('deletions', 'elements', self._remove_from_category),
                 ('deletions', 'categories', self._remove_category)):
            try:
                instances = diff[action][obj]
            except KeyError:  # diff can be sparse
                instances = []
                pass
            if obj == 'links':
                # sort the link pairs and then the links
                instances = sorted([sorted(link) for link in instances])
            for instance in instances:
                if not isinstance(instance, (tuple, list)):
                    instance = (instance,)
                if action == 'renamings' and obj == 'elements':
                    instance = flatten(instance)
                msg, md_msg = func(*instance, emit=False, update_msg=False,
                                   impl_creations=impl_creations)
                msgs.append(msg)
                md_msgs.append(md_msg)
        if msgs:
            return '\n'.join(msgs), '\n'.join(md_msgs)

    @optionally_emitting
    @optionally_add_update_msg
    @no_markdown_escape
    @manage_impl_creations(lambda diffs: diffs)
    def _diff_actions(self, diffs, impl_creations=None):
        msgs = []
        md_msgs = []
        for diff in diffs:
            msg, md_msg = self._diff_action(diff, emit=False, update_msg=False,
                                            impl_creations=impl_creations)
            if msg is not None:
                assert md_msg is not None
                msgs.append(msg)
                md_msgs.append(md_msg)
        if msgs:
            return '\n'.join(msgs), '\n'.join(md_msgs)
