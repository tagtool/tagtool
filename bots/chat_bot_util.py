def split_msg(msg, max_length=None):
    if max_length is None:
        return [msg]
    lines = msg.split('\n')
    msgs = [""]
    for line in lines:
        nl = bool(msgs[-1])
        line_len = len(line)
        if line_len + len(msgs[-1]) + nl <= max_length:
            # line fits into current message
            msgs[-1] += ("\n" if nl else "") + line
        else:
            # will need a new message
            if not msgs[-1]:
                # current message is empty
                msgs.pop()
            # line does not fit into current message
            if line_len <= max_length:
                # start a new line if message fits into it
                msgs.append(line)
            else:
                # message doesn't fit into a line => split the line
                msgs += [line[i:i + max_length]
                         for i in range(0, line_len, max_length)]
    return msgs


