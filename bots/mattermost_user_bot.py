from chat_bot_base import ChatBotBase
import requests
import json
from datetime import datetime
from time import sleep
from pyquery import PyQuery as pq
import re
import socket
import websocket

from bots.chat_bot_util import split_msg

from logging import getLogger
log = getLogger(__name__)


class MattermostUserBot(ChatBotBase):
    MAX_MSG_CHARS = 5000

    def __init__(self, store, settings, url, chat_url, team_name, use_gitlab_auth,
                 use_gitlab_ldap_auth, gitlab_url, login_name, password,
                 app_pw, app_salt, query_templates, ignored_chans=[]):
        self.chat_url = chat_url[:-1] if chat_url.endswith('/') else chat_url
        self.team_name = team_name
        self.use_gitlab_auth = use_gitlab_auth
        self.use_gitlab_ldap_auth = use_gitlab_ldap_auth
        self.gitlab_url = \
            gitlab_url[:-1] if chat_url.endswith('/') else gitlab_url
        self.login_name = login_name
        self.password = password
        self.channel_ids = None
        self.team_id = None
        self.user_id = None
        self.session = requests.Session()
        self.session.hooks = {'response': self._result_hook}
        self.session.headers['X-Requested-With'] = 'XMLHttpRequest'
        self.session.headers['Content-Type'] = 'application/json;text/html'
        self.request_errors = 0
        self.websocket = None
        self.ignored_chans = ignored_chans
        ChatBotBase.__init__(self, store, settings, self.login_name, url,
                             password=app_pw, salt=app_salt,
                             query_templates=query_templates)

    def _result_hook(self, r, *args, **kwargs):
        if self.request_errors >= 5:
            log.error('MattermostUserBot encountered a problem.')
            log.error(r.status_code, r.reason, r.text)
            r.raise_for_status()
        if r.status_code == requests.codes.unauthorized:
            self.request_errors += 1
            if not self._mattermost_cookie_was_set():
                raise ValueError('Login data is invalid.')
            # we've been logged out => try to log back in
            self._reset_mattermost_cookie()
            self._do_login()
            return self.session.send(r.requset)
        elif r.status_code == requests.codes.too_many:
            sleep(1)
            self.request_errors += 1
            return self.session.send(r.request)
        elif r.status_code >= 400:
            self.request_errors += 1
            return self.session.send(r.request)
        else:
            if 200 <= r.status_code < 300:
                # don't do it on redirects
                self.request_errors = 0
            return r

    def _gitlab_login(self):
        # as copied from
        # http://stackoverflow.com/questions/36650437/using-mattermost-api-via-gitlab-oauth-as-an-end-user-with-username-and-password
        r = self.session.get(self.chat_url + '/api/v3/oauth/gitlab/login')
        q = pq(r.content)
        if self.use_gitlab_ldap_auth:
            form_id = '#new_ldap_user'
            user_field = 'username'
            pw_field = 'password'
        else:
            form_id = '#new_user'
            user_field = 'user[login]'
            pw_field = 'user[password]'
        csrf_token = q(form_id + ' input[name="authenticity_token"]')[0].value

        self.session.post(self.gitlab_url + q(form_id).attr('action'),
                          data={'authenticity_token': csrf_token,
                                user_field: self.login_name,
                                pw_field: self.password},
                          headers={'content-type':
                                   'application/x-www-form-urlencoded'})

        if not self._mattermost_cookie_was_set():
            raise ValueError('Gitlab login failed.')

    def _api_login(self):
        self.session.post(self.chat_url + '/api/v3/users/login',
                          data=json.dumps({'name': self.team_name,
                                           'login_id': self.login_name,
                                           'password': self.password}))

    def _set_user_id(self):
        self.user_id = self.session.get(
            self.chat_url + '/api/v3/users/me').json()['id']

    def _set_team_id(self):
        teams = self.session.get(self.chat_url +
                                 '/api/v3/teams/all').json()
        for team_id in teams:
            if teams[team_id]['name'] == self.team_name:
                self.team_id = team_id
                break
        if self.team_id is None:
            raise Exception('Team name W%sW could not be found'
                            % self.team_name)

    def _set_channel_ids(self):
        url = self.chat_url + '/api/v3/teams/' + self.team_id + '/channels/'
        channels = self.session.get(url).json()['channels']
        self.channel_ids = [c['id'] for c in channels
                            if c['display_name'] not in self.ignored_chans and
                            c['type'] == 'O']  # typ 'O' excludes private chats

    def _mattermost_cookie_was_set(self):
        return self.session.cookies.get('MMAUTHTOKEN') is not None

    def _reset_mattermost_cookie(self):
        self.session.cookies.pop('MMAUTHTOKEN')

    def _do_login(self):
        if self.use_gitlab_auth:
            self._gitlab_login()
        else:
            self._api_login()
        self._set_user_id()
        self._set_team_id()
        self._set_channel_ids()

    def before_start(self):
        self._do_login()

    def emit(self, message, md_message, channel_ids=None, *args, **kwargs):
        message = md_message
        if channel_ids is None:
            channel_ids = self.channel_ids
        now = datetime.now()
        now_s = int(now.strftime('%s'))
        msgs = split_msg(message, max_length=MattermostUserBot.MAX_MSG_CHARS)
        for channel_id in channel_ids:
            for msg in msgs:
                self.session.post(self.chat_url + '/api/v3/teams/' +
                                  self.team_id + '/channels/' + channel_id +
                                  '/posts/create',
                                  data=json.dumps(
                                      {'message': msg,
                                       'channel_id': channel_id,
                                       'created_at': now_s,
                                       'filename': [],
                                       'pending_post_id':
                                           '%s:%i' % (self.user_id, now_s),
                                       'user_id': self.user_id}))

    def _get_websocket(self):
        ws_url = re.sub(r'^(http)', 'ws', self.chat_url)
        url = ws_url + '/api/v3/users/websocket'
        log.debug('Starting websocket %s' % url)
        return websocket.create_connection(
            url, timeout=5,
            header={"Cookie": "MMAUTHTOKEN=%s" %
                    self.session.cookies.get('MMAUTHTOKEN')})

    def listen(self):
        reconnects = 1
        ws = self._get_websocket()
        try:
            while self.running:
                try:
                    res = ws.recv()
                except websocket.WebSocketTimeoutException:
                    continue
                except (websocket.WebSocketException, socket.error) as e:
                    if reconnects > 5:
                        raise
                    log.error('Websocket error: %s (%s) '
                              'Trying to reconnect %i/5' % (e, e.message,
                                                            reconnects))
                    ws.close()
                    ws = self._get_websocket()
                    reconnects += 1
                    continue
                reconnects = 1
                res = json.loads(res)
                if res['action'] == 'posted':
                    log.debug('Got message: %s' % res)
                    self.receive(json.loads(res['props']['post'])['message'],
                                 private=res['props']['channel_type'] == 'D',
                                 channel_ids=[res['channel_id']])
        finally:
            log.debug('MattermostUserBot terminated')
            ws.close()
