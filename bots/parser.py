from copy import deepcopy
import re
import pyparsing as pp

from logging import getLogger
log = getLogger(__name__)


def _build_parser():
    ################
    # basic tokens #
    ################
    ws = pp.White()
    please = pp.CaselessLiteral("please")
    help = pp.CaselessLiteral("help")
    from_ = pp.CaselessLiteral("from")
    of = pp.CaselessLiteral("of")
    to = pp.CaselessLiteral("to")
    in_ = pp.CaselessLiteral("in")
    is_ = pp.CaselessLiteral("is")
    for_ = pp.CaselessLiteral("for")
    what = pp.CaselessLiteral("what")
    whats = pp.CaselessLiteral("what's")
    are = pp.CaselessLiteral("are")
    me = pp.CaselessLiteral("me")
    the = pp.CaselessLiteral("the")
    inside = pp.CaselessLiteral("inside")
    within = pp.CaselessLiteral("within")
    into = pp.CaselessLiteral("into")
    and_ = pp.CaselessLiteral("and")
    with_ = pp.CaselessLiteral("with")
    between = pp.CaselessLiteral("between")
    delete = pp.CaselessLiteral("delete")
    remove = pp.CaselessLiteral("remove")
    drop = pp.CaselessLiteral("drop")
    link = pp.CaselessLiteral("link")
    edge = pp.CaselessLiteral("edge")
    connection = pp.CaselessLiteral("connection")
    links = pp.CaselessLiteral("links")
    unlink = pp.CaselessLiteral("unlink")
    linked = pp.CaselessLiteral("linked")
    connected = pp.CaselessLiteral("connected")
    element = pp.CaselessLiteral("element")
    elements = pp.CaselessLiteral("elements")
    item = pp.CaselessLiteral("item")
    items = pp.CaselessLiteral("items")
    category = pp.CaselessLiteral("category")
    categories = pp.CaselessLiteral("categories")
    cloud = pp.CaselessLiteral("cloud")
    clouds = pp.CaselessLiteral("clouds")
    add = pp.CaselessLiteral("add")
    new = pp.CaselessLiteral("new")
    create = pp.CaselessLiteral("create")
    rename = pp.CaselessLiteral("rename")
    change = pp.CaselessLiteral("change")
    move = pp.CaselessLiteral("move") | pp.CaselessLiteral("mv")
    show = pp.CaselessLiteral("show")
    list_ = pp.CaselessLiteral("list") | pp.CaselessLiteral("ls")
    dump = pp.CaselessLiteral("dump")
    all_ = pp.CaselessLiteral("all")
    total = pp.CaselessLiteral("total")
    the = pp.CaselessLiteral("the")
    whole = pp.CaselessLiteral("whole")
    complete = pp.CaselessLiteral("complete")
    everything = pp.CaselessLiteral("everything")
    search = pp.CaselessLiteral("search")
    find = pp.CaselessLiteral("find")
    qm = pp.Literal("?")
    em = pp.Literal("!")

    total_word = (
        (all_ + pp.Optional(ws + the)) |
        (pp.Optional(the + ws) + (total | whole | complete)) |
        everything)
    inside_word = inside | in_ | within
    of_word = of | from_
    what_is = (what + ws + is_) | whats
    what_are = what + ws + are
    linked_word = linked | connected
    cat_word = category | cloud
    cats_word = categories | clouds
    elem_word = element | item
    elems_word = elements | items
    link_word = link | connection | edge
    with_or_to = with_ | to

    unquoted_id = pp.Regex(r'\S+')
    unquoted_last_id = pp.Regex(r'(?!(please))\S+(?<!\!)')
    quoted_id = pp.Regex(r'"([^"]|\\")+(?<!\\)"')

    id_ = (quoted_id | unquoted_id)
    last_id = (quoted_id | unquoted_last_id)
    d_id = (id_.setResultsName('c') + ws + id_.setResultsName('e'))
    last_d_id = (id_.setResultsName('c') + ws + last_id.setResultsName('e'))

    help_kw = help
    add_kw = (add + ws + new) | add | new | create
    remove_kw = drop | remove | delete
    rename_kw = rename | change | move
    list_kw = (show + pp.Optional(ws + me)) | list_ | dump | what_are
    search_kw = search + pp.Optional(ws + for_) | find
    mv_infix = into | to | in_

    #################
    # help commands #
    #################
    help_base = help_kw.setResultsName('target_none')
    help_cmds = (help_base + pp.Optional(ws + me)).setResultsName('cmd_help')

    ####################
    # element commands #
    ####################
    elem_args = d_id.setResultsName('elem')
    last_elem_args = last_d_id.setResultsName('elem')
    add_elem_cmd = (
        add_kw + ws +
        (pp.Optional(elem_word + ws) +
         last_elem_args).setResultsName('target_elem'))
    rm_elem_cmd = (
        remove_kw + ws +
        (pp.Optional(elem_word + ws) +
         last_elem_args).setResultsName('target_elem'))
    rename_elem_cmd = (
        rename_kw + ws +
        (pp.Optional(elem_word + ws) + elem_args + ws +
         pp.Optional(mv_infix + ws) +
         (last_d_id.setResultsName('new') | last_id.setResultsName('new'))
         ).setResultsName('target_elem'))
    list_elem_cmd = (
        ((list_kw +
          pp.Optional(ws + total_word) + pp.Optional(ws + the) +
          ((pp.Optional(ws + links) + pp.Optional(ws + of_word)) |
           pp.Optional(ws + linked_word + ws + with_or_to)))
         ).setResultsName('target_elem') + ws +
        last_elem_args) | (
        (what_is + ws + linked_word + ws + with_or_to
         ).setResultsName('target_elem') + ws + last_elem_args
    )
    elem_cmds = (add_elem_cmd.setResultsName('cmd_add') |
                 rm_elem_cmd.setResultsName('cmd_rm') |
                 rename_elem_cmd.setResultsName('cmd_rename') |
                 list_elem_cmd.setResultsName('cmd_list'))

    #####################
    # category commands #
    #####################
    cat_args = id_.setResultsName('cat')
    last_cat_args = last_id.setResultsName('cat')
    add_cat_cmd = (
        add_kw + ws +
        (pp.Optional(cat_word + ws) +
         last_cat_args).setResultsName('target_cat'))
    rm_cat_cmd = (
        remove_kw + ws +
        (pp.Optional(cat_word + ws) +
         last_cat_args).setResultsName('target_cat'))
    rename_cat_cmd = (
        rename_kw + ws +
        (pp.Optional(cat_word + ws) + cat_args + ws +
         pp.Optional(mv_infix + ws) + last_id.setResultsName('new')
         ).setResultsName('target_cat'))
    list_cats_cmd = (
        list_kw.setResultsName('target_none') +
        pp.Optional(ws + total_word) + pp.Optional(ws + cats_word) +
        pp.NotAny(last_cat_args))
    # NotAny (lookahead) must be inside cmd to distinguish it from list_cat_cmd
    list_cat_cmd = (
        ((list_kw +
          pp.Optional(ws + total_word) + pp.Optional(ws + elems_word) +
          pp.Optional(ws + (inside_word | of_word))) |
         (what + ws + elems_word + pp.Optional(ws + are) + ws + inside_word) |
         (what_is + ws + inside_word)).setResultsName('target_cat') + ws +
        last_cat_args)
    cat_cmds = (add_cat_cmd.setResultsName('cmd_add') |
                rm_cat_cmd.setResultsName('cmd_rm') |
                rename_cat_cmd.setResultsName('cmd_rename') |
                list_cats_cmd.setResultsName('cmd_list') |
                list_cat_cmd.setResultsName('cmd_list'))

    #################
    # link commands #
    #################
    link_args = (
        (from_ + ws + d_id.setResultsName('src') + ws +
         to + ws + last_d_id.setResultsName('dest')) |
        (between + ws + d_id.setResultsName('src') + ws +
         and_ + ws + last_d_id.setResultsName('dest')) |
        (d_id.setResultsName('src') + ws +
         pp.Optional((and_ | with_ | from_) + ws) +
         last_d_id.setResultsName('dest')))
    link_base = link.setResultsName('target_link') + ws + link_args
    add_link_cmd = pp.Optional(add_kw + ws) + link_base
    rm_link_cmd = (
        remove_kw + ws + link_base |
        unlink + (ws + link_args).setResultsName('target_link'))
    link_cmds = (
        add_link_cmd.setResultsName('cmd_add') |
        rm_link_cmd.setResultsName('cmd_rm'))

    ###################
    # search commands #
    ###################
    search_cmds = (search_kw.setResultsName('cmd_search') + ws +
                   last_id.setResultsName('target_id'))

    ################
    # all commands #
    ################
    please_ws = pp.Optional(please + ws)
    ws_please = pp.Optional(ws + please)
    force_kw = em.setResultsName('force')

    start = pp.StringStart() + please_ws
    end = ws_please + pp.Optional(force_kw) + pp.StringEnd()

    cmds = ((start + search_cmds + end) |
            (start + help_cmds + end) |
            (start + link_cmds + end) |
            (start + cat_cmds + end) |
            (start + elem_cmds + end))

    return cmds


def normalize_id(id_str):
    if isinstance(id_str, (list, tuple)):
        return list(map(normalize_id, id_str))
    # remove trailing and ending whitespace
    s = re.subn(r'^\s+|\s+$', '', id_str)[0]
    # remove enclosing quotation marks
    s, n = re.subn(r'^"(.+)"$', '\g<1>', s)
    if n:
        # quoted id => unquote escaped quotation marks
        return s.replace(r'\"', r'"')
    else:
        # unquoted id => quotation marks are not unescaped
        return s


def normalize_cmd(parse_result):
    log.debug('Normalizing %s', parse_result)
    parse_result_dict = parse_result.asDict()
    log.debug('parse_result.asDict() %s', parse_result_dict)
    keys = parse_result_dict.keys()
    if 'target_none' in keys:
        args = []
        target = None
    elif 'target_cat' in keys:
        args = [parse_result.cat]
        target = 'cat'
    elif 'target_elem' in keys:
        args = [[parse_result.elem.c, parse_result.elem.e]]
        target = 'elem'
    elif 'target_link' in keys:
        args = [[parse_result.src.c, parse_result.src.e],
                [parse_result.dest.c, parse_result.dest.e]]
        target = 'link'
    elif 'target_id' in keys:
        args = [parse_result.target_id]
        target = 'id'
    else:
        raise ValueError("Unknown target parsed: %s" % str(keys))
    if 'new' in keys:
        if (isinstance(parse_result.new, pp.ParseResults) and
                'c' in parse_result.new.keys() and
                'e' in parse_result.new.keys()):
            args.append([parse_result.new.c, parse_result.new.e])
        else:
            args.append(parse_result.new)
    args = list(map(normalize_id, args))
    if 'cmd_list' in keys:
        cmd = 'list'
    elif 'cmd_add' in keys:
        cmd = 'add'
    elif 'cmd_rm' in keys:
        cmd = 'remove'
    elif 'cmd_rename' in keys:
        cmd = 'rename'
    elif 'cmd_help' in keys:
        cmd = 'help'
    elif 'cmd_search' in keys:
        cmd = 'search'
    else:
        raise ValueError("Unknown command parsed: %s" % str(keys))
    force = 'force' in keys
    log.debug('cmd: "%s" target: "%s" args: "%s" force: "%s"',
              cmd, target, args, force)
    return cmd, target, args, force


_parser = _build_parser()


def understand(msg):
    try:
        r = _parser.parseString(msg)
    except pp.ParseException:
        log.info("Parser couldn't understand this message: '%s'" % msg)
        return -1, None, None, None
    try:
        return normalize_cmd(r)
    except ValueError:
        log.error("Couldn't normalize command '%s'" % msg)
        return -1, None, None, None
