from threading import Thread
from abc import ABCMeta, abstractmethod

import os
import threading

from logging import getLogger
log = getLogger(__name__)


class StoreListener(object):
    __metaclass__ = ABCMeta

    _KNOWN_CALLBACKS = ('add_category', 'remove_category', 'rename_category',
                        'add_to_category', 'remove_from_category',
                        'rename_element', 'add_link', 'remove_link',
                        'diff_action', 'diff_actions')

    def __init__(self, store, callbacks, digest_minutes=None):
        self.store = store
        diff = set(StoreListener._KNOWN_CALLBACKS) - set(callbacks.keys())
        if diff:
            raise ValueError('Missing callback(s) %s' % diff)
        if digest_minutes and 'diff_actions' not in callbacks.keys():
            raise ValueError('Must support callback diff_actions for digests')
        diff = set(callbacks.keys()) - set(StoreListener._KNOWN_CALLBACKS)
        if diff:
            raise ValueError('Unknown callback(s) %s' % diff)
        self.callbacks = callbacks
        log.debug('%s %s Created StoreListener' % (os.getpid(),
                                                   threading.current_thread()))

        self.digest_minutes = digest_minutes
        self._digest_timer = None
        self._digest_timer_id = 0
        self._digest_lock = threading.RLock()
        self._digest_started_timestamp = None
        self._digest_end_timestamp = None

    def register(self):
        self.store.register_listener(self)

    def unregister(self):
        self.force_digest_flush()
        self.store.unregister_listener(self)

    def __getattr__(self, item):
        if item in self.callbacks:
            if not self.digest_minutes:
                log.debug('StoreListener callback %s queried.' % item)
                return self.callbacks[item]
            else:
                return self.ignore_and_digest_update
        else:
            super(item)

    def ignore_and_digest_update(self, *args, **kwargs):
        self._digest_update()

    def _digest_update(self):
        with self._digest_lock:
            self._digest_timer_id += 1
            all_ts = self.store.all_available_timestamps()
            if self._digest_timer is not None:
                self._digest_timer.cancel()
            else:
                if len(all_ts) > 2:
                    self._digest_started_timestamp = all_ts[-2]
                else:
                    self._digest_started_timestamp = None
                log.debug('Started digest buffering: %s',
                          self._digest_started_timestamp)
            self._digest_end_timestamp = all_ts[-1]
            log.debug('Updated digest buffer end: %s',
                      self._digest_end_timestamp)
            if self._digest_timer is not None:
                self._digest_timer.cancel()
            self._digest_timer = threading.Timer(self.digest_minutes * 60,
                                                 self._send_digest,
                                                 args=(self._digest_timer_id,))
            self._digest_timer.start()

    def _send_digest(self, timer_id):
        with self._digest_lock:
            if self._digest_timer is None:
                log.debug('Aborted digest: empty')
                return
            if timer_id is not None and self._digest_timer_id != timer_id:
                log.debug('Aborted digest timer (id clash).')
                return
            from_ = self._digest_started_timestamp
            to = self._digest_end_timestamp
            self._digest_timer = None
        diffs = self.store.high_level_diffs(from_, to)
        log.debug('Calling digest callback: %s', diffs)
        self.callbacks['diff_actions'](diffs)

    def force_digest_flush(self):
        if self.digest_minutes:
            with self._digest_lock:
                if self._digest_timer is not None:
                    self._digest_timer.cancel()
                self._send_digest(None)


class StorePoller(Thread):
    def __init__(self, store, diff_callback):
        self.store = store
        self.dbv = store.timestamp
        self.running = True
        self.cb = diff_callback
        super(StorePoller, self).__init__()

    def run(self):
        while self.running:
            res = self.store.listen_for_changes(self.dbv, is_user=False,
                                                timeout=5)
            if res is not None:
                self.dbv = res['to']
                self.cb(res)

    def stop(self):
        self.running = False
