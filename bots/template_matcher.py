import re

from fuzzywuzzy import fuzz

from logging import getLogger
log = getLogger(__name__)

# matches template and extracts "{{category2|sim}}"
TEMPLATE_REGEX = re.compile(
    r'^.*({{(?:([^|]*)\|(1(?:\.0*)?|0?\.\d+|0)?|(.*))}}).*$')
WHITESPACE_RE = re.compile(r'\s+')
PUNCTUATION_RE = re.compile(r'[\u2000-\u206F\u2E00-\u2E7F'
                            r'\'!"#$%&()*+,\-.\\/:;<=>?@\[\]^_`{|}~]')

invalid_templates = []
valid_templates = {}


def prepare_template(template):
    template_low = template.lower()
    if template_low in invalid_templates or template_low in valid_templates:
        return
    m = TEMPLATE_REGEX.match(template)
    if not m:
        invalid_templates.append(template_low)
        log.warning('Bot query template invalid: %s', template)
        return
    g = m.groups()
    sim = g[2] if g[2] is not None else 1
    target_cat = g[1] if g[1] is not None else g[3]
    regex = re.compile(template.replace(g[0], '(.*)'), flags=re.IGNORECASE)
    assert regex.groups-re.compile(template.replace(g[0], '')).groups == 1
    target_idx = re.compile(template.split(g[0])[0]).groups
    valid_templates[template_low] = [target_cat, float(sim), regex, target_idx]
    log.info("Parsed template for %s: %s (sim: %s, group: %s)",
             target_cat, regex.pattern, float(sim), target_idx)


def prepare_templates(templates):
    for _, template in templates:
        prepare_template(template)


def token_match_similarity(a, b):
    a, b = map(lambda x: WHITESPACE_RE.subn(
        ' ', PUNCTUATION_RE.subn('', x)[0])[0].strip().lower(), [a, b])
    # w.l.o.g. |a| >= |b|
    a, b = (a, b) if len(a) >= len(b) else (b, a)
    tokens_a, tokens_b = map(lambda x: x.split(), [a, b])
    len_t_a, len_t_b = len(tokens_a), len(tokens_b)
    best = 0
    for offset in range(len_t_a-len_t_b+1):
        sim = 0
        weight_sum = 0
        for i in range(len_t_b):
            ta, tb = tokens_a[offset+i], tokens_b[i]
            weight = max(len(ta), len(tb))
            sim += fuzz.ratio(ta, tb)*weight
            weight_sum += weight
        sim /= weight_sum
        best = max(best, sim)
    return best


def match_templates(msg, templates, db):
    res = set()
    matching_templates = 0
    for cat, template in templates:
        template_low = template.lower()
        prepare_template(template)
        if cat not in db.get_all_categories():
            log.warning('Bot query template for not existing category %s', cat)
            continue
        if template_low in invalid_templates:
            continue
        target_cat, sim, r, target_idx = valid_templates[template_low]
        if target_cat not in db.get_all_categories():
            log.warning('Bot query template for not existing category %s',
                        target_cat)
            continue
        m = r.search(msg)
        if m is None:
            continue
        matching_templates += 1
        target = m.groups()[target_idx]
        log.debug('Template "%s" matched message "%s". Category2 (%s) => "%s"',
                  template, msg, target_cat, target)
        if target == '':
            log.warning('Template "%s" matched message "%s". But category2 was '
                        'empty. Make sure your RegEx does not swallow '
                        'everything e.g. with a ".*" in front of {{cat2}}.',
                        template, msg)
            continue
        template_hits = []
        hits = set()
        for elem in db.get_category(target_cat):
            tokem_sim = token_match_similarity(elem, target)/100
            if tokem_sim >= sim:
                catelem = [target_cat, elem]
                template_hits.append(catelem)
                linked = [x for x in db.get_all_linked(catelem) if x[0] == cat]
                linked_set = set(map(tuple, linked))
                res = res.union(linked_set)
                hits = hits.union(linked_set)
        if template_hits:
            log.info('Template "%s" matched message "%s", found %s in "%s" '
                     'which are linked to %s', template, msg, template_hits,
                     target_cat, hits)
        else:
            log.debug('Target "%s" didn\'t match anything in "%s"',
                      target, target_cat)
    return res


if __name__ == '__main__':
    from models import store
    db = store.Store()
    r = match_templates('Who knows something about Python2?',
                        [('Person',
                          '(who|any(one|body)|some(one|body))'
                          '.*(experienced?|knows?|use(s|ed)?){{Tag|.85}}')],
                        db)
    print(r)
