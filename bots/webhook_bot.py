from .chat_bot_base import ChatBotBase
import requests
from flask import request
from flask import abort

from util import api_route

from .chat_bot_util import split_msg

from logging import getLogger
log = getLogger(__name__)


class WebhookBot(ChatBotBase):
    MAX_MSG_CHARS = 4000

    def __init__(self, app, store, settings, digest_minutes, url, hook_pairs,
                 name, logo_url, app_pw, app_salt, query_templates):
        app.add_url_rule('/api/chat/webhook', 'webhook', self.incoming,
                         methods=['POST'])
        self.in_tokens = [p[0] for p in hook_pairs if p[0] is not None]
        self.out_hooks = [p[1] for p in hook_pairs if p[1] is not None]
        self.url_md_flag = {p[1]: p[2] for p in hook_pairs if p[1] is not None}
        self.hook_pairs = hook_pairs
        self.name = name
        self.logo_url = logo_url
        self.app = app
        ChatBotBase.__init__(self, store, settings, name, url,
                             digest_minutes=digest_minutes,
                             password=app_pw, salt=app_salt,
                             query_templates=query_templates)

    def emit(self, message, md_message, hook_urls=None, *args, **kwargs):
        msgs = split_msg(message, max_length=WebhookBot.MAX_MSG_CHARS)
        md_msgs = split_msg(md_message, max_length=WebhookBot.MAX_MSG_CHARS)
        urls = self.out_hooks if not hook_urls else hook_urls
        if not urls:
            return
        res = []
        for msg, md_msg in zip(msgs, md_msgs):
            payload = {"username": self.name, "icon_url": self.logo_url}
            for url in urls:
                if self.url_md_flag[url]:
                    payload['text'] = md_msg
                else:
                    payload['text'] = msg
                res.append(requests.post(url, json=payload))
            max_code = max([r.status_code for r in res])
            if max_code < 300:
                log_func = log.info
            elif max_code < 400:
                log_func = log.warning
            else:
                log_func = log.error
            res = [str(r) if 200 <= r.status_code < 300 else str(r)+': '+r.text
                   for r in res]
            log_func('WebhookBot sent %s to %s', payload, zip(urls, res))

    @api_route()
    def incoming(self):
        if request.content_type == 'application/json':
            res = request.get_json()
            log.debug("Receiving json webhook: {}".format(res))
            if not isinstance(res, dict):
                log.debug("json webhook aborted with 400 (no json object)")
                abort(400, 'json object expected')
            if 'token' not in res:
                log.debug("json webhook aborted with 403 (no token)")
                abort(403, 'Token missing')
            if not res['token'] in self.in_tokens:
                log.debug("json webhook aborted with 403 (wrong token)")
                abort(403, 'Invalid token')
            if 'text' not in res:
                log.debug("json webhook aborted with 400 (no message text)")
                abort(400, 'Text is missing')
            if not isinstance(res['text'], str):
                log.debug("json webhook aborted with 400 (text is no string)")
                abort(400, 'Text is expected to be a string')
            msg = res['text']
            token = res['token']
        else:
            log.debug("Receiving {} webhook: {}".format(request.content_type,
                                                        request))
            msg = request.values.get('text')
            token = request.values.get('token')
            log.debug("token: {}, msg {}".format(token, msg))
            if token is None:
                log.debug("webhook aborted with 403 (no token)")
                abort(403, 'Token missing')
            if token not in self.in_tokens:
                log.debug("webhook aborted with 403 (wrong token)")
                abort(403, 'Invalid token')
            if msg is None:
                log.debug("webhook aborted with 400 (no message text)")
                abort(400, 'Text is missing')
        response_urls = [p[1] for p in self.hook_pairs if p[0] == token]
        log.info("Accepted webhook '%s' from %s", msg, requests)
        self.receive(msg, hook_urls=response_urls)
        return '', 204

    def after_run(self):
        # ugly hack to remove webhook handler
        log.info("Unregistering bot's webhook handler")
        self.app.url_map._rules = [r for r in self.app.url_map._rules
                                   if r.rule != '/api/chat/webhook']
        del self.app.url_map._rules_by_endpoint['webhook']
        self.app.url_map._remap = True
        self.app.url_map.update()
        del self.app.view_functions['webhook']
