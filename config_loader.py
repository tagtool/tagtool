import sys
import html
from html.parser import HTMLParser
import default_settings as default_conf
import importlib.machinery
import importlib.util

from util import merge_dicts


html_parser = HTMLParser()


def html_escape(s, key):
    if not s:
        return s
    return html.escape(s,  quote=True)


def html_url_escape(s, key):
    if not s:
        return s
    if html.unescape(s).strip().lower().startswith('javascript:'):
        s = vars(default_conf)[key]
        print('WARNING: Javascript directives are forbidden in URLs. '
              'Setting "%s" falls back to default: "%s".' % (key, s),
              file=sys.stderr)
    return html.escape(s, quote=True)


TO_ESCAPE = dict((
    ('BRANDING_NAME', html_escape),
    ('PASSWORD_HINT', html_escape),
    ('URL', html_url_escape),
    ('BRANDING_LOGO_URL', html_url_escape),
))


def escape_conf(conf):
    for key in conf:
        if key in TO_ESCAPE:
            conf[key] = TO_ESCAPE[key](conf[key], key)
    return conf


def load_config(config_path=None):  # pragma: no cover
    if config_path is None:
        config_path = 'settings.py'

    try:
        loader = importlib.machinery.SourceFileLoader('settings', config_path)
        spec = importlib.util.spec_from_loader(loader.name, loader)
        mod = importlib.util.module_from_spec(spec)
        loader.exec_module(mod)
        conf = merge_dicts(vars(default_conf), vars(mod))
        settings_found = True
    except IOError:
        conf = vars(default_conf)
        settings_found = False

    return escape_conf(conf), settings_found
