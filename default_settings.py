# -*- coding: utf-8 -*-

####################
# Primary Settings #
####################
# ------------------------------------------------------------------------------
BRANDING_NAME = 'TagTool'
PASSWORD = None  # team password
PASSWORD_HINT = None  # hint shown on password page
ALLOW_READ_ONLY = False  # display a read-only version for not logged in users
URL = 'http://demo.letstag.it'  # used for back-links sent by chat bots
ATTACH_PW = True  # set to True to include hashed password in back links
# ------------------------------------------------------------------------------

################
# Bot Settings #
################
# ------------------------------------------------------------------------------
# Webhook bot settings
# ====================
# Webhook bot currently supports Slack and Mattermost.
USE_WEBHOOK_BOT = False
# If USE_WEBHOOK_BOT is set to True, please adjust the following settings.
# Enable chat bot integration as follows:
# Mattermost:
#   open Mattermost > main menu > Integrations
#   outgoing in Mattermost is incoming here and vice versa
#   incoming url is http(s)://<url-of-this-server>/api/chat/webhook
#   You will need to create an incoming hook on mattermost for each channel the
#   bot should post updates to. You'll also need a outgoing hook on mattermost
#   for each channel the server should listen to. Please choose
#   'application/json' as Content Type and leave trigger words blank or set it
#   to '@<HOOK_NAME>' where HOOK_NAME to save some requests against this server.
# Slack:
#   open Slack > Apps & Integrations > Manage > Custom Integrations
#   For the rest see "Mattermost" above.
# Now enter the matching outgoing tokens with the corresponding incoming hook
# urls here as pairs like
# ('tokenXYZ', 'http://chat.example.com/hooks/ZYX', True) for Mattermost
# ('tokenXYZ', 'http://chat.example.com/hooks/ZYX', False) for Slack
# The last element specifies if the chat endpoint understands Markdown (True),
# like Mattermost does, or not (False), like Slack does.
# You can set the token to None if the bot should only post to that channel but
# not listen. Or you can set the hook to None, if you want the channel to be
# read only for the bot (may be strange as the bot won't react on help
# requests). Example:
# WEBHOOK_PAIRS = [
#     (
#         "some-token-abc123",
#         "https://chat.example.com/hooks/some-hook-token-123abc",
#         True
#     ),
#     (
#         "another-token-xyz789",
#         "https://chat.example.de/hooks/another-hook-token-789xyz",
#         False
#     )
# ]
WEBHOOK_PAIRS = []
# Set an absolute icon url (must be reachable from every chat client) to be used
# for chat messages.
WEBHOOK_ICON_URL = None
# Set a name to be displayed as the message author. At the same time this will
# be the name messages for the bot have to be prefixed with. If set to None
# it will default to the value of BRANDING_NAME.
WEBHOOK_BOT_NAME = 'tagtool'
# ------------------------------------------------------------------------------
# General bot settings
# ==============================
# The bot will wait unit nothing happened for the last n minutes before sending
# a digest. The information will be minimal. If something was added a removed
# in the same time window, the bot will not tell so.
DIGEST_MESSAGES_OVER_MINUTES = 5

# You can specify templates that are matched against all messages that the bot
# does not understand. If the pattern matches, all elements of category1 that
# are linked to the matching element of category2 are then posted as chat
# message. Templates are a tuple:
# (category1, 'regex{{category2|sim}}regex')
# where regex can be some regular expression in python flavour while sim is a
# float in the range 0 - 1 that gives the minimum similarity of matches in
# category2. It's optional and 'regex{{category2}}regex' will work as well and
# only consider perfect matches (case and whitespace insensitive).
# Here is an example that would match all items in the category 'Person' that
# are linked (i.e. familiar) with the element 'XY' in the category 'Skill' if
# e.g. the message 'who knows XY?' is asked in the chat:
# BOT_QUERY_TEMPLATES = [
#     ('Person',
#      '(who|any(one|body)|some(one|body))'
#      '.*(experienced?|knows?|familiar|use(s|ed)?){{Skill|.85}}')
# ]
# Pro-tip: don't put ".*" in front of "{{category2}}" as it's greedy and the
# template match fot category2 will always be empty.
BOT_QUERY_TEMPLATES = []
# ------------------------------------------------------------------------------

#####################
# Frontend Settings #
#####################
# ------------------------------------------------------------------------------
# branding
# ========
BRANDING_LOGO_URL = '/img/icon_white.png'
TOUCH_APPS = []
# ------------------------------------------------------------------------------
# appearance
# ==========
# How many clouds to show in one row on min width of 1200. Smaller resolutions
# are handled automatically depending on this choice.
# Must be either 1, 2, 3, 4, 6, or 12
CLOUD_COLUMN_COUNT = 2
# If set to true, the demo mode will not only use a circular highlight to
# indicate a simulated click but an additional hand cursor
DEMO_HAND = True
# ------------------------------------------------------------------------------

###################
# Server Settings #
###################
# ------------------------------------------------------------------------------
# Don't change these if you are running the server inside docker except for
# those where explicitly marked.
# Host and port to listen on
HOST = '0.0.0.0'
PORT = 8080
# Set this to True to run the app in debug mode (only suitable when not
# running in production environments). If set to True while not running the
# app by starting 'server.py' directly the chat bots may break.
# CAN BE CHANGED EVEN IF IN DOCKER CONTAINER
DEBUG = False
# Adjust the version query part of asset-urls. By adding a '?v=...' to the asset
# urls one can make sure that the browser cache has a miss and the asset is
# loaded from the server. However, this will cause malfunction of the browser
# development tools because the static files seem to move on each request.
# True if DEBUG==True: Update the version for every request.
# True if DEBUG==False: Update the version on each server startup.
# None: Don't use any version query
# Anything else: will be converted to string and used as version.
# CAN BE CHANGED EVEN IF IN DOCKER CONTAINER
ASSET_VERSIONING = True
# Where the data is going to be stored (use an own directory). Folder will be
# created if it does not exist yet. Paths relative to this file are allowed.
DATA_DIR = 'data'
# These values determine how json responses should be indented
# CAN BOTH BE CHANGED EVEN IF IN DOCKER CONTAINER
JSON_INDENT_DEBUG_MODE = 2   # in dev/debug mode
JSON_INDENT_DEPLOY_MODE = 2  # in productive system
# Can be replaced with a secret (random) string per instance as well
SECRET_KEY = 'boh0oon1ung6iephah4Aizaech0phei8'
# Set AGING to be a number 'n' if you want the server to 'forget' a thing every
# 'n' minutes. The server will then delete a link, an element or a category each
# 'n' minutes. No elements will be deleted that have any links. Nor will
# categories be deleted that have any elements. The server will delete one of
# the oldest things that can be found. This feature is meant to make people stay
# active on the tool. Best is to use it together with the chat bot feature so
# that the users take note of deletions.
# Set 'AGING = None' to completely disable the aging.
# CAN BE CHANGED EVEN IF IN DOCKER CONTAINER
AGING = None
# This value determines which changes are considered to be recent. It must be
# given in seconds.
# CAN BE CHANGED EVEN IF IN DOCKER CONTAINER
RECENT_SECONDS = 7*24*60*60
# ------------------------------------------------------------------------------

###########
# Logging #
###########
# ------------------------------------------------------------------------------
LOG_LEVEL = 'INFO'
USE_MAIL_LOGGER = False
MAIL_LOGGING_SMTP_SERVER = 'localhost'
MAIL_LOGGING_FROM_ADDRESS = 'tags'
MAIL_LOGGING_TO_ADDRESS = 'root'
# ------------------------------------------------------------------------------

#####################
# More Bot Settings #
#####################
# ------------------------------------------------------------------------------
# Mattermost full user bot settings
# =================================
# ATTENTION: if you want to use this bot you must uncomment 'pyquery' in
#            'requirements.txt'. If you are using docker you also have to
#            edit 'dockerfiles/tagtrool-env/Dockerfile' according to the
#            comments and build the image yourself.
# Set USE_MATTERMOST_USERBOT to True if you want to use a full functioning user
# as bot. To do so first use a Mattermost client to create that user, then
# enter your group with that user and join all channels you'd like the bot to
# interact with.
USE_MATTERMOST_USERBOT = False
# The next constants must be set, if you've set USE_MATTERMOST_USERBOT to True
# The login name of the user
MM_LOGIN_NAME = 'my-bot-name'
# The password of the user
MM_LOGIN_PWD = 'password'
MM_TEAM_NAME = 'my-team'
MM_CHAT_URL = 'https://chat.example.com'
# If your mattermost is integrated with your gitlab you may set
# MM_USE_GITLAB_AUTH to True so that the bot uses the gitlab login
MM_USE_GITLAB_AUTH = False
# Only needed if MM_USE_GITLAB_AUTH is set to True
# Set MM_USE_GITLAB_LDAP to True to use gitlab's LDAP login.
# If set to False, the standard gitlab login is used.
MM_USE_GITLAB_LDAP = False
# Only needed if MM_USE_GITLAB_AUTH is set to True
MM_GITLAB_URL = 'https://git.example.com'
# Channel names that should be ignored by the bot (it will neither listen nor
# write on those channels. E.g ['General', 'Secret channel']
MM_IGNORED_CHANS = []
# ------------------------------------------------------------------------------
