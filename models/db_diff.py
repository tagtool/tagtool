from copy import deepcopy
from datetime import datetime
from collections import OrderedDict


class DB_Diff(OrderedDict):
    ALLOWED_ACTIONS = ("insertions", "deletions")
    ALLOWED_X_NAMES = ('categories', 'elements', 'links')

    EMPTY_VALUE = OrderedDict([
        ('insertions', OrderedDict([('categories', []),
                                    ('elements', []),
                                    ('links', [])])),
        ('deletions', OrderedDict([('categories', []),
                                   ('elements', []),
                                   ('links', [])]))
    ])

    def __init__(self, from_dbv=None, to_dbv=None, raw_diff=None, db_diff=None):
        from .store import Store
        val = deepcopy(self.__class__.EMPTY_VALUE)
        if db_diff is not None:
            if not isinstance(self, db_diff.__class__):
                raise TypeError('db_diff must be of same or super-class (%s) '
                                'but found %s' % (type(self), type(db_diff)))
            raw_diff = OrderedDict(deepcopy(db_diff))
            val['from'] = raw_diff.pop('from')
            val['to'] = raw_diff.pop('to')
        else:
            val['from'] = from_dbv
            val['to'] = to_dbv
            for key in ['from', 'to']:
                if isinstance(val[key], Store):
                    val[key] = str(val[key].timestamp)
                elif isinstance(val[key], datetime):
                    val[key] = str(val[key])
            if raw_diff is None:
                raw_diff = {}
        for key in raw_diff:
            if key not in self.__class__.EMPTY_VALUE:
                raise ValueError('Invalid raw diff: Unknown key %s' % key)
            for x in raw_diff[key]:
                if x not in self.__class__.EMPTY_VALUE[key]:
                    raise ValueError('Invalid raw diff: Unknown target %s '
                                     'for key %s' % (x, key))
                val[key][x] = raw_diff[key][x]

        super(DB_Diff, self).__init__(val)

        self.add_cat = self.__add_cat
        self.add_cats = self.__add_cats
        self.add_elem = self.__add_elem
        self.add_elems = self.__add_elems
        self.add_link = self.__add_link
        self.add_links = self.__add_links
        self.remove_cat = self.__remove_cat
        self.remove_cats = self.__remove_cats
        self.remove_elem = self.__remove_elem
        self.remove_elems = self.__remove_elems
        self.remove_link = self.__remove_link
        self.remove_links = self.__remove_links
        self._diff_x = self.___diff_x

    def __setitem__(self, key, value):
        from .store import Store
        if key in ('from', 'to'):
            if isinstance(value, Store):
                value = str(value.timestamp)
            elif isinstance(value, datetime):
                value = str(value)
        super(DB_Diff, self).__setitem__(key, value)

    @classmethod
    def add_cat(cls, cat, from_dbv=None, to_dbv=None):
        return cls._diff_x("insertions", "categories", [cat], from_dbv, to_dbv)

    @classmethod
    def add_cats(cls, cats, from_dbv=None, to_dbv=None):
        return cls._diff_x("insertions", "categories", cats, from_dbv, to_dbv)

    @classmethod
    def add_elem(cls, elem, from_dbv=None, to_dbv=None):
        return cls._diff_x("insertions", "elements", [elem], from_dbv, to_dbv)

    @classmethod
    def add_elems(cls, elems, from_dbv=None, to_dbv=None):
        return cls._diff_x("insertions", "elements", elems, from_dbv, to_dbv)

    @classmethod
    def add_link(cls, link, from_dbv=None, to_dbv=None):
        return cls._diff_x("insertions", "links", [link], from_dbv, to_dbv)

    @classmethod
    def add_links(cls, links, from_dbv=None, to_dbv=None):
        return cls._diff_x("insertions", "links", links, from_dbv, to_dbv)

    @classmethod
    def remove_cat(cls, cat, from_dbv=None, to_dbv=None):
        return cls._diff_x("deletions", "categories", [cat], from_dbv, to_dbv)

    @classmethod
    def remove_cats(cls, cats, from_dbv=None, to_dbv=None):
        return cls._diff_x("deletions", "categories", cats, from_dbv, to_dbv)

    @classmethod
    def remove_elem(cls, elem, from_dbv=None, to_dbv=None):
        return cls._diff_x("deletions", "elements", [elem], from_dbv, to_dbv)

    @classmethod
    def remove_elems(cls, elems, from_dbv=None, to_dbv=None):
        return cls._diff_x("deletions", "elements", elems, from_dbv, to_dbv)

    @classmethod
    def remove_link(cls, link, from_dbv=None, to_dbv=None):
        return cls._diff_x("deletions", "links", [link], from_dbv, to_dbv)

    @classmethod
    def remove_links(cls, links, from_dbv=None, to_dbv=None):
        return cls._diff_x("deletions", "links", links, from_dbv, to_dbv)

    @classmethod
    def _diff_x(cls, action, x_name, x_values, from_dbv=None, to_dbv=None):
        res = cls(from_dbv, to_dbv)
        res._diff_x(action, x_name, x_values)
        return res

    def __add_cat(self, cat):
        return self._diff_x("insertions", "categories", [cat])

    def __add_cats(self, cats):
        return self._diff_x("insertions", "categories", cats)

    def __add_elem(self, elem):
        return self._diff_x("insertions", "elements", [elem])

    def __add_elems(self, elems):
        return self._diff_x("insertions", "elements", elems)

    def __add_link(self, link):
        return self._diff_x("insertions", "links", [link])

    def __add_links(self, links):
        return self._diff_x("insertions", "links", links)

    def __remove_cat(self, cat):
        return self._diff_x("deletions", "categories", [cat])

    def __remove_cats(self, cats):
        return self._diff_x("deletions", "categories", cats)

    def __remove_elem(self, elem):
        return self._diff_x("deletions", "elements", [elem])

    def __remove_elems(self, elems):
        return self._diff_x("deletions", "elements", elems)

    def __remove_link(self, link):
        return self._diff_x("deletions", "links", [link])

    def __remove_links(self, links):
        return self._diff_x("deletions", "links", links)

    def ___diff_x(self, action, x_name, x_values):
        if not isinstance(action, str):
            raise TypeError("action bust be a string")
        if not isinstance(x_name, str):
            raise TypeError("x_name bust be a string")
        if action not in self.__class__.EMPTY_VALUE:
            raise ValueError("action must be one of " +
                             str(self.__class__.ALLOWED_ACTIONS))
        if x_name not in self.__class__.EMPTY_VALUE[action]:
            raise ValueError("x_name must be one of " +
                             str(self.__class__.ALLOWED_X_NAMES))
        if not isinstance(x_values, list):
            raise TypeError("x_values must be a list")
        self[action][x_name] += x_values

    def is_empty(self):
        for action in self.__class__.EMPTY_VALUE:
            for target in self.__class__.EMPTY_VALUE[action]:
                if len(self[action][target]) > 0:
                    return False
        return True

    def action_count(self):
        len_ = 0
        for action in self.__class__.EMPTY_VALUE:
            for target in self.__class__.EMPTY_VALUE[action]:
                len_ += len(self[action][target])
        return len_

    def __copy__(self):
        return self.__class__(db_diff=self)

    def __deepcopy__(self, memo=None):
        dc = deepcopy(OrderedDict(self))
        from_ = dc.pop('from')
        to_ = dc.pop('to')
        return self.__class__(from_dbv=from_, to_dbv=to_, raw_diff=dc)


class HighLevelDB_Diff(DB_Diff):
    EMPTY_VALUE = OrderedDict([
        ('insertions', OrderedDict([('categories', []),
                                    ('elements', []),
                                    ('links', [])])),
        ('deletions', OrderedDict([('categories', []),
                                   ('elements', []),
                                   ('links', [])])),
        ('renamings', OrderedDict([('categories', []),
                                   ('elements', [])]))
    ])

    def __init__(self, from_dbv=None, to_dbv=None, raw_diff=None, db_diff=None):
        super(HighLevelDB_Diff, self).__init__(from_dbv=from_dbv, to_dbv=to_dbv,
                                               raw_diff=raw_diff,
                                               db_diff=db_diff)
        self.rename_elem = self.__rename_elem
        self.rename_elems = self.__rename_elems
        self.rename_cat = self.__rename_cat
        self.rename_cats = self.__rename_cats

    @classmethod
    def rename_elem(cls, elem_renaming, from_dbv=None, to_dbv=None):
        return cls._diff_x("renamings", "elements", [elem_renaming],
                           from_dbv, to_dbv)

    @classmethod
    def rename_elems(cls, elem_renamings, from_dbv=None, to_dbv=None):
        return cls._diff_x("renamings", "elements", elem_renamings,
                           from_dbv, to_dbv)

    @classmethod
    def rename_cat(cls, cat_renaming, from_dbv=None, to_dbv=None):
        return cls._diff_x("renamings", "categories", [cat_renaming],
                           from_dbv, to_dbv)

    @classmethod
    def rename_cats(cls, cat_renamings, from_dbv=None, to_dbv=None):
        return cls._diff_x("renamings", "categories", cat_renamings,
                           from_dbv, to_dbv)

    def __rename_elem(self, elem_renaming):
        return self._diff_x("renamings", "elements", [elem_renaming])

    def __rename_elems(self, elem_renamings):
        return self._diff_x("renamings", "elements", elem_renamings)

    def __rename_cat(self, cat_renaming):
        return self._diff_x("renamings", "categories", [cat_renaming])

    def __rename_cats(self, cat_renamings):
        return self._diff_x("renamings", "categories", cat_renamings)
