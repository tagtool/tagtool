class StoreError(ValueError):
    def __init__(self, *args, **kwargs):
        self.strict_error = kwargs.pop('strict_error', False)
        self.strict_fixable = kwargs.pop('strict_fixable', False)
        self.args = args
        self.kwargs = {'strict_error': self.strict_error,
                       'strict_fixable': self.strict_fixable}
        super(StoreError, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "%s(%s, %s)" %\
               (self.__class__.__name__,
                ', '.join(map(lambda x: x.__repr__(), self.args)),
                ', '.join(map(lambda x: '%s=%s' % (x[0], x[1].__repr__()),
                              self.kwargs.items())))


class ReadOnlyStoreError(StoreError):
    pass


class CategoryMissingError(StoreError):
    pass


class CategoryExistsError(StoreError):
    pass


class ElementMissingError(StoreError):
    pass


class ElementExistsError(StoreError):
    pass


class LinkMissingError(StoreError):
    pass


class LinkExistsError(StoreError):
    pass
