"""
Create migrations in sub-folder `migrations`. Call them `t<timestamp>.py`.
The files are supposed to contain a class called `Migration` that implements
the abstract class `MigrationBase` in this file. `Migration.run(db)` will be
called for each db version where db is the parsed json file with
`object_pairs_hook=OrderedDict`. `Migration.run(db, timestamp)` is then supposed
to return the changed db object. `timestamp` is the datetime of the db. The
migrations will be run in chronological order (oldest to newest)
All migrations will be executed in lexicographical order of their file name.
But only if not already applied to the DB. This information is kept in
`<db_dir>/.version`.
"""

import os
import errno
import shutil
from glob import glob
import json
from collections import OrderedDict
import importlib
from abc import ABCMeta
from abc import abstractmethod

from logging import getLogger
log = getLogger(__name__)


VERSION_FILE = '.version'


class MigrationError(RuntimeError):
    pass


class MigrationInvalidError(MigrationError):
    pass


class MigrationManager(object):
    DEFAULT_DATA_PATH = os.path.join(os.path.dirname(__file__), '..', "data")
    DEFAULT_MIGRATION_PATH = os.path.join(os.path.dirname(__file__),
                                          'migrations')

    ran = False

    @staticmethod
    def run_pending_migrations(force=False, **kwargs):
        """:keyword migration_path (string):
                    default is MigrationManager.DEFAULT_MIGRATION_PATH
           :keyword data_path (string):
                    default is MigrationManager.DEFAULT_DATA_PATH"""
        data_path = kwargs.get('path', MigrationManager.DEFAULT_DATA_PATH)
        migration_path = kwargs.get('path',
                                    MigrationManager.DEFAULT_MIGRATION_PATH)
        if MigrationManager.ran and not force:
            return False
        if not MigrationManager.db_is_inited(path=data_path):
            return False
        log.info("Checking for migrations.")
        db_ver = MigrationManager.get_db_version(path=data_path)
        migrations = MigrationManager.get_all_migration_levels(
            path=migration_path)
        pending_migrations = [m for m in migrations if m > db_ver]
        for m in pending_migrations:
            MigrationManager._run_migration(m, data_path=data_path,
                                            migration_path=migration_path)
        log.info("All pending migrations (%s) ran.", pending_migrations)
        MigrationManager.ran = True

    @staticmethod
    def get_db_version(**kwargs):
        """:keyword path (string):
                    default is MigrationManager.DEFAULT_DATA_PATH"""
        path = kwargs.get('path', MigrationManager.DEFAULT_DATA_PATH)
        fp = os.path.join(path, VERSION_FILE)
        if os.path.isfile(fp):
            with open(fp) as f:
                return f.read()
        else:
            return ''

    @staticmethod
    def db_is_inited(**kwargs):
        """:keyword path (string):
                    default is MigrationManager.DEFAULT_DATA_PATH"""
        path = kwargs.get('path', MigrationManager.DEFAULT_DATA_PATH)
        return os.path.exists(path) and glob(path + '/*/*/*/*.json')

    @staticmethod
    def get_all_migration_levels(**kwargs):
        """:keyword path (string):
                    default is MigrationManager.DEFAULT_MIGRATION_PATH"""
        path = kwargs.get('path', MigrationManager.DEFAULT_MIGRATION_PATH)
        res = []
        if os.path.exists(path):
            for fn in os.listdir(path):
                fp = os.path.join(path, fn)
                if (os.path.isfile(fp) and fn.endswith('.py') and
                        fn.startswith('t')):
                    res.append(fn[:-3])
        return sorted(res)

    @staticmethod
    def _run_migration(migration, **kwargs):
        """:keyword migration_path (string):
                    default is MigrationManager.DEFAULT_MIGRATION_PATH
           :keyword data_path (string):
                    default is MigrationManager.DEFAULT_DATA_PATH"""
        log.info("Running migration %s", migration)
        migration_path = kwargs.get('migration_path',
                                    MigrationManager.DEFAULT_MIGRATION_PATH)
        data_path = kwargs.get('data_path', MigrationManager.DEFAULT_DATA_PATH)
        module = '.' + (os.path.relpath(os.path.normpath(migration_path),
                                        os.path.dirname(__file__))
                        .replace(os.path.sep, '.'))
        exec('from .migrations import %s as i' % migration)

        loader = importlib.machinery.SourceFileLoader(
            migration, os.path.join(migration_path, '%s.py' % migration)
        )
        spec = importlib.util.spec_from_loader(loader.name, loader)
        mod = importlib.util.module_from_spec(spec)
        loader.exec_module(mod)

        if not hasattr(mod, 'Migration'):
            raise MigrationInvalidError('Missing class `Migration` in %s.%s.' %
                                        (migration_path, migration))
        if not issubclass(mod.Migration, MigrationBase):
            raise MigrationInvalidError('Migration %s is not a subclass of '
                                        'MigrationBase.' % migration)
        if hasattr(mod.Migration, 'DESCRIPTION'):
            log.info('Migration description: %s', mod.Migration.DESCRIPTION)
        mod.Migration(data_path, migration).start()


class MigrationBase(object):
    FORCE_ORDERED_DICT = True
    FORCE_ORDERED_DICT_DIFF_LOG = True
    FORCE_ORDERED_DICT_SETTINGS = True

    def __init__(self, path, name):
        self.path = path
        self.bak_path = os.path.normpath(self.path)+'.migration_bak'
        self.name = name

    def run(self, db, timestamp):
        pass

    def run_diff_log(self, diff_log):
        pass

    def run_settings(self, settings):
        pass

    @staticmethod
    def _contains_unordered_dict(obj):
        if isinstance(obj, list):
            for o in obj:
                if MigrationBase._contains_unordered_dict(o):
                    return True
        if isinstance(obj, dict):
            if not isinstance(obj, OrderedDict):
                return True
            for key in obj:
                if MigrationBase._contains_unordered_dict(obj[key]):
                    return True
        return False

    def write_db(self, new_db, fp):
        if self.__class__.FORCE_ORDERED_DICT:
            if MigrationBase._contains_unordered_dict(new_db):
                raise MigrationInvalidError(
                    "Migration %s returned db with ordinary (unordered) dict. "
                    "To enable this, set class variable "
                    "`FORCE_ORDERED_DICT = False`" % self.name)
        with open(fp, 'w') as _f:
            json.dump(new_db, _f, indent=1)

    def start(self):
        run_implemented = self.__class__.run is not MigrationBase.run
        run_diff_log_impl = \
            self.__class__.run_diff_log is not MigrationBase.run_diff_log
        run_settings_impl = \
            self.__class__.run_settings is not MigrationBase.run_settings
        if os.path.exists(self.bak_path):
            raise MigrationError("Migration backup path %s exists. It's likely "
                                 "that an earlier migration failed." %
                                 self.bak_path)
        log.debug('Copying db to backup path %s', self.bak_path)
        shutil.copytree(self.path, self.bak_path, symlinks=True)
        log.debug('Backup done. Starting migration.')
        try:
            if run_implemented:
                log.info('Migrating database files.')
                for db, timestamp, writer in self._all_dbs():
                    migrated_db = self.run(db, timestamp)
                    assert migrated_db is not None
                    writer(migrated_db)
            if run_diff_log_impl:
                log.info('Migrating diff_log file.')
                diff_log, writer = self._diff_log()
                if diff_log is None:
                    log.info('Skipped due to missing diff_log file')
                else:
                    migrated_diff_log = self.run_diff_log(diff_log)
                    assert migrated_diff_log is not None
                    writer(migrated_diff_log)
            if run_settings_impl:
                log.info('Migrating settings file.')
                settings, writer = self._settings()
                if settings is None:
                    log.info('Skipped due to missing settings file')
                else:
                    migrated_settings = self.run_settings(settings)
                    assert migrated_settings is not None
                    writer(migrated_settings)
            with open(os.path.join(self.path, VERSION_FILE), 'w') as f:
                f.write(self.name)
        except Exception:
            log.error("Migration %s failed. Restoring DB." % self.name)
            shutil.rmtree(self.path, ignore_errors=True)
            shutil.move(self.bak_path, self.path)
            raise
        log.debug('Migration done. Removing Backup.')
        shutil.rmtree(self.bak_path)

    def get_latest_db(self):
        fns = sorted(glob(self.path + '/*/*/*/*.json'))
        if fns:
            with open(fns[-1], 'r') as fd:
                return json.load(fd, object_pairs_hook=OrderedDict)
        else:
            return None

    def _all_dbs(self):
        from .store import path_to_datetime
        for fp in sorted(glob(self.path + '/*/*/*/*.json')):
            with open(fp, 'r') as f:
                db = json.load(f, object_pairs_hook=OrderedDict)
                timestamp = path_to_datetime(fp)
            yield db, timestamp, lambda new_db: self.write_db(new_db, fp)

    def _diff_log(self):
        fn = os.path.join(self.path, 'diff_log.jsonl')

        def write(diff_log):
            if self.__class__.FORCE_ORDERED_DICT_DIFF_LOG:
                if MigrationBase._contains_unordered_dict(diff_log):
                    raise MigrationInvalidError(
                        "Migration %s returned diff_log with ordinary "
                        "(unordered) dict. To enable this, set class variable "
                        "`FORCE_ORDERED_DICT_DIFF_LOG = False`" % self.name)
            with open(fn, 'w') as _f:
                for diff in diff_log:
                    _f.write(json.dumps(diff) + '\n')

        try:
            with open(fn, 'r') as f:
                return ([json.loads(line, object_pairs_hook=OrderedDict)
                         for line in f.read().splitlines()],
                        write)
        except IOError as e:
            if e.errno == errno.ENOENT:
                return None, None
            raise

    def _settings(self):
        fn = os.path.join(self.path, 'settings.json')

        def write(settings):
            if self.__class__.FORCE_ORDERED_DICT_SETTINGS:
                if MigrationBase._contains_unordered_dict(settings):
                    raise MigrationInvalidError(
                        "Migration %s returned settings with ordinary "
                        "(unordered) dict. To enable this, set class variable "
                        "`FORCE_ORDERED_DICT_SETTINGS = False`" % self.name)
            with open(fn, 'w') as _f:
                json.dump(settings, _f, indent=1)
        try:
            with open(fn, 'r') as f:
                return json.load(f, object_pairs_hook=OrderedDict), write
        except IOError as e:
            if e.errno == errno.ENOENT:
                return None, None
            raise

