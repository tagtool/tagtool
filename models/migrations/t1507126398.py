from os import path
from copy import deepcopy
import json
from models.migration import MigrationBase

from logging import getLogger
log = getLogger(__name__)

HIGHLEVEL_DIFF = {'insertions': {'categories': [], 'elements': [], 'links': []},
                  'deletions': {'categories': [], 'elements': [], 'links': []},
                  'renamings': {'categories': [], 'elements': []}}


def cat_elems(db):
    res = []
    for cat in db['categories']:
        for elem in db['categories'][cat]:
            res.append([cat, elem])
    return res


def calculate_diff(db1, db2):
    diff = deepcopy(HIGHLEVEL_DIFF)
    # insertions
    # cat
    for new_cat in db2['categories']:
        if new_cat not in db1['categories']:
            diff['insertions']['categories'].append(new_cat)
    # elem
    for new_elem in cat_elems(db2):
        if new_elem not in cat_elems(db1):
            diff['insertions']['elements'].append(new_elem)
    # link
    for new_link in db2['links']:
        if (new_link not in db1['links'] and
                list(reversed(new_link)) not in db1['links']):
            diff['insertions']['links'].append(new_link)

    # deletions
    # cat
    for old_cat in db1['categories']:
        if old_cat not in db2['categories']:
            diff['deletions']['categories'].append(old_cat)
    # elem
    for old_elem in cat_elems(db1):
        if old_elem not in cat_elems(db2):
            diff['deletions']['elements'].append(old_elem)
    # link
    for old_link in db1['links']:
        if (old_link not in db2['links'] and
                list(reversed(old_link)) not in db2['links']):
            diff['deletions']['links'].append(old_link)
    return diff


class Migration(MigrationBase):
    DESCRIPTION = "Create diff_log and pre-fill with low-level diffs."

    def __init__(self, *args, **kwargs):
        super(Migration, self).__init__(*args, **kwargs)
        self.previous_db = None
        self.previous_t = None
        self.log_path = path.join(self.path, 'diff_log.jsonl')

    def run(self, db, timestamp):
        db_data, dbv = db
        if self.previous_t is not None:
            assert self.previous_t < timestamp
            diff = calculate_diff(self.previous_db, db_data)
        else:
            diff = HIGHLEVEL_DIFF
            log.info('Creating diff_log.jsonl at %s', self.log_path)
            open(self.log_path, 'w').close() # empty diff_log
        with open(self.log_path, 'a') as f:
            f.write(json.dumps([str(timestamp), diff]) + "\n")
        self.previous_t = timestamp
        self.previous_db = db_data
        return db


