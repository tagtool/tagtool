from collections import OrderedDict

from models.migration import MigrationBase

from logging import getLogger
log = getLogger(__name__)


# this makes sure the new order matches the initialization of high level diffs
# and the settings
def key_func(obj):
    order = ('insertions', 'deletions', 'renamings',
             'categories', 'elements', 'links',
             'sizing', 'display-priority', 'display-type')
    return order.index(obj) if obj in order else obj


def replace_dicts_with_sorted_ordered_dicts(obj):
    if isinstance(obj, list):
        return [replace_dicts_with_sorted_ordered_dicts(o) for o in obj]
    if isinstance(obj, tuple):
        return tuple(replace_dicts_with_sorted_ordered_dicts(o) for o in obj)
    if isinstance(obj, dict):
        return OrderedDict(
            [(key, replace_dicts_with_sorted_ordered_dicts(obj[key]))
             for key in sorted(obj, key=key_func)])
    return obj


class Migration(MigrationBase):
    DESCRIPTION = "Use only ordered dicts for diff_log and settings.json."

    def __init__(self, *args, **kwargs):
        super(Migration, self).__init__(*args, **kwargs)

    def run_diff_log(self, diff_log):
        # order dicts
        diff_log = replace_dicts_with_sorted_ordered_dicts(diff_log)
        return diff_log

    def run_settings(self, settings):
        # order settings dict
        return replace_dicts_with_sorted_ordered_dicts(settings)
