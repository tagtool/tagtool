from collections import OrderedDict

from models.store import Store
from models.migration import MigrationBase

from logging import getLogger
log = getLogger(__name__)


class Migration(MigrationBase):
    DESCRIPTION = "Give all categories a display-priority."

    def __init__(self, *args, **kwargs):
        super(Migration, self).__init__(*args, **kwargs)

    def run_settings(self, settings):
        latest_db = self.get_latest_db()
        if latest_db is not None:
            cats = list(latest_db[0]['categories'].keys())
        else:
            cats = []
        max_prio = max(settings['display-priority'].values())
        missing_cats = [cat for cat in cats
                        if cat not in settings['display-priority']]
        missing_cats.sort()
        missing_prios = zip(missing_cats, range(1, len(missing_cats)+1))
        for cat, add_prio in missing_prios:
            settings['display-priority'][cat] = max_prio + add_prio

        return settings
