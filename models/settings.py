import os
import errno
from copy import deepcopy
from collections import OrderedDict
import json

from logging import getLogger
log = getLogger(__name__)

from .store import StoreListener

__author__ = 'rouven'


class InvalidSettingsError(ValueError):
    pass


class Settings(object):
    DEFAULT_DATA_PATH = os.path.join(os.path.dirname(__file__), "..", "data")
    DEFAULT_FILE_NAME = 'settings.json'
    EMPTY_SETTINGS = OrderedDict([('sizing', OrderedDict()),
                                  ('display-priority', OrderedDict()),
                                  ('display-type', OrderedDict())])
    ALLOWED_SIZINGS = ["links", "recency", "none"]
    ALLOWED_DISPLAY_TYPES = ["cloud", "links"]

    def __init__(self, path=DEFAULT_DATA_PATH, fn=DEFAULT_FILE_NAME,
                 cb=None, store=None):
        self.store = store
        if store:
            self.store_listener = StoreListener(
                store, add_category=self._store_add_category)
        else:
            self.store_listener = None
        self.path = path
        self.fn = fn
        self._data = None
        self._cb = cb
        self._load_from_file(create_if_needed=True)

    def _get_file(self, fn=None, create_if_needed=False):
        if fn is None:
            fn = os.path.join(self.path, self.fn)
        log.debug('Loading settings from %s', fn)
        if not os.path.isfile(fn) and create_if_needed:
            log.info("Creating settings file")
            self._data = deepcopy(Settings.EMPTY_SETTINGS)
            if self.store is not None:
                cats = self.store.get_all_categories()
                self._data['display-priority'] = OrderedDict(
                    zip(sorted(cats), range(1, len(cats)+1)))
            self.flush(force=True)
            res = self._data
        else:
            with open(fn, 'r+') as f:
                data = json.load(f, object_pairs_hook=OrderedDict)
                log.debug("Raw settings read: %s", data)
                loaded_keys = set(data.keys())
                expected_keys = set(Settings.EMPTY_SETTINGS.keys())
                if len(loaded_keys - expected_keys) > 0:
                    raise InvalidSettingsError(
                        "Invalid key(s) found in settings file: {}".format(
                            loaded_keys - expected_keys))
                if loaded_keys < expected_keys:
                    new_keys = expected_keys - loaded_keys
                    log.info("Creating new setting key(s): %s" %
                             ", ".join(map(str, new_keys)))
                    data.update(deepcopy({k: Settings.EMPTY_SETTINGS[k]
                                          for k in new_keys}))
                res = data
        log.debug("Loaded settings: %s", res)
        return res

    def _load_from_file(self, fn=None, create_if_needed=False):
        self._data = self._get_file(fn, create_if_needed)

    def _is_file_different(self, fn):
        file_data = None
        try:
            file_data = self._get_file(fn)
        except InvalidSettingsError:
            file_data = None
        except IOError as e:
            if e.errno != errno.ENOENT:
                raise
        log.debug('Compare mem db against drive db:\n_data: %s\nfile_data: %s',
                  self._data, file_data)
        res = False
        if file_data is None or file_data != self._data:
            res = True
        log.debug("dbs are different result? %s", res)
        return res

    def flush(self, fn=None, force=False):
        if not force and not self._is_file_different(fn):
            log.debug('No settings flush because no effective change.')
            return False
        if fn is None:
            fn = os.path.join(self.path, self.fn)
        path = os.path.dirname(fn)
        if not path:
            path = self.path
            fn = os.path.join(path, fn)
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno == errno.EEXIST:
                pass
            else:  # pragma: no cover
                raise
        with open(fn, 'w') as f:
            json.dump(self._data, f, indent=1)
        log.info('Flushed settings to %s: %s', fn, self._data)
        log.debug('Settings update cb: %s', self._cb)
        if self._cb is not None:
            self._cb(self._data)
        return True

    def get(self):
        return deepcopy(self._data)

    def update(self, settings, flush=True, previously_changed=False):
        changed = previously_changed
        if isinstance(settings, str):
            try:
                settings = json.loads(settings, object_pairs_hook=OrderedDict)
            except ValueError:
                raise InvalidSettingsError(
                    "Settings must be a json object or a dict."
                )
        if not isinstance(settings, dict):
            raise InvalidSettingsError(
                "Settings must be a json object or a dict."
            )

        if 'sizing' in settings:
            for cat in settings['sizing']:
                if not (settings['sizing'][cat] in Settings.ALLOWED_SIZINGS or
                        settings['sizing'][cat] is None):
                    raise InvalidSettingsError(
                        "Only these sizings are allowed: {} or None.\n"
                        "But got {} for category {}.".format(
                            Settings.ALLOWED_SIZINGS,
                            json.dumps(settings['sizing'][cat]),
                            cat
                        ))
        if 'display-priority' in settings:
            for cat in settings['display-priority']:
                val = settings['display-priority'][cat]
                if not (isinstance(val, int) or val is None):
                    raise InvalidSettingsError(
                        "Only ints or None are accepted as display-priority.\n"
                        "But got {} for category {}.".format(
                            json.dumps(val), cat
                        ))
        if 'display-type' in settings:
            for cat in settings['display-type']:
                if not (settings['display-type'][cat]
                        in Settings.ALLOWED_DISPLAY_TYPES or
                        settings['display-type'][cat] is None):
                    raise InvalidSettingsError(
                        "Only these display types are allowed: {} or None.\n"
                        "But got {} for category {}.".format(
                            Settings.ALLOWED_DISPLAY_TYPES,
                            json.dumps(settings['display-type'][cat]),
                            cat
                        ))

        allowed_keys_set = Settings.EMPTY_SETTINGS.keys()
        if not set(settings.keys()).issubset(set(allowed_keys_set)):
            raise InvalidSettingsError(
                "Only accepting settings keys: {}.\nBut got: {}".format(
                    allowed_keys_set, settings.keys()))
        log.debug('Settings before update: %s', self._data)
        for setting in settings.keys():
            for key in settings[setting]:
                val = settings[setting][key]
                if val is not None:
                    if (not changed and (
                            key not in self._data[setting] or
                            self._data[setting][key] != val)):
                        changed = True
                    self._data[setting][key] = val
                elif key in self._data[setting]:
                    changed = True
                    self._data[setting].pop(key)
        log.info("Updated settings (%s) now: %s", settings, self._data)
        if flush:
            if changed:
                self.flush()
            else:
                log.debug('No flush because no effective change happened.')
        log.debug('Settings changed effectively: %s', changed)
        return changed

    def clone_category(self, source, target, flush=True,
                       previously_changed=False):
        changed = previously_changed
        for key in ('sizing', 'display-type', 'display-priority'):
            if source in self._data[key]:
                if (target not in self._data[key] or
                        self._data[key][target] != self._data[key][source]):
                    changed = True
                self._data[key][target] = self._data[key][source]
        if flush:
            if changed:
                self.flush()
            else:
                log.debug('No flush because no effective change happened.')
        return changed

    def _store_add_category(self, cat, **__):
        changed = False
        disp_prio_vals = list(self._data['display-priority'].values()) or [0]
        if cat not in self._data['display-priority']:
            changed = True
            self._data['display-priority'][cat] = max(disp_prio_vals) + 1
        else:
            current_prio = self._data['display-priority'][cat]
            if disp_prio_vals.count(current_prio) > 1:
                possible_prios = (set(range(current_prio + 1,
                                             max(disp_prio_vals)+1)) -
                                  set(disp_prio_vals))
                changed = True
                self._data['display-priority'][cat] = min(possible_prios)
        if changed:
            self.flush()
        else:
            log.debug('No flush because no effective change happened.')
