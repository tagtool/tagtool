import threading
from operator import itemgetter
from threading import RLock
from threading import Condition

import os
import io
import re
import math
from glob import glob
import errno
from math import ceil
from copy import deepcopy
from datetime import datetime
from datetime import timedelta
import dateutil.parser
import json
from functools import wraps
from functools import cmp_to_key
from collections import OrderedDict
from xlrd import XLRDError

from fuzzywuzzy import fuzz
import numpy as np
import pandas as pd

from util import readlines_reverse

from .migration import MigrationManager

from .errors import ElementExistsError
from .errors import ElementMissingError
from .errors import CategoryExistsError
from .errors import CategoryMissingError
from .errors import LinkExistsError
from .errors import LinkMissingError
from .errors import ReadOnlyStoreError
from .errors import StoreError

from .db_diff import DB_Diff
from .db_diff import HighLevelDB_Diff

from logging import getLogger
log = getLogger(__name__)

__author__ = 'rouven'


def copy_before_return(func):
    def inner(*args, **kwargs):
        return deepcopy(func(*args, **kwargs))
    return inner


def element_to_str(e):
    return " ".join(map(lambda x: '"' + x + '"', e))


def link_to_str(e1, e2):
    return "between %s and %s" % tuple(map(element_to_str, [e1, e2]))


class OldDBVersionError(RuntimeError):
    pass


def mkdir_p(path):
    # http://stackoverflow.com/questions/600268/mkdir-p-functionality-in-python
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno != errno.EEXIST or not os.path.isdir(path):
            raise


def path_to_datetime(fn):
    fn = fn.split('.json')[0]
    fn = fn.split('/')[-4:]
    date_str = '-'.join(fn[:3])
    time_str = fn[-1]
    return dateutil.parser.parse(date_str+" "+time_str)


def datetime_to_path(timestamp, base_path=""):
    fn = str(timestamp.time()) + ".json"
    path = os.path.join(*str(timestamp.date()).split('-'))
    return os.path.join(base_path, path, fn)


def fill_diff(diff, high_level=True):
    def fill(diff_, actions, inner_):
        for action_ in actions:
            if action_ not in diff_:
                diff_[action_] = {key: [] for key in inner_}
            else:
                for sub_ in inner_:
                    if sub_ not in diff_[action_]:
                        diff_[action_][sub_] = []

    diff = deepcopy(diff)
    fill(diff, ('insertions', 'deletions'),
         ('categories', 'elements', 'links'))
    if high_level:
        fill(diff, ('renamings',),
             ('categories', 'elements'))
    return diff


class LockLogger(object):
    def __init__(self, func_name, func_args, func_kwargs):
        self.name = func_name
        self.args = func_args
        self.kwargs = func_kwargs

    def _get_info(self):
        return (os.getpid(), threading.current_thread(), self.name,
                self.args, self.kwargs)

    @staticmethod
    def _as_func_call(func_name, args, kwargs):
        args = list(args)
        args += ["%s=%s" % tuple(map(repr, e)) for e in kwargs.items()]
        return "%s(%s)" % (func_name, ', '.join(map(repr, args)))

    def __enter__(self):
        pid, tid, fn, a, ka = self._get_info()
        log.debug("%s %s **ENTERING** %s" %
                  (str(pid), str(tid), LockLogger._as_func_call(fn, a, ka)))

    def __exit__(self, exc_type, exc_val, exc_tb):
        pid, tid, fn, a, ka = self._get_info()
        log.debug("%s %s **LEAVING** %s" %
                  (str(pid), str(tid), LockLogger._as_func_call(fn, a, ka)))


def with_write_lock(func):
    @wraps(func)
    def inner(*args, **kwargs):
        self = args[0]
        if self._read_only:
            raise IOError("This database instance is read only.")

        with Store.LOCK:
            log.debug('Latest timestamp: %s' % str(Store.latest_timestamp))
            if isinstance(self, Store):
                log.debug('Store timestamp: %s' % str(self.timestamp))
            if (isinstance(self, Store) and self.timestamp is not None and
                    self.timestamp != Store.latest_timestamp):
                log.debug("Found store out of sync %s, %s" %
                          (str(self.timestamp), str(Store.latest_timestamp)))
                self._load_db_from_disk(
                    self.find_timestamp(datetime.utcnow()))
            with LockLogger(func, args, kwargs):
                return func(*args, **kwargs)
    return inner


def with_read_lock(func):
    @wraps(func)
    def inner(*args, **kwargs):
        self = args[0]
        if self._read_only:
            return func(*args, **kwargs)
        else:
            with Store.LOCK:
                return func(*args, **kwargs)
    return inner


class StoreListener(object):
    def __init__(self, store, **callbacks):
        self.cbs = callbacks
        self.store = store
        store.register_listener(self)

    def _ignore(self, *args, **kwargs):
        pass

    def __getattr__(self, item):
        if item in self.cbs:
            return self.cbs[item]
        return self._ignore

    def __del__(self):
        self.store.unregister_listener(self)


class Store(object):
    DEFAULT_DATA_PATH = os.path.join(os.path.dirname(__file__), '..', "data")
    DIFF_LOG_FN = 'diff_log.jsonl'

    latest_timestamp = None
    timestamps_cache = None

    def push_update(self, diff):
        if self.diff_callback:
            self.diff_callback(diff)

    LOCK = RLock()

    def __init__(self, path=DEFAULT_DATA_PATH, timestamp=None,
                 diff_callback=None):
        """If timestamp is set, load the specified db. The db will then be read
        only (except for the restore method)."""
        MigrationManager.run_pending_migrations(data_path=path)
        log.debug('Init Store(timestamp=%s); pid %s; thread %s' %
                  (str(timestamp), str(os.getpid()),
                   str(threading.current_thread())))
        super(Store, self).__init__()
        self.timestamp = None
        self._high_level_diff = None
        self.path = path
        self._read_only = timestamp is not None
        self.db = None
        self.listeners = []
        self.listener_lock = RLock()
        self.diff_callback = diff_callback
        if not self._db_is_inited():
            if self._read_only:
                raise IOError('Init db without timestamp before loading one '
                              'with timestamp.')
            self._init_db()
        with Store.LOCK:
            if Store.latest_timestamp is None:
                Store.latest_timestamp = self.find_timestamp(datetime.utcnow())
                log.debug("Inited latest_timestamp %s" %
                          str(Store.latest_timestamp))
        self._load_db_from_disk(timestamp)
        log.debug('Done init Store(timestamp=%s); pid %s; thread %s' %
                  (str(timestamp), str(os.getpid()),
                   str(threading.current_thread())))

    def _db_is_inited(self):
        return os.path.exists(self.path) and glob(self.path + '/*/*/*/*.json')

    @with_write_lock
    def _init_db(self):
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        log.info('initializing a new database')
        self.db = OrderedDict(
            [("categories", OrderedDict()), ("links", [])])
        self.flush_db(force=True)

    @with_read_lock
    def _read_db_from_disk(self, timestamp=None):
        log.debug('_read_db_from_disk(timestamp=%s)' % timestamp)
        if timestamp is None:
            timestamp = self.find_timestamp(datetime.utcnow())
        else:
            timestamp = self.find_timestamp(timestamp)
        log.debug('reading timestamp: %s' % timestamp)
        fn = datetime_to_path(timestamp, self.path)
        with open(fn, "r") as f:
            return json.load(f, object_pairs_hook=OrderedDict), timestamp

    @with_read_lock
    def _load_db_from_disk(self, timestamp=None):
        log.debug('_load_db_from_disk(timestamp=%s), self.timestamp: %s' %
                  (timestamp, self.timestamp))
        if timestamp is None:
            timestamp = self.timestamp
        (self.db, _), self.timestamp = self._read_db_from_disk(timestamp)
        log.debug('loaded db with timestamp %s' % str(self.timestamp))

    @with_write_lock
    def reset_changes(self):
        log.debug('reset_changes, self.timestamp: %s' % self.timestamp)
        self._load_db_from_disk(self.timestamp)

    @with_read_lock
    def db_changed(self):
        assert isinstance(self.db, OrderedDict)
        return self._read_db_from_disk()[0][0] != self.db

    @with_write_lock
    def _append_high_level_diff(self, timestamp, high_level_diff):
        if type(high_level_diff) == DB_Diff:
            high_level_diff = OrderedDict(deepcopy(high_level_diff))
            del high_level_diff['from'], high_level_diff['to']
            high_level_diff = HighLevelDB_Diff(raw_diff=high_level_diff)
        elif type(high_level_diff) == HighLevelDB_Diff:
            high_level_diff = deepcopy(high_level_diff)
        elif isinstance(high_level_diff, dict):
            high_level_diff = HighLevelDB_Diff(raw_diff=high_level_diff)
        else:
            raise TypeError('high_level_diff must be DB_Diff, HighLevelDB_Diff '
                            'or a dict, but got %s' % type(high_level_diff))
        high_level_diff = OrderedDict(high_level_diff)
        del high_level_diff['from'], high_level_diff['to']
        diff_log = []
        try:
            with open(os.path.join(self.path, Store.DIFF_LOG_FN), "r") as f:
                for line in f:
                    diff_log.append(json.loads(line))
        except IOError as e:
            if e.errno != errno.ENOENT:
                raise
        log.info('Adding to diff log: %s',
                 json.dumps([str(timestamp), high_level_diff]))
        diff_log.append([str(timestamp), high_level_diff])
        with open(os.path.join(self.path, Store.DIFF_LOG_FN), "w") as f:
            for diff in diff_log:
                f.write(json.dumps(diff) + '\n')

    @with_write_lock
    def flush_db(self, timestamp=None, force=False, push_diff=True,
                 high_level_diff=None):
        # if push diff is set a diff is calculated and emitted poll result
        log.debug('dbv (server side) entering flush_db: %s' %
                  str(self.timestamp))
        if not isinstance(timestamp, datetime) and timestamp is not None:
            timestamp = dateutil.parser.parse(timestamp)
        now = datetime.utcnow()
        if force or self.db_changed():
            fn = str(datetime.time(now)) + ".json"
            date_path = os.path.join(*str(now.date()).split("-"))
            mkdir_p(os.path.join(self.path, date_path))

            if timestamp is not None and timestamp != self.timestamp:
                # old db => rollback all changes by reloading from disk
                self._load_db_from_disk(self.timestamp)
                raise OldDBVersionError(
                    "Writing on old db version: %s. Latest: %s." %
                    (timestamp, self.timestamp))
            log.debug('Attempt flush db to %s',
                      os.path.join(self.path, date_path, fn))
            if high_level_diff is None:
                if self._db_is_inited() and self.db_changed():
                    raise ValueError('high_level_diff must not be None if '
                                     'db is initialized and modified.')
                high_level_diff = {}
            self._append_high_level_diff(now, high_level_diff)
            with open(os.path.join(self.path, date_path, fn), "w") as f:
                json.dump([self.db, str(now)], f, indent=1)
            log.info('Flushed db to %s', os.path.join(self.path, date_path, fn))
            old_timestamp = self.timestamp
            self.timestamp = now
            Store.latest_timestamp = now
            Store.timestamps_cache = None
            log.debug("Updated latest_timestamp %s" %
                      str(Store.latest_timestamp))
            if push_diff:
                self.push_update(self.calc_db_diff(old_timestamp, now))
            log.debug('dbv (server side) leaving flush_db (success): %s' %
                      str(self.timestamp))
            return True
        else:
            log.debug('dbv (server side) leaving flush_db (fail): %s' %
                      str(self.timestamp))
            return False

    @with_read_lock
    def get_all_categories(self):
        return list(self.db['categories'].keys())

    @with_read_lock
    @copy_before_return
    def get_all_categories_with_elements(self):
        return self.db['categories']

    @with_read_lock
    def get_all_elements(self):
        res = []
        for cat in self.get_all_categories():
            for elem in self.db['categories'][cat]:
                res.append([cat, elem])
        return res

    @with_read_lock
    def get_all_categories_with_elements_and_weights(self):
        data = {k: {k2: 0 for k2 in v}
                for k, v in self.db['categories'].items()}
        for link in self.get_all_links():
            src, dst = link
            src_cat, src_elem = src
            dst_cat, dst_elem = dst
            data[src_cat][src_elem] += 1
            data[dst_cat][dst_elem] += 1
        return data

    @with_read_lock
    def get_all_category_members(self, cat):
        if cat not in self.get_all_categories():
            raise CategoryMissingError("There is no category '%s'." % cat)
        return [[cat, elem] for elem in self.db['categories'][cat]]

    @with_write_lock
    def add_category(self, cat, dbv=None, strict=False, flush=True, diff=None):
        if diff is None:
            diff = DB_Diff()
        if cat in self.get_all_categories():
            if strict:
                raise CategoryExistsError("Category '%s' already exists" % cat,
                                          strict_error=True)
            else:
                log.debug('Ignored add_category (category exists)')
                return False
        if cat == "":
            raise ValueError('Cannot add category "".')
        self.db['categories'][cat] = []
        log.info('Added category "%s" to db', cat)
        diff.add_cat(cat)
        if flush:
            diff['from'] = dbv
            self.flush_db(
                dbv, force=True, push_diff=False,
                high_level_diff={"insertions": {"categories": [cat]}}
            )
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.add_category(cat, raw_diff=diff)
        return True

    LINK_RE = re.compile(r"^\[((?:[^\]\[]|\\\[|\\\])*)\]\(([^()]*)\)$")

    def _check_is_link_elem(self, elem):
        return bool(Store.LINK_RE.findall(elem))

    def _title_from_link_elem(self, elem):
        if self._check_is_link_elem(elem):
            raw_title = Store.LINK_RE.sub(r"\1", elem)
            return re.sub(r"\\(\]|\[)", r"\1", raw_title)
        else:
            return elem

    def _link_elem_from_title(self, cat, elem, throw=False):
        if self._check_is_link_elem(elem):
            return elem
        for e in self.db['categories'][cat]:
            if self._title_from_link_elem(e) == elem:
                return e
        if throw:
            raise ValueError("Elem %s couldn't be unnormalized: Not found in "
                             "cat %s" % (elem, cat))
        else:
            return elem

    def _elem_in_cat(self, cat, elem, normalize_links=True):
        if elem in self.db['categories'][cat]:
            return True
        if normalize_links:
            elem = self._title_from_link_elem(elem)
            if elem in map(self._title_from_link_elem,
                           self.db['categories'][cat]):
                return True
        return False

    @with_write_lock
    def add_to_category(self, cat, elem, dbv=None, strict=False, flush=True,
                        diff=None):
        if diff is None:
            diff = DB_Diff()
        if cat not in self.get_all_categories():
            if not strict:
                self.add_category(cat, dbv, strict=strict, flush=False,
                                  diff=diff)
            else:
                raise CategoryMissingError("Unknown category '%s'" % cat,
                                           strict_error=True,
                                           strict_fixable=True)
        if self._elem_in_cat(cat, elem):
            if strict:
                raise ElementExistsError("Element '%s' in category '%s' "
                                         "already exists" % (cat, elem),
                                         strict_error=True)
            else:
                log.debug('Ignored add_to_category (element exists)')
                return False
        if elem == "":
            raise ValueError('Cannot add element "".')
        self.db['categories'][cat].append(elem)
        log.info('Added "%s" to category "%s"', elem, cat)
        diff.add_elem([cat, elem])
        if flush:
            diff['from'] = dbv
            self.flush_db(
                dbv, force=True, push_diff=False,
                high_level_diff={"insertions": {"elements": [[cat, elem]]}}
            )
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.add_to_category(cat, elem, raw_diff=diff)
        return True

    @with_read_lock
    @copy_before_return
    def get_category(self, cat):
        if cat not in self.get_all_categories():
            raise CategoryMissingError("Unknown category '%s'" % cat)
        return self.db['categories'][cat]

    @with_write_lock
    def remove_from_category(self, cat, elem, dbv=None, strict=False,
                             flush=True, diff=None):
        if diff is None:
            diff = DB_Diff()
        if cat not in self.get_all_categories():
            if strict:
                raise CategoryMissingError("Unknown category '%s'" % cat,
                                           strict_error=True)
            else:
                log.debug("Ignored remove_from_cat (element doesn't exists).")
                return False
        if not self._elem_in_cat(cat, elem):
            if strict:
                raise ElementMissingError("Unknown element '%s' "
                                          "in category '%s'" % (cat, elem),
                                          strict_error=True)
            else:
                log.debug("Ignored remove_from_cat "
                          "(element doesn't exists).")
                return False
        elem = self._link_elem_from_title(cat, elem)
        for e1, e2 in self.get_all_links([cat, elem]):
            self.remove_link(e1, e2, dbv=dbv, strict=strict, flush=False,
                             diff=diff)
        self.db['categories'][cat].remove(elem)
        log.info('Removed "%s" from category "%s"', elem, cat)
        diff.remove_elem([cat, elem])
        if flush:
            diff['from'] = dbv
            self.flush_db(
                dbv, force=True, push_diff=False,
                high_level_diff={"deletions": {"elements": [[cat, elem]]}}
            )
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.remove_from_category(cat, elem, raw_diff=diff)
        return True

    @with_write_lock
    def remove_category(self, cat, dbv=None, strict=False, flush=True,
                        diff=None):
        if diff is None:
            diff = DB_Diff()
        if cat not in self.get_all_categories():
            if strict:
                raise CategoryMissingError("Unknown category '%s'" % cat,
                                           strict_error=True)
            else:
                log.debug("Ignored remove_category (category doesn't exists).")
                return False
        for elem in self.get_category(cat):
            self.remove_from_category(cat, elem, dbv=dbv, strict=strict,
                                      flush=False, diff=diff)
        self.db['categories'].pop(cat)
        log.info('Removed category "%s" from db', cat)
        diff.remove_cat(cat)
        if flush:
            diff['from'] = dbv
            self.flush_db(
                dbv, force=True, push_diff=False,
                high_level_diff={"insertions": {"categories": [cat]}}
            )
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.remove_category(cat, raw_diff=diff)
        return True

    @with_read_lock
    def _get_all_links(self):
        return self.db["links"]

    @with_read_lock
    def _get_links_between(self, e1, e2):
        links = self._get_all_links()
        return [link for link in links if
                list(link) == [list(e1), list(e2)] or
                list(link) == [list(e2), list(e1)]]

    @with_read_lock
    def _get_links_of_one_elem(self, e):
        links = self._get_all_links()
        return [link for link in links if list(e) in link]

    @with_read_lock
    def get_all_links(self, e1=None, e2=None):
        for e in (e1, e2):
            if e is not None:
                self._elem_validation(e)
        # specify e1 or e1 and e2 or none of them
        if e1 is None and e2 is None:
            return self._get_all_links()
        elif e1 is not None and e2 is None:
            return self._get_links_of_one_elem(e1)
        elif e1 is not None and e2 is not None:
            return self._get_links_between(e1, e2)
        else:
            raise ValueError("Specify None ^ e1 ^ e1&e2")

    @with_read_lock
    def get_all_linked(self, e):
        e = list(e)
        links = self.get_all_links(e1=e)
        return [l[0] if e == l[1] else l[1] for l in links]

    @with_read_lock
    def _elem_validation(self, e, must_exist=True):
        # If must exist is set to False, no error will be raised if the category
        # or the element does not exist. The flag will also make the method
        # return True or False, depending on the existence of the element
        if not isinstance(e, (list, tuple)):
            raise TypeError("Element must be list or tuple. Found %s" % type(e))
        e = list(e)
        if not (len(e) == 2):
            raise ValueError("Length of e must be 2 not %i" % len(e))
        if not all(map(lambda x: isinstance(x, str), e)):
            raise ValueError("Elements must 2-tuple of strings not %s" %
                             str(map(type, e)))
        if e[0] not in self.get_all_categories():
            if must_exist:
                raise CategoryMissingError("Unknown category '%s'" % e[0],
                                           strict_error=True,
                                           strict_fixable=True)
            else:
                return False
        exists = self._elem_in_cat(*e)
        if must_exist and not exists:
            raise ElementMissingError(
                "Unknown element '%s' in category '%s'" % (e[1], e[0]),
                strict_error=True,
                strict_fixable=True)
        elif not must_exist:
            return exists

    @with_read_lock
    def _link_validation(self, e1, e2, must_exist=True):
        self._elem_validation(e1, must_exist=must_exist)
        self._elem_validation(e2, must_exist=must_exist)

    @with_read_lock
    def _elem_normalization(self, e):
        if not self._elem_validation(e, must_exist=False):
            return list(e)
        return [e[0], self._link_elem_from_title(*e)]

    @with_write_lock
    def add_link(self, e1, e2, dbv=None, strict=False, flush=True,
                 diff=None):
        if diff is None:
            diff = DB_Diff()

        def create_element_if_needed(cat, elem):
            self.add_to_category(cat, elem, dbv=dbv, strict=False,
                                 flush=False, diff=diff)

        if e1 == e2:
            raise StoreError("Can't link an element to itself.")
        if not strict:
            create_element_if_needed(*e1)
            create_element_if_needed(*e2)
        else:
            self._link_validation(e1, e2)
        e1 = self._elem_normalization(e1)
        e2 = self._elem_normalization(e2)
        if [e1, e2] in self.db["links"]:
            log.debug('Skip adding link. Already exists: %s' % str([e1, e2]))
            if strict:
                raise LinkExistsError(
                    "Link %s already exists" % link_to_str(e1, e2),
                    strict_error=True)
            else:
                log.debug('Ignored add_link (link exists).')
                return False
        if [e2, e1] in self.db["links"]:
            log.debug('Skip adding link. Already exists: %s' % str([e2, e1]))
            if strict:
                raise LinkExistsError(
                    "Link %s already exists" % link_to_str(e2, e1),
                    strict_error=True)
            else:
                log.debug('Ignored add_link (link exists).')
                return False
        self.db["links"].append([e1, e2])
        log.info('Added link from %s to %s', e1, e2)
        diff.add_link([e1, e2])
        if flush:
            diff['from'] = dbv
            self.flush_db(
                dbv, force=True, push_diff=False,
                high_level_diff={"insertions": {"links": [[e1, e2]]}}
            )
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.add_link(e1, e2, raw_diff=diff)
        return True

    @with_write_lock
    def remove_link(self, e1, e2, dbv=None, strict=False, flush=True,
                    diff=None):
        if diff is None:
            diff = DB_Diff()
        self._link_validation(e1, e2, must_exist=strict)
        e1 = self._elem_normalization(e1)
        e2 = self._elem_normalization(e2)
        # assumes that link list is a set
        if [e1, e2] in self.db["links"]:
            link = [e1, e2]
            self.db["links"].remove([e1, e2])
            log.info('Removed link from %s to %s', e1, e2)
            diff.remove_link([e1, e2])
        elif [e2, e1] in self.db["links"]:
            link = [e2, e1]
            self.db["links"].remove([e2, e1])
            log.info('Removed link from %s to %s', e2, e1)
            diff.remove_link([e2, e1])
        else:
            if strict:
                raise LinkMissingError(
                    "Link %s doesn't exists" % link_to_str(e1, e2),
                    strict_error=True)
            else:
                log.debug("Ignored remove_link (link doesn't exists).")
                return False
        if flush:
            diff['from'] = dbv
            self.flush_db(
                dbv, force=True, push_diff=False,
                high_level_diff={"deletions": {"links": [link]}}
            )
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.remove_link(e1, e2, raw_diff=diff)
        return True

    @with_write_lock
    def rename_category(self, old_name, new_name, dbv=None, flush=True,
                        diff=None):
        if diff is None:
            diff = DB_Diff()
        if old_name not in self.get_all_categories():
            raise CategoryMissingError(
                "Category '%s' doesn't exist." % old_name)
        if new_name in self.get_all_categories():
            raise CategoryExistsError(
                "Category '%s' already exists." % new_name)
        for e1, e2 in self.db['links']:
            old_link = deepcopy([e1, e2])
            if e1[0] == old_name:
                e1[0] = new_name
            if e2[0] == old_name:
                e2[0] = new_name
            if old_link != [e1, e2]:
                diff.remove_link(old_link)
                diff.add_link([e1, e2])

        # doing it in a copy loop to keep cats at the same place.
        # Old way (faster but destroying the order):
        # self.db['categories'][new_name] = self.db['categories'].pop(old_name)
        new_db = deepcopy(self.db)
        new_db['categories'] = OrderedDict()
        for cat in self.db['categories']:
            new_db['categories'][cat if cat != old_name else new_name] =\
                self.db['categories'][cat]
        diff.remove_elems(self.get_all_category_members(old_name))
        diff.remove_cat(old_name)
        self.db = new_db
        log.info('Renamed category "%s" to "%s"', old_name, new_name)
        diff.add_cat(new_name)
        diff.add_elems(self.get_all_category_members(new_name))

        if flush:
            diff['from'] = dbv
            self.flush_db(
                dbv, force=True, push_diff=False,
                high_level_diff={"renamings": {"categories": [[old_name,
                                                               new_name]]}}
            )
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.rename_category(old_name, new_name, raw_diff=diff)

    @with_write_lock
    def _rename_element_in_cat(self, in_category, old_name, new_name, diff):
        for link in self.db['links']:
            old_link = deepcopy(link)
            for e in link:
                if e == [in_category, old_name]:
                    e[1] = new_name
            if old_link != link:
                diff.remove_link(old_link)
                diff.add_link(link)
        idx = self.db['categories'][in_category].index(old_name)
        self.db['categories'][in_category][idx] = new_name
        log.info('Renamed "%s" to "%s" within "%s"', old_name, new_name,
                 in_category)
        diff.remove_elem([in_category, old_name])
        diff.add_elem([in_category, new_name])

    @with_write_lock
    def _rename_element_over_cat(self, old_category, old_name,
                                 new_category, new_name, diff):
        for link in self.db['links']:
            old_link = deepcopy(link)
            for e in link:
                if e == [old_category, old_name]:
                    e[0], e[1] = new_category, new_name
            if old_link != link:
                diff.remove_link(old_link)
                diff.add_link(link)
        self.db['categories'][old_category].remove(old_name)
        self.db['categories'][new_category].append(new_name)
        log.info('Renamed ["%s", "%s"] to ["%s", "%s"]',
                 old_category, old_name, new_category, new_name)
        diff.remove_elem([old_category, old_name])
        diff.add_elem([new_category, new_name])

    @with_write_lock
    def rename_element(self, old_category, old_name, new_category, new_name,
                       dbv=None, flush=True, diff=None):
        if diff is None:
            diff = DB_Diff()
        for cat in (old_category, new_category):
            if cat not in self.get_all_categories():
                raise CategoryMissingError(
                    "Category '%s' doesn't exist." % cat)
        if old_name not in self.db['categories'][old_category]:
            raise ElementMissingError(
                "Element '%s' doesn't exist in category '%s'." %
                (old_name, old_category))
        if new_name in self.db['categories'][new_category]:
            raise ElementExistsError(
                "Element '%s' already exist in category '%s'." %
                (new_name, new_category))
        old_name = self._link_elem_from_title(old_category, old_name)
        if old_category == new_category:
            self._rename_element_in_cat(old_category, old_name, new_name, diff)
        else:
            self._rename_element_over_cat(
                old_category, old_name, new_category, new_name, diff)
        if flush:
            diff['from'] = dbv
            self.flush_db(
                dbv, force=True, push_diff=False,
                high_level_diff={"renamings": {"elements": [
                    [[old_category, old_name], [new_category, new_name]]
                ]}}
            )
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.rename_element(old_category, old_name,
                                            new_category, new_name,
                                            raw_diff=diff)

    @with_write_lock
    def diff_action(self, diff_todo, dbv=None, strict=False, flush=True,
                    diff=None):
        try:
            if diff is None:
                diff = DB_Diff()
            if isinstance(diff_todo, str):
                diff_todo = json.loads(diff_todo)
            if not isinstance(diff_todo, dict):
                raise ValueError("diff must be json string or dict. "
                                 "But was %s." % type(diff_todo))
            for key in diff_todo:
                if not isinstance(diff_todo[key], dict):
                    raise ValueError("diff dict values must be dicts but found "
                                     "type(%s) = %s" %
                                     (key, type(diff_todo[key])))
            diff_todo = fill_diff(diff_todo, high_level=True)
            log.debug('Expanded diff: %s' % diff_todo)
            effective_diff = HighLevelDB_Diff()

            if (len(diff_todo) != 3 or
                    len(diff_todo['insertions']) != 3 or
                    len(diff_todo['deletions']) != 3 or
                    len(diff_todo['renamings']) != 2):
                raise ValueError('diff contained invalid or too many keys.')
            for action in diff_todo:
                for sub in diff_todo[action]:
                    if not isinstance(diff_todo[action][sub], (list, tuple)):
                        raise ValueError("Values of insertion and deletion "
                                         "dicts must be lists")
            for cat in diff_todo['insertions']['categories']:
                if not isinstance(cat, str):
                    raise ValueError("Invalid category insertion: %s" % cat)
                if self.add_category(cat, dbv=dbv, strict=strict, flush=False,
                                     diff=diff):
                    effective_diff.add_cat(cat)
            for catelem in diff_todo['insertions']['elements']:
                if (not isinstance(catelem, (list, tuple)) or
                        len(catelem) != 2 or
                        not isinstance(catelem[0], str) or
                        not isinstance(catelem[1], str)):
                    raise ValueError("Invalid element insertion: %s" % catelem)
                cat, elem = catelem
                if self.add_to_category(cat, elem, dbv=dbv, strict=strict,
                                        flush=False, diff=diff):
                    effective_diff.add_elem(catelem)
            for link in diff_todo['insertions']['links']:
                if (not isinstance(link, (list, tuple)) or len(link) != 2 or
                        not isinstance(link[0], (list, tuple)) or
                        not isinstance(link[1], (list, tuple)) or
                        len(link[0]) != 2 or len(link[1]) != 2 or
                        not isinstance(link[0][0], str) or
                        not isinstance(link[0][1], str) or
                        not isinstance(link[1][0], str) or
                        not isinstance(link[1][1], str)):
                    raise ValueError("Invalid link insertion: %s" % link)
                if self.add_link(link[0], link[1], dbv=dbv, strict=strict,
                                 flush=False, diff=diff):
                    effective_diff.add_link(link)

            for cat in diff_todo['renamings']['categories']:
                if (not isinstance(cat, (list, tuple)) or len(cat) != 2 or
                        not isinstance(cat[0], str) or
                        not isinstance(cat[1], str)):
                    raise ValueError("Invalid category renaming: %s" % cat)
                self.rename_category(cat[0], cat[1], dbv=dbv, flush=False,
                                     diff=diff)
                effective_diff.rename_cat([cat[0], cat[1]])
            for elem in diff_todo['renamings']['elements']:
                if (not isinstance(elem, (list, tuple)) or len(elem) != 2 or
                        not isinstance(elem[0], (list, tuple)) or
                        not isinstance(elem[1], (list, tuple)) or
                        not isinstance(elem[0][0], str) or
                        not isinstance(elem[0][1], str) or
                        not isinstance(elem[1][0], str) or
                        not isinstance(elem[1][1], str)):
                    raise ValueError("Invalid element renaming: %s" % elem)
                self.rename_element(
                    elem[0][0], elem[0][1], elem[1][0], elem[1][1], dbv=dbv,
                    flush=False, diff=diff)
                effective_diff.rename_elem(elem)

            for link in diff_todo['deletions']['links']:
                if (not isinstance(link, (list, tuple)) or len(link) != 2 or
                        not isinstance(link[0], (list, tuple)) or
                        not isinstance(link[1], (list, tuple)) or
                        len(link[0]) != 2 or len(link[1]) != 2 or
                        not isinstance(link[0][0], str) or
                        not isinstance(link[0][1], str) or
                        not isinstance(link[1][0], str) or
                        not isinstance(link[1][1], str)):
                    raise ValueError("Invalid link deletion: %s" % link)
                if self.remove_link(link[0], link[1], dbv=dbv, strict=strict,
                                    flush=False, diff=diff):
                    effective_diff.remove_link(link)
            for catelem in diff_todo['deletions']['elements']:
                if (not isinstance(catelem, (list, tuple)) or
                        len(catelem) != 2 or
                        not isinstance(catelem[0], str) or
                        not isinstance(catelem[1], str)):
                    raise ValueError("Invalid element deletion: %s" % catelem)
                cat, elem = catelem
                if self.remove_from_category(cat, elem, dbv=dbv, strict=strict,
                                             flush=False, diff=diff):
                    effective_diff.remove_elem(catelem)
            for cat in diff_todo['deletions']['categories']:
                if not isinstance(cat, str):
                    raise ValueError("Invalid category deletion: %s" % cat)
                if self.remove_category(cat, dbv=dbv, strict=strict,
                                        flush=False, diff=diff):
                    effective_diff.remove_cat(cat)

            log.info('Done with bulk action %s', diff_todo)
            if effective_diff.is_empty():
                log.debug('Canceling bulk action with no effect.')
                return False
            log.debug('Effective diff: %s', effective_diff)
        except Exception:
            # roll-back
            self._load_db_from_disk(self.timestamp)
            raise
        if flush:
            diff['from'] = dbv
            self.flush_db(dbv, force=True, push_diff=False,
                          high_level_diff=effective_diff)
            diff['to'] = self
            self.push_update(diff)
            log.debug('Informing %i listener(s)' % len(self.listeners))
            with self.listener_lock:
                for listener in self.listeners:
                    listener.diff_action(diff_todo, raw_diff=diff)
        return True

    @with_read_lock
    def search_element(self, query, cat=None,
                       fuzzy_threshold=None, fuzzy_limit=None,
                       force_fuzzy=False):
        if fuzzy_threshold is None:
            fuzzy_threshold = 60
        if fuzzy_limit is None:
            fuzzy_limit = 3
        perfect = []
        case = []
        space = []
        fuzzy = []
        perfect_fuzzy_found = False
        if cat is None:
            elems = self.get_all_elements()
        else:
            elems = [[cat, e] for e in self.get_category(cat)]
        for cat, elem in elems:
            if elem == query:
                perfect.append([cat, elem])
            if perfect:
                continue
            if elem.lower() == query.lower():
                case.append([cat, elem])
            if case:
                continue
            if ''.join(elem.lower().split()) == ''.join(query.lower().split()):
                space.append([cat, elem])
            if space:
                continue
            score = fuzz.partial_ratio(re.subn(r'\s+', ' ', elem.lower())[0],
                                       re.subn(r'\s+', ' ', query.lower())[0])
            if score == 100:
                perfect_fuzzy_found = True
            fuzzy.append((score, [cat, elem]))
        if not force_fuzzy:
            for quality in (perfect, case, space):
                if quality:
                    return quality
        else:
            for i, quality in enumerate((space, case, perfect)):
                fuzzy += list(map(lambda x: [101+i, x], quality))
        if perfect_fuzzy_found and not force_fuzzy:
            fuzzy_threshold = 100
        # sort by elem length to prefer longer matches
        fuzzy = sorted(fuzzy, reverse=True,
                       key=cmp_to_key(lambda x, y: len(x[1][1])-len(y[1][1])))
        plus_100_hits = len([x for x in fuzzy if x[0] >= 100])
        hits = [h[1] for h in sorted(fuzzy, key=itemgetter(0), reverse=True)
                if h[0] >= fuzzy_threshold]
        if fuzzy_limit >= 0:
            return hits[:max(plus_100_hits, fuzzy_limit)]
        else:
            return hits

    @with_read_lock
    def all_available_timestamps(self, nocache=False):
        if Store.timestamps_cache is None or nocache:
            all_dbs = sorted(glob(self.path + '/*/*/*/*.json'))
            Store.timestamps_cache = all_dbs = list(map(path_to_datetime,
                                                        all_dbs))
            return all_dbs
        else:
            return Store.timestamps_cache

    @with_read_lock
    def find_timestamp(self, timestamp, offset=0):
        # if the exact timestamp does not exist it will return the latest db
        # before the timestamp.
        # Only exception: When given timestamp is before first db version the
        # first version will be returned.
        # offset=1 will find next db after given timestamp
        # offset=-1 the previous
        # if offset causes index to be out if bounds false will be returned
        if not isinstance(timestamp, datetime):
            timestamp = dateutil.parser.parse(timestamp)
        all_dbs = self.all_available_timestamps()
        for choice in range(len(all_dbs)-1, -1, -1):
            if timestamp >= all_dbs[choice]:
                return (all_dbs[choice+offset] if
                        0 <= choice+offset < len(all_dbs) else False)
        return all_dbs[offset] if 0 <= offset < len(all_dbs) else False

    @with_read_lock
    def find_timestamp_ceil(self, timestamp, offset=0, floor_last=True):
        # like find_timestamp but if exact timestamp does not exist it will
        # return the latest db after the timestamp
        # Set floor_last to True if you want the latest timestamp to be
        # returned in case you provide a timestamp that's newer than the latest
        # database version.
        if not isinstance(timestamp, datetime):
            timestamp = dateutil.parser.parse(timestamp)
        all_dbs = self.all_available_timestamps()
        for choice in range(len(all_dbs)):
            if timestamp <= all_dbs[choice]:
                return (all_dbs[choice+offset] if
                        0 <= choice+offset < len(all_dbs) else False)
        if floor_last:
            return all_dbs[-1+offset] if -len(all_dbs) < offset <= 0 else False
        else:
            return all_dbs[offset] if -len(all_dbs) <= offset < 0 else False

    @with_read_lock
    def get_db_by_timestamp(self, timestamp, offset=0, path=None):
        if path is None:
            path = self.path
        if not offset:
            return Store(path=path, timestamp=timestamp)
        else:
            timestamp = self.find_timestamp(timestamp, offset=offset)
            if not timestamp:
                raise ValueError("Couldn't find a db for that timestamp with "
                                 "that offset. This cannot happen if you don't "
                                 "use an offset.")
            return Store(path=path, timestamp=timestamp)

    @with_read_lock
    def calc_db_diff(self, old_timestamp=None, new_timestamp=None,
                     allow_empty_diff=True):
        # allow_empty_diff is False None will be returned instead of empty diffs
        if new_timestamp is None:
            new_timestamp = datetime.utcnow()
        new_timestamp = self.find_timestamp(new_timestamp)
        if not isinstance(new_timestamp, datetime):
            new_timestamp = dateutil.parser.parse(new_timestamp)
        if old_timestamp is None:
            return self.full_db_as_diff(new_timestamp)
        if not isinstance(old_timestamp, datetime):
            old_timestamp = dateutil.parser.parse(old_timestamp)
        old_timestamp = self.find_timestamp(old_timestamp)
        if old_timestamp == new_timestamp:
            if not allow_empty_diff:
                return None
            else:
                return DB_Diff(old_timestamp, new_timestamp)
        old_db = Store(path=self.path, timestamp=old_timestamp)
        new_db = Store(path=self.path, timestamp=new_timestamp)
        diff = DB_Diff(old_db, new_db)
        old_cats = old_db.get_all_categories()
        new_cats = new_db.get_all_categories()
        old_elems = old_db.get_all_elements()
        new_elems = new_db.get_all_elements()
        old_links = old_db.get_all_links()
        new_links = new_db.get_all_links()

        # TODO: this can be optimized by taking into account, that the db
        # is ordered (a diff like git does would be enough O(n))
        # insertions
        # cat
        for new_cat in new_cats:
            if new_cat not in old_cats:
                diff.add_cat(new_cat)
        # elem
        for new_elem in new_elems:
            if new_elem not in old_elems:
                diff.add_elem(new_elem)
        # link
        for new_link in new_links:
            if (new_link not in old_links and
                    list(reversed(new_link)) not in old_links):
                diff.add_link(new_link)

        # deletions
        # cat
        for old_cat in old_cats:
            if old_cat not in new_cats:
                diff.remove_cat(old_cat)
        # elem
        for old_elem in old_elems:
            if old_elem not in new_elems:
                diff.remove_elem(old_elem)
        # link
        for old_link in old_links:
            if (old_link not in new_links and
                    list(reversed(old_link)) not in new_links):
                diff.remove_link(old_link)

        return diff

    @property
    @with_read_lock
    def high_level_diff(self, timestamp=None):
        if timestamp is None:
            if self._high_level_diff is not None:
                return self._high_level_diff
            timestamp = self.timestamp
        res = None
        t_pre = None
        try:
            with open(os.path.join(self.path, Store.DIFF_LOG_FN), "r") as f:
                for line in f:
                    line = json.loads(line)
                    if line[0] == str(timestamp):
                        res = line[1]
                        break
                    t_pre = line[0]
        except IOError as e:
            if e.errno != errno.ENOENT:
                raise
        if res is None:
            res = False
        else:
            res = HighLevelDB_Diff(from_dbv=t_pre, to_dbv=line[0], raw_diff=res)
        if timestamp == self.timestamp:
            self._high_level_diff = res
        return res

    @with_read_lock
    def high_level_diffs(self, old_timestamp=None, new_timestamp=None,
                         force_high_level=False):
        low_level_diff = self.calc_db_diff(old_timestamp, new_timestamp,
                                           allow_empty_diff=True)
        # pre-process timestamp args
        if low_level_diff.is_empty():
            return []
        if new_timestamp is None:
            new_timestamp = datetime.utcnow()
        if not isinstance(new_timestamp, datetime):
            new_timestamp = dateutil.parser.parse(new_timestamp)
        if old_timestamp is None:
            old_timestamp = self.all_available_timestamps()[0]
        if not isinstance(old_timestamp, datetime):
            old_timestamp = dateutil.parser.parse(old_timestamp)

        # load high level diffs from diff log file
        found = False
        diffs = []
        new_diff = None
        new_t = None
        for line in readlines_reverse(os.path.join(self.path,
                                                   Store.DIFF_LOG_FN)):
            if not line or not line.strip():  # e.g. empty lines
                continue
            t, diff = json.loads(line)
            t = dateutil.parser.parse(t)
            if new_t is not None:
                if new_t <= old_timestamp:
                    found = True
                    break
                if new_t <= new_timestamp:
                    diffs = [HighLevelDB_Diff(from_dbv=t, to_dbv=new_t,
                                              raw_diff=new_diff)]+diffs
            new_t = t
            new_diff = diff

        # collapse adjacent diff pairs if possible
        i = 0
        while i + 1 < len(diffs):
            if self.calc_db_diff(diffs[i]['from'],
                                 diffs[i + 1]['to'],
                                 allow_empty_diff=True).is_empty():
                diffs = diffs[:i] + diffs[i+2:]
                i = max([0, i - 1])
            else:
                i += 1

        if not found:
            log.warning('Fall back to low level because no high level diff '
                        'found')
            return [low_level_diff]
        if force_high_level:
            return diffs
        high_level_action_count = sum((d.action_count() for d in diffs))
        low_level_action_count = low_level_diff.action_count()
        if high_level_action_count > low_level_action_count:
            log.debug("Fall back to low level diff because it's shorter")
            return [low_level_diff]
        return diffs

    @with_read_lock
    def full_db_as_diff(self, timestamp):
        if timestamp is not None:
            s = Store(path=self.path, timestamp=timestamp)
        else:
            s = self
        diff = DB_Diff(to_dbv=s.timestamp)
        diff.add_cats(s.get_all_categories())
        diff.add_elems(s.get_all_elements())
        diff.add_links(s.get_all_links())
        return diff

    @with_read_lock
    def recently_changed(self, tdelta, ignore_if_all=True):
        timestamp = self.find_timestamp_ceil(datetime.utcnow()-tdelta,
                                             offset=-1, floor_last=False)
        diff = self.calc_db_diff(old_timestamp=timestamp or None)
        res = []
        cats = self.get_all_categories()
        elems = self.get_all_elements()
        for link in (diff['insertions']['links'] +
                     diff['deletions']['links']):
            if link[0] in elems and link[0] not in res:
                res.append(link[0])
            if link[1] in elems and link[1] not in res:
                res.append(link[1])
        for elem in diff['insertions']['elements']:
            if elem in elems and elem not in res:
                res.append(elem)
        for cat in diff['insertions']['categories']:
            if cat in cats and cat not in res:
                res.append(cat)
        if len(res) == len(elems) + len(cats) and ignore_if_all:
            return tuple()
        return tuple(res)

    def register_listener(self, listener):
        if self._read_only:
            raise ReadOnlyStoreError
        log.debug('%s %s Register listener #%i: %s' %
                  (os.getpid(), threading.current_thread(),
                   len(self.listeners)+1, listener))
        with self.listener_lock:
            if listener not in self.listeners:
                self.listeners.append(listener)

    def unregister_listener(self, listener):
        log.debug('%s %s Unregister listener %s' % (os.getpid(),
                                                    threading.current_thread(),
                                                    listener))
        with self.listener_lock:
            if listener in self.listeners:
                self.listeners.remove(listener)

    def unregister_all_listeners(self):
        log.debug('%s %s Unregister all %i listener(s)' %
                  (os.getpid(), threading.current_thread(),
                   len(self.listeners)))
        with self.listener_lock:
            self.listeners = []

    def similarities(self):
        vertices = self.get_all_elements()
        links = self.get_all_links()
        idx_of_v = {}
        for i, v in enumerate(vertices):
            idx_of_v[tuple(v)] = i

        a = np.zeros((len(vertices), len(vertices)), dtype=np.float)
        for v_s, v_t in links:
            a[idx_of_v[tuple(v_s)], idx_of_v[tuple(v_t)]] = 1
            a[idx_of_v[tuple(v_t)], idx_of_v[tuple(v_s)]] = 1

        sim = a.dot(a)
        out_degrees = a.sum(axis=1)
        od1 = out_degrees.repeat(out_degrees.size).reshape((out_degrees.size,
                                                            out_degrees.size))
        od2 = od1.transpose()
        quotient = (od1+od2)*.5
        quotient[quotient == 0] = 1
        sim /= quotient

        sim = np.tril(sim, -1)  # set all elements on and above diagonal to 0

        res = []
        for i, j in zip(*sim.nonzero()):
            v1 = vertices[i]
            v2 = vertices[j]
            res.append([v1, v2, float(sim[i, j])])

        return res

    IMPORT_HEADER = ['Category1', 'Element1', 'Category2', 'Element2']

    def _df_to_diff(self, df):
        if df.shape[0] and df.head(1).values.tolist()[0] == Store.IMPORT_HEADER:
                df = df.iloc[1:]
        df = df.replace('', float('nan'))
        cat_cols = Store.IMPORT_HEADER[::2]
        df[cat_cols] = df[cat_cols].fillna(method='ffill')
        df = df.fillna('')
        if not df.shape[0]:
            return fill_diff({}, high_level=False)
        first_entry = df.iloc[0][0]
        if (isinstance(first_entry, float) and math.isnan(first_entry) or
                isinstance(first_entry, str) and not first_entry):
            raise ValueError('First row must have first category set')
        if not df.iloc[0][0]:
            raise ValueError('First row must have first category set')
        todo = fill_diff({}, high_level=False)
        to_add_cats = set()
        to_add_elems = set()
        for row in df.values:
            assert row[0]
            if row[1]:
                if row[3]:
                    if not row[2]:  # c1, e1, !c2, e2
                        raise ValueError('No category specified for %s' %
                                         row[3])
                    # c1, e1, c2, e2
                    todo['insertions']['links'].append(
                        [[row[0], row[1]], [row[2], row[3]]])
                else:
                    if row[2]:  # c1, e1, c2, !e2
                        to_add_cats.add(row[2])
                    # c1, e1, ?, !e2
                    to_add_elems.add((row[0], row[1]))
            else:
                if row[3]:
                    if not row[2]:   # c1, !e1, !c2, e2
                        raise ValueError('No category specified for %s' %
                                         row[3])
                    # c1, !e1, c2, e2
                    to_add_elems.add((row[2], row[3]))
                else:
                    if row[2]:  # c1, e1, c2, !e2
                        to_add_cats.add(row[2])
                    # c1, !e1, ?, !e2
                    to_add_cats.add(row[0])
        todo['insertions']['categories'] = list(to_add_cats)
        todo['insertions']['elements'] = list(map(list, to_add_elems))

        return todo

    def _xls_to_diff(self, fd):
        try:
            df = pd.read_excel(fd, names=Store.IMPORT_HEADER,
                               keep_default_na=False, dtype=str)
        except XLRDError as e:
            raise ValueError(e.message)
        return self._df_to_diff(df)

    def _csv_to_diff(self, fd):
        df = pd.read_csv(fd, names=Store.IMPORT_HEADER,
                         keep_default_na=False, dtype=str)
        return self._df_to_diff(df)

    def import_(self, fn, fd, format_=None, dbv=None):
        log.info('File upload %s, format: %s', fn, format_)
        if format_ not in (None, 'csv', 'excel'):
            raise ValueError('Can only import CSV and Excel files.')
        if format_ is not None:
            format_tries = [format_]
        else:
            format_tries = ['excel', 'csv']
        diff_todo = None
        last_err = None
        for format_trie in format_tries:
            if format_trie == 'excel':
                converter = self._xls_to_diff
            else:  # csv
                converter = self._csv_to_diff
            try:
                diff_todo = converter(fd)
                log.debug('Parsed as %s', format_trie)
            except ValueError as e:
                last_err = e
        if diff_todo is None:
            raise ValueError(str(last_err))
        res = self.diff_action(diff_todo, dbv=dbv)
        log.info('Import done')
        return res

    def _db_to_df(self, encode=True):
        elements = set()
        categories = set()
        out = []
        for link in self.get_all_links():
            out.append(sum(link, []))  # ugly way to concat catelems
            for catelem in map(tuple, link):
                elements.add(catelem)
                categories.add(catelem[0])
        for catelem in map(tuple, self.get_all_elements()):
            if catelem not in elements:
                out.append(list(catelem) + ['', ''])
                elements.add(catelem)
                categories.add(catelem[0])
        for cat in self.get_all_categories():
            if cat not in categories:
                out.append([cat] + ['']*3)
                categories.add(cat)
        if encode:
            if not isinstance(encode, str):
                encode = 'utf-8'
            out = [list(map(lambda s: s.encode(encode), row)) for row in out]
        return pd.DataFrame(out, columns=Store.IMPORT_HEADER, dtype=str)

    def _db_to_excel(self, out_buf):
        df = self._db_to_df(encode=False)
        writer = pd.ExcelWriter(out_buf, engine='xlsxwriter')
        df.to_excel(writer, index=False, sheet_name='Sheet1')  # send df to writer
        worksheet = writer.sheets['Sheet1']  # pull worksheet object
        for idx, col in enumerate(df):  # loop through all columns
            series = df[col]
            max_entry_len = series.map(len).max() if len(series) else 0
            max_len = min(max((
                max_entry_len,  # len of largest item
                len(str(series.name))  # len of column name/header
            )) + 1, 35)  # adding a little extra space
            worksheet.set_column(idx, idx, max_len)  # set column width
        writer.save()

        # self._db_to_df().to_excel(out_buf, index=False)

    def _db_to_csv(self, out_buf):
        self._db_to_df().to_csv(out_buf, index=False)

    def export(self, dbv=None, format_='csv'):
        if dbv is not None and self.timestamp != self.find_timestamp(dbv):
            store = Store(path=self.path, timestamp=dbv)
            return store.export(format_=format_), store
        if format_ not in ('csv', 'excel'):
            raise ValueError('Can only export to CSV and Excel.')
        if format_ == 'excel':
            converter = self._db_to_excel
        else:
            converter = self._db_to_csv
        buf = io.StringIO()
        converter(buf)
        buf.seek(0)
        return buf, self
