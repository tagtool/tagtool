# -*- coding: utf-8 -*-
from logging import getLogger
import os
from os.path import dirname, isfile, join
import errno
import shutil
from distutils.dir_util import mkpath
from distutils.dir_util import remove_tree
from datetime import datetime
import subprocess
import urllib.parse as urlparse
from urllib.parse import urlencode

from jinja2 import Environment, FileSystemLoader
from csscompressor import compress

log = getLogger(__name__)

cur_dir = dirname(__file__)
templates_dir = join(cur_dir, 'templates')
static_dir = join(cur_dir, 'static')
touch_subdir = 'touch'
private_subdir = 'private'
env = Environment(loader=FileSystemLoader(templates_dir))


def _static_getter(version=None, css_build=False, js_build=False):
    def get(url, tag=None, url_attr=None, close=None, **attr):
        is_absolute_url = bool(urlparse.urlparse(url).netloc)
        if url.startswith('/css/'):
            if css_build:
                url = '/css/build/' + url[5:]
            url_attr = url_attr or 'href'
            tag = tag or 'link'
            close = False if close is None else close
            attr.setdefault('rel', 'stylesheet')
        elif url.startswith('/js/'):
            if js_build:
                url = '/js/build/' + url[4:]
            url_attr = url_attr or 'src'
            tag = tag or 'script'
            close = True if close is None else close
        else:
            assert tag is not None
            if tag == 'img':
                url_attr = url_attr or 'src'
                attr.setdefault('alt', '')
            else:
                url_attr = url_attr or 'href'
            close = False
        if version is not None and not is_absolute_url:
            url_parts = list(urlparse.urlparse(url))
            query = urlparse.parse_qsl(url_parts[4])
            query.append(('v', version))
            url_parts[4] = urlencode(query)
            url = urlparse.urlunparse(url_parts)
        attr[url_attr] = url
        attr_strs = []
        for key in attr:
            if attr[key] is not None:
                assert '"' not in key and '<' not in key and '>' not in key
                assert '"' not in attr[key]
                attr_strs.append('{}="{}"'.format(key, attr[key]))
        assert '<' not in tag and '>' not in tag
        if close:
            tag_str = "<{} {}>".format(tag, " ".join(attr_strs))
            return tag_str + "</{}>".format(tag)
        else:
            return "<{} {}/>".format(tag, " ".join(attr_strs))

    def wrap(*args, **kwargs):
        log.debug('version=%s, css_build=%s, js_build=%s',
                  version, css_build, js_build)
        log.debug('static get(%s, %s)', args, kwargs)
        ret = get(*args, **kwargs)
        log.debug(' ==> %s', ret)
        return ret

    return wrap


def _cloud_classes(conf):
    cols = conf['CLOUD_COLUMN_COUNT'] or 2
    assert cols in (1, 2, 3, 4, 6, 12)
    if cols == 1:
        return 'col-xs-12'
    if cols == 2:
        return 'col-xs-12 col-md-6'
    if cols == 3:
        return 'col-xs-12 col-sm-6 col-lg-4'
    if cols == 4:
        return 'col-xs-12 col-sm-6 col-md-4 col-lg-3'
    if cols == 6:
        return 'col-xs-12 col-sm-4 col-md-3 col-lg-2'
    if cols == 12:
        return 'col-xs-6 col-sm-3 col-md-2 col-lg-1'


def optimize_js():
    try:
        subprocess.check_output(["r.js", "-o", "static/build.js"])
    except subprocess.CalledProcessError as e:
        log.error('r.js failed: %s', e.output)
        raise


# https://gist.github.com/stucka/48179a6defbdc53690254f15a9639cbc
def optimize_css(in_dir='static/css', out_dir='static/css/build'):
    try:
        remove_tree(out_dir)
    except OSError as e:
        if e.errno == errno.ENOENT:
            pass
        else:
            raise
    file_paths = []  # List which will store all of the full filepaths.
    # Walk the tree.
    for root, directories, files in os.walk(in_dir):
        for filename in files:
            # Join the two strings in order to form the full filepath.
            file_path = os.path.join(root, filename)
            file_paths.append(file_path)  # Add it to the list.
    # Now we have a list of all CSS files in directory.
    for source in file_paths:
        # if we have a CSS file and it's not already minimized ...
        if source[-4:] == ".css" and source[-8:-4] != "-min":
            with open(source, "r") as sourcefilehandle:
                rawcss = sourcefilehandle.read()
            rel_source = os.path.relpath(source, in_dir)
            target = os.path.join(out_dir, rel_source)
            mkpath(os.path.split(target)[0])
            with open(target, "w") as targetfilehandle:
                targetfilehandle.write(compress(rawcss))
            log.debug("%s -min-> %s", source, target)
        else:
            rel_source = os.path.relpath(source, in_dir)
            target = os.path.join(out_dir, rel_source)
            mkpath(os.path.split(target)[0])
            shutil.copy(source, target)
            log.debug("%s -cpy-> %s", source, target)


def compile_assets():
    log.info('Optimizing css')
    optimize_css()
    log.info('Optimizing css done')
    log.info('Optimizing javascript')
    optimize_js()
    log.info('Optimizing javascript done')


def render(in_path, out_path, version, year, touch_support, conf):
    log.info("Rendering %s" % in_path)
    template = env.get_template(in_path)
    output_from_parsed_template = template.render(
        version=version,
        year=year,
        branding_logo_url=conf['BRANDING_LOGO_URL'],
        branding_name=conf['BRANDING_NAME'],
        demo_hand=conf['DEMO_HAND'],
        touch_support=touch_support,
        touch_apps=conf['TOUCH_APPS'],
        cloud_classes=_cloud_classes(conf),
        password_hint=conf['PASSWORD_HINT'],
        debug=conf['DEBUG']
    )
    # save the result
    with open(out_path, "wb") as f:
        f.write(output_from_parsed_template.encode('utf8'))


def render_templates(conf):
    now = datetime.utcnow()
    if conf['ASSET_VERSIONING'] is True:
        version = now.strftime("%s")
    elif conf is not None:
        version = str(conf['ASSET_VERSIONING'])
    else:
        version = None
    year = now.strftime("%Y")
    if not conf['DEBUG']:
        env.globals.update(static_tag=_static_getter(version,
                                                     css_build=True,
                                                     js_build=True))
    else:
        env.globals.update(static_tag=_static_getter(version))
    for fn in os.listdir(templates_dir):
        if isfile(join(templates_dir, fn)):
            if fn.endswith('.no_touch.html'):
                options = [False]
                ofn = ''.join(fn.rsplit('.no_touch', 1))
            else:
                options = [True, False]
                ofn = fn
            for touch in options:
                fps = [static_dir] + ([touch_subdir] if touch else []) + [ofn]
                opath = join(*fps)
                render(fn, opath, version, year, touch, conf)
    private_template_path = join(templates_dir, private_subdir)
    for fn in os.listdir(private_template_path):
        if isfile(join(private_template_path, fn)):
            out_path = join(static_dir, private_subdir, fn)
            render(join(private_subdir, fn), out_path, version, year, False,
                   conf)


def render_all(conf):
    compile_assets()
    render_templates(conf)


def _main():
    from config_loader import load_config
    conf, _ = load_config()
    render_all(conf)


if __name__ == '__main__':
    _main()
