#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
import sys
import shutil
import errno
import signal
import logging
import logging.handlers
import re
import json
from urllib.parse import unquote as url_unquote
from urllib.parse import urlparse
from urllib.parse import quote as url_quote

import datetime
import dateutil
import dateutil.parser
from functools import wraps

from werkzeug.serving import is_running_from_reloader

from flask import Flask
from flask import request
from flask import abort
from flask import redirect
from flask import make_response
from flask.helpers import send_from_directory
from flask.helpers import send_file
from flask_restful import Resource
from flask_restful import Api
from flask_socketio import SocketIO
from flask_socketio import emit

from models.store import Store
from models.store import OldDBVersionError
from models.settings import Settings as SettingsModel
from models.settings import InvalidSettingsError

from bots.webhook_bot import WebhookBot
try:
    from bots.mattermost_user_bot import MattermostUserBot
    user_bot_available = True
except ImportError:
    # pyquery not installed
    user_bot_available = False

import auth_util
from util import api_route
from util import nocache


# load config files and merge with default settings-----------------------------
import argparse
import os

from config_loader import load_config


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='TagTool server.')
    parser.add_argument(
        "--config", "-c",
        help="Path of python config file (default: settings.py)",
        type=str,
        action="store",
        default=None
    )
    parser.add_argument('--precompile', '-p', dest='precompile',
                        metavar='assets', type=str, nargs='?', default=False,
                        help='If present assets will be precompiled. '
                             'Program exits afterwards. You may specify one or '
                             'more of "t": templates, "j": js files, "c": css. '
                             'Example: -p=tc for templates and css.')
    parser.add_argument('--no-compile', '-n', dest='compile',
                        action='store_const', const=False, default=True,
                        help='If present assets will not be compiled on '
                             'runtime. Make sure you have called '
                             '`--precompile` before')

    args = parser.parse_args()
    config_path = args.config
else:
    args = None
    config_path = 'settings.py'

conf, settings_found = load_config(config_path)
default_conf, _ = load_config('default_settings.py')

# done with config stuff--------------------------------------------------------

__author__ = 'rouven'

# logging stuff ----------------------------------------------------------------
log_format = "%(asctime)s|%(levelname)7s|%(name)20s:%(lineno)4d|%(message)s"
log_formatter = logging.Formatter(log_format)
logging.basicConfig(level=conf['LOG_LEVEL'], format=log_format)
if (not conf['DEBUG'] and conf['LOG_LEVEL'] != 'DEBUG' and
        (not isinstance(conf['LOG_LEVEL'], (int, float)) or
         conf['LOG_LEVEL'] > 10)):  # this meas we're not debugging
    # info logging of engineio is way too verbose!
    logging.getLogger('engineio').setLevel(logging.WARNING)
rot_handler = logging.handlers.RotatingFileHandler(
    'log/tagtool.log', maxBytes=500*1024, backupCount=10, encoding=None)
rot_handler.formatter = log_formatter
logging.getLogger().addHandler(rot_handler)
logging.info("tags server init")
# done with logging stuff ------------------------------------------------------

# config pre-processing and sanity checks
if not conf['DATA_DIR'].startswith('/'):
    conf['DATA_DIR'] = os.path.join(os.path.dirname(__file__), conf['DATA_DIR'])
if conf['SECRET_KEY'] == default_conf['SECRET_KEY']:
    logging.warn("For security it's necessary to change `SECRET_KEY` in your "
                 "settings file.")
if conf['ALLOW_READ_ONLY'] and conf['PASSWORD'] is None:
    logging.warn("Settings enable read-only mode but no password set. "
                 "There might be a mistake in you settings. `ALLOW_READ_ONLY` "
                 "will be ignored unless PASSWORD is specified.")
# sanity checks done -----------------------------------------------------------

MAX_RETRY_ON_OLD_DB_VERSION = 5
AUTH_FREE_PAGES = ('/login', r'/login\.html', r'/api/auth_status',
                   '/(img|js|css)/.*',
                   r'.*\.(js|css|jpg|png|ico|woff|woff2|ttf)',
                   r'/api/chat/.*')  # RegExps

app = Flask(__name__)

app.config['SECRET_KEY'] = conf['SECRET_KEY']
socket_io = SocketIO(app)


# E-Mail Logger ----------------------------------------------------------------
if conf['USE_MAIL_LOGGER']:
    class TitledSMTPHandler(logging.handlers.SMTPHandler):
        def __init__(self, app_, *args, **kwargs):
            self.app = app_
            logging.handlers.SMTPHandler.__init__(self, *args, **kwargs)

        def getSubject(self, record):
            formatter = logging.Formatter(fmt=self.subject)
            return formatter.format(record)

        def emit(self, *args, **kwargs):
            if not self.app.debug:
                logging.handlers.SMTPHandler.emit(self, *args, **kwargs)


    mail_handler = TitledSMTPHandler(
        app, conf['MAIL_LOGGING_SMTP_SERVER'],
        conf['MAIL_LOGGING_FROM_ADDRESS'], conf['MAIL_LOGGING_TO_ADDRESS'],
        '[TagTool][log][%(asctime)s] Error occurred')
    mail_handler.setLevel(logging.ERROR)
    logging.getLogger().addHandler(mail_handler)
    app.logger.addHandler(mail_handler)
    logging.getLogger('werkzeug').addHandler(mail_handler)
# ------------------------------------------------------------------------------
if not settings_found:
    logging.warning("Couldn't find settings file '%s'. Used fallback settings "
                    "which might not suit your needs and is a SECURITY "
                    "ISSUE. Please refer to the README.md file for more "
                    "information. Args: %s", config_path, args)

api = Api(app)

db = None


def open_store():
    global db, settings
    db = Store(conf['DATA_DIR'])
    db.diff_callback = ws_diff_update
    settings = SettingsModel(path=conf['DATA_DIR'], store=db)


def register_webhook_bot():
    return WebhookBot(app, db, settings, conf['DIGEST_MESSAGES_OVER_MINUTES'],
                      conf['URL'], conf['WEBHOOK_PAIRS'],
                      conf['WEBHOOK_BOT_NAME'] or conf['BRANDING_NAME'],
                      conf['WEBHOOK_ICON_URL'],
                      conf['PASSWORD'], conf['SECRET_KEY'],
                      conf['BOT_QUERY_TEMPLATES'])


def register_mattermost_user_bot():
    return MattermostUserBot(db, settings, conf['URL'], conf['MM_CHAT_URL'],
                             conf['MM_TEAM_NAME'], conf['MM_USE_GITLAB_AUTH'],
                             conf['MM_USE_GITLAB_LDAP'], conf['MM_GITLAB_URL'],
                             conf['MM_LOGIN_NAME'], conf['MM_LOGIN_PWD'],
                             conf['PASSWORD'], conf['SECRET_KEY'],
                             conf['BOT_QUERY_TEMPLATES'],
                             conf['MM_IGNORED_CHANS'])


bots = []


def start_bots():
    if conf['USE_WEBHOOK_BOT']:
        logging.debug('Starting up webhook bot')
        bots.append(register_webhook_bot())
        logging.info('Started webhook bot')

    if conf['USE_MATTERMOST_USERBOT']:
        if not user_bot_available:
            logging.error('If you want to use the mattermost user bot '
                          'please make sure that pyquery is installed.')
        logging.debug('Starting up Mattermost bot')
        bots.append(register_mattermost_user_bot())
        logging.info('Started Mattermost bot (using user account)')


##############
# auth stuff #
##############

@app.route('/login.html', methods=['POST'])
def login():
    pwd = request.form.get('password', None)
    redir = request.args.get('redir', '/')
    real_pwd = conf['PASSWORD']
    salt = conf['SECRET_KEY']
    response = app.make_response(redirect(redir))
    if real_pwd is None:
        return response
    if pwd == real_pwd:
        return auth_util.set_auth_cookie(response, real_pwd, salt)
    else:
        response = send_file('static/private/login_failed.html')
        response.status_code = 403
        return response


@app.route('/logout', methods=['GET'])
def logout():
    redir = request.args.get('redir', '/')
    response = app.make_response(redirect(redir))
    return auth_util.remove_auth_cookie(response)


logging.debug('Auth free pages: '
              '^(?:'+'|'.join(map(lambda x: '(?:'+x+')', AUTH_FREE_PAGES))+')$')
NON_AUTH_RE = re.compile(
    '^(?:'+'|'.join(map(lambda x: '(?:'+x+')', AUTH_FREE_PAGES))+')$')


def deny_request():
    if request.path.startswith('/api/'):
        # api call
        abort(403,
              'please send the correct password with the auth parameter.')
    else:
        return redirect('/login.html?redir=' +
                        url_quote(request.path, safe=''))


def before_request_auth():
    if (not NON_AUTH_RE.match(request.path) and  # routes no auth required
            # check auth
            not auth_util.auth_request(conf['PASSWORD'], conf['SECRET_KEY']) and
            # read only permissions
            not (request.method == 'GET' and conf['ALLOW_READ_ONLY'])):
        # auth needed but missing or invalid
        return deny_request()


def after_request_auth(response):
    # don't set cookie for api calls (auth param)
    pwd = conf['PASSWORD']
    salt = conf['SECRET_KEY']
    if (auth_util.auth_request(pwd, salt) and pwd is not None and
            not auth_util.auth_cookie_set(response)):
        auth_util.set_auth_cookie(response, pwd, salt)
    return response


def auth_required(view):
    @wraps(view)
    def inner(*_args, **kwargs):
        if not auth_util.auth_request(conf['PASSWORD'], conf['SECRET_KEY']):
            return deny_request()
        return view(*_args, **kwargs)
    logging.debug('Forcing auth on %s', view)
    return inner


app.before_request(before_request_auth)
app.after_request(after_request_auth)


##############
# Util funcs #
##############

def url_unquote_args(func):
    def try_unquote(s):
        if isinstance(s, str):
            return url_unquote(s)
        else:
            return s

    @wraps(func)
    def unquoter(*args, **kwargs):
        return func(*[try_unquote(a) for a in args],
                    **{k: try_unquote(v) for k, v in kwargs.items()})
    return unquoter


def get_dbv():
    dbv = request.values.get("db_version") or request.headers.get("ETag")
    if dbv == 'null':
        dbv = None
    if dbv is not None:
        try:
            dateutil.parser.parse(dbv)
        except ValueError:
            abort(400, "DB version must be a valid timestamp")
    return dbv


def jsonify(obj, *args, **kwargs):
    indent = (conf['JSON_INDENT_DEBUG_MODE'] if app.debug
              else conf['JSON_INDENT_DEPLOY_MODE'])
    kwargs.update({'indent': indent})
    return json.dumps(obj, *args, **kwargs)


def get_prepared_strict_param():
    return request.form.get('strict', default='').lower() == 'true'


#############
# Resources #
#############

class APIResource(Resource):
    method_decorators = [api_route(jsonify=jsonify)]


class Category(APIResource):
    @url_unquote_args
    def get(self, name=None):
        if name is not None:
            if name in db.get_all_categories():
                return db.get_all_category_members(name)
            else:
                abort(404)
        else:
            return db.get_all_categories_with_elements_and_weights()

    @url_unquote_args
    def post(self, name=None):
        if name is None:
            abort(405)
        dbv = get_dbv()
        strict = get_prepared_strict_param()
        try:
            db.add_category(name, dbv=dbv, strict=strict)
        except OldDBVersionError:
            abort(409, "Old data version. You could try to reload.")
        except ValueError as e:
            abort(400, str(e))

    @url_unquote_args
    def delete(self, name=None):
        if name is None:
            abort(405)
        dbv = get_dbv()
        strict = get_prepared_strict_param()
        try:
            db.remove_category(name, dbv=dbv, strict=strict)
        except OldDBVersionError:
            abort(409, "Old data version. You could try to reload.")
        except ValueError as e:
            abort(404, str(e))


class Member(APIResource):
    @url_unquote_args
    def get(self, cat, elem):
        if cat in db.get_all_categories():
            if elem in db.get_category(cat):
                return db.get_all_links([cat, elem])
        abort(404)

    @url_unquote_args
    def post(self, cat, elem):
        dbv = get_dbv()
        strict = get_prepared_strict_param()
        try:
            db.add_to_category(cat, elem, dbv=dbv, strict=strict)
        except OldDBVersionError:
            abort(409, "Old data version. You could try to reload.")
        except ValueError as e:
            abort(400, str(e))

    @url_unquote_args
    def delete(self, cat, elem):
        dbv = get_dbv()
        strict = get_prepared_strict_param()
        try:
            db.remove_from_category(cat, elem, dbv=dbv, strict=strict)
        except OldDBVersionError:
            abort(409, "Old data version. You could try to reload.")
        except ValueError as e:
            abort(404, str(e))


class Link(APIResource):
    @staticmethod
    def _check_arguments(method, e1_cat, e1_elem, e2_cat, e2_elem):
        arglist = [e1_cat, e1_elem, e2_cat, e2_elem]
        e1 = arglist[:2]
        e2 = arglist[2:]

        def valid_get():
            return arglist == [None]*4 or None not in e1 and e2 == [None]*2

        def valid_post():
            return None not in arglist

        def valid_delete():
            return valid_post()

        valid_methods = []
        if valid_get():
            valid_methods.append("GET")
        if valid_post():
            valid_methods.append("POST")
        if valid_delete():
            valid_methods.append("DELETE")

        if method.upper() in valid_methods:
            return
        elif valid_methods:
            abort(405, valid_methods=valid_methods)
        else:
            abort(405,
                  "Valid methods are:\n"
                  "POST   /api/link/between/<string:e1_cat>/<string:e1_elem>/"
                  "and/<string:e2_cat>/<string:e2_elem>\n"
                  "DELETE /api/link/between/<string:e1_cat>/<string:e1_elem>/"
                  "and/<string:e2_cat>/<string:e2_elem>\n"
                  "GET    /api/links/of/<string:e1_cat>/<string:e1_elem>\n"
                  "GET    /api/links")

    @url_unquote_args
    def get(self, e1_cat=None, e1_elem=None, e2_cat=None, e2_elem=None):
        self._check_arguments("get", e1_cat, e1_elem, e2_cat, e2_elem)
        if e1_cat is None:
            return db.get_all_links()
        else:
            if [e1_cat, e1_elem] in db.get_all_elements():
                try:
                    return db.get_all_links([e1_cat, e1_elem])
                except ValueError:
                    abort(400)
            else:
                abort(404)

    @url_unquote_args
    def post(self, e1_cat=None, e1_elem=None, e2_cat=None, e2_elem=None):
        self._check_arguments("post", e1_cat, e1_elem, e2_cat, e2_elem)
        dbv = get_dbv()
        strict = get_prepared_strict_param()
        e1 = [e1_cat, e1_elem]
        e2 = [e2_cat, e2_elem]
        try:
            db.add_link(e1, e2, dbv=dbv, strict=strict)
        except ValueError as e:
            abort(400, str(e))
        except OldDBVersionError:
            abort(409, "Old data version. You could try to reload.")

    @url_unquote_args
    def delete(self, e1_cat=None, e1_elem=None, e2_cat=None, e2_elem=None):
        self._check_arguments("delete", e1_cat, e1_elem, e2_cat, e2_elem)
        dbv = get_dbv()
        strict = get_prepared_strict_param()
        e1 = [e1_cat, e1_elem]
        e2 = [e2_cat, e2_elem]
        try:
            db.remove_link(e1, e2, dbv=dbv, strict=strict)
        except ValueError as e:
            abort(404, str(e))
        except OldDBVersionError:
            abort(409, "Old data version. You could try to reload.")


class Settings(APIResource):
    @api_route(jsonify=jsonify)
    def get(self):
        return settings.get()

    @api_route(jsonify=jsonify)
    def put(self):
        if not request.headers.get('Content-Type', None) == 'application/json':
            abort(400, "Only accepts application/json")
        try:
            settings.update(request.json)
        except InvalidSettingsError as e:
            abort(400, "Invalid Settings", str(e))


@app.route('/api/rename/<string:old_name>/to/<string:new_name>',
           methods=['PUT'])
@url_unquote_args
@api_route(jsonify=jsonify)
def rename_category(old_name, new_name):
    dbv = get_dbv()
    if old_name not in db.get_all_categories():
        abort(404, "Category '%s' not found." % old_name)
    if new_name in db.get_all_categories():
        abort(400, "Category '%s' already exists." % new_name)
    try:
        db.rename_category(old_name, new_name, dbv=dbv)
    except OldDBVersionError:
        abort(409, "Old data version. You could try to reload.")
    settings.clone_category(old_name, new_name)


@app.route('/api/rename/<string:cat>/<string:old_name>/to/'
           '<string:new_name>',
           methods=['PUT'])
@url_unquote_args
@api_route(jsonify=jsonify)
def rename_element(cat, old_name, new_name):
    dbv = get_dbv()
    if cat not in db.get_all_categories():
        abort(404, "Category '%s' doesn't exist." % cat)
    if old_name not in db.get_category(cat):
        abort(404, "Element '%s' doesn't exist in category '%s'."
              % (old_name, cat))
    if new_name in db.get_category(cat):
        abort(400, "Element '%s' already exist in category '%s'."
              % (new_name, cat))
    try:
        db.rename_element(cat, old_name, cat, new_name, dbv=dbv)
    except OldDBVersionError:
        abort(409, "Old data version. You could try to reload.")


@app.route('/api/rename/<string:old_cat>/<string:old_name>/to/'
           '<string:new_cat>/<string:new_name>',
           methods=['PUT'])
@url_unquote_args
@api_route(jsonify=jsonify)
def rename_element_over_cat(old_cat, old_name, new_cat, new_name):
    dbv = get_dbv()
    if old_cat not in db.get_all_categories():
        abort(404, "Category '%s' doesn't exist." % old_cat)
    if old_name not in db.get_category(old_cat):
        abort(404, "Element '%s' doesn't exist in category '%s'."
              % (old_name, old_cat))
    if new_cat not in db.get_all_categories():
        abort(404, "Category '%s' doesn't exist." % new_cat)
    if new_name in db.get_category(new_cat):
        abort(400, "Element '%s' already exist in category '%s'."
              % (new_name, new_cat))
    try:
        db.rename_element(old_cat, old_name, new_cat, new_name, dbv=dbv)
    except OldDBVersionError:
        abort(409, "Old data version. You could try to reload.")


@app.route('/api/bulkaction', methods=['POST'])
@api_route(jsonify=jsonify)
def bulk_action():
    dbv = get_dbv()
    strict = get_prepared_strict_param()
    diff = request.form.get("diff")
    try:
        db.diff_action(diff, dbv, strict=strict)
    except ValueError as e:
        abort(400, "Invalid argument(s): %s" % e)
    except OldDBVersionError:
        abort(409, "Old data version. You could try to reload.")


@app.route('/api/io', methods=['GET', 'POST'])
@nocache
def io():
    dbv = get_dbv()
    format_ = request.values.get('format', None)
    if request.method == 'GET':
        format_ = format_ or 'csv'
        try:
            buf, store = db.export(dbv=dbv, format_=format_)
        except ValueError as e:
            logging.debug('Export error: %s', e, exc_info=True)
            return make_response(("Couldn't export: %s" % e, 400,
                                  {'content-type': 'text/plain'}))
        if format_ == 'csv':
            mimetype = 'text/csv'
            fn_ext = 'csv'
        else:
            mimetype = 'application/vnd.openxmlformats-officedocument.' \
                       'spreadsheetml.sheet'
            fn_ext = 'xlsx'
        fn = '%s-%s.%s' % (conf['BRANDING_NAME'], store.timestamp, fn_ext)
        return send_file(buf, mimetype=mimetype, as_attachment=True,
                         attachment_filename=fn)
    else:
        file_ = request.files.get('file', None)
        if file_ and file_.filename:
            try:
                db.import_(file_.filename, file_, dbv=dbv, format_=format_)
            except ValueError as e:
                return make_response(('Invalid file: %s' % str(e), 400,
                                      {'content-type': 'text/plain'}))
            return '', 204
        else:
            return make_response(('Missing file.', 400,
                                  {'content-type': 'text/plain'}))


@app.route('/api/diff/query', methods=['GET'])
@api_route(jsonify=jsonify)
def query():
    dbv = get_dbv()
    start = request.args.get("start")
    end = request.args.get("end")
    if dbv is not None and start is not None and end is not None:
        abort(400, "You can't pass data base version, start and end parameter.")
    if dbv is None:
        s = start if start is not None else None
        e = end if end is not None else None
    else:
        if end is not None:
            s, e = dbv, end
        else:
            s, e = start, dbv
    return db.calc_db_diff(s, e)


@app.route('/api/all_timestamps', methods=['GET'])
@api_route(jsonify=jsonify)
def all_timestamps():
    return list(map(str, db.all_available_timestamps()))


@app.route('/api/similarities', methods=['GET'])
@api_route(jsonify=jsonify)
def similarities():
    return db.similarities()


@app.route('/api/search', methods=['GET'])
@api_route(jsonify=jsonify)
def search():
    query_str = request.args.get('q', '')
    cat = request.args.get('cat', None)
    threshold = request.args.get('fuzzthresh', None, type=float)
    limit = request.args.get('fuzzlimit', None, type=int)
    force_fuzzy = request.args.get("forcefuzzy", 'True')
    force_fuzzy = force_fuzzy.lower() not in ('false', '0', 'no', 'nope')
    try:
        return db.search_element(query_str, cat=cat,
                                 fuzzy_threshold=threshold, fuzzy_limit=limit,
                                 force_fuzzy=force_fuzzy)
    except ValueError as e:
        abort(404, str(e))


@app.route('/api/recently_changed', methods=['GET'])
@api_route(jsonify=jsonify)
def recently_changed():
    seconds = request.args.get("seconds", conf['RECENT_SECONDS'])
    ignore_if_all = request.args.get("ignore_if_all", 'True')
    ignore_if_all = ignore_if_all.lower() not in ('false', '0', 'no', 'nope')
    try:
        seconds = int(seconds)
    except ValueError:
        abort(400, 'seconds must be integer.')
    return db.recently_changed(datetime.timedelta(seconds=seconds),
                               ignore_if_all)


@app.route('/api/token', methods=['GET'])
@api_route(jsonify=jsonify, returns_str=True)
@auth_required
def token():
    if conf['PASSWORD']:
        return auth_util.auth_token(conf['PASSWORD'], conf['SECRET_KEY'])


@app.route('/api/auth_status', methods=['GET'])
@api_route(jsonify=jsonify)
def auth_status():
    password_set = conf['PASSWORD'] is not None
    return {'passwordSet': password_set,
            'readOnly': conf['ALLOW_READ_ONLY'] and password_set,
            'loggedIn': auth_util.auth_request(conf['PASSWORD'],
                                               conf['SECRET_KEY'])}


# Failing routes for test purpose
@app.route('/fail', methods=['GET'])
def fail():
    raise Exception("Server failed (non-api route), as desired.")


@app.route('/api/fail', methods=['GET'])
@api_route(jsonify=jsonify)
def api_fail():
    raise Exception("Server failed (api route), as desired.")


api.add_resource(Category, '/api/category/<string:name>', '/api/categories')
api.add_resource(Member, '/api/category/<string:cat>/<string:elem>')
api.add_resource(Link,
                 '/api/link/between/<string:e1_cat>/<string:e1_elem>/and/'
                 '<string:e2_cat>/<string:e2_elem>',
                 '/api/links/of/<string:e1_cat>/<string:e1_elem>',
                 '/api/links')
api.add_resource(Settings, '/api/settings')


##################
# SocketIO stuff #
##################

ws_clients = 0


@socket_io.on('connect')
def ws_connect():
    global ws_clients
    ws_clients += 1
    dbv = get_dbv()
    with db.LOCK:
        diff = db.calc_db_diff(dbv, allow_empty_diff=False)
        if diff is not None:
            emit('diff', jsonify(diff))
    ws_user_count_update()


@socket_io.on('disconnect')
def ws_disconnect():
    global ws_clients
    if ws_clients:
        ws_clients -= 1
        ws_user_count_update()


def ws_user_count_update():
    logging.debug('Socket event user_count %s', ws_clients)
    socket_io.emit('user_count', jsonify(ws_clients))


def ws_diff_update(diff):
    logging.debug('Socket event diff %s', diff)
    socket_io.emit('diff', jsonify(diff))


def ws_settings_update(_settings):
    logging.debug('Socket event settings %s', _settings)
    socket_io.emit('settings', jsonify(_settings))


def bind_ws_to_db_and_settings():
    settings._cb = ws_settings_update
    db.diff_callback = ws_diff_update


@app.route('/api/user_count', methods=['GET'])
@api_route(jsonify=jsonify)
def user_count():
    return ws_clients


def main():
    import render_templates

    if args is not None and args.precompile is not False:
        if args.precompile is None:
            render_templates.render_all(conf)
        else:
            if 'j' in args.precompile:
                logging.info('Pre-compiling JavaScript...')
                render_templates.optimize_js()
            if 'c' in args.precompile:
                logging.info('Pre-compiling CSS...')
                render_templates.optimize_css()
            if 't' in args.precompile:
                logging.info('Pre-compiling templates...')
                render_templates.render_templates(conf)
        logging.info('Pre-compilation done. Exiting...')
        exit(0)

    # In productive systems this redirect must be done with apache or so.
    @app.route('/')
    def index():
        return redirect('/clouds.html')

    @app.route('/touch/')
    def touch_index():
        return redirect('/touch/clouds.html')

    # Static files should be served directly with apache or similar.
    # This is just for testing
    @app.route('/<path:path>', methods=['GET', 'PUT', 'POST', 'DELETE'])
    def assets(path):
        # This endpoint will be the default for all unmatched requests.
        # Thus, we want it to accept all methods and throw 404s instead of 405s.
        if request.method != 'GET':
            abort(404)  # act as if this endpoint didn't exist ;)
        if path.startswith('/private'):
            abort(404)
        return send_from_directory('static', path)

    # In dev mode render templates for all request.
    # This is a little slow, but way easier to handle.

    if conf['DEBUG']:
        @app.before_request
        def before_req():
            if urlparse(request.path).path.endswith('.html'):
                render_templates.render_templates(conf)
    elif args is None or args.compile:
        @app.before_first_request
        def before_first_req():
            render_templates.render_all(conf)


def run():
    try:
        logging.info("Starting socket io server on %s:%s",
                     conf['HOST'], conf['PORT'])
        socket_io.run(app, host=conf['HOST'], port=conf['PORT'],
                      debug=conf['DEBUG'], use_reloader=False)
    finally:
        logging.info("Cleaning up...")
        for bot in bots:
            bot.stop()
        logging.info("Done cleaning up.")


if __name__ == '__main__':
    # Needed so that server also stops properly when PID 1 (in docker)
    signal.signal(signal.SIGTERM, lambda _, __: sys.exit(0))
    open_store()
    bind_ws_to_db_and_settings()
    start_bots()
    main()
    run()
