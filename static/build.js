({
    baseUrl: 'js',                //where to find all our scripts
    dir: 'js/build',                      //where to output scripts

    //optimize: 'none',
    mainConfigFile: 'js/common.js',
    findNestedDependencies: true,
    removeCombined: true,
    skipDirOptimize: true,

    paths: {
        'bootstrap': 'libs/bootstrap',
        'bootstrap-modal': 'libs/bootstrap-modal-wrapper',
        'bootstrap-slider': 'libs/bootstrap-slider',
        'clock': 'libs/clock',
        'cookies': 'libs/js.cookie',
        'd3': 'libs/d3',
        'd3pie': 'libs/d3pie',
        'domReady': 'libs/domReady',
        'jquery': 'libs/jquery',
        'jquery.keyboard': 'libs/jquery.keyboard',
        'jquery-ui.raw': 'libs/jquery-ui',
        'jquery-ui-memfix': 'my_libs/jquery-ui-memfix',
        'jquery-ui.touch-punch': 'libs/jquery-ui.touch-punch',
        'jquery-ui': 'my_libs/jquery-ui.touch',
        'mathjax_raw': 'libs/mathjax/MathJax?config=TeX-AMS-MML_HTMLorMML',
        'mathjax': 'my_libs/mathjax_wrapper',
        'moment': 'libs/moment',
        'socketio': 'libs/socketio',
        'wordcloud': 'libs/wordcloud2',

        'api': 'my_libs/api',
        'changed_counter': 'my_libs/changed_counter',
        'clock_support': 'my_libs/clock_support',
        'color_generator': 'my_libs/color_generator',
        'custom_forces': 'my_libs/custom_forces',
        'keyboard_support': 'my_libs/keyboard_support',
        'settings': 'my_libs/settings',
        'user_counter': 'my_libs/user_counter'
    },
    shim : {
        "bootstrap" : { "deps": ['jquery'] },
        "bootstrap-switch" : { "deps": ['jquery', 'bootstrap'] },
        "clock": {
            "deps": ["jquery"],
            "exports": "clock"
        },
        // Dirty hack to make sure jqueryUI is loaded first.
        // This forces bootstrap-slider to bind to an alternative name.
        // Dropping this will result in random import order thus random binding.
        "bootstrap-slider": { "deps": ['jquery-ui', 'jquery', 'bootstrap'] },
        "mathjax_raw": { "exports": "MathJax" }
    },

    modules: [  //what should actually be built into bundles
        {
            name: 'common'                //the common bundle
        },
        {
            name: 'clouds',               //the clouds page bundle
            exclude: ['jquery', 'bootstrap', 'api']
        },
        {
            name: 'graphs',               //the graphs page bundle
            exclude: ['jquery', 'bootstrap', 'api']
        },
        {
            name: 'io',                   //the csv im-/export page bundle
            exclude: ['jquery', 'bootstrap', 'api']
        },
        {
            name: 'stats',                //the statistics page bundle
            exclude: ['jquery', 'bootstrap', 'api']
        },
        {
            name: 'global',               //the global bundle
            exclude: ['jquery', 'bootstrap', 'api']
        },
        {
            name: 'api_doc',               //the api docs page bundle
            exclude: ['jquery', 'bootstrap', 'api']
        },
        {
            name: 'touch_support',         //the touch support bundle
            exclude: ['jquery', 'bootstrap', 'api']
        }
    ],
})
