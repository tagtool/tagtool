/**
 * Created by rouven on 04.02.16.
 */

require(['mathjax']);
require(['domReady', 'jquery', 'bootstrap', 'api'], function (domReady, $, bs, api) {
    domReady(function () {
        $(window).scroll(function() {
            var sidebar_bottom = $("#sidebar").position().top + $("#sidebar").outerHeight(true);
            var maxSidebar = undefined;
            var maximizerSidebar = function(i, e) {
                var bottom = e.getBoundingClientRect().bottom;
                if (maxSidebar != null) {
                    if (maxSidebar < bottom) {
                        maxSidebar = bottom
                    }
                } else {
                    maxSidebar = bottom;
                }
            };
            $("#sidebar .nav-stacked:visible").each(maximizerSidebar);
            $("#sidebar li.active").each(maximizerSidebar);
            var winHeight = $(window).height();
            var footerTop = $("footer")[0].getBoundingClientRect().top-10; // -10 is for some margin
            var minBottom = Math.min(winHeight, footerTop);
            var diff =  minBottom - maxSidebar;
            var sidebarNav = $("nav.bs-docs-sidebar");
            var newTop = Math.min(sidebarNav.position().top+diff, 60);
            sidebarNav.css("top", newTop);
        });

        $('body').scrollspy({
            target: '.bs-docs-sidebar',
            offset: 15
        });

        $('.api-example').prepend('<span class="glyphicon glyphicon-play" aria-hidden="true"></span> ')
            .click(function () {
            var modal = $("#live-api-modal");
            var method = $(this).attr('data-api-method');
            var absPath = $(this).attr('data-api-url');
            var queryVar = $(this).attr('data-api-query-var') || '<query>';

            if (queryVar === 'null') {
                modal.find('#api-var').attr('placeholder', "").attr("disabled", true).val("");
            } else {
                modal.find('#api-var').attr('placeholder', queryVar).attr("disabled", false).val("");
            }
            modal.find('.result').hide();
            modal.find('#api-method').text(method);
            modal.find('#api-url').text(absPath);

            modal.find('form').unbind('submit').submit(function () {
                var query = modal.find('#api-var').val();
                var resDiv = modal.find('.result');
                var resPre = resDiv.find('pre');
                resPre.removeClass('alert alert-danger').attr('role', null).text('LOADING...');
                resDiv.slideDown(400);
                api._call("", absPath+query, {
                    success: function (data, textStatus, response) {
                        if (response.status == 204) {
                            // empty response
                            resPre.text("204 - Empty result");
                        } else {
                            resPre.text(JSON.stringify(data, null, 2));
                        }
                    },
                    error: function (msg, jqXHR) {
                        resPre.addClass('alert alert-danger').attr('role', 'alert')
                            .text(jqXHR.status+ ' - ' + jqXHR.statusText + ': ' + msg);
                    }}, method);

                return false;
            });

            modal.modal();
        });

        $('.modal').bind('shown.bs.modal', function () {
            $(this).find('.auto-focus').focus();
        })
    });
});
