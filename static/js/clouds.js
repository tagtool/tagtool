/**
 * Created by rouven on 15.10.15.
 *
 * This module requires api.js to be loaded
 *
 * wordcloud2.js doku https://github.com/timdream/wordcloud2.js/blob/master/API.md
 */

require([
    'jquery',
    'api',
    'settings',
    'wordcloud',
    'moment',
    'user_counter',
    'changed_counter',
    'bootstrap-modal',
    'jquery-ui',
    'bootstrap',
    'bootstrap-slider',
    'typeahead'],
    function ($, API, Settings, wordcloud, moment, UserCounter, ChangedCounter, BootstrapModal) {

    Array.prototype.containsArray = function(val) {
        var hash = {};
        for(var i=0; i<this.length; i++) {
            hash[this[i]] = i;
        }
        return hash.hasOwnProperty(val);
    };

    Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    // patch typeahead: ESC closes typeahead and stops event propagation
    (function() {
        var Typeahead = $.fn.typeahead.Constructor;
        var th_ku = Typeahead.prototype.keyup;
        Typeahead.prototype.keyup = function (e) {
            if (event.which === 27) {  // ESC
                if (this.shown) {
                    this.hide();
                    console.log("Cought typeahead keyup");
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }
            } else {
                return th_ku.call(this, e);
            }
        };

        var Modal = $.fn.modal.Constructor;
        // var m_esc = Typeahead.prototype.escape;
        Modal.prototype.escape = function () {
          if (this.isShown && this.options.keyboard) {
            this.$element.on('keyup.dismiss.bs.modal', $.proxy(function (e) {
              e.which == 27 && this.hide()
            }, this))
          } else if (!this.isShown) {
            this.$element.off('keyup.dismiss.bs.modal')
          }
        }
    })();

    // patch typeahead: ignore mouse if pop-up appears under mouse
    (function() {
        var mousePos = null;
        function mouseUpdate(e) {
          mousePos = [e.pageX, e.pageY];
        }
        document.addEventListener('mousemove', mouseUpdate, false);
        document.addEventListener('mouseenter', mouseUpdate, false);

        var Typeahead = $.fn.typeahead.Constructor;
        var th_sw = Typeahead.prototype.show;
        var th_me = Typeahead.prototype.mouseenter;
        var mousePosShow = null;

        Typeahead.prototype.show = function () {
            mousePosShow = mousePos;
            return th_sw.call(this);
        };

        Typeahead.prototype.mouseenter = function (e) {
            if (mousePosShow != null) {
                var mps = mousePosShow;
                var mp = mousePos;
                if (mps[0] === mp[0] && mps[1] === mp[1]) {
                    // pop-up appearance triggered mouse-over
                    mousePosShow = null;
                    return;
                }
                // only check first time
                mousePosShow = null;
            }
            return th_me.call(this, e);
        };
    })();

    // utf-8 save versions of atob and btoa from
    // https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#The_Unicode_Problem
    btoa = (function(btoa) {
                function func(str) {
                    // first we use encodeURIComponent to get percent-encoded UTF-8,
                    // then we convert the percent encodings into raw bytes which
                    // can be fed into btoa.
                    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                        function toSolidBytes(match, p1) {
                            return String.fromCharCode(parseInt(p1, 16));
                        }));
                }
                return func;
            })(btoa);

    atob = (function(atob) {
                function func(str) {
                    // Going backwards: from bytestream, to percent-encoding, to original string.
                    return decodeURIComponent(atob(str).split('').map(function (c) {
                        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                    }).join(''));
                }
                return func;
            })(atob);



    //Copied from http://www.foliotek.com/devblog/getting-the-width-of-a-hidden-element-with-jquery-using-width/
    //Optional parameter includeMargin is used when calculating outer dimensions
    (function ($) {
        $.fn.getHiddenDimensions = function (includeMargin) {
            var $item = this,
            props = { position: 'absolute', visibility: 'hidden', display: 'block' },
            dim = { width: 0, height: 0, innerWidth: 0, innerHeight: 0, outerWidth: 0, outerHeight: 0},
            $hiddenParents = $item.parents().addBack().not(':visible'),
            includeMargin = (includeMargin == null) ? false : includeMargin;

            var oldProps = [];
            $hiddenParents.each(function () {
                var old = {};

                for (var name in props) {
                    old[name] = this.style[name];
                    this.style[name] = props[name];
                }

                oldProps.push(old);
            });

            dim.width = $item.width();
            dim.outerWidth = $item.outerWidth(includeMargin);
            dim.innerWidth = $item.innerWidth();
            dim.height = $item.height();
            dim.innerHeight = $item.innerHeight();
            dim.outerHeight = $item.outerHeight(includeMargin);

            $hiddenParents.each(function (i) {
                var old = oldProps[i];
                for (var name in props) {
                    this.style[name] = old[name];
                }
            });

            return dim;
        }
    }(jQuery));

    var Clouds = {};

    Clouds.DEFAULT_SIZING = ["links", "recency", "none"][0];

    Clouds.SpanHelper = "SpanHelper" in Clouds ? Clouds.SpanHelper : {};

    Clouds.SpanHelper.getContext = function (element) {
        var e = $(element);
        if (e.hasClass("cloud-cell")) {
            return e;
        } else {
            var p = e.parents(".cloud-cell");
            if (p.length === 1) {
                return $(p[0]);
            } else if (p.length === 0) {
                console.warn("No Context found for ", element);
                return null;
            } else {
                console.error("Context not unique for ", element);
                return false;
            }
        }
    };
    Clouds.SpanHelper.catOfContext = function (element) {
        var ctx = Clouds.SpanHelper.getContext(element);
        return atob(ctx.attr("id"));
    };
    Clouds.SpanHelper.setCatOfContext = function (element, cat) {
        var ctx = Clouds.SpanHelper.getContext(element);
        ctx.attr("id", btoa(cat));
    };
    Clouds.SpanHelper.contextOfCat = function (cat) {
        if (cat == null || cat.match(/^\W*$/)) return $();
        return $("#"+Clouds.SpanHelper.escapeID(btoa(cat)));
    };

    Clouds.SpanHelper.catOfId = function (id) {
        return id.split("_").map(function(e) { return atob(e); })[0]
    };
    Clouds.SpanHelper.catOfSpan = function (span) {
        return Clouds.SpanHelper.catOfId($(span).attr("id"));
    };
    Clouds.SpanHelper.elemOfId = function (id) {
        return id.split("_").map(function(e) { return atob(e); })[1];
    };
    Clouds.SpanHelper.elemOfSpan = function (span) {
        return Clouds.SpanHelper.elemOfId($(span).attr("id"));
    };
    Clouds.SpanHelper.catElemOfId = function (id) {
        return id.split("_").map(function(e) { return atob(e); });
    };
    Clouds.SpanHelper.catElemOfSpan = function (span) {
        return Clouds.SpanHelper.catElemOfId($(span).attr("id"));
    };
    Clouds.SpanHelper.getIdFromCatElem = function (cat, elem) {
        return btoa(cat)+"_"+btoa(elem);
    };
    Clouds.SpanHelper.getIdFromCat = function (cat) {
        return btoa(cat);
    };
    Clouds.SpanHelper.setIdOfSpan = function (span, cat, elem) {
        $(span).attr("id", Clouds.SpanHelper.getIdFromCatElem(cat, elem));
    };
    Clouds.SpanHelper.escapeID = function(id) {
        // see http://stackoverflow.com/questions/12208390/jquery-selector-contains-equals-sign
        id = id.replace(/([ #;&,.+*~\\':"!^$[\]()=>|\/@])/g,'\\$1');
        return id;
    };
    Clouds.SpanHelper.getElemFromID = function(id) {
        return $("#"+Clouds.SpanHelper.escapeID(id));
    };
    Clouds.SpanHelper.getSpanFromCatElem = function (cat, elem) {
        var id = Clouds.SpanHelper.getIdFromCatElem(cat, elem);
        return Clouds.SpanHelper.getElemFromID(id);
    };
    Clouds.SpanHelper.getCatFromID = function (id) {
        return $("#"+Clouds.SpanHelper.escapeID(id));
    };
    Clouds.SpanHelper.getDivFromCat = function (cat) {
        var id = Clouds.SpanHelper.getIdFromCat(cat);
        return Clouds.SpanHelper.getCatFromID(id);
    };


    Clouds.elementID = function() {
        // convert arguments object to array
        var args = Array.prototype.slice.call(arguments);
        return args.map(function (e) { return btoa(e) }).join("_");
    };

    Clouds.resetGui = function () {
        Clouds.Buttons.State.setState(Clouds.Buttons.State.States.Single);
        $('#main').click();  // will cancel selections, stop recent mode and stuff
        Clouds.InteractiveMode._show();  // hide timeslider
        Clouds.showLatestTimestamp(); // quit timeslider mode
        $('#versions-toolbox').addClass('collapse');  // close timeslider
        $('.time-mode').removeClass('active').find('.explanation').removeClass('visible');
        $('.modal').modal('hide');
    };

    Clouds.eventListeners = {
        disabledClickCheck: function(event) {
            if ($(this).hasClass('disabled')) {
                event.stopImmediatePropagation();
                event.preventDefault();
                event.stopPropagation();
                console.info("Swallowed click");
                return false;
            }
            if ($(this).hasClass('no-interactive-trigger')) {
                $(this).parent().trigger(event, {pseudo: true});
                event.stopPropagation();
            }
        },
        unselectAll: function () {
            // ignore click when it's actually a drag release
            if ($('.cloud.item-dragging').length === 0) Clouds.unselectAll();
        },
        recentlyChangedCounterClick: function () {
            var active = $(this).find('.explanation').hasClass("visible");
            $(".counter .explanation").removeClass("visible");
            if (!active) {
                $(this).find('.explanation').addClass("visible");
                Clouds.showRecentlyChanged();
            } else {
                Clouds.unshowRecentlyChanged();
            }
        },
        userCounterClick: function () {
            var active = $(this).find('.explanation').hasClass("visible");
            $(".counter .explanation").removeClass("visible");
            Clouds.unshowRecentlyChanged();
            if (!active) {
                $(this).find('.explanation').addClass("visible");
            }
        },
        swallow: function() {
            return false;
        },
        openedAdvancedCloudSettings: function () {
            $("#advanced-cloud-settings").removeClass("panel-default").addClass("panel-danger");
        },
        closedAdvancedCloudSettings: function () {
            $("#advanced-cloud-settings").removeClass("panel-danger").addClass("panel-default");
        },
        escapeKey: function (event) {
            if (event.keyCode === 27) {
                if (!Clouds.unpushState(true)) {
                    Clouds.resetGui();
                }
                event.preventDefault();
            }
        },
        popstate: function (event, args) {
            if (typeof args != 'object' || !'soft' in args || !args.soft) {
                Clouds.resetGui();
            }
        },
        cloudsDragover: function (event) {
            var types = event.originalEvent.dataTransfer.types;
            for (var i=0; i < types.length; i++) {
                if (['text/plain', 'text/html', 'text/uri-list'].includes(types[i])) {
                    event.preventDefault();
                    event.originalEvent.dataTransfer.dropEffect = "move";
                    return;
                }
            }
            console.log("Unsupported drag mime type", event.originalEvent.dataTransfer.types, event.originalEvent.dataTransfer);
        },
        cloudsDrop: function (event) {
            if (event.originalEvent instanceof jQuery.Event) return;  // happens caused by jQuery-UI's draggable/droppable
            var types = event.originalEvent.dataTransfer.types;
            var typePrevs = ['text/plain', 'text/uri-list', 'text/html'];
            var bestTypeMatch = -1;
            for (var i=0; i < types.length; i++) {
                bestTypeMatch = Math.max(bestTypeMatch, typePrevs.indexOf(types[i]));
            }
            if (bestTypeMatch <= 0) return;

            var text;
            switch (typePrevs[bestTypeMatch]) {
                case 'text/html':
                    text = event.originalEvent.dataTransfer.getData('text/html');
                    // replace links with markdown links
                    var $doc = $(new DOMParser().parseFromString(text, 'text/html'));
                    $doc.find('a').replaceWith(function () {
                        var $a = $(this);
                        var href = $a.attr('href').replace(/\s/g, ' ');
                        var text = $a.text().replace(/\s/g, ' ');
                        var res;
                        if (href != null && href.replace(/\s/g, '') !== '') {
                            if (text.replace(/\s/g, '') === '') text = href;
                            res = "[" + text.replace(/]/g, '\\]').replace(/\[/g, '\\[') + "](" + href + ')';
                        } else {
                            res = text;
                        }
                        return $('<div>').text(res).html();
                    });

                    function flattenHtml($elem) {
                        var $children = $elem.children();
                        if ($children.length === 0) {
                            if ($elem.is('br, hr')) {
                                $elem.replaceWith("\n");
                            }
                            if ($elem.is('address, article, aside, blockquote, details, dialog, dd, div, dl, dt, fieldset, figcaption, figure, form, h1, h2, h3, h4, h5, h6, header, hgroup, hr, li, main, nav, ol, p, pre, section, table, tr, ul') ||
                                    ['block', 'flex', 'list-item', 'table', 'table-row', 'grid'].indexOf($elem.css('display')) >= 0) {
                                // block elem or elem that usually takes up an own row
                                $elem.replaceWith("\n" + $elem.html() + "\n");
                            } else {
                                // inline elem
                                $elem.replaceWith($elem.html());
                            }
                            return;
                        }

                        $children.each(function (i, el) {
                            flattenHtml($(el));
                        });
                    }

                    flattenHtml($doc.find('body'));
                    text = $doc.text();
                    break;
                case 'text/uri-list':
                    text = event.originalEvent.dataTransfer.getData('text/uri-list');
                    text.replace(/#[^\n]*\n/g, '');
                    text = text.split('\n').map(function (line) { return "[" + line + "](" + line + ")"; }).join('\n');
                    break;
                case 'text/plain':
                    text = event.originalEvent.dataTransfer.getData('text/plain');
                    break;
            }

            text = text.replace(/^\s+/, '').replace(/\s+$/, '').replace(/\n\s*\n/g, '\n');
            var cat = Clouds.SpanHelper.catOfContext(event.target);
            Clouds.Buttons.addElement(null, cat, text);
            event.preventDefault();
        }
    };

    Clouds.setBindings = function() {
        // must be bound first (because it cancels later handlers if
        // element is disabled
        $("*").off('click', Clouds.eventListeners.disabledClickCheck)
              .on('click', Clouds.eventListeners.disabledClickCheck);

        Clouds.Buttons.setBindings();
        Clouds.SearchBar.setBindings();
        Clouds.InteractiveMode.setBindings();

        $("#main").off('click', Clouds.eventListeners.unselectAll)
                  .on('click', Clouds.eventListeners.unselectAll);
        ChangedCounter
            .off('click', Clouds.eventListeners.recentlyChangedCounterClick)
            .on('click', Clouds.eventListeners.recentlyChangedCounterClick);
        UserCounter
            .off('click', Clouds.eventListeners.userCounterClick)
            .on('click', Clouds.eventListeners.userCounterClick);
        $(".modal-load-overlay")
            .off('click', Clouds.eventListeners.swallow)
            .on('click', Clouds.eventListeners.swallow);
        $("#advanced-cloud-settings-body")
            .off('show.bs.collapse', Clouds.eventListeners.openedAdvancedCloudSettings)
            .on('show.bs.collapse', Clouds.eventListeners.openedAdvancedCloudSettings)
            .off('hide.bs.collapse', Clouds.eventListeners.closedAdvancedCloudSettings)
            .on('hide.bs.collapse', Clouds.eventListeners.closedAdvancedCloudSettings);
        $(window).off('keyup', Clouds.eventListeners.escapeKey)
                 .on('keyup', Clouds.eventListeners.escapeKey);
        $(window).off('popstate', Clouds.eventListeners.popstate)
                 .on('popstate', Clouds.eventListeners.popstate);

        $(".cloud-cell .panel-body").off('dragover', Clouds.eventListeners.cloudsDragover)
                                    .on('dragover', Clouds.eventListeners.cloudsDragover)
                                    .off('drop', Clouds.eventListeners.cloudsDrop)
                                    .on('drop', Clouds.eventListeners.cloudsDrop);
    };

    Clouds.startWaiting = function() {
        $('.modal-load-overlay').addClass('active');
    };

    Clouds.stopWaiting = function() {
        $('.modal-load-overlay').removeClass('active');
    };

    Clouds.settingsCB = function(settings) {
        var redraw = [];
        var reorder = false;
        var recalcWeights = false;
        if (Clouds.settings != null) {
            for (var i=0; i < 3; i++) {
                var attr = ['sizing', 'display-type', 'display-priority'][i];
                for (var cat in Clouds.data.categories) {
                    if (!Clouds.data.categories.hasOwnProperty(cat)) continue;
                    var local_exist = Clouds.settings.hasOwnProperty(attr) && Clouds.settings[attr].hasOwnProperty(cat);
                    var new_exist = settings.hasOwnProperty(attr) && settings[attr].hasOwnProperty(cat);
                    var changed = local_exist && new_exist && settings[attr][cat] !== Clouds.settings[attr][cat];
                    if (local_exist !== new_exist || changed) {
                        if (attr === 'display-priority') {
                            reorder = true;
                        } else {
                            if (!redraw.includes(cat)) redraw.push(cat);
                            if (attr === 'sizing') recalcWeights = true;
                        }
                    }
                }
            }
        }

        Clouds.settings = settings;
        if (Clouds._diffBeforeSettings != null) {
            Clouds._showDiff(Clouds._diffBeforeSettings);
            Clouds._diffBeforeSettings = null;
        }

        if (reorder) Clouds.reorderClouds();
        if (recalcWeights) Clouds.calculateWeights();
        for (var i=0; i < redraw.length; i++) {
            Clouds.drawCloud(redraw[i], null, null, null, true);
        }
    };

    Clouds.showingRevision = null;
    Clouds.allTimestamps = [];
    Clouds.showingOldRevision = function() {
        return Clouds.showingRevision != Clouds.allTimestamps[Clouds.allTimestamps.length-1];
    };

    Clouds.editEnabled = false;
    Clouds.fragmentChecked = false;

    Clouds.main = function() {
        API.get.authStatus({
            start: null,
            end: null,
            success: function(data) {
                Clouds.editEnabled = data.loggedIn;
                if (Clouds.editEnabled) $('.write-action').show();
                var cloudColSizing = $('#example-cloud').attr('class').split(' ')
                    .filter(function(e) {return e.startsWith('col-');}).join(' ');
                $("#main").sortable({
                    handle: ".panel-heading",
                    revert: true,
                    placeholder: cloudColSizing + " cloud-placeholder",
                    delay: 150,
                    distance: 10,
                    start: function (event, ui) {
                        $(window).trigger("mousedown");
                        ui.helper.find('.ui-sortable-handle')
                            .addBack('.ui-sortable-handle')
                            .addClass('ui-sortable-dragging');
                    },
                    update: function (event, ui) {
                        var clouds = $("ul#main > li:not(#example-cloud)");
                        for (var i = 0; i < clouds.length; i++) {
                            clouds[i] = Clouds.SpanHelper.catOfContext(clouds[i]);
                        }
                        Settings.saveOrderServer(clouds);
                    }
                });
                Clouds.setBindings();
                Settings.fetch(Clouds.settingsCB.bind(Clouds));
                API.startCB = Clouds.startWaiting.bind(Clouds);
                API.stopCB = Clouds.stopWaiting.bind(Clouds);
                API.registerPollCB(Clouds.pollUpdateDiff.bind(Clouds), 'diff');
                API.registerPollCB(ChangedCounter.updateRecentlyChanged.bind(ChangedCounter), 'diff');
                Settings.registerCB(Clouds.settingsCB.bind(Clouds));

                API.startPollLoop();
            }
        });

        API.get.allTimestamps({
            success: function(data) {
                var slider = $("#timeslider-input");
                Clouds.allTimestamps = data;
                Clouds.showingRevision = Clouds.allTimestamps[Clouds.allTimestamps.length-1];
                var max = data.length-1;
                slider.bootstrapSlider({
                    max: max,
                    value: max,
                    tooltip_position: 'bottom',
                    formatter: function(value) {
                        var utc_str = Clouds.allTimestamps[parseInt(value)].split('.')[0];
                        return moment.utc(utc_str).local().format("YYYY-MM-DD HH:mm:ss.SSS");
                    }
                }).on('slideStop', function(slideEvt) {Clouds.showTimestamp(slideEvt.value)});
            }
        });
    };

    Clouds.showLatestTimestamp = function() {
        if (Clouds.showingOldRevision()) {
            Clouds.showTimestamp(Clouds.allTimestamps.length-1);
        }
    };

    Clouds.stepTimestamp = function (step) {
        var curIdx = Clouds.allTimestamps.indexOf(Clouds.showingRevision);
        var newIdx = Math.min(Math.max(curIdx + step , 0), Clouds.allTimestamps.length - 1);
        if (curIdx !== newIdx) Clouds.showTimestamp(newIdx);
        return false;
    };

    Clouds.showTimestamp = function(timestamp_idx) {
        Clouds.unshowRecentlyChanged();
        var slider = $('#timeslider-input');
        if (slider.bootstrapSlider('getValue') != timestamp_idx)
            slider.bootstrapSlider('setValue', timestamp_idx);
        if (Clouds.showingRevision == Clouds.allTimestamps[timestamp_idx]) return;
        if (timestamp_idx == Clouds.allTimestamps.length-1) {
            // showing latest version
            API.get.diffQuery({
                start: Clouds.showingRevision,
                end: Clouds.allTimestamps[timestamp_idx],
                success: function(data) {
                    //API.startPollLoop();
                    API.attachPoll();
                    Clouds._showDiff(data);
                }
            });
        } else {
            //API.stopPollLoop();
            API.detachPoll();
            if (Clouds.Buttons.State.currentState == Clouds.Buttons.State.States.Link ||
                Clouds.Buttons.State.currentState == Clouds.Buttons.State.States.Move) {
                Clouds.Buttons.State.setState(Clouds.Buttons.State.States.Single);
            }
            API.get.diffQuery({
                start: Clouds.showingRevision,
                end: Clouds.allTimestamps[timestamp_idx],
                success: function(data) {
                    Clouds._showDiff(data);
                }
            });
        }
        Clouds.showingRevision = Clouds.allTimestamps[timestamp_idx];
    };

    Clouds.isPseudoClickHandler = function (arguments) {
        return !(arguments.length < 2 || typeof arguments[1] != 'object' ||
                 !("pseudo" in arguments[1]) || !arguments[1].pseudo)
    };

    Clouds.pseudoTouch = function(element, cb) {
        element = $(element);
        var touchElement = $("#touch-animation");

        if ('scrollIntoViewIfNeeded' in element[0])
            element[0].scrollIntoViewIfNeeded();
        else
            element[0].scrollIntoView();

        // position touch element
        var elemPosition = element.offset();
        var elemXCenter = elemPosition.left + element.outerWidth() / 2;
        var elemYCenter = elemPosition.top + element.outerHeight() / 2;
        touchElement.css("left", elemXCenter-touchElement.outerWidth()/2);
        touchElement.css("top", elemYCenter-touchElement.outerWidth()/2);


        // fade in -> click -> fade out
        var handler = function() {
            element.trigger("click", {pseudo: true});
            touchElement.off("animationend webkitanimationEnd oAnimationEnd MSAnimationEnd", handler);
            touchElement.addClass("collapse");
            if (cb != null) {
                cb();
            }
        };
        touchElement.on("animationend webkitanimationEnd oAnimationEnd MSAnimationEnd", handler);
        touchElement.removeClass("collapse");
    };

    ChangedCounter.registerUpdateCB(function () {
        if (Clouds.showingRecentlyChanged()) {
            Clouds.showRecentlyChanged();  // update highlights
        }
    });

    Clouds.showingRecentlyChanged = function () {
        return Clouds._showingRecentlyChanged;
    };

    Clouds._unshowRecentlyChanged = function (stateChange) {
        if (stateChange == null) stateChange = true;
        if (stateChange) Clouds._showingRecentlyChanged = false;
        $('#recently-changed-explain').removeClass("visible");
        $('.recently-changed').removeClass('recently-changed');
    };

    Clouds.unshowRecentlyChanged = function () {
        if (!Clouds._runningShowRecentlyChanged) {
            Clouds._unshowRecentlyChanged();
        }
    };

    Clouds._showingRecentlyChanged = false;
    Clouds._runningShowRecentlyChanged = false;
    Clouds.showRecentlyChanged = function () {
        Clouds._runningShowRecentlyChanged = true;
        if (!Clouds.showingOldRevision()) {
            Clouds.unselectAll();
            Clouds._unshowRecentlyChanged(false);
            $('#recently-changed-explain').addClass("visible");
            for (var i=0; i < ChangedCounter.recentlyChanged.length; i++) {
                var e = ChangedCounter.recentlyChanged[i];
                if (typeof e == 'string') {
                    // changed category
                    Clouds.SpanHelper.getDivFromCat(e).addClass('recently-changed')
                } else if (e.constructor === Array && e.length == 2) {
                    // changed element
                    Clouds.SpanHelper.getSpanFromCatElem(e[0], e[1]).addClass('recently-changed')
                } else {
                    console.error('Unknown recent change:', e)
                }
            }
        } else {
            Clouds._unshowRecentlyChanged(false);
            $('#recently-changed-explain').removeClass("visible");
        }
        Clouds._showingRecentlyChanged = true;
        Clouds._runningShowRecentlyChanged = false;
    };


    Clouds.settings = null;
    Clouds.data = null;

    Clouds._diffBeforeSettings = null;

    Clouds._showDiff = function (diff) {
        if (Clouds.settings == null) {
            Clouds._diffBeforeSettings = diff;
            return;
        }

        if (diff == null) diff = {};
        var actions_enum = {"new": 1, "del": 2, "redraw": 3, "null": null,
                            "remove_elems": 4};
        var actionList = {};
        var cats;
        if (! $.isEmptyObject(diff)) {
            var myThrow = function(err, msg) {
                if (err == null) err = "AssertionError";
                if (msg == null) msg = "Something went wrong sorry. We'll reload the page.";
                console.error(err, msg);
                //alert(msg);
                //window.setTimeout(function() {window.location.reload()}, 100000);
                //throw msg;
                window.location.reload();
            };
            if ('to' in diff && diff.to != null) Clouds.showingRevision = diff.to;
            cats = Object.keys(Clouds.data.categories);
            for (var i=0; i <= cats.length; i++) {
                actionList[cats[i]] = actions_enum.null;
            }

            $(diff.insertions.categories).each(function(idx, val) {
                if (val in Clouds.data.categories) myThrow();
                actionList[val] = actions_enum.new;
                Clouds.data.categories[val] = [];
            });
            $(diff.insertions.elements).each(function(idx, val) {
                if (!(val[0] in Clouds.data.categories)) myThrow();
                if (Clouds.data.categories[val[0]].containsArray(val[1])) myThrow();
                if (actionList[val[0]] == actions_enum.null) {
                    actionList[val[0]] = actions_enum.redraw;
                }
                Clouds.data.categories[val[0]].push(val[1]);
            });
            $(diff.insertions.links).each(function(idx, val) {
                if (Clouds.data.links.containsArray(val) ||
                    Clouds.data.links.containsArray(val.slice().reverse())) myThrow();
                Clouds.data.links.push(val);
            });

            $(diff.deletions.links).each(function(idx, val) {
                if (Clouds.data.links.containsArray(val) ||
                    Clouds.data.links.containsArray(val.reverse())) {
                    Clouds.data.links = $.grep(Clouds.data.links, function(elem) {
                        return !(elem.containsArray(val[0]) && elem.containsArray(val[1]));
                    });
                } else {
                    myThrow();
                }
            });
            $(diff.deletions.elements).each(function(idx, val) {
                if (!(val[0] in Clouds.data.categories)) myThrow();
                if (!Clouds.data.categories[val[0]].containsArray(val[1])) myThrow();
                Clouds.data.categories[val[0]].splice(
                    $.inArray(val[1], Clouds.data.categories[val[0]]), 1
                );
                if (actionList[val[0]] == actions_enum.null) {
                    actionList[val[0]] = actions_enum.remove_elems;
                }
                var span = Clouds.SpanHelper.getSpanFromCatElem(val[0], val[1]);
                if ($(span).hasClass("selected")) {
                    Clouds.unselect(span);
                }
                $(span).remove();
            });
            $(diff.deletions.categories).each(function(idx, val) {
                if (!(val in Clouds.data.categories)) myThrow();
                actionList[val] = actions_enum.del;
                Clouds.currentlySelected = Clouds.currentlySelected.filter(function (e) {
                    return e[0] != val;
                });
                delete Clouds.data.categories[val];
                $(Clouds.SpanHelper.contextOfCat(val)).remove();
            });
        }

        if (Clouds.settings != null) {
            Clouds.calculateWeights();
            var drawn = false;
            var waiting = 0;
            var funcs = [];
            cats = Object.keys(Clouds.data.categories);
            for (var i=0; i < cats.length; i++) {
                var cat = cats[i];
                if (cat in actionList && actionList.hasOwnProperty(cat)) {
                    switch (actionList[cat]) {
                        case actions_enum.new:
                        case actions_enum.redraw:
                            drawn = true;
                            waiting++;
                            funcs.push(
                                Clouds.drawCloud.bind(Clouds, cat, null, null, function() {
                                    if (--waiting == 0) {
                                        Clouds.restoreHighlighting();
                                        Clouds.setBindings();
                                        if (!Clouds.fragmentChecked) {
                                            Clouds.fragmentChecked = true;
                                            var fragment = window.location.hash.slice(1);
                                            var split = fragment.split('/');
                                            var catelem = [split.shift(), split.join('/')].map(decodeURIComponent);
                                            var $span = Clouds.SpanHelper.getSpanFromCatElem(catelem[0], catelem[1]);
                                            if ($span.length) {
                                                Clouds.spanClick($span);
                                                Clouds.DemoMode.resetIdleTimer(Clouds.DemoMode.AFTER_ACTION_INTERVAL);
                                                if ('scrollIntoViewIfNeeded' in $span[0]) $span[0].scrollIntoViewIfNeeded(); else $span[0].scrollIntoView();
                                            }
                                        }
                                    }
                                }, true)
                            );
                            break;
                    }
                }
            }
            $(funcs).each(function(i, f) { f() });
            if(drawn) $(function(){Clouds.reorderClouds()});

            Clouds.Buttons.State.updateButtons();
            Clouds.restoreHighlighting();
        }
    };

    Clouds.pollUpdateDiff = function(diff) {
        if (Clouds.data == null) {
            Clouds.data = {"categories": {}, "links": []};
        }
        if (diff == null) { diff = {}; }

        API.get.allTimestamps({
            success: function (data) {
                var max_timestamp_idx = data.length-1;
                $('#timeslider-input').bootstrapSlider('setAttribute', 'max', max_timestamp_idx);
                var onOldRev = Clouds.showingOldRevision();
                Clouds.allTimestamps = data; // this must be set between showingOldRevision check and slider update
                if (!onOldRev) {
                    Clouds.showTimestamp(max_timestamp_idx);
                }
            }
        });

        if (!Clouds.showingOldRevision() ) {
            if ('to' in diff && diff.to != null) {
                if ($.inArray(diff.to, Clouds.allTimestamps) < 0) {
                    // Temporary completion of allTimestamps-list until the
                    // API.get.allTimestamps-call returns (see next command)
                    // The list will then be overwritten.
                    // This completion can only be temporary as it is possible to
                    // miss updates if the server decides to send multiple actions
                    // in one diff. But it's necessary to keep
                    // Clouds.showingOldRevision working properly after _showDiff
                    // changes Clouds.showingRevision.
                    Clouds.allTimestamps.push(diff.to);
                }
            }
            Clouds._showDiff(diff);
        }
    };

    Clouds.weights = null;

    Clouds.calculateWeights = function() {
        Clouds.weights = {};
        for (var cat in Clouds.data.categories) {
            if (Clouds.data.categories.hasOwnProperty(cat)) {
                Clouds.weights[cat] = {};
                for (var i = 0; i < Clouds.data.categories[cat].length; i++) {
                    var elem = Clouds.data.categories[cat][i];
                    Clouds.weights[cat][elem] = 1;
                }
            }
        }

        for (cat in Clouds.data.categories) {
            if (Clouds.data.categories.hasOwnProperty(cat)) {
                var sizing = cat in Clouds.settings.sizing ?
                    Clouds.settings.sizing[cat] :
                    Clouds.DEFAULT_SIZING;
                switch (sizing) {
                    case "links":
                        for (i = 0; i < Clouds.data.links.length; i++) {
                            var link = Clouds.data.links[i];
                            for (var j = 0; j < 2; j++) {
                                if (cat == link[j][0]) {
                                    Clouds.weights[link[j][0]][link[j][1]]++;
                                }
                            }
                        }
                        break;
                    case "recency":
                        for (i = 0; i < Clouds.data.categories[cat].length; i++) {
                            elem = Clouds.data.categories[cat][i];
                            Clouds.weights[cat][elem] = i + 1;
                        }
                        break;
                    case "none": // nothing to do here ;)
                        break;
                    default:
                        throw "Unknown sizing option " + sizing;

                }
            }
        }
    };

    Clouds._bindItemDragDropToItem = function (elem, removeSource) {
        if (!Clouds.editEnabled) return;
        if (removeSource == null) removeSource = true;
        var $elem = $(elem);
        if ( $elem.draggable('instance') != null ) {
            if ( $elem.droppable('instance') == null ) {
                console.warn('Please panic!');
            }
            console.log('Reusing draggable/droppable binding of element.');
            return;
        }
        $elem.draggable({
            zIndex: 99999,
            helper: 'clone',
            appendTo: '#main',
            containment: '#main',
            // restraint: "#main",
            // preventProtrusion: true,
            distance: 10,
            start: function (event, ui) {
                var $this = $(this);
                $this.parents('.cloud').addClass('item-dragging');
                var parent = ui.helper.parent();
                ui.helper.detach();
                var wasLinkMode = Clouds.Buttons.State.currentState === Clouds.Buttons.State.States.Link;
                ui.helper.data('wasLinkMode', wasLinkMode);
                if (Clouds.Buttons.State.currentState !== Clouds.Buttons.State.States.Move) {
                    if (!$(this).hasClass('selected')) Clouds.spanClick(this, true, event);
                    if(!wasLinkMode) {
                        Clouds.Buttons.State.setState(Clouds.Buttons.State.States.Link);
                    }
                    ui.helper.addClass('link');
                } else {
                    ui.helper.addClass('move');
                }
                if (removeSource)
                    $(this).hide();
                else
                    $(this).css('opacity', .25).addClass('grayscale');
                ui.helper.width($(this).width());
                ui.helper.removeClass (function (index, className) {
                    return (className.match(/(^|\s)col-(xs|sm|md|lg)-([1-9]|1[0-2])(\s|$)/g) || []).join(' ');
                });
                ui.helper.removeClass('selected').addClass('pseudo-selected');
                ui.helper.appendTo(parent);
                // ooh, ouch so ugly. Seems like jquery UI does not want to fix this soon
                // https://bugs.jqueryui.com/ticket/10660
                $this.data('uiDraggable')._cacheHelperProportions();
                $this.data('uiDraggable')._setContainment();

                var reorderHandler = function () {
                    $(this).off('linkListReordered', reorderHandler);
                };
                $('.link-cloud').on('linkListReordered', reorderHandler);

            },
            stop: function (event, ui) {
                var wasLinkMode = ui.helper.data('wasLinkMode');
                ui.helper.remove();
                $(this).show();
                if (Clouds.Buttons.State.currentState !== Clouds.Buttons.State.States.Move) {
                    if(!wasLinkMode) {
                        Clouds.Buttons.State.setState(Clouds.Buttons.State.States.Single);
                    }
                    $(this).show().removeClass('selected');
                    Clouds.spanClick(this, true, event);
                }
                $(this).css('opacity', 1).removeClass('grayscale');
                $(this).parents('.cloud').removeClass('item-dragging');
                Clouds.orderLinkLists(Clouds.SpanHelper.catOfSpan(this));
            },
            revert: function (event) {
                return !$('.ui-draggable-dragging').data('moveDropped');
            }
        }).droppable({
            accept: '.cloud-elem',
            greedy: true,
            tolerance: "pointer",
            over: function (event, ui) {
                $(this).addClass('drop-highlight');
            },
            out: function (event, ui) {
                $(this).removeClass('drop-highlight');
            },
            drop: function (event, ui) {
                $(this).removeClass('drop-highlight');
                Clouds.spanClick(this, true, event);
                ui.helper.data('linkDropped', true);
            }
        });
    };

    Clouds._bindItemDropToCloud = function(elem, cat) {
        if (!Clouds.editEnabled) return;
        $(elem).droppable({
            accept: ".cloud-elem",
            tolerance: "pointer",
            over: function (event, ui) {
                if (!$(this).droppable("option", "disabled")) {
                    var old_cat = Clouds.SpanHelper.catOfSpan(ui.draggable);
                    if (old_cat != cat) {
                        $(this).addClass('drop-highlight');
                    }
                }
            },
            out: function (event, ui) {
                $(this).removeClass('drop-highlight');
            },
            drop: function (event, ui) {
                $(this).removeClass('drop-highlight');
                var old_cat = Clouds.SpanHelper.catOfSpan(ui.draggable);
                var new_cat = cat;
                if (old_cat != new_cat) {
                    var elem = Clouds.SpanHelper.elemOfSpan(ui.draggable);
                    API.put.renameElementOverCategory(old_cat, elem, new_cat, elem);
                    ui.helper.data('moveDropped', true);
                }
            }
        });
    };

    Clouds.drawCloud = function(category, maxSize, minSize, cb, ordered) {
        if (Clouds.settings.hasOwnProperty('display-type') &&
                Clouds.settings['display-type'][category] === 'links') {
            return Clouds.drawList(category, cb, ordered);
        }
        var elems = Clouds.data.categories[category].slice();
        if (maxSize == null) { maxSize = 25 }
        if (minSize == null) { minSize = 12}
        var minWeight, maxWeight;
        var list = [];
        for (var i=0; i < elems.length; i++) {
            var elem = elems[i];
            var weight = Clouds.weights[category][elem];
            list.push([elem, weight]);
        }
        elems.sort(function(e1, e2) { // sort elements ascending by their weights
            return Clouds.weights[category][e1]-Clouds.weights[category][e2];
        });
        var five_percent = Math.round(elems.length/20);
        minWeight = Clouds.weights[category][elems[five_percent]];
        maxWeight = Clouds.weights[category][elems[elems.length-1-five_percent]];

        var main = $("#main");

        $(function() {
            var ctx = $(Clouds.SpanHelper.contextOfCat(category));
            var container, cloud;
            if (ctx.length >= 1) {
                container = $(ctx[0]);
                cloud = container.find(".cloud");
                cloud.html("");
            } else {
                container = $('#example-cloud').clone(true, true);
                Clouds.SpanHelper.setCatOfContext(container, category);
                container.find(".panel-title").text(category);
                cloud = container.find(".cloud");
                main.append(container);
            }
            if (!ordered) Clouds.reorderClouds();
            var f = function(x) { // scaling function [0,1] |--> [0,1]
                return 1/(1+Math.pow(Math.E, -(x*8-4)));
            };
            wordcloud(cloud[0],
                {list: list,
                    minRotation: 0,
                    maxRotation: 0,
                    weightFactor: function(weight) {
                        if ((maxWeight-minWeight) == 0) return minSize;
                        var x = (weight-minWeight)/(maxWeight-minWeight);
                        return (maxSize-minSize)*f(x)+minSize;
                    },
                    color: 'random-dark',
                    gridSize: 7});
            cloud.bind("wordcloudstop", function() { // after cloud has been drawn
                var cat = atob($(this).parents(".cloud-cell").attr("id"));
                cloud.find("span").addClass('cloud-elem').unbind('click').click(function(e) {
                    Clouds.spanClick(this, true, e, !Clouds.isPseudoClickHandler(arguments));
                    Clouds.InteractiveMode.clickHandler.apply(this, arguments);
                    return false;
                }).each(function() {
                    Clouds.SpanHelper.setIdOfSpan(this, cat, $(this).text());
                    Clouds._bindItemDragDropToItem(this);
                });
                Clouds._bindItemDropToCloud(cloud.parents('.cloud-panel'), cat);
                if (cb != null) {
                    cb();
                }
            });
            Clouds.Buttons.State.newStateCB = Clouds.spanSelectModeChanged;
        });
    };

    Clouds.drawClouds = function(cb) {
        Clouds.calculateWeights();
        $(".cloud-cell").not("#example-cloud").remove();
        var done = 0;
        var size = Object.size(Clouds.data.categories);
        var ordered_cats = Settings.orderedCategories(Object.keys(Clouds.data.categories));
        for (var i=0; i < ordered_cats.length; i++) {
            var category = ordered_cats[i];
            Clouds.drawCloud(category, null, null, function () {
                done += 1;
                if (done === size) {
                    Clouds.setBindings();
                    if (cb != null) {
                        cb();
                    }
                }
            }, true);
        }
    };

    Clouds.reorderClouds = function() {
        var orderedCats = Settings.orderedCategories(Object.keys(Clouds.data.categories));
        var $main = $('#main');
        for (var i=0; i < orderedCats.length; i++) {
            var $cat = Clouds.SpanHelper.contextOfCat(orderedCats[i]);
            $cat.appendTo($main);
        }
    };

    Clouds.sanitizeLinkUrl = function(url) {
        var allowedProtocols = ['https?', 'ftps?'];  // RegEx format allowed

        var regex = new RegExp("^(" + allowedProtocols.join('|') + ")://");
        if (regex.test(url)) {
            return url
        } else {
            // not allowed or no protocol
            var match = url.match(/^([a-zA-Z]+:\/\/)(.*)/);
            if (match != null) {
                // found not allowed protocol => strip it
                url = match[2]
            }
            // enforce http
            return 'http://' + url;
        }
    };

    Clouds.LinkRegEx = /^\[((?:[^\]\[]|\\\[|\\\])*)\]\(([^()]*)\)$/;

    Clouds.drawList = function(category, cb, ordered) {
        var main = $("#main");
        var elems = Clouds.data.categories[category].slice();

        $(function() {
            var $ctx = $(Clouds.SpanHelper.contextOfCat(category));
            var $container, $cloud;
            if ($ctx.length >= 1) {
                $container = $ctx.first();
                $cloud = $container.find(".panel-body");
                $cloud.html("");
            } else {
                $container = $('#example-cloud').clone(true, true);
                Clouds.SpanHelper.setCatOfContext($container, category);
                $container.find(".panel-title").text(category);
                $cloud = $container.find(".panel-body");
                main.append($container);
                $container.find('.cloud').remove();
            }
            if (!ordered) Clouds.reorderClouds();
            $cloud.addClass('link-cloud container-fluid cloud');
            var $row = $('<div class="row"></div>');
            for (var i=0; i < elems.length; i++) {
                var $col = $('<div class="col-xs-4 list-group link-elem cloud-elem"></div>');
                Clouds.SpanHelper.setIdOfSpan($col, category, elems[i]);
                $col.unbind('click').click(function(e) {
                    Clouds.spanClick(this, true, e, !Clouds.isPseudoClickHandler(arguments));
                    Clouds.InteractiveMode.clickHandler.apply(this, arguments);
                    return false;
                });
                Clouds._bindItemDragDropToItem($col, false);
                var match = elems[i].match(Clouds.LinkRegEx);
                var $listItem = $('<div class="list-group-item"></div>');
                var $itemTitle = $('<h4 class="list-group-item-heading"></h4>');
                var $itemText;

                if (match != null) {
                    $itemTitle.text(match[1].replace(/\\([\[\]])/g, '$1'));
                    //$itemTitle.text(match[1]);
                    if (match[2].replace(/\s/g, "") != "") {
                        var saveUrl = Clouds.sanitizeLinkUrl(match[2]);
                        var $a = $('<a class="list-group-item-text"></a>').attr('href', saveUrl).text(decodeURI(saveUrl));
                        $a.click(function (e) { e.stopPropagation(); }); // make sure link is clickable
                        $itemText = $('<div class="list-group-item-text">').append($a);
                    } else {
                        $itemText = $('<p class="list-group-item-text">&nbsp;</p>');
                    }
                    $listItem.append($itemTitle).append($itemText);
                } else {
                    $itemTitle.text(elems[i]);
                    $itemText = $('<p class="list-group-item-text">&nbsp;</p>');
                    $listItem.append($itemTitle).append($itemText);
                }
                $itemTitle.attr('title', $itemTitle.text());
                $row.append($col.append($listItem));

            }
            $cloud.append($row);
            Clouds._bindItemDropToCloud($cloud.parents('.cloud-panel'), category);
            Clouds.orderLinkLists(category);
            if (cb != null) cb();
        });
    };


    Clouds.currentlySelected = [];

    Clouds.restoreHighlighting = function () {
        var res;
        if (Clouds.currentlySelected.length) {
            for (var i=0; i < Clouds.currentlySelected.length; i++) {
                var cat = Clouds.currentlySelected[i][0];
                var elem = Clouds.currentlySelected[i][1];
                var span = Clouds.SpanHelper.getSpanFromCatElem(cat, elem);
                // skip dummy elements (used for drag an drop)
                if (span.hasClass('.ui-draggable-dragging')) continue;
                if (!span.hasClass('selected')) {
                    Clouds.spanClick(span, false);
                } else {
                    // if the elem is already selected un- and reselect it, to make sure
                    // everything is up to date.
                    Clouds.spanClick(span, false);
                    Clouds.spanClick(span, false);
                }
            }
            res = true;
        } else {
            res = false;
        }
        Clouds.Buttons.State.updateButtons();
        Clouds.SearchBar.restore();
        return res
    };

    Clouds.selectSingle = function (span, updateSelected, manualClick) {
        if (updateSelected == null) { updateSelected = true; }
        $(".cloud .cloud-elem").removeClass("selected");
        $(span).addClass("selected");
        var cat = Clouds.SpanHelper.catOfSpan(span);
        var elem = Clouds.SpanHelper.elemOfSpan(span);
        if (updateSelected) {
            Clouds.currentlySelected = [[cat, elem]];
        }
        Clouds.highlightLinkedSpans(span);
        Clouds.Buttons.State.updateButtons();
        if (manualClick) {
            var fragment = '#' + [cat, elem].map(encodeURIComponent).join('/');
            var loc = window.location.pathname + window.location.search + fragment;
            if (window.location.hash.slice(1)) {
                // if fragment exists: overwrite it
                window.history.replaceState({pushed: true}, document.title, loc);
            } else {
                // if fragment does not exist: push new state with fragment
                window.history.pushState({pushed: true}, document.title, loc);
            }
        }
    };

    Clouds.selectMulti = function (span, updateSelected) {
        if (updateSelected == null) { updateSelected = true; }
        $(span).addClass("selected");
        if (updateSelected){
            var selected = [];
            $(".selected").each(function(idx, span) {
                selected.push(Clouds.SpanHelper.catElemOfSpan(span));
            });
            Clouds.currentlySelected = selected;
        }
        Clouds.highlightLinkedSpans();
        Clouds.Buttons.State.updateButtons();
    };

    Clouds.unselect = function (span, updateSelected) {
        if (updateSelected == null) { updateSelected = true; }
        $(span).removeClass("selected");
        if (updateSelected) {
            var catelems = [];
            $(".selected").each(function(idx, span) {
                var cat = Clouds.SpanHelper.catOfSpan(span);
                var elem = Clouds.SpanHelper.elemOfSpan(span);
                catelems.push([cat, elem]);
            });
            if (catelems.length) {
                Clouds.currentlySelected = catelems;
            } else {
                Clouds.currentlySelected = [];
                Clouds.unpushState();
            }
        }
        Clouds.highlightLinkedSpans();
        Clouds.Buttons.State.updateButtons();
    };

    Clouds.spanSelectModeChanged = function(newState, oldState) {
        var selected = $(".selected");
        switch (newState) {
            case Clouds.Buttons.State.States.Single:
                if (selected.length !== 1) {
                    Clouds.unselectAll();
                } else {
                    Clouds.selectSingle(selected);
                    Clouds.highlightLinkedSpans(selected);
                }
                break;
            case Clouds.Buttons.State.States.MultiAnd:
            case Clouds.Buttons.State.States.MultiOr:
                Clouds.highlightLinkedSpans(null);
                break;
            case Clouds.Buttons.State.States.Link:
                if (selected.length !== 1) {
                    Clouds.unselectAll();
                } else {
                    Clouds.selectSingle(selected);
                    Clouds.highlightLinkedSpans(selected);
                }
                break;
            case Clouds.Buttons.State.States.Move:
                Clouds.unselectAll();
                break;
        }
        Clouds.Buttons.State.updateButtons();
    };

    Clouds.inTemporalMultiSelectState = false;

    Clouds.leaveTemporalMultiSelectState = function () {
        if (Clouds.inTemporalMultiSelectState) {
            Clouds.inTemporalMultiSelectState = false;
            if (Clouds.Buttons.State.currentState == Clouds.Buttons.State.States.MultiAnd ||
                   Clouds.Buttons.State.currentState == Clouds.Buttons.State.States.MultiOr) {
                Clouds.Buttons.State.setState(Clouds.Buttons.State.States.Single);
            }
        }
    };

    Clouds._settingsLinkListComparator = function(a, b) {
        var $a = $(a), $b = $(b);
        var e1 = Clouds.SpanHelper.elemOfSpan($a);
        var e2 = Clouds.SpanHelper.elemOfSpan($b);
        var cat = Clouds.SpanHelper.catOfSpan($a);
        var dbOrder = Clouds.data.categories[cat].indexOf(e1) - Clouds.data.categories[cat].indexOf(e2);
        if (Clouds.settings.hasOwnProperty('sizing') &&
                Clouds.settings['sizing'].hasOwnProperty(cat) &&
                !Clouds.settings['sizing'][cat] in ['links', 'recency']) {
            // fallback: sort by order of server (unspecified)
            return dbOrder;
        }
        // sort by descending weight
        var weightOrder = Clouds.weights[cat][e2] - Clouds.weights[cat][e1];
        if (weightOrder != 0) return weightOrder;
        else return dbOrder;
    };

    Clouds._pendingOrdering = {};

    Clouds.orderLinkLists = function (category) {
        if (Clouds.settings.hasOwnProperty('display-type')) {
            for (var cat in Clouds.settings['display-type']) {
                if (category != null && cat != category) continue; // this is not efficient but easier to read
                if (!Clouds.settings['display-type'].hasOwnProperty(cat)) continue;
                if (Clouds.settings['display-type'][cat] === 'links') {
                    var $cloud = Clouds.SpanHelper.contextOfCat(cat).find('.cloud');
                    // don't reorder lists with elements that are dragged
                    if ($cloud.hasClass('item-dragging')) continue;
                    var $container = $cloud.find('.row');
                    var $unordered = $container.find('.link-elem');
                    if (!Clouds._pendingOrdering.hasOwnProperty(cat)) {
                        Clouds._pendingOrdering[cat] = $unordered;
                    }
                    var latestOrdering = Clouds._pendingOrdering[cat];
                    var ordered = $container.find('.link-elem').slice().sort(function (a, b) {
                        var $a = $(a), $b = $(b);
                        var classOrder = ['selected', 'search-hit', 'linked'];
                        for (var i = 0; i < classOrder.length; i++) {
                            var cls = classOrder[i];
                            if ($a.hasClass(cls) && !$b.hasClass(cls))
                                return -1; // $a left of $b
                            if (!$a.hasClass(cls) && $b.hasClass(cls))
                                return 1; // $b left of $a
                        }
                        return Clouds._settingsLinkListComparator($a, $b);
                    });
                    // check if anything to reorder
                    if (latestOrdering.length == ordered.length && latestOrdering.toArray().every(function(e, i) { return e == ordered[i]; })) continue;
                    Clouds._pendingOrdering[cat] = ordered;
                    (function ($unordered, $container, $cloud, cat) {
                        $container.fadeOut(150, function () {
                            $unordered.detach();
                            Clouds._pendingOrdering[cat].appendTo($container);
                            $container.show().css('opacity', 0).animate({opacity: 1, scrollTop: 0}, 250);
                            $cloud.trigger('linkListReordered');
                            // $container[0].getBoundingClientRect();
                            // $container.scrollTop(0);
                        });
                    })($unordered, $container, $cloud, cat);
                }
            }
        }
    };

    Clouds.unpushState = function (hard) {
        if (hard == null) hard = false;
        if (window.history.state && window.history.state['pushed']) {
             if (hard) window.history.back();
             else window.history.replaceState({pushed: true}, document.title,
                                              window.location.pathname + window.location.search);
             return true;
        }
        return false;
    };

    Clouds.unselectAll = function () {
        Clouds.leaveTemporalMultiSelectState();
        Clouds.unshowRecentlyChanged();
        Clouds.currentlySelected = [];
        $(".cloud .cloud-elem").removeClass("selected");
        Clouds.highlightLinkedSpans(null);
        Clouds.Buttons.State.updateButtons();
        Clouds.unpushState();
    };

    Clouds.spanClick = function (span, updateSelected, event, manualClick) {
        if (event != null) {
            // check if command+click on mac or or ctrl+click on other was
            // used. If so, enter temporal multi-select mode.
            var isMac = /mac os x/i.test(navigator.userAgent);
            var multiModKey = !isMac && (event.ctrlKey || event.shiftKey);
            var multiModKeyMac = isMac && (event.metaKey || event.shiftKey);
            if (event != null && (multiModKey || multiModKeyMac)) {
                if (Clouds.Buttons.State.currentState != Clouds.Buttons.State.States.MultiAnd &&
                    Clouds.Buttons.State.currentState != Clouds.Buttons.State.States.MultiOr) {
                    Clouds.inTemporalMultiSelectState = true;
                    Clouds.Buttons.State.setState(Clouds.Buttons.State.States.MultiOr);
                }
            } else if (Clouds.inTemporalMultiSelectState) {
                Clouds.leaveTemporalMultiSelectState();
            }
        }
        if (manualClick == null) manualClick = false;
        Clouds.unshowRecentlyChanged();
        if (updateSelected == null) { updateSelected = true; }
        Clouds.SearchBar.filter();
        span = $(span);
        if (span.hasClass("selected")) {
            Clouds.unselect(span, updateSelected);
        } else {
            switch (Clouds.Buttons.State.currentState) {
                case Clouds.Buttons.State.States.Single:
                    Clouds.selectSingle(span, updateSelected, manualClick);
                    break;
                case Clouds.Buttons.State.States.MultiAnd:
                case Clouds.Buttons.State.States.MultiOr:
                    Clouds.selectMulti(span, updateSelected, manualClick);
                    break;
                case Clouds.Buttons.State.States.Link:
                    if ($(".selected").length == 0) {
                        Clouds.selectSingle(span, updateSelected, manualClick);
                        break;
                    }
                    var e1_cat = Clouds.SpanHelper.catOfSpan($(".selected"));
                    var e1_elem = Clouds.SpanHelper.elemOfSpan($(".selected"));
                    var e2_cat = Clouds.SpanHelper.catOfSpan(span);
                    var e2_elem = Clouds.SpanHelper.elemOfSpan(span);
                    if (span.hasClass("linked")) {
                        API.delete.link(e1_cat, e1_elem, e2_cat, e2_elem, {
                            'success': function () {
                                span.removeClass("linked");
                            }
                        })
                    } else {
                        API.post.link(e1_cat, e1_elem, e2_cat, e2_elem, {
                            'success': function () {
                                span.addClass("linked");
                            }
                        })
                    }
                    break;
                case Clouds.Buttons.State.States.Move:
                    Clouds.unselectAll();
                    break;
            }
        }
    };


    Clouds.getDataForLinks = function(cat, elem, cb) {
        var res = [];
        for (var i=0; i < Clouds.data.links.length; i++) {
            var link = Clouds.data.links[i];
            if (cat == link[0][0] && elem == link[0][1] ||
                    cat == link[1][0] && elem == link[1][1]) {
                res.push(link);
            }
        }
        cb(res);
    };
    Clouds.highlightLinkedSpans = function(from_spans, union) {
        $(".cloud .cloud-elem").removeClass("linked");
        if (union == null) union =
            Clouds.Buttons.State.currentState ==
            Clouds.Buttons.State.States.MultiOr;
        var results = [];
        if (from_spans == null) from_spans = $(".cloud .selected");
        from_spans = $(from_spans);
        var done = 0;
        var todo = from_spans.length;
        if (from_spans.length > 0) {
            from_spans.each(function(i, span) {
                var result = [];
                var cat = Clouds.SpanHelper.catOfSpan(span);
                var elem = Clouds.SpanHelper.elemOfSpan(span);
                Clouds.getDataForLinks(cat, elem, function(data) {
                    for (var link in data) {
                        for (var member in data[link]) {
                            if (data[link][member][0] !== cat ||
                                data[link][member][1] !== elem) {
                                result.push(Clouds.SpanHelper.getSpanFromCatElem.apply(
                                    this, data[link][member]));
                            }
                        }
                    }
                    results.push(result);
                    done++;
                    if (done == todo) {
                        // join the results
                        var toLink = [];
                        if (union) {
                            $(results).each(function(i, result) {
                                $(result).each(function(i, linked) {
                                    if ($.inArray(linked, toLink) < 0) {
                                        toLink.push(linked);
                                    }
                                });
                            });
                        } else {
                            // intersection to calculate
                            $(results).each(function(i, result) {
                                if (i != 0) {
                                    toLink = $.grep(toLink, function(value) {
                                        for (var i=0; i < result.length; i++) {
                                            if (value.attr('id') == result[i].attr('id')) {
                                                return true
                                            }
                                        }
                                        return false;
                                        // This did not work because spans that seemed
                                        // to be equal weren't recognized as such:
                                        // return $.inArray(value, result) >= 0;
                                    });
                                } else {
                                    toLink = result;
                                }
                            });
                        }
                        $(toLink).each(function(i, elem) {
                            $(elem).addClass("linked");
                        });
                    }
                });
            });
        }
        Clouds.orderLinkLists();
    };


    Clouds.resizeEnd = function(){
        // Haven't resized in 200ms! => redraw cloud
        Clouds.drawClouds(function() {
            Clouds.restoreHighlighting();
        });
    };

    Clouds.resizeTimeout = null;
    $(window).resize(function(){
        clearTimeout(Clouds.resizeTimeout);
        Clouds.resizeTimeout = setTimeout(Clouds.resizeEnd, 200);
    });


    Clouds.Buttons = "Buttons" in Clouds ? Clouds.Buttons : {};

    Clouds.Buttons.State = "State" in Clouds.Buttons ? Clouds.State : {};
    Clouds.Buttons.State.States = {Single: 0, MultiAnd: 1, MultiOr: 2,
                                   Link: 3, Move: 4};
    Clouds.Buttons.State.currentState = Clouds.Buttons.State.States.Single;
    Clouds.Buttons.State.newStateCB = null;

    Clouds.Buttons.State._ClickHandler = function(btnSelector, toState) {
        var btn = $(btnSelector);
        if (btn.not(".active").length) {  // one or more matches are not active
            $(".toolbar .cloud-control").removeClass("active");
            btn.addClass("active");
            Clouds.Buttons.State.setState(toState);
        } else {
            $(".toolbar .cloud-control").removeClass("active");
            Clouds.Buttons.State.setState(Clouds.Buttons.State.States.Single);
        }
    };

    Clouds.Buttons.State.clickedMultiAndSelect = function() {
        Clouds.Buttons.State._ClickHandler(
            ".toolbar .cloud-control.multi-select-and",
            Clouds.Buttons.State.States.MultiAnd
        );
    };

    Clouds.Buttons.State.clickedMultiOrSelect = function() {
        Clouds.Buttons.State._ClickHandler(
            ".toolbar .cloud-control.multi-select-or",
            Clouds.Buttons.State.States.MultiOr
        );
    };

    Clouds.Buttons.State.clickedLinkMode = function() {
        Clouds.Buttons.State._ClickHandler(
            ".toolbar .cloud-control.link-mode",
            Clouds.Buttons.State.States.Link
        );
    };

    Clouds.Buttons.State.clickedMoveMode = function() {
        Clouds.Buttons.State._ClickHandler(
            ".toolbar .cloud-control.move-mode",
            Clouds.Buttons.State.States.Move
        );
    };


    Clouds.Buttons.State.setState = function(newState) {
        Clouds.unshowRecentlyChanged();
        var oldState = Clouds.Buttons.State.currentState;
        if (newState !== oldState) {
            var invalidNewStateError = Error("newState must be a string out of Clouds.Buttons.State.states or an integer in range of it's length.");
            if (typeof newState == 'string') {
                if (Clouds.Buttons.State.States.hasOwnProperty(newState)) {
                    Clouds.Buttons.State.currentState = Clouds.Buttons.State.States[newState];
                } else {
                    throw invalidNewStateError;
                }
            } else {
                var found = false;
                for (var key in Clouds.Buttons.State.States) {
                    if (!Clouds.Buttons.State.States.hasOwnProperty(key)) continue;
                    if (Clouds.Buttons.State.States[key] === newState) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    Clouds.Buttons.State.currentState = newState;
                } else {
                    throw invalidNewStateError;
                }
            }

            // calling updateButtons before callback to ensure the button states are
            // up to date for the callback and after it in case it changes the span
            // selection
            Clouds.Buttons.State.updateButtons();
            if (Clouds.Buttons.State.newStateCB) {
                Clouds.Buttons.State.newStateCB(newState, oldState);
            }
            Clouds.Buttons.State.updateButtons();
        }
    };

    Clouds.Buttons.State.updateButtons = function() {
        var selectedSpans = $(".cloud-cell .cloud-elem.selected");
        var ctx = selectedSpans.parents(".cloud-cell");
        $(".toolbar .cloud-control").removeClass("active");
        $(".edit-icons .rename").addClass("disabled");
        $(".edit-icons .delete").addClass("disabled");
        $(".edit-icons .new").addClass("disabled");
        $(".edit-icons .config-cloud").addClass("disabled");
        // .multi-select = .multi-select-or + .multi-select-and
        // none is disabled because they are available in every state...
        // $(".toolbar .cloud-control.multi-select").addClass("disabled");
        // $(".toolbar .cloud-control.multi-select-and").addClass("disabled");
        // $(".toolbar .cloud-control.multi-select-or").addClass("disabled");
        $(".toolbar .cloud-control.link-mode").addClass("disabled");
        $(".toolbar .cloud-control.move-mode").addClass("disabled");
        $("#add-cloud").parent("li").addBack().addClass("disabled");
        $(".toolbar .cloud-control .explanation").removeClass("visible");
        $("#main").sortable("disable");
        var dragSpans = $('.cloud-cell:not(.ui-sortable-helper) .cloud .cloud-elem.ui-draggable')
            .filter(function(){return $(this).draggable('instance')!=null});
        var dropSpans = $('.cloud-cell:not(.ui-sortable-helper) .cloud .cloud-elem.ui-droppable')
            .filter(function(){return $(this).droppable('instance')!=null});
        var dropClouds = $('.cloud-panel.ui-droppable')
            .filter(function(){return $(this).droppable('instance')!=null});
        dragSpans.draggable("disable");
        dropSpans.droppable("disable");
        dropClouds.droppable("disable");
        if (!Clouds.showingOldRevision()) {
            $(".edit-icons .new").removeClass("disabled");
            $(".edit-icons .config-cloud").removeClass("disabled");
            $("#add-cloud").parent("li").addBack().removeClass("disabled");
            dragSpans.draggable("enable");
            dragSpans.droppable("enable");
        }
        switch (Clouds.Buttons.State.currentState) {
            case Clouds.Buttons.State.States.Move:  // not available in RO mode
                $(".toolbar .cloud-control.move-mode").addClass("active");
                $(".toolbar .move-mode .explanation").addClass("visible");
                if (!Clouds.showingOldRevision()) {
                    $(".toolbar .cloud-control.multi-select").removeClass("disabled");
                    $(".toolbar .cloud-control.move-mode").removeClass("disabled");
                    $(".toolbar .cloud-control.link-mode").removeClass("disabled");
                    dropClouds.droppable("enable");
                }
                dropSpans.droppable("disable");
                $(".edit-icons .new").addClass("disabled");
                $(".edit-icons .config-cloud").addClass("disabled");
                $("#add-cloud").parent("li").addBack().addClass("disabled");
                $("#main").sortable("enable");
                break;
            case Clouds.Buttons.State.States.Single:
                if (!Clouds.showingOldRevision()) {
                    $(".toolbar .cloud-control.multi-select").removeClass("disabled");
                    if (!Clouds.editEnabled) return;
                    if (selectedSpans.length > 0) {
                        ctx.find(".edit-icons .rename").removeClass("disabled");
                        ctx.find(".edit-icons .delete").removeClass("disabled");
                    }
                    $(".toolbar .cloud-control.move-mode").removeClass("disabled");
                    $(".toolbar .cloud-control.link-mode").removeClass("disabled");
                }
                break;
            case Clouds.Buttons.State.States.MultiAnd:
            case Clouds.Buttons.State.States.MultiOr:
                var button;
                if (Clouds.Buttons.State.currentState == Clouds.Buttons.State.States.MultiAnd) {
                     button = $(".toolbar .cloud-control.multi-select-and");
                } else {
                     button = $(".toolbar .cloud-control.multi-select-or");
                }
                button.addClass("active");
                button.find(".explanation").addClass("visible");
                if (!Clouds.editEnabled) return;
                if (!Clouds.showingOldRevision()) {
                    $(".toolbar .cloud-control.move-mode").removeClass("disabled");
                    if (selectedSpans.length === 0) {
                        $(".toolbar .cloud-control.link-mode").removeClass("disabled");
                    } else if (selectedSpans.length === 1) {
                        ctx.find(".edit-icons .rename").removeClass("disabled");
                        ctx.find(".edit-icons .delete").removeClass("disabled");
                        // ctx.find(".toolbar .cloud-control.multi-select").removeClass("disabled");
                        $(".toolbar .cloud-control.link-mode").removeClass("disabled");
                    } else if (selectedSpans.length > 1) {
                        ctx.find(".edit-icons .delete").removeClass("disabled");
                    }
                }
                break;
            case Clouds.Buttons.State.States.Link:  // not available in RO mode
                $(".toolbar .cloud-control.link-mode").addClass("active");
                $(".toolbar .link-mode .explanation").addClass("visible");
                if (!Clouds.showingOldRevision()) {
                    ctx.find(".edit-icons .delete").removeClass("disabled");
                    $(".toolbar .cloud-control.link-mode").removeClass("disabled");
                    $(".toolbar .cloud-control.multi-select").removeClass("disabled");
                    $(".toolbar .cloud-control.move-mode").removeClass("disabled");
                }
                break;
        }
    };

    Clouds.Buttons._isLinkListModal = function(cat) {
        return (Clouds.settings.hasOwnProperty('display-type') &&
                Clouds.settings['display-type'].hasOwnProperty(cat) &&
                Clouds.settings['display-type'][cat] === 'links');
    };
    Clouds.Buttons.getModalCategory = function(event) {
        if (Clouds.SpanHelper.getContext(event.target)) {
            return Clouds.SpanHelper.catOfContext(event.target);
        } else {
            return null;
        }
    };
    Clouds.Buttons.getModalElement = function() {
        if (Clouds.currentlySelected.length == 1) {
            return Clouds.currentlySelected[0][1];
        } else {
            return null;
        }
    };
    Clouds.Buttons.getModalCatElem = function() {
        if (Clouds.currentlySelected.length) {
            return Clouds.currentlySelected;
        } else {
            return null;
        }
    };
    Clouds.Buttons.setModalCatElem = function(modal, cat, elem, catelem) {
        modal = $(modal);
        if (cat != null) {
            modal.find(".cat, .category").text(cat);
        }
        if (elem != null) {
            modal.find(".elem, .element").text(elem);
        }
        if (catelem != null) {
            var catelemText = catelem.map(function(e) {
                var $span = $('<span class="catelem-label">').append($('<b>').text(e[0]));
                return $('<div>').append($span.html($span.html() + ': ' + $('<div>').text(e[1]).html())).html();
            }).join(', ');
            modal.find(".catelem").html(catelemText);
        }
    };
    Clouds.Buttons.setSelected = function(modal) {
        var $modal = $(modal);
        $modal.find(".cur-selected").text("");
        $modal.find(".if-selected").hide();
        if (Clouds.currentlySelected.length) {
            var curSelectedText = Clouds.currentlySelected.map(function(e) {
                var $span = $('<span class="catelem-label">').append($('<b>').text(e[0]));
                return $('<div>').append($span.html($span.html() + ': ' + $('<div>').text(e[1]).html())).html();
            }).join(', ');
            modal.find(".cur-selected").html(curSelectedText);
            $modal.find(".if-selected").show();
        }
    };
    Clouds.Buttons.modalChanged = function(modal) {
        modal = $(modal);
        var data = modal.data();
        if (data == null || !data.hasOwnProperty('initData')) {
            console.warn('Called modalChanged for not initialized modal:');
            console.warn(modal);
            return true; // return true just to be safe
        }
        return data['initData'] != modal.find('input, select, textarea').serialize();
    };
    Clouds.Buttons.tryCloseModals = function () {
        var openModal = $('.modal.in');
        if ($('.modal-load-overlay').hasClass('active')) return false;
        if (openModal.length > 0) {
            for (var i=0; i < openModal.length; i++) {
                if (Clouds.Buttons.modalChanged(openModal[i])) return false;
            }
            for (var i=0; i < openModal.length; i++) {
                $(openModal[i]).modal('hide');
            }
        }
        return true;
    };
    Clouds.Buttons.closeModal = function () {
        var openModal = $('.modal.in');
        for (var i=0; i < openModal.length; i++) {
            $(openModal[i]).modal('hide');
        }
    };
    Clouds.Buttons.initModal = function(event, modal, cat, elems, catelems) {
        modal = $(modal);
        if (event != null) {
            cat = cat === undefined ? Clouds.Buttons.getModalCategory(event) : cat;
        }
        elems = elems === undefined ? Clouds.Buttons.getModalElement() : elems;
        catelems = catelems === undefined ? Clouds.Buttons.getModalCatElem() : catelems;
        Clouds.Buttons.setModalCatElem(modal, cat, elems, catelems);
        Clouds.Buttons.setSelected(modal);
        modal.find(".alert").hide();
        modal.find('.cloud-setting, .link-list-setting').show();
        if (cat != null) {
            if (Clouds.Buttons._isLinkListModal(cat)) modal.find('.cloud-setting').hide();
            else modal.find('.link-list-setting').hide();
        }
        modal.find('input[type="text"], textarea').each(function (i, e) {
            $(e).val($(this).prop("defaultValue") || "");
        });
        return [cat, elems];
    };
    Clouds.Buttons.modalSetupDone = function(modal) {
        modal.data('initData', modal.find('input, select, textarea').serialize());
        modal.modal();
    };
    Clouds.Buttons.errorModal = function(modal, msg) {
        modal.find(".alert").text(msg).slideDown(500);
    };

    Clouds.Buttons.addCloud = function (event) {
        var modal = $("#add-category-modal");
        var catelem = Clouds.Buttons.initModal(event, modal);
        modal.find("button.create").unbind("click").click(function() {
            var cat = modal.find('input[name="name"]').val();
            if (cat) {
                API.post.category(cat, {
                    'success': function() { modal.modal('hide'); },
                    'error': Clouds.Buttons.errorModal.bind(this, modal)
                });
            } else {
                Clouds.Buttons.errorModal(modal, "Please give the category a name!");
            }
            return false;
        });
        Clouds.Buttons.modalSetupDone(modal);
    };

    Clouds.Buttons._textLocalDefault = null;
    Clouds.Buttons.configCloud = function (event) {
        $("#advanced-cloud-settings-body").collapse('hide'); // init collapse advanced settings
        var modal = $("#config-category-modal");
        var catelem = Clouds.Buttons.initModal(event, modal);
        var isLinkList = Clouds.Buttons._isLinkListModal(catelem[0]);
        var nameInput = modal.find('input[name="name"]');
        var localSizingSelect = $("#local-sizing");
        var serverSizingSelect = $("#server-sizing");
        var localSizingVal, serverSizingVal;
        if (catelem[0] in Settings.getLocalSettings().sizing != null) {
            localSizingVal = Settings.getLocalSettings().sizing[catelem[0]];
            if (localSizingVal == null) { localSizingVal = "null"; }
        } else {
            localSizingVal = "null";
        }
        if (Settings.serverSettings != null &&
            catelem[0] in Settings.serverSettings.sizing) {
            serverSizingVal = Settings.serverSettings.sizing[catelem[0]];
            if (serverSizingVal == null) { serverSizingVal = "null"; }
        } else {
            serverSizingVal = "null";
        }
        // update selections
        var optionSelector = isLinkList ? 'option:not(.cloud-setting)' : 'option:not(.link-list-setting)';
        var localSizingSelectDefaultOpt = localSizingSelect.find(optionSelector+"[value='null']");
        if (Clouds.Buttons._textLocalDefault == null)
            Clouds.Buttons._textLocalDefault = localSizingSelectDefaultOpt.text();

        localSizingSelect.find('option').removeClass("current").prop('selected', false);
        localSizingSelect.find(optionSelector+'[value="'+localSizingVal+'"]').addClass("current").prop('selected', true);
        serverSizingSelect.find('option').removeClass("current").prop('selected', false);
        serverSizingSelect.find(optionSelector+'[value="'+serverSizingVal+'"]').addClass("current").prop('selected', true);

        if (serverSizingVal !== "null") {
            var curServerTxt = serverSizingSelect.find("option.current").text();
            localSizingSelectDefaultOpt.text(Clouds.Buttons._textLocalDefault + " (" + curServerTxt + ")");
        } else {
            // client side sizing default is by relevance (i.e. link count)
            var defaultSpecificationText = " ("+serverSizingSelect.find("option[value='"+Clouds.DEFAULT_SIZING+"']").text()+")";
            localSizingSelectDefaultOpt.text(Clouds.Buttons._textLocalDefault + defaultSpecificationText);
        }

        var displayTypeVal = Settings.serverSettings['display-type'][catelem[0]] || "null";
        var displayTypeSelect = $("#server-display-type");
        displayTypeSelect.find("option").removeClass("current");
        displayTypeSelect.val(displayTypeVal)
            .find("option[value='"+displayTypeVal+"']").addClass("current");


        // bind buttons
        modal.find("button.delete").unbind("click").click(function() {
            Clouds.Buttons.deleteCloud(event, catelem[0], function() {
                modal.modal('hide');
            });
            return false;
        });
        var saveSizingSetting = function(local, server) {
            if (local == null) local = false; if (server == null) server = false;
            if (local) Settings.saveLocal("sizing", [catelem[0], localSizingSelect.val()]);
            if (server) Settings.saveServer("sizing", [catelem[0], serverSizingSelect.val()]);
        };
        var saveDisplayTypeSetting = function () {
            Settings.saveServer("display-type", [catelem[0], displayTypeSelect.val()]);
        };
        modal.find("button.save-server-sizing").unbind("click").click(function() {
            saveSizingSetting(false, true);
            modal.modal('hide');
            return false;
        });
        modal.find("button.save-local-sizing").unbind("click").click(function() {
            saveSizingSetting(true);
            modal.modal('hide');
            return false;
        });
        modal.find("button.save-server-display-type").unbind("click").click(function() {
            saveDisplayTypeSetting();
            modal.modal('hide');
            return false;
        });
        modal.find("button.rename").unbind("click").click(function() {
            var new_name = nameInput.val();
            API.put.renameCategory(catelem[0], new_name, {
                'success': function() { modal.modal('hide'); },
                'error': Clouds.Buttons.errorModal.bind(this, modal)
            });
            return false;
        });
        nameInput.val(catelem[0]);
        Clouds.Buttons.modalSetupDone(modal);
    };
    Clouds.Buttons.deleteCloud = function(event, cat, yes_cb, no_cb) {
        var modal = $("#delete-category-modal");
        Clouds.Buttons.initModal(event, modal, cat);
        modal.find(".yes").unbind("click").click(function() {
            API.delete.category(cat, {
                'success': function() {
                    modal.modal('hide');
                    if (yes_cb) yes_cb();
                },
                'error': Clouds.Buttons.errorModal.bind(this, modal)
            });
            return false;
        });
        modal.find('.no').unbind('click').click(function() {
            if (no_cb) no_cb();
        });
        Clouds.Buttons.modalSetupDone(modal);
    };

    Clouds.Buttons.renameElement = function(event) {
        var modal = $("#rename-element-modal");
        var catelem = Clouds.Buttons.initModal(event, modal);
        var isLinkModal = Clouds.Buttons._isLinkListModal(catelem[0]);
        var nameInput = modal.find('input[name="name"]');
        var titleInput = modal.find('input[name="title"]');
        var urlInput = modal.find('input[name="url"]');
        modal.find("button.rename").unbind("click").click(function() {
            var newName = isLinkModal ? Clouds.Buttons._normalizeTitleUrlToString(titleInput.val(), urlInput.val()) : nameInput.val();
            API.put.renameElement(catelem[0], catelem[1], newName, {
                'success': function() {
                    Clouds.currentlySelected = [[catelem[0], newName]];
                    modal.modal('hide');
                },
                'error': Clouds.Buttons.errorModal.bind(this, modal)
            });
            return false;
        });
        if (isLinkModal) {
            var match = catelem[1].match(Clouds.LinkRegEx);
            if (match != null) {
                titleInput.val(match[1].replace(/\\([\[\]])/g, '$1'));
                urlInput.val(match[2]);
            } else {
                titleInput.val(catelem[1]);
            }
        } else {
            nameInput.val(catelem[1]);
        }
        Clouds.Buttons.modalSetupDone(modal);
    };

    Clouds.Buttons.deleteElement = function(event) {
        var modal = $("#delete-element-modal");
        var catelem = Clouds.Buttons.initModal(event, modal);
        modal.find(".yes").unbind("click").click(function() {
            var action = {'deletions': {'elements': []}};
            for (var i=0; i < Clouds.currentlySelected.length; i++) {
                action.deletions.elements.push(Clouds.currentlySelected[i]);
            }
            API.post.bulkaction(action, {
                'success': function() { modal.modal('hide'); },
                'error': Clouds.Buttons.errorModal.bind(this, modal)
            });
            return false;
        });
        Clouds.Buttons.modalSetupDone(modal);
    };

    Clouds.Buttons.addElementNoCategory = function() {
        var catsAvail = Object.keys(Clouds.data.categories);
        var $modal = $("#new-element-no-category-modal");
        Clouds.Buttons.initModal(null, $modal);
        var $selectBtn = $modal.find(".select");
        var $input = $modal.find('input[name="name"]');
        $input.typeahead({
            source: catsAvail,
            minLength: 0,
            showHintOnFocus: true
        });
        $modal.off('shown.bs.modal.initCatVal').on('shown.bs.modal.initCatVal', function() {
            $input.typeahead('lookup');
            $input.val(catsAvail[0]);
        });
        $selectBtn.unbind('click').bind('click', function() {
            var val = $input.val();
            var ctx = Clouds.SpanHelper.contextOfCat(val);
            if (ctx.length) {
                var addBtn = ctx.find('.edit-icons .new:not(.disabled)');
                if (addBtn.length) {
                    $modal.off('hidden.bs.modal.clickAddBtn').on('hidden.bs.modal.clickAddBtn', function() {
                        addBtn.click();
                        $modal.off('hidden.bs.modal.clickAddBtn');
                    });
                    $modal.modal('hide');
                } else {
                    Clouds.Buttons.errorModal($modal, "Cannot add element to that cloud right now.");
                }
            } else {
                Clouds.Buttons.errorModal($modal, "Unknown cloud");
            }
        });
        Clouds.Buttons.modalSetupDone($modal);
    };

    Clouds.Buttons._titleTabRegEx = /^(.+)\t([^\t]+)$/;
    Clouds.Buttons._splitLink = function(link) {
        var match = link.match(Clouds.LinkRegEx);
        if (match == null) return null;
        var title = match[1].replace(/\\([\[\]])/g, "$1");
        var url = decodeURIComponent(match[2]);
        return [title, url]
    };
    Clouds.Buttons._buildLink = function(title, url) {
        title = title.replace(/([\[\]])/g, "\\$1");
        url = url.replace(/\)/g, "%29");
        url = url.replace(/\(/g, "%28");
        return '['+title+']('+url+')';
    };
    Clouds.Buttons._normalizeTitleUrlToString = function(title, url) {
        if (title.match(Clouds.LinkRegEx) != null && url == "") return title;
        if (url.match(Clouds.LinkRegEx) != null && title == "") return url;
        var match = title.match(Clouds.Buttons._titleTabRegEx);
        if (match != null && url == "") return Clouds.Buttons._buildLink(match[1], match[2]);
        match = url.match(Clouds.Buttons._titleTabRegEx);
        if (match != null && title == "") return Clouds.Buttons._buildLink(match[1], match[2]);
        return Clouds.Buttons._buildLink(title, url);
    };
    Clouds.Buttons._normalizeTitleUrlToArray = function(title, url) {
        var str = Clouds.Buttons._normalizeTitleUrlToString(title, url);
        var match = str.match(Clouds.LinkRegEx);
        return [match[1], match[2]];
    };
    Clouds.Buttons.addElement = function(event, cat, insertText) {
        var modal = $("#new-element-modal");
        var catelem = Clouds.Buttons.initModal(event, modal, cat);
        modal.find('.bulk-list li:not(.example)').remove();
        var toAdd = [];
        var bulkListMarker = modal.find('input[name="bulk-list-marker"]');
        bulkListMarker.val("");
        var nameField = modal.find('textarea[name="name"]');
        var titleField = modal.find('textarea[name="title"]');
        var urlField = modal.find('textarea[name="url"]');
        var addButton = modal.find('button.add');
        var addLinkButton = modal.find('button.add-link-elem');
        nameField.typeahead('destroy');
        titleField.typeahead('destroy');
        urlField.typeahead('destroy');
        modal.find("button.create").unbind("click").click(function() {
            if (nameField.val() != "") {
                // add recent element before submitting
                addButton.click();
            }
            if (titleField.val() != "" || urlField.val() != "") {
                addLinkButton.click();
            }
            if (toAdd.length == 0) { return; }
            var link = modal.find('input[name="add-link"]:visible');
            link = link.length > 0 ? link.prop("checked") : false;
            var links = [];
            var i;
            if (link) {
                // creating a link to a non existent element will create that
                // element as well
                for (i=0; i < toAdd.length; i++) {
                    for (var j=0; j < Clouds.currentlySelected.length; j++) {
                        links.push([[catelem[0], toAdd[i]],
                                    [Clouds.currentlySelected[j][0], Clouds.currentlySelected[j][1]]]);
                    }
                }
            }
            var elems = [];
            for (i=0; i < toAdd.length; i++) {
                elems.push([catelem[0], toAdd[i]]);
            }
            API.post.bulkaction({'insertions': {"elements": elems,
                                                "links": links}}, {
                'success': function() {
                    // make newly added element selected
                    if (toAdd.length > 1) {
                        Clouds.inTemporalMultiSelectState = true;
                        Clouds.Buttons.State.setState(Clouds.Buttons.State.States.MultiAnd);
                    }
                    Clouds.currentlySelected = toAdd.map(function(e) { return [catelem[0], e] });
                    $(".cloud .cloud-elem").removeClass("selected");
                    modal.modal('hide');
                    // If all entered elements already exist in the db no event will
                    // be triggered. In that case the element selection must be visualized
                    // manually.
                    Clouds.restoreHighlighting();
                },
                'error': Clouds.Buttons.errorModal.bind(this, modal)
            });
            return false;
        });
        addButton.unbind('click').click(function() {
            var name = nameField.val().replace(/(^\s+)|(\s+$)/g, "");
            if (name.replace(/\s/g, "") == "") {
                return;
            }
            nameField.val("").trigger("input").trigger("change").focus();
            toAdd.push(name);
            bulkListMarker.val('changed');
            var li = modal.find('.bulk-list > li.example')
                .clone().removeClass('example').appendTo('.bulk-list');
            li.find('.bulk-element-label').text(name);
            li.find('button.delete').click(function () {
                li.fadeOut(200, function(){li.remove()});
                toAdd.splice(toAdd.indexOf(name), 1);
                if (toAdd.length == 0) bulkListMarker.val('');
            });
        });
        addLinkButton.unbind('click').click(function() {
            var title = titleField.val().replace(/(^\s+)|(\s+$)/g, "");
            var url = urlField.val().replace(/(^\s+)|(\s+$)/g, "");
            if ((title+url).replace(/\s/g, "") == "") {
                return;
            }
            urlField.val("").trigger("input").trigger("change");
            titleField.val("").trigger("input").trigger("change").focus();
            var name = Clouds.Buttons._normalizeTitleUrlToString(title, url);
            toAdd.push(name);
            bulkListMarker.val('changed');
            var li = modal.find('.bulk-list > li.example')
                .clone().removeClass('example').appendTo('.bulk-list');
            li.find('.bulk-element-label').text(name);
            li.find('button.delete').click(function () {
                li.fadeOut(200, function(){li.remove()});
                toAdd.splice(toAdd.indexOf(name), 1);
                if (toAdd.length == 0) bulkListMarker.val('');
            });
        });
        var filterNewline = function () {
            $(this).val($(this).val().replace(/\n/g, " "));
        };
        var nameFieldPaste = function (text, force) {
            if (force == null) force = false;
            if (text.search(/[^\n]\n+[^\n]/) >= 0 || force) {
                var items = text.split("\n");
                for (var i = 0; i < items.length; i++) {
                    nameField.val(items[i]);
                    addButton.click();
                }
                return true;
            }
            return false;
        };
        var backspaceStates = Object.freeze({NOT_STARTED: 0, STARTED_EMPTY: 1, STARTED_NON_EMPTY: 2, STARTED: 3});
        var backspaceState = backspaceStates.NOT_STARTED;
        nameField.unbind('keyup').bind('keyup', function(event) {
            var val = $(this).val();
            if (event.which == 8) backspaceState = backspaceStates.NOT_STARTED;
            else if (event.which == 13) {
                var $typeahead = $(this).data('typeahead');
                if ($typeahead.shown && $typeahead.$menu.find('.active').length > 0) {
                    // ignore enter to select typeahead suggestion
                    return;
                }
                if (val.replace(/\s/g, "") != "") {
                    $(this).parent().find('.btn').click();
                } else {
                    // no further input => submit
                    modal.find('button.create').click();
                }
                event.preventDefault();
            }
        }).unbind('keydown').bind('keydown', function(event) {
            var val = $(this).val();
            if (event.which == 8) {
                if (backspaceState === backspaceStates.NOT_STARTED) {
                    if (val === "") {
                        backspaceState = backspaceStates.STARTED_EMPTY;
                        modal.find('.bulk-list li:last-child button.delete').click();
                    } else {
                        backspaceState = backspaceStates.STARTED_NON_EMPTY;
                    }
                } else if (backspaceState === backspaceStates.STARTED_EMPTY) {
                    modal.find('.bulk-list li:last-child button.delete').click();
                }
            }
        }).off('paste').on('paste', function (e) {
            // copied from http://stackoverflow.com/questions/6902455/how-do-i-capture-the-input-value-on-a-paste-event#30496488
            // common browser -> e.originalEvent.clipboardData
            // uncommon browser -> window.clipboardData
            var clipboardData = e.clipboardData || e.originalEvent.clipboardData || window.clipboardData;
            var val = clipboardData.getData('text');
            if (nameFieldPaste(val)) e.preventDefault();
        });
        modal.find('textarea').off('keydown keypress keyup input change', filterNewline)
                              .on('keydown keypress keyup input change', filterNewline);
        var linkPaste = function(text, force) {
            if (force == null) force = false;
            var title = titleField.val();
            var url = urlField.val();
            if (text.search(/[^\n]\n+[^\n]/) >= 0 || force) {
                var items = text.split("\n");
                urlField.val("");
                for (var i=0; i < items.length; i++) {
                    var titleUrl = Clouds.Buttons._normalizeTitleUrlToArray(items[i], "");
                    titleField.val(titleUrl[0]);
                    urlField.val(titleUrl[1]);
                    addLinkButton.click();
                }
                titleField.val(title);
                urlField.val(url);
                return true;
            }
            return false;
        };
        titleField.unbind('keyup').bind('keyup', function(event) {
            var val = $(this).val();
            if (event.which == 8) backspaceState = backspaceStates.NOT_STARTED;
            else if (event.which == 13) {
                var $typeahead = $(this).data('typeahead');
                if ($typeahead.shown && $typeahead.$menu.find('.active').length > 0) {
                    // ignore enter to select typeahead suggestion
                    return;
                }
                if (val.replace(/\s/g, "") != "") {
                    urlField.focus();
                } else if (event.which) {
                    // no further input => submit
                    modal.find('button.create').click();
                }
                event.preventDefault();
            }
        }).unbind('keydown').bind('keydown', function(event) {
            var val = $(this).val();
            if (event.which == 8) {
                if (backspaceState === backspaceStates.NOT_STARTED) {
                    if (val === "") {
                        backspaceState = backspaceStates.STARTED_EMPTY;
                        modal.find('.bulk-list li:last-child button.delete').click();
                    } else {
                        backspaceState = backspaceStates.STARTED_NON_EMPTY;
                    }
                } else if (backspaceState === backspaceStates.STARTED_EMPTY) {
                    modal.find('.bulk-list li:last-child button.delete').click();
                }
            } else if (event.which == 9 && !event.shiftKey) {
                 urlField.focus();
                 event.preventDefault();
            }
        });
        urlField.unbind('keyup').bind('keyup', function(event) {
            var val = $(this).val();
            if (event.which == 8) backspaceState = backspaceStates.NOT_STARTED;
            else if (event.which == 13) {
                var $typeahead = $(this).data('typeahead');
                if ($typeahead.shown && $typeahead.$menu.find('.active').length > 0) {
                    // ignore enter to select typeahead suggestion
                    return;
                }
                if (val.replace(/\s/g, "") != "") {
                    $('.btn.add-link-elem').click();
                } else {
                    if (titleField.val().replace(/\s/g, "") != "") {
                        $('.btn.add-link-elem').click();
                    } else {
                        // no further input => submit
                        modal.find('button.create').click();
                    }
                }
                event.preventDefault();
            }
        }).unbind('keydown').bind('keydown', function(event) {
            var val = $(this).val();
            if (event.which == 8) {
                if (backspaceState === backspaceStates.NOT_STARTED) {
                    if (val === "") titleField.focus();
                    backspaceState = backspaceStates.STARTED;
                }
            } else if (event.which == 9 && event.shiftKey) {
                 titleField.focus();
                 event.preventDefault();
            }
        });
        modal.find('textarea[name="title"], textarea[name="url"]')
            .off('paste').on('paste', function (e) {
            var clipboardData = e.clipboardData || e.originalEvent.clipboardData || window.clipboardData;
            var val = clipboardData.getData('text');
            if (linkPaste(val)) e.preventDefault();
        });

        var typeaheadOptions = {
            minLength: 2,
            fitToElement: true,
            delay: 250,
            autoSelect: false,
            selectOnBlur: false,
            matcher: function () { return true; },
            source: function (query, cb) {
                API.get.search(query, {
                    cat: catelem[0],
                    fuzzthresh: 40,
                    start: false,
                    stop: false,
                    success: function(data) {
                        for (var i=0; i < data.length; i++) {
                            data[i] = data[i][1]
                        }
                        cb(data);
                    }
                });
            },
            // overwriting updater and afterSelect to make sure typeahead does
            // not change .text / defaultValue. It's used to empty the text fields
            // on modal init
            updater: function (item) {
                this.$element.data('tmp-default-value', this.$element.prop('defaultValue'));
                this.$element.val(item);
                return item;
            },
            afterSelect: function () {
                this.$element.val(this.$element.data('tmp-default-value'));
                this.$element.removeData('tmp-default-value');
            },
            select: function () {
                var val = this.$menu.find('.active').data('value');
                if (val == null) return;
                this.updater(val);
                // this.$element.val(this.displayText(val) || val);
                this.$element.parents('.modal-body').find('.btn').click();
                this.afterSelect(val);
                return this.hide();
            }
        };

        nameField.typeahead(typeaheadOptions);

        typeaheadOptions['afterSelect'] = function () {
            this.$element.text(this.$element.data('tmp-default-value'));
            this.$element.removeData('tmp-default-value');
            if (this.$element[0] === urlField[0]) {
                // typeahead will force the focus to urlField after we're done here
                // but want it to be at titleField
                this.$element.one('focus', function() {
                    titleField.focus();
                })
            }
        };
        typeaheadOptions['fitToElement'] = false;

        function commonUpdater(item) {
            // if (item == null) return [null, null];
            this.$element.data('tmp-default-value', this.$element.prop('defaultValue'));
            var titleUrl = Clouds.Buttons._splitLink(item);
            if (titleUrl == null) titleUrl = [item, ''];
            titleField.val(titleUrl[0]);
            urlField.val(titleUrl[1]);
            return titleUrl;
        }
        typeaheadOptions['updater'] = function (item) { return commonUpdater.call(this, item)[0] };
        titleField.typeahead(typeaheadOptions);
        typeaheadOptions['updater'] = function (item) { return commonUpdater.call(this, item)[1] };
        urlField.typeahead(typeaheadOptions);
        // monkey patch typeahead.show to establish custom layout
        // i.e. dropdown should span over both title and url fields
        $([titleField.data('typeahead'), urlField.data('typeahead')]).each(function(i, typeahead) {
            typeahead.titleUrlWidthMonkeyShow = typeahead.show;
            typeahead.show = function () {
                // make sure dropdown is placed beneath title field
                var $oldElem = typeahead.$element;
                typeahead.$element = titleField;
                typeahead.titleUrlWidthMonkeyShow.apply(typeahead, arguments);
                typeahead.$element = $oldElem;
                // set width to span both input fields
                var width = urlField.offset().left + urlField.outerWidth() - titleField.offset().left;
                this.$menu.css("width", width + "px");
            };
        });

        // custom tab action
        $([nameField, titleField, urlField]).each(function (i, $elem) {
            var typeahead = $elem.data('typeahead');
            $elem.off('keyup.bootstrap3Typeahead').on('keyup.bootstrap3Typeahead', function(event) {
                if (event.which != 9) {
                    typeahead.keyup(event);
                } else {
                    event.preventDefault();
                    typeahead.hide();
                }
            }).off('keydown.bootstrap3Typeahead').on('keydown.bootstrap3Typeahead', function(event) {
                if (event.which != 9) {
                    typeahead.keydown(event);
                } else {
                    event.preventDefault();
                }
            });
        });

        if (insertText != null) {
            if (Clouds.Buttons._isLinkListModal(catelem[0])) {
                linkPaste(insertText, true);
            } else {
                nameFieldPaste(insertText, true);
            }
        }

        Clouds.Buttons.modalSetupDone(modal);
    };

    Clouds.Buttons.selectMultiAnd = function() {
        Clouds.Buttons.State.clickedMultiAndSelect();
    };

    Clouds.Buttons.selectMultiOr = function() {
        Clouds.Buttons.State.clickedMultiOrSelect();
    };

    Clouds.Buttons.linkMode = function() {
        Clouds.Buttons.State.clickedLinkMode();
    };

    Clouds.Buttons.moveMode = function() {
        Clouds.Buttons.State.clickedMoveMode();
    };

    Clouds.Buttons.eventListeners = {
        toggleTimeSliderClick: function () {
            var vb = $('#versions-toolbox');
            if (!vb.hasClass('collapse')) {
                vb.addClass('collapse');
                Clouds.showLatestTimestamp();
                $('.time-mode').removeClass('active').find('.explanation').removeClass('visible');
            } else {
                vb.removeClass('collapse');
                $('.time-mode').addClass('active').find('.explanation').addClass('visible');
            }
        },
        configCloudClick: function(event) {
            Clouds.Buttons.configCloud(event);
            return false;
        },
        renameElementClick: function(event) {
            Clouds.Buttons.renameElement(event);
            return false;
        },
        deleteElementClick: function(event) {
            Clouds.Buttons.deleteElement(event);
            return false;
        },
        addElementClick: function(event) {
            Clouds.Buttons.addElement(event);
            return false;
        },
        selectMultiAndClick: function(event) {
            Clouds.Buttons.selectMultiAnd(event);
        },
        selectMultiOrClick: function(event) {
            Clouds.Buttons.selectMultiOr(event);
        },
        linkModeClick: function(event) {
            Clouds.Buttons.linkMode(event);
            return false;
        },
        moveModeClick: function(event) {
            Clouds.Buttons.moveMode(event);
            return false;
        },
        addCloudClick: function(event) {
            Clouds.Buttons.addCloud(event);
            return false;
        },
        modalFocus: function () {
            $(this).find('.auto-focus').focus();
        },
        modalSubmitGroupWithEnter: function(event) {
            if (event.which == 13) {
                // enter key pressed => submit
                var btn = $(this).parents('.input-group, .form-group').find('.btn');
                if (btn.length) {
                    btn.click();
                    return false;
                }
            }
        },
        modalSubmitWithEnter: function(event) {
            if (event.which == 13) {
                // enter key pressed => submit
                var btn = $(this).find('.submit');
                if (btn.length) {
                    btn.click();
                    return false;
                }
            }
        },
        globalKeyupShortcuts: function(event) {
            var tagName = $(event.target).prop('tagName');
            if (['INPUT', 'TEXTAREA'].includes(tagName)) return;
            if ($('.modal-load-overlay').hasClass('active') || $('body').hasClass('modal-open')) return;
            switch(event.key) {
                case 'a':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    $(".multi-select-and").click();
                    return false;
                case 'o':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    $(".multi-select-or").click();
                    return false;
                case 'l':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    $(".link-mode").click();
                    return false;
                case 'm':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    $(".move-mode").click();
                    return false;
                case 'v':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    $(".time-mode").first().click();
                    return false;
                case 'r':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    if (Clouds.currentlySelected.length !== 1) return;
                    var ctx = Clouds.SpanHelper.contextOfCat(Clouds.currentlySelected[0][0]);
                    ctx.find('.edit-icons .rename').click();
                    return false;
                case 'Insert':
                case 'i':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    if (!Clouds.editEnabled || Clouds.showingOldRevision()) return;
                    Clouds.Buttons.addElementNoCategory();
                    return false;
                case 'Delete':
                case 'Backspace':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    $(".edit-icons .delete:not(.disabled)").first().click();
                    return false;
                case 'End':
                    if (event.altKey || event.ctrlKey || event.metaKey || !event.shiftKey) return;
                    if ($('#versions-toolbox').hasClass('collapse')) return;
                    Clouds.stepTimestamp(Infinity);
                    return false;
                case 'Home':
                    if (event.altKey || event.ctrlKey || event.metaKey || !event.shiftKey) return;
                    if ($('#versions-toolbox').hasClass('collapse')) return;
                    Clouds.stepTimestamp(-Infinity);
                    return false;
                case '?':
                case 'h':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    var cmds = [[["A", "O"], "Toggle Multiselect AND / OR"],
                                ["L", "Toggle Link Mode"],
                                ["M", "Toggle Move Mode"],
                                ["V", "Toggle Version Slider"],
                                [["&larr;", "&rarr;"], "Only with Version Slider: Previous / Next Version"],
                                [["Shift + &larr;", "&rarr;"], "Only with Version Slider: Step 10 Versions"],
                                [["Shift + Home", "End"], "Only with Version Slider: First / Latest Version"],
                                [["Insert", "I"], "Add new Element"],
                                [["Delete", "Backspace"], "Delete selected Element(s)"],
                                ["R", "Rename selected Element"],
                                [["Ctrl", "Shift + Click"], "Select multiple Elements"],
                                [["?", "H"], "Show this help"],
                                ["Esc", "Quit all Modes and Modals"]];
                    var msg = '<table class="table">';
                    for (var i=0; i < cmds.length; i++) {
                        var code = "";
                        if (Array.isArray(cmds[i][0])) {
                            code = cmds[i][0].map(function(x) { return '<code>'+x+'</code>' }).join(' / ');
                        } else {
                            code = '<code>'+cmds[i][0]+'</code>';
                        }
                        msg += '<tr>' +
                                 '<td>'+code+'</td>' +
                                 '<td>'+cmds[i][1]+'</td>' +
                               '</tr>';
                    }
                    msg += '</table>';
                    var closeHelpBind = function(e) {
                        $(this).modal('hide');
                        return false;
                    };
                    BootstrapModal({
                        title: "Help",
                        body: msg,
                        onShow: function() {
                            $(this).bind('keyup', closeHelpBind);
                        },
                        onHide: function() {
                            $(this).bind('keyup', closeHelpBind);
                        }
                    });
                    return false;
            }
        },
        globalKeydownShortcuts: function(event) {
            var tagName = $(event.target).prop('tagName');
            if (['INPUT', 'TEXTAREA'].includes(tagName)) return;
            switch(event.key) {
                case 'ArrowLeft':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    if ($('#versions-toolbox').hasClass('collapse') ||
                        $('.modal-load-overlay').hasClass('active')) return;
                    Clouds.stepTimestamp(event.shiftKey ? -10 : -1);
                    return false;
                case 'ArrowRight':
                    if (event.altKey || event.ctrlKey || event.metaKey) return;
                    if ($('#versions-toolbox').hasClass('collapse') ||
                        $('.modal-load-overlay').hasClass('active')) return;
                    Clouds.stepTimestamp(event.shiftKey ? 10 : 1);
                    return false;
            }
        }
    };


    Clouds.Buttons.setBindings = function() {
        $('.toolbar .time-mode')
            .off('click', Clouds.Buttons.eventListeners.toggleTimeSliderClick)
            .on('click', Clouds.Buttons.eventListeners.toggleTimeSliderClick);

        $(".config-cloud").off('click', Clouds.Buttons.eventListeners.configCloudClick)
                          .on('click', Clouds.Buttons.eventListeners.configCloudClick);
        $(".edit-icons .rename").off('click', Clouds.Buttons.eventListeners.renameElementClick)
                                .on('click', Clouds.Buttons.eventListeners.renameElementClick);
        $(".edit-icons .delete").off('click', Clouds.Buttons.eventListeners.deleteElementClick)
                                .on('click', Clouds.Buttons.eventListeners.deleteElementClick);
        $(".edit-icons .new").off('click', Clouds.Buttons.eventListeners.addElementClick)
                             .on('click', Clouds.Buttons.eventListeners.addElementClick);
        $(".toolbar .multi-select-and").off('click', Clouds.Buttons.eventListeners.selectMultiAndClick)
                                       .on('click', Clouds.Buttons.eventListeners.selectMultiAndClick);
        $(".toolbar .multi-select-or").off('click', Clouds.Buttons.eventListeners.selectMultiOrClick)
                                      .on('click', Clouds.Buttons.eventListeners.selectMultiOrClick);
        $(".toolbar .link-mode").off('click', Clouds.Buttons.eventListeners.linkModeClick)
                                .on('click', Clouds.Buttons.eventListeners.linkModeClick);
        $(".toolbar .move-mode").off('click', Clouds.Buttons.eventListeners.moveModeClick)
                                .on('click', Clouds.Buttons.eventListeners.moveModeClick);
        $("#add-cloud").off('click', Clouds.Buttons.eventListeners.addCloudClick)
                       .on('click', Clouds.Buttons.eventListeners.addCloudClick);

        $('.modal').off('shown.bs.modal', Clouds.Buttons.eventListeners.modalFocus)
                   .on('shown.bs.modal', Clouds.Buttons.eventListeners.modalFocus)
                   .filter(':not(#new-element-modal)')
                   .off('keypress', Clouds.Buttons.eventListeners.modalSubmitWithEnter)
                   .on('keypress', Clouds.Buttons.eventListeners.modalSubmitWithEnter)
                   .find('input[type="text"], textarea')
                   .off('keypress', Clouds.Buttons.eventListeners.modalSubmitGroupWithEnter)
                   .on('keypress', Clouds.Buttons.eventListeners.modalSubmitGroupWithEnter);
        $(document).off('keyup', Clouds.Buttons.eventListeners.globalKeyupShortcuts)
                   .on('keyup', Clouds.Buttons.eventListeners.globalKeyupShortcuts)
                   .off('keydown', Clouds.Buttons.eventListeners.globalKeydownShortcuts)
                   .on('keydown', Clouds.Buttons.eventListeners.globalKeydownShortcuts);
    };


    Clouds.InteractiveMode = "InteractiveMode" in Clouds ? Clouds.InteractiveMode : {};
    Clouds.InteractiveMode.INTERVAL = 120*1000;
    Clouds.InteractiveMode.BODY_PADDING_BOTTOM = 0;
    Clouds.InteractiveMode.timer = null;

    Clouds.InteractiveMode._show = function() {
        $(".interactive").finish().slideDown(500);
    };
    Clouds.InteractiveMode._hide = function(forceModalClose) {
        if (forceModalClose == null) forceModalClose = false;
        if (forceModalClose) Clouds.Buttons.closeModal();
        else if (!Clouds.Buttons.tryCloseModals()) return false;
        $(".interactive").finish().slideUp(300);
        Clouds.Buttons.State.setState(Clouds.Buttons.State.States.Single);
        Clouds.SearchBar.filter();
        Clouds.showLatestTimestamp();
        Clouds.unselectAll();
        return true;
    };
    Clouds.InteractiveMode._startTimer = function (forceModalClose) {
        if (Clouds.InteractiveMode.timer) window.clearTimeout(Clouds.InteractiveMode.timer);
        function tryHide() {
            if (!Clouds.InteractiveMode._hide(forceModalClose)) {
                Clouds.InteractiveMode.timer = window.setTimeout(
                    tryHide, Clouds.InteractiveMode.INTERVAL);
            }
        }
        Clouds.InteractiveMode.timer = window.setTimeout(
                tryHide,Clouds.InteractiveMode.INTERVAL);
    };

    Clouds.InteractiveMode.forceOn = function() {
        if (Clouds.InteractiveMode.timer) {
            $('body').trigger('clouds.demo-mode.stop');
            if (Clouds.editEnabled) Clouds.InteractiveMode._show();
            window.clearTimeout(Clouds.InteractiveMode.timer);
            Clouds.InteractiveMode._startTimer();
        } else {
            console.error("Start Interactive mode with 'Clouds.InteractiveMode.start()' " +
                "before calling forceOn or forceOff")
        }
    };
    Clouds.InteractiveMode.forceOff = function(forceModalClose) {
        if (Clouds.InteractiveMode.timer) {
            if (!Clouds.InteractiveMode._hide(forceModalClose)) return false;
            window.clearTimeout(Clouds.InteractiveMode.timer);
            $('body').trigger('clouds.demo-mode.start');
            return true;
        } else {
            console.error("Start Interactive mode with 'Clouds.InteractiveMode.start()' " +
                "before calling forceOn or forceOff");
            return false;
        }
    };

    Clouds.InteractiveMode._bound = false;
    Clouds.InteractiveMode.setBindings = function () {
        if (Clouds.InteractiveMode._bound) return;
        Clouds.InteractiveMode._bound = true;
    };

    Clouds.InteractiveMode.clickHandler = function(event) {
        if (!Clouds.isPseudoClickHandler(arguments)) {
            if (event.data == null || event.data.noInteractive != true) {
                Clouds.InteractiveMode.forceOn()
            }
        }
    };

    Clouds.InteractiveMode.start = function() {
        Clouds.InteractiveMode._startTimer();
        $(window).bind("click mouseup mousedown keyup keydown keypress",
            Clouds.InteractiveMode.clickHandler);
    };

    Clouds.InteractiveMode.start();


    Clouds.DemoMode = "DemoMode" in Clouds ? Clouds.DemoMode : {};
    Clouds.DemoMode.WAIT_INTERVAL = 5*1000; // interval between clicks in demo mode
    Clouds.DemoMode.INIT_INTERVAL = 10*1000; // interval to wait till entering demo mode initially
    Clouds.DemoMode.AFTER_ACTION_INTERVAL = Clouds.InteractiveMode.INTERVAL; // interval to wait till entering demo mode after first user action
    Clouds.DemoMode.interval = Clouds.DemoMode.INIT_INTERVAL;
    Clouds.DemoMode.idleTimer = null;
    Clouds.DemoMode.waitTimer = null;
    Clouds.DemoMode.onceActive = false;

    Clouds.DemoMode.clearIdleTimer = function() {
        window.clearTimeout(Clouds.DemoMode.idleTimer);
    };
    Clouds.DemoMode.setIdleTimer = function(interval) {
        if (interval == null) interval = Clouds.DemoMode.interval;
        Clouds.DemoMode.idleTimer = window.setTimeout(Clouds.DemoMode.active, interval);
    };
    Clouds.DemoMode.resetIdleTimer = function(interval) {
        Clouds.DemoMode.clearIdleTimer();
        Clouds.DemoMode.setIdleTimer(interval);
    };

    Clouds.DemoMode.SHOW_RECENT_PROB = 0.1;
    Clouds.DemoMode.active = function() {
        if ($(".cloud-cell:not(#example-cloud)").length === 0) {
            console.info("Demo mode opening add cloud modal.");
            // empty instance => open "add cloud" modal
            Clouds.pseudoTouch($('#add-cloud'));
            Clouds.InteractiveMode.start();
            return;
        }
        var clickableSpans = $(".cloud .cloud-elem:not(.selected), .link-cloud .link-elem");
        Clouds.DemoMode.clearIdleTimer();
        if (clickableSpans.length === 0) {
            console.info("Demo mode stopped because there are no items.");
            Clouds.DemoMode.waitTimer = window.setTimeout(Clouds.DemoMode.active, Clouds.DemoMode.WAIT_INTERVAL);
            return;
        }
        if (!Clouds.InteractiveMode.forceOff()) {
            console.info("Demo mode stopped because there are modified/undismissable modals.");
            Clouds.DemoMode.waitTimer = window.setTimeout(Clouds.DemoMode.active, Clouds.DemoMode.WAIT_INTERVAL);
            return;
        }
        // random click
        if (Clouds.data == null || Clouds.data.links.length == 0) {
            console.info('Demo mode stopped because there are no links');
            Clouds.DemoMode.waitTimer = window.setTimeout(Clouds.DemoMode.active, Clouds.DemoMode.WAIT_INTERVAL);
            return;
        }
        var wantToShowRecent = Math.random() <= Clouds.DemoMode.SHOW_RECENT_PROB;
        if (wantToShowRecent) {
            var recentSpans = [];
            for (var i = 0; i < ChangedCounter.recentlyChanged.length; i++) {
                if (ChangedCounter.recentlyChanged[i].length == 2) {
                    var cat = ChangedCounter.recentlyChanged[i][0];
                    var elem = ChangedCounter.recentlyChanged[i][1];
                    if (Clouds.currentlySelected.length &&
                            $.inArray([cat, elem], Clouds.currentlySelected)) {
                        continue;
                    }
                    recentSpans.push(Clouds.SpanHelper.getSpanFromCatElem(
                        ChangedCounter.recentlyChanged[i][0],
                        ChangedCounter.recentlyChanged[i][1]
                    ));
                }
            }
        }
        if (wantToShowRecent && recentSpans.length > 0) {
            var recentLink = $("#recently-changed-counter").find("a");
            Clouds.pseudoTouch(recentLink, function () {
                var randomSpan = recentSpans[Math.floor(Math.random() * recentSpans.length)];
                window.setTimeout(function () {
                    Clouds.pseudoTouch(randomSpan);
                    Clouds.DemoMode.waitTimer = window.setTimeout(Clouds.DemoMode.active, Clouds.DemoMode.WAIT_INTERVAL);
                }, 3000);
            });
        } else {
            var randomSpan = clickableSpans[Math.floor(Math.random()*clickableSpans.length)];
            if (randomSpan) {
                Clouds.pseudoTouch(randomSpan);
            }
            Clouds.DemoMode.waitTimer = window.setTimeout(Clouds.DemoMode.active, Clouds.DemoMode.WAIT_INTERVAL);
        }
    };

    Clouds.DemoMode.wait = function(interval) {
        Clouds.DemoMode.setIdleTimer(interval);
    };

    Clouds.DemoMode.init = function() {
        $(window).on("click mouseup mousedown keyup keydown keypress", function(event) {
            if (!Clouds.isPseudoClickHandler(arguments)){
                if (!Clouds.DemoMode.onceActive) {
                    Clouds.DemoMode.onceActive = true;
                    Clouds.DemoMode.interval = Clouds.DemoMode.AFTER_ACTION_INTERVAL;
                }
                if (Clouds.DemoMode.waitTimer) {
                    window.clearTimeout(Clouds.DemoMode.waitTimer);
                    Clouds.DemoMode.waitTimer = null;
                }
                Clouds.DemoMode.resetIdleTimer();
            }
        });
        Clouds.DemoMode.wait();
    };
    Clouds.DemoMode.init();


    Clouds.SearchBar = "SearchBar" in Clouds ? Clouds.SearchBar : {};
    Clouds.SearchBar._bound = false;
    Clouds.SearchBar.setBindings = function() {
        if (Clouds.SearchBar._bound) return;
        Clouds.SearchBar._bound = true;
        // focus search bar on Alt+Shift+F
        $(window).keydown(function(e) {
            if (e.keyCode === 70 && !e.altKey && !e.shiftKey && (e.metaKey ^ e.ctrlKey)) {
                // ctrl+f, cmd+f, sadly win+f as well
                Clouds.InteractiveMode.forceOn();
                $("#searchbar").focus();
                return false;
            }
            if (e.keyCode === 27 && $("#searchbar").is(':focus')) { // Esc key => clear search
                Clouds.SearchBar.filter();
                $("#searchbar").blur();
            }
        });

        $("#searchbar").bind('input', function() {
            Clouds.SearchBar.filter($(this).val());
        });
    };

    Clouds.SearchBar.escapeRegEx = function(str) {
      return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    };

    Clouds.SearchBar.restore = function() {
        Clouds.SearchBar.filter($("#searchbar").val());
    };

    Clouds.SearchBar.filter = function(text) {
        Clouds.unshowRecentlyChanged();
        if (text == null) {text = ""}
        if (text) {
            var escaped = Clouds.SearchBar.escapeRegEx(text);
            var query = escaped.toLowerCase();
            $(".cloud .cloud-elem").each(function(i, e) {
                e = $(e);
                if (e.text().toLowerCase().search(query) >= 0) {
                    e.addClass("search-hit")
                } else {
                    e.removeClass("search-hit")
                }
            });
        } else {
            $(".cloud .cloud-elem").removeClass("search-hit");
        }
        $("#searchbar").val(text);
        Clouds.orderLinkLists();
    };


    require(['domReady'], function (domReady) {
        domReady(function () {
            Clouds.main();
        });
    });
});
