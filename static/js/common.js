require.config({
    paths: {
        'bootstrap': 'libs/bootstrap',
        'bootstrap-modal': 'libs/bootstrap-modal-wrapper',
        'bootstrap-slider': 'libs/bootstrap-slider',
        'clock': 'libs/clock',
        'cookies': 'libs/js.cookie',
        'd3': 'libs/d3',
        'd3pie': 'libs/d3pie',
        'domReady': 'libs/domReady',
        'jquery': 'libs/jquery',
        'jquery.keyboard': 'libs/jquery.keyboard',
        'jquery-ui.raw': 'libs/jquery-ui',
        'jquery-ui-memfix': 'my_libs/jquery-ui-memfix',
        'jquery-ui.touch-punch': 'libs/jquery-ui.touch-punch',
        'jquery-ui': 'my_libs/jquery-ui.touch',
        'mathjax_raw': ['//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-AMS-MML_HTMLorMML',
                        'libs/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML'],
        'mathjax': 'my_libs/mathjax_wrapper',
        'moment': 'libs/moment',
        'socketio': 'libs/socketio',
        'typeahead': 'libs/bootstrap3-typeahead',
        'wordcloud': 'libs/wordcloud2',

        'api': 'my_libs/api',
        'changed_counter': 'my_libs/changed_counter',
        'clock_support': 'my_libs/clock_support',
        'color_generator': 'my_libs/color_generator',
        'custom_forces': 'my_libs/custom_forces',
        'keyboard_support': 'my_libs/keyboard_support',
        'settings': 'my_libs/settings',
        'user_counter': 'my_libs/user_counter'
    },
    shim : {
        "bootstrap" : { "deps": ['jquery'] },
        "bootstrap-switch" : { "deps": ['jquery', 'bootstrap'] },
        "clock": {
            "deps": ["jquery"],
            "exports": "clock"
        },
        'typeahead': { "deps": ['jquery', 'bootstrap'] },
        // Dirty hack to make sure jqueryUI is loaded first.
        // This forces bootstrap-slider to bind to an alternative name.
        // Dropping this will result in random import order thus random binding.
        "bootstrap-slider": { "deps": ['jquery-ui', 'jquery', 'bootstrap'] },
        "mathjax_raw": { "exports": "MathJax" }
    },
    baseUrl: '/js'
});

// common libs (so that they don't need to be included in each page's compiled
// js files)
require(['jquery', 'bootstrap', 'api']);
