require(['jquery'], function ($) {
    // disables all links that are disabled
    $('a.disabled, .disabled > a').bind('click', function (event) {
        event.preventDefault();
    });
});

require(['jquery', 'domReady', 'api', 'bootstrap'], function ($, domReady, api) {

    function updateQueryStringParameter(uri, key, value) {
      var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
      var separator = uri.indexOf('?') !== -1 ? "&" : "?";
      if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
      }
      else {
        return uri + separator + key + "=" + value;
      }
    }

    api.get.authStatus({
        start: null,
        stop: null,
        success: function(data) {
            if (data.readOnly && !data.loggedIn) {
                var $loginA = $('#login').show().find('a');
                $loginA.attr('href', $loginA.attr('href') + '?redir=' + encodeURIComponent(window.location.pathname));
            } else {
                if (data.readOnly) {
                    var logoutA = $('#logout').show().find('a');
                    logoutA.attr('href', logoutA.attr('href') + '?redir=' + encodeURIComponent(window.location.pathname));
                }
                api.get.token({
                    allowFail: true,
                    retries: 1,
                    start: null,
                    stop: null,
                    success: function(data) {
                        if (data == null) { return; }  // happens if instance has no password set
                        var logo_a = $('a.navbar-brand');
                        var uri = logo_a.attr('href');
                        logo_a.attr('href', updateQueryStringParameter(uri, 'auth', data));
                    }
                });
            }
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(document).on('keypress', '[role="button"]', function(e) {
        switch(e.which) {
            case 13: // enter
            case 32: //space
                $(this).click();
                return false;
        }
    });
});
