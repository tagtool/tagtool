/**
 * Created by rouven on 25.02.16.
 */

if (! document.implementation.hasFeature("http://www.w3.org/TR/SVG2/feature#GraphicsAttribute", 2.0) ) {
    alert('Your browser does not support SVG 2. That might look shitty!');
}

var RADIUS = 15;

Object.defineProperty(
  Object.prototype, 'isPartialObjectOf',
    {
        enumerable: false,
        value: function (obj) {
            if (typeof obj !== "object") {
                console.error("Called isPartialObjectOf with:", obj);
                return null;
            }
            for (var key in this) {
                if (this.hasOwnProperty(key)) {
                    if (!obj.hasOwnProperty(key)) {
                        return false;
                    }
                    if (typeof this[key] === "object") {
                        if (typeof obj[key] !== "object" ||
                            (this[key] !== obj[key] && !this[key].isPartialObjectOf(obj[key]))) {
                            return false;
                        }
                    } else if (this[key] !== obj[key]) {
                        return false;
                    }
                }
            }
            return true;
        }
    }
);

Array.prototype.indexOfPartialObject = function(obj) {
    if (typeof obj !== "object") {
        console.error("Called indexOfPartialObject with:", obj);
        return null;
    }
    for (var i=0; i < this.length; i++) {
        var cmp = this[i];
        if (typeof cmp === "object") {
            if (obj.isPartialObjectOf(cmp)) {
                return i;
            }
        }
    }
    return -1;
};

Array.prototype.containsPartialObject = function(obj) {
    return this.indexOfPartialObject(obj) >= 0;
};

require([
    'jquery',
    'api',
    'd3',
    'custom_forces',
    'user_counter',
    'color_generator',
    'bootstrap',
    'jquery-ui',
    'bootstrap-slider'],
    function ($, API, d3, CustomForces, UserCounter, ColorGenerator) {

    // utf-8 save versions of atob and btoa from
    // https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#The_Unicode_Problem
    btoa = (function(btoa) {
                function func(str) {
                    // first we use encodeURIComponent to get percent-encoded UTF-8,
                    // then we convert the percent encodings into raw bytes which
                    // can be fed into btoa.
                    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                        function toSolidBytes(match, p1) {
                            return String.fromCharCode(parseInt(p1, 16));
                        }));
                }
                return func;
            })(btoa);

    atob = (function(atob) {
                function func(str) {
                    // Going backwards: from bytestream, to percent-encoding, to original string.
                    return decodeURIComponent(atob(str).split('').map(function (c) {
                        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                    }).join(''));
                }
                return func;
            })(atob);

    // https://github.com/wbkd/d3-extended
    d3.selection.prototype.moveToFront = function() {
      return this.each(function(){
        this.parentNode.appendChild(this);
      });
    };
    d3.selection.prototype.moveToBack = function() {
        return this.each(function() {
            var firstChild = this.parentNode.firstChild;
            if (firstChild) {
                this.parentNode.insertBefore(this, firstChild);
            }
        });
    };

    function userCounterClick() {
        var active = $(this).find('.explanation').hasClass("visible");
        $(".counter .explanation").removeClass("visible");
        if (!active) {
            $(this).find('.explanation').addClass("visible");
        }
    }
    UserCounter.on('click', userCounterClick);


    var CategoryFocusForce = function() {
        var _strength = 1;

        var force = function (alpha) {
            for (var i=0; i < force._nodes.length; i++) {
                var node = force._nodes[i];
                var focus = Graphs.data.categories[node.category].center;
                node.vx += (focus.x-node.x)*alpha*_strength;
                node.vy += (focus.y-node.y)*alpha*_strength;
            }
        };

        force.initialize = function(nodes) {
            force._nodes = nodes;
        };

        force.strength = function (strength) {
            _strength = strength;
            return force;
        };

        return force;
    };

    var Graphs = {width: 0, height: 0};

    Graphs.main = function () {
        API.startCB = Graphs.startWaiting.bind(Graphs);
        API.stopCB = Graphs.stopWaiting.bind(Graphs);
        API.registerPollCB(Graphs.pollUpdate.bind(Graphs), 'diff');
        Graphs.setBindings();
        Graphs.initD3();
        API.startPollLoop();
    };

    Graphs.usingSimilarities = false;

    Graphs.setBindings = function () {

        Graphs.sliders = {
            chargeStrength: $('#slider-charge-strength'),
            linkStrength: $('#slider-link-strength'),
            collisionStrength: $('#slider-collision-strength'),
            focusStrength: $('#slider-focus-strength'),
            similarityStrength: $('#slider-similarity-strength'),
            all: $('.force-slider')
        };

        $(window).resize(function () {
            $("svg#canvas").resize();
        });

        $("svg#canvas").resize(function () {
            Graphs.width = $(this).width();
            Graphs.height = $(this).height();
            Graphs.Layout.forceLayout.force("center", d3.forceCenter(Graphs.width / 2, Graphs.height / 2));
            Graphs.Layout.forceLayout.force("boundingBox").options({width: Graphs.width, height: Graphs.height});
            Graphs.calcCatCenters();
            Graphs.Layout.forceLayout.alpha(.1);
            Graphs.Layout.forceLayout.restart();
            return false;
        });

        $('#similarity').on('change', function () {
            Graphs.usingSimilarities = this.checked;
            Graphs.Layout.forceLayout.alpha(1);
            Graphs.start();
            Graphs.Layout.forceLayout.restart();
        });

        $('#similarity-function').on('change', function () {
            if (Graphs.usingSimilarities) {
                Graphs.Layout.forceLayout.alpha(.3).restart();
            }
        });

        $('.open-graph-controls').click(function () {
            var gc = $('#graph-controls');
            if (!gc.hasClass('collapse')) {
                gc.addClass('collapse');
                $(window).trigger('resize');
                $('.open-graph-controls').removeClass('active').find('.explanation').removeClass('visible');
            } else {
                gc.removeClass('collapse');
                $(window).trigger('resize');
                $('.open-graph-controls').addClass('active').find('.explanation').addClass('visible');
            }
        });

        Graphs.sliders.chargeStrength.on('change', function (e) {
            var val = e.value.newValue;
            $('#'+$(this).attr('name')+'-value').text(val);
            Graphs.Layout.forceLayout.force("charge").force().strength(val);
        });
        Graphs.sliders.linkStrength.on('change', function (e) {
            var val = e.value.newValue;
            $('#'+$(this).attr('name')+'-value').text(val);
            if (!Graphs.usingSimilarities) {
                Graphs.Layout.forceLayout.force('link').strength(val);
            }
        });
        Graphs.sliders.collisionStrength.on('change', function (e) {
            var val = e.value.newValue;
            $('#'+$(this).attr('name')+'-value').text(val);
            Graphs.Layout.forceLayout.force("collide").force().strength(val);
        });
        Graphs.sliders.focusStrength.on('change', function (e) {
            var val = e.value.newValue;
            $('#'+$(this).attr('name')+'-value').text(val);
            Graphs.Layout.forceLayout.force("focus").strength(val);
        });
        Graphs.sliders.similarityStrength.on('change', function (e) {
            var val = e.value.newValue;
            $('#'+$(this).attr('name')+'-value').text(val);
            if (Graphs.usingSimilarities) {
                Graphs.Layout.forceLayout.force('link').strength(val);
            }
        });
        Graphs.sliders.all.on('slideStop', function (e) {
            Graphs.Layout.forceLayout.alpha(.3).restart();
        })
    };

    Graphs.startWaiting = function () {
        $('.modal-load-overlay').addClass('active');
    };

    Graphs.stopWaiting = function () {
        $('.modal-load-overlay').removeClass('active');
    };


    Graphs.data = {nodes: [], edges: [], categories: {}, similarities: []};

    Graphs.DataHelper = "DataHelper" in Graphs ? Graphs.DataHelper : {};
    Graphs.DataHelper.throw = function (err, msg) {
        if (err == null) err = "AssertionError";
        if (msg == null) msg = "Something went wrong sorry. We'll reload the page.";
        console.error(err, msg);
        alert(msg);
        window.setTimeout(function () {
            window.location.reload();
        }, 100000);
        throw msg;
    };
    Graphs.DataHelper.catElemToObj = function (cat, elem) {
        return {category: cat, text: elem};
    };
    Graphs.DataHelper.addCatToData = function (cat, calc_data) {
        if (calc_data == null) calc_data = false;
        if (cat in Graphs.data.categories) Graphs.DataHelper.throw("Category exists");
        Graphs.data.categories[cat] = {};
        if (calc_data) {
            Graphs.calcCatColors();
            Graphs.calcCatCenters();
        }
    };
    Graphs.DataHelper.addElemToData = function (cat, elem) {
        if (!cat in Graphs.data.categories) Graphs.DataHelper.throw("Category doesn't exist");
        var obj = Graphs.DataHelper.catElemToObj(cat, elem);
        if (Graphs.data.nodes.containsPartialObject(obj)) Graphs.DataHelper.throw("Element exists");
        Graphs.data.nodes.push(obj);
    };
    Graphs.DataHelper.addLinkToData = function (cat1, elem1, cat2, elem2) {
        var obj1 = Graphs.DataHelper.catElemToObj(cat1, elem1);
        var obj2 = Graphs.DataHelper.catElemToObj(cat2, elem2);
        var obj1_idx = Graphs.data.nodes.indexOfPartialObject(obj1);
        var obj2_idx = Graphs.data.nodes.indexOfPartialObject(obj2);
        if (!cat1 in Graphs.data.categories) Graphs.DataHelper.throw("Cat1 doesn't exist");
        if (!cat2 in Graphs.data.categories) Graphs.DataHelper.throw("Cat2 doesn't exist");
        if (obj1_idx < 0) Graphs.DataHelper.throw("Elem1 doesn't exist");
        if (obj2_idx < 0) Graphs.DataHelper.throw("Elem2 doesn't exist");
        var l1 = {
            source: Graphs.data.nodes[obj1_idx],
            target: Graphs.data.nodes[obj2_idx]
        };
        var l2 = {
            source: Graphs.data.nodes[obj2_idx],
            target: Graphs.data.nodes[obj1_idx]
        };
        if (Graphs.data.edges.containsPartialObject(l1) ||
            Graphs.data.edges.containsPartialObject(l2)) Graphs.DataHelper.throw("Link exists");
        Graphs.data.edges.push(l1);
    };
    Graphs.DataHelper.removeLinkFromData = function (cat1, elem1, cat2, elem2) {
        var obj1 = Graphs.DataHelper.catElemToObj(cat1, elem1);
        var obj2 = Graphs.DataHelper.catElemToObj(cat2, elem2);
        var obj1_idx = Graphs.data.nodes.indexOfPartialObject(obj1);
        var obj2_idx = Graphs.data.nodes.indexOfPartialObject(obj2);
        if (!cat1 in Graphs.data.categories) Graphs.DataHelper.throw("Cat1 doesn't exist");
        if (!cat2 in Graphs.data.categories) Graphs.DataHelper.throw("Cat2 doesn't exist");
        if (obj1_idx < 0) Graphs.DataHelper.throw("Elem1 doesn't exist");
        if (obj2_idx < 0) Graphs.DataHelper.throw("Elem2 doesn't exist");
        var l1 = {
            source: Graphs.data.nodes[obj1_idx],
            target: Graphs.data.nodes[obj2_idx]
        };
        var l2 = {
            source: Graphs.data.nodes[obj2_idx],
            target: Graphs.data.nodes[obj1_idx]
        };
        var l1_idx = Graphs.data.edges.indexOfPartialObject(l1);
        var l2_idx = Graphs.data.edges.indexOfPartialObject(l2);
        if (l1_idx >= 0) {
            Graphs.data.edges.splice(l1_idx, 1);
        } else if (l2_idx >= 0) {
            Graphs.data.edges.splice(l2_idx, 1);
        } else {
            Graphs.DataHelper.throw("Link doesn't exist");
        }
    };
    Graphs.DataHelper.removeElemFromData = function (cat, elem) {
        var obj = Graphs.DataHelper.catElemToObj(cat, elem);
        var obj_idx = Graphs.data.nodes.indexOfPartialObject(obj);
        if (!cat in Graphs.data.categories) Graphs.DataHelper.throw("Category doesn't exist");
        if (obj_idx < 0) Graphs.DataHelper.throw("Element doesn't exist");
        Graphs.data.nodes.splice(obj_idx, 1);
        // update link indices
        for (var i = 0; i < Graphs.data.edges.length; i++) {
            var edge = Graphs.data.edges[i];
            if (edge.source > obj_idx) edge.source -= obj_idx;
            if (edge.target > obj_idx) edge.target -= obj_idx;
        }
    };
    Graphs.DataHelper.removeCatFromData = function (cat, calc_data) {
        if (calc_data == null) calc_data = false;
        if (!cat in Graphs.data.categories) Graphs.DataHelper.throw("Category doesn't exist");
        delete Graphs.data.categories[cat];
        if (calc_data) {
            Graphs.calcCatColors();
            Graphs.calcCatCenters();
        }
    };


    Graphs.calcCatCenters = function() {
        var w = 1;
        var h = 1;
        var numOfCats = Object.keys(Graphs.data.categories).length;
        while (numOfCats > w*h) {
            if (Graphs.width/w > Graphs.height/h) w++;
            else h++;
        }
        var cats = Object.keys(Graphs.data.categories);
        var x_stride = Graphs.width/w;
        var y_stride = Graphs.height/h;

        // group of category centers
        Graphs.Layout.svg.select('#centers').remove();
        var g = Graphs.Layout.svg.append('g').attr('id', 'centers');

        for (var i=0; i < numOfCats; i++) {
            var cat = cats[i];
            var column = i % w;
            var row = Math.floor(i / w);
            var x = (column + 1) * x_stride - x_stride / 2;
            var y = (row + 1) * y_stride - y_stride / 2;
            Graphs.data.categories[cat].center = {x: x, y: y};
            // category centers
            g.append('circle').attr('class', 'cat-center').attr('r', 2).attr('cx', x).attr('cy', y).attr('style', 'fill: black;');
        }
    };

    Graphs.calcCatColors = function () {
        var todo = false;
        for (var key in Graphs.data.categories) {
            if (Graphs.data.categories[key] == null ||
                    Graphs.data.categories[key].color == null) {
                todo = true;
                break;
            }
        }
        if (!todo) {
            return;
        }
        var keys = Object.keys(Graphs.data.categories);
        var num_colors = keys.length;
        var colors = ColorGenerator(num_colors);
        for (var i=0; i < colors.length; i++) {
            Graphs.data.categories[keys[i]].color = colors[i];
        }
    };

    Graphs.updateSimilarities = function (cb) {
        API.get.similarities({success: function (data) {
            Graphs.data.similarities = [];
            for (var i=0; i < data.length; i++) {
                var obj1 = Graphs.DataHelper.catElemToObj(data[i][0][0], data[i][0][1]);
                var obj2 = Graphs.DataHelper.catElemToObj(data[i][1][0], data[i][1][1]);
                var obj1_idx = Graphs.data.nodes.indexOfPartialObject(obj1);
                var obj2_idx = Graphs.data.nodes.indexOfPartialObject(obj2);
                if (obj1_idx < 0) Graphs.DataHelper.throw("Elem1 doesn't exist");
                if (obj2_idx < 0) Graphs.DataHelper.throw("Elem2 doesn't exist");

                Graphs.data.similarities.push({
                    source: Graphs.data.nodes[obj1_idx],
                    target: Graphs.data.nodes[obj2_idx],
                    similarity: data[i][2]
                });
            }
            if (cb != null) cb();
        }});
    };


    Graphs.pollUpdate = function (diff) {
        $(diff.insertions.categories).each(function (idx, val) {
            Graphs.DataHelper.addCatToData(val, false);
        });
        $(diff.insertions.elements).each(function (idx, val) {
            Graphs.DataHelper.addElemToData(val[0], val[1]);
        });
        $(diff.insertions.links).each(function (idx, val) {
            Graphs.DataHelper.addLinkToData(val[0][0], val[0][1], val[1][0], val[1][1]);
        });

        $(diff.deletions.links).each(function (idx, val) {
            Graphs.DataHelper.removeLinkFromData(val[0][0], val[0][1], val[1][0], val[1][1]);
        });
        $(diff.deletions.elements).each(function (idx, val) {
            Graphs.DataHelper.removeElemFromData(val[0], val[1]);
        });
        $(diff.deletions.categories).each(function (idx, val) {
            Graphs.DataHelper.removeCatFromData(val, false);
        });

        Graphs.calcCatColors();
        Graphs.calcCatCenters();

        Graphs.updateSimilarities(function () {
            Graphs.start();
        });
    };

    Graphs.Layout = {};

    Graphs.Layout.dragStarted = function(d) {
        if (!d3.event.active) Graphs.Layout.forceLayout.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
        d3.select(this).classed('dragging', true).moveToFront();
    };

    Graphs.Layout.dragged = function(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    };

    Graphs.Layout.dragEnded = function(d) {
        if (!d3.event.active) Graphs.Layout.forceLayout.alphaTarget(0);
        d.fx = null;
        d.fy = null;
        d3.select(this).classed("dragging", false);
    };

    Graphs.Layout.tick = function () {
        d3.selectAll('.link')
            .attr('x1', function (d) {
                return d.source.x;
            })
            .attr('y1', function (d) {
                return d.source.y;
            })
            .attr('x2', function (d) {
                return d.target.x;
            })
            .attr('y2', function (d) {
                return d.target.y;
            });

        d3.selectAll('.node-group')
            .attr('transform', function (d) {
                return "translate(" + d.x + "," + d.y + ")";
            });
    };

    Graphs.initD3 = function () {
        var jQuerySVG = $('svg#canvas');
        Graphs.Layout.svg = d3.select('svg#canvas');

        Graphs.width = jQuerySVG.width();
        Graphs.height = jQuerySVG.height();
        Graphs.Layout.forceLayout = d3.forceSimulation()
            // .size([width, height])
            .force("charge", CustomForces.IncreasingForce()
                .force(d3.forceManyBody().strength(Graphs.sliders.chargeStrength.val()))
                .func(function (x) { return 1-Math.pow(x, 3);})
                // .modFunc(function (forceManyBody, value) {
                //     forceManyBody.strength((1-value)*-60);
                // })
            )
            .force("link", d3.forceLink().strength(Graphs.sliders.linkStrength.val()))
            .force("collide", CustomForces.IncreasingForce()
                .force(d3.forceCollide(RADIUS*1.2).iterations(7).strength(Graphs.sliders.collisionStrength.val()))
                .modFunc(function (forceCollide, value) {
                    value = 1-Math.pow(value, 3);
                    forceCollide.radius(value*RADIUS*1.2);
                })
            )
            //.force("center", d3.forceCenter(Graphs.width/2, Graphs.height/2))
            // .force("positionX", d3.forceX(Graphs.width/2).strength(.1))
            // .force("positionY", d3.forceY(Graphs.height/2).strength(.1))
            .force("focus", CategoryFocusForce().strength(Graphs.sliders.focusStrength.val()))
            .force("boundingBox", CustomForces.BoundingBoxForce(
                {width: Graphs.width, height: Graphs.height, padding: RADIUS+2}));
            // .nodes(nodes)
            // .links(links)
            // .charge(-200)
            // .linkStrength(0.9)
            // .linkDistance(0)
            // .chargeDistance(100)
            // .gravity(0.1);
        /*.linkDistance(150)
         .linkStrength(1)
         .charge(-200);*/
    };

    Graphs.start = function () {
        var nodes = Graphs.data.nodes;
        console.log(Graphs.width, Graphs.height);
        for (var i=0; i < nodes.length; i++) {
            nodes[i].x = Math.random()*Graphs.width;
            nodes[i].y = Math.random()*Graphs.height;
        }
        var edges = Graphs.data.edges;

        Graphs.Layout.linkJoin = Graphs.Layout.svg.selectAll('.link')
            .data(edges);
        Graphs.Layout.linkJoin.enter().append('line')
            .attr('class', 'link');
        Graphs.Layout.linkJoin.exit().remove();

        Graphs.Layout.nodeJoin = Graphs.Layout.svg.selectAll(".node-group")
            .data(nodes, function(d) {
                return btoa(d['category'])+"_"+btoa(d['text']);
        });
        var nodeGroups = Graphs.Layout.nodeJoin.enter().append("g").attr('class', 'node-group');
        nodeGroups.append('circle')
            .attr('class', 'shadow')
            .attr("r", RADIUS)
            .style('filter', 'url(#gaussianBlur)');
        nodeGroups.append('circle')
            .attr('class', 'node')
            .attr("r", RADIUS)
            .style('fill', function (d) {
                return Graphs.data.categories[d.category].color;
            });
        nodeGroups.call(d3.drag()
                .on("start", Graphs.Layout.dragStarted)
                .on("drag", Graphs.Layout.dragged)
                .on("end", Graphs.Layout.dragEnded));
        nodeGroups.append('text')
            .attr("text-anchor", "middle")
            .attr("dy", ".35em")
            .text(function (d) { return d.text; });

        var nodeTransforms = [];
        function restoreNodes() {
            for (var i=0; i < nodeTransforms.length; i++) {
                nodeTransforms[i][0].attr('transform', nodeTransforms[i][1]);
            }
            nodeTransforms = [];
            nodeGroups.classed('linked', false);
            nodeGroups.classed('selected', false);
            Graphs.Layout.svg.classed('selected', false);
        }

        function restoreLinks() {
            d3.selectAll('.link')
                .classed('linking', false)
                .style('stroke', '');
        }

        function highlightLink(l) {
            d3.select(l)
                .classed('linking', true)
                .style('stroke', 'black');
        }

        function focusNode(nodeGroup, data) {
            unfocusNodes();
            var group = d3.select(nodeGroup);
            if (d3.select('.dragging')._groups[0][0] != null) return;  // dragging some node
            var text = group.select('text');
            d3.selectAll('.link').each(function(l) {
                var other;
                if (data === l.source) {
                    other = l.target;
                } else if (data === l.target) {
                    other = l.source;
                }
                if (other) {
                    highlightLink(this);
                    other = nodeGroups.nodes()[other.index];
                    d3.select(other).classed('linked', true);
                }
            });
            group.moveToFront();
            group.classed('selected', true);
            Graphs.Layout.svg.classed('selected', true);
            nodeTransforms.push([text, text.attr('transform')]);
            // force render to update classes before scale transformation
            group._groups[0][0].getBoundingClientRect();
            text.attr('transform', 'scale('+1+')');
            d3.event.stopPropagation();
        }

        function unfocusNodes() {
            restoreNodes();
            restoreLinks();
        }

        nodeGroups.on('click', function (d) {
            focusNode(this, d);
        });
        nodeGroups.on('mouseover', function (d) {
             focusNode(this, d);
             d3.select(this).on('mouseout', function () {
                 d3.select(this).on('mouseout', null);
                 unfocusNodes();
             })
        });

        d3.select('body').on('click', function() {
            unfocusNodes();
        });
        $('.node-group text').each(function (i, t) {
            var bb = t.getBBox();
            var w = bb.width;
            var h = bb.height;
            var cos = w/Math.sqrt(w*w+h*h);
            var widthTransform = RADIUS*cos*2 / w;
            var heightTransform = RADIUS*Math.sin(Math.acos(cos))*2 / h;
            var value = widthTransform < heightTransform ? widthTransform : heightTransform;
            value = value > 1 ? 1: value;
            t.setAttribute("transform", "scale("+value+")");
            // force render to update scale transformation before class rules are applied
            t.getBoundingClientRect();
            t.classList.add("smooth-transform");
        });
        Graphs.Layout.nodeJoin.exit().remove();

        Graphs.Layout.forceLayout.nodes(nodes);

        if (Graphs.usingSimilarities) {
            Graphs.Layout.forceLayout.force("link")
                .links(Graphs.data.similarities)
                .strength(function (d) {
                    var factor = Graphs.sliders.similarityStrength.val();
                    var val = d.similarity;
                    var func = function (x) {return x;};
                    switch ($('#similarity-function').val()){
                        case 'linear':
                            func = function (x) {return x};
                            break;
                        case 'quadratic':
                            func = function (x) {return Math.pow(x, 2)};
                            break;
                        case 'logistic':
                            func = function (x) {return 1/(1+Math.exp(-(x*6-3)))};
                            break;
                    }
                    return factor*func(val);
                });
            Graphs.Layout.forceLayout.force("focus").strength(.03);
        } else {
            Graphs.Layout.forceLayout.force("link").links(edges).strength(Graphs.sliders.linkStrength.val());
            Graphs.Layout.forceLayout.force("focus", CategoryFocusForce().strength(Graphs.sliders.focusStrength.val()));
        }

        Graphs.Layout.forceLayout.on("tick", Graphs.Layout.tick);
    };

    require(['domReady'], function (domReady) {
        domReady(function () {
            Graphs.main();
        });
    });

    return Graphs;
});
