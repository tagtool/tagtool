require(['jquery', 'api'], function ($, api) {
    $(function() {
        $('.upload-input').change(function (){
            var $uploadOutput = $(this).parents('form').find('.upload-output');
            switch (this.files.length) {
                case 0:
                    $uploadOutput.html("&hellip;");
                    break;
                default:
                    $uploadOutput.text(this.files[0].name);
                    break;
            }
        });


        var lastStatusClass;
        var statusTimer;
        function _resetStatus($uploadStatus) {
            statusTimer = null;
            $uploadStatus.stop(true, true).fadeOut(400);
        }
        function _setStatus($uploadStatus, html, cls) {
            $uploadStatus.stop(true, true).hide();
            if (lastStatusClass != null) {
                $uploadStatus.removeClass(lastStatusClass)
            }
            if (cls != null) {
                $uploadStatus.addClass(cls);
            }
            lastStatusClass = cls;
            $uploadStatus.html(html).fadeIn(100);
        }
        function setStatus($uploadStatus, html, cls, timer) {
            if (timer == null) timer = true;
             _setStatus($uploadStatus, html, cls);
             if (timer) {
                if (statusTimer != null) window.clearTimeout(statusTimer);
                statusTimer = window.setTimeout(function() {
                    _resetStatus($uploadStatus);
                }, 10000);
            }
        }
        function resetStatus($uploadStatus) {
            if (statusTimer != null) window.clearTimeout(statusTimer);
            _resetStatus($uploadStatus);
        }


         $('.upload-form').submit(function(event) {
             var $form = $(this);
             var $uploadInput = $form.find('.upload-input');
             var $uploadStatus = $form.find('.upload-status');
             var $uploadSubmit = $form.find('.upload-submit');
             var fd = new FormData();
             fd.append('file', $uploadInput[0].files[0]);
             event.preventDefault();

             (function($form, $uploadInput, $uploadStatus, $uploadSubmit) {
                $.ajax({
                     url: $form.attr('action'),
                     data: fd,
                     processData: false,
                     contentType: false,
                     type: $(this).attr('method') || 'POST',
                     beforeSend: function () {
                         setStatus($uploadStatus,
                                   '<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> uploading&hellip;',
                                   'text-muted',
                                   false);
                         $uploadSubmit.prop('disabled', true);
                     },
                     complete: function () {
                         $uploadSubmit.prop('disabled', false);
                     },
                     success: function(){
                         setStatus($uploadStatus,
                                   '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> success',
                                   'text-success');
                         $form.trigger('reset').find('*').trigger('change');
                     },
                     error: function(jqXHR, textStatus, errorThrown) {
                         var msg = "Ooops. Something went wrong.";
                         if (jqXHR.status != 0) {
                             if(jqXHR.getResponseHeader("content-type") === 'text/plain') {
                                msg += " "+jqXHR.responseText;
                             } else {
                                msg += " "+jqXHR.status+": "+jqXHR.statusText+".";
                             }
                         } else {
                             msg += " It might well be that the server is not reachable. Please make sure your connection is fine.";
                         }
                         msg = $('<span>').text(msg).html();
                         setStatus($uploadStatus,
                                   '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ' + msg,
                                   'text-danger');
                     }
                 });
             })($form, $uploadInput, $uploadStatus, $uploadSubmit);
         });
    });
});
