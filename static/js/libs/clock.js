function clock(selector, options) {
    var parent = $(selector);

    if (options.toggleColon == null) { options.toggleColon = true }

    var FILL_IMG = $('<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAFCAYAAACAcVaiAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AQODzYukVm7VgAAAExJREFUCB0BQQC+/wH///8AAAAAAAAAAAACAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAvckDBzhpyJ8AAAAASUVORK5CYII=">');

    var tClock = new Date();

    $(document).ready(function(){
        clockInit();
        clock();
    });

    function clockInit() {
        buildClock();
        updateMinutes(tClock.getMinutes());
        updateHours(tClock.getHours());
    }
    function buildClock() {
        parent.append('<ul class="clock"><li class="digit h1"></li><li class="digit h2"></li><li class="colon cv c1"></li><li class="digit m1"></li><li class="digit m2"></li></ul>');
        parent.find('li').prepend(FILL_IMG);
    }
    function clock(){
        var t_now = new Date();
        var mins  = t_now.getMinutes();
        var hs    = t_now.getHours();
        if (options.toggleColon) {
            var secs  = t_now.getSeconds();
            if (secs != tClock.getSeconds()) {
                // in case the function was called to early
                // the colon isn't supposed to toggle
                toggle_colon1();
            }
        }
        if (mins != tClock.getMinutes()) {
            updateMinutes(mins);
        }
        if (hs != tClock.getHours()) {
            updateHours(hs);
        }
        tClock = t_now;
        window.setTimeout(clock, 1000);
    }

    function toggle_colon1(){
        var c1 = parent.find(".c1");
        if (c1.hasClass("cv")){
            c1.removeClass("cv").addClass("ci");
        } else {
            c1.removeClass("ci").addClass("cv");
        }
    }

    function updateMinutes(mins){
        mins = (mins < 10) ? "0"+mins : ""+mins;
        parent.find(".m1").removeClass("de d0 d1 d2 d3 d4 d5 d6 d7 d8 d9");
        parent.find(".m1").addClass("d"+mins.charAt(0));
        parent.find(".m2").removeClass("de d0 d1 d2 d3 d4 d5 d6 d7 d8 d9");
        parent.find(".m2").addClass("d"+mins.charAt(1));
    }

    function updateHours(hs){
        hs = (hs < 10) ? "0"+hs : ""+hs;
        parent.find(".h1").removeClass("de d0 d1 d2 d3 d4 d5 d6 d7 d8 d9");
        parent.find(".h1").addClass("d"+hs.charAt(0));
        parent.find(".h2").removeClass("de d0 d1 d2 d3 d4 d5 d6 d7 d8 d9");
        parent.find(".h2").addClass("d"+hs.charAt(1));
    }
}

