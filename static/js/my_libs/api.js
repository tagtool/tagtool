/**
 * Created by rouven on 29.10.15.
 */

define(['jquery', 'socketio'], function ($, socketio) {

    var errorModalTemplate = '\
      <div class="modal fade" id="api-error-modal" tabindex="-1" role="dialog" aria-labelledby="apiError">\
        <div class="modal-dialog" role="document">\
          <div class="modal-content panel-danger">\
            <div class="modal-header panel-heading">\
              <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
              <h3 class="modal-title" id="apiError">API Error</h3>\
            </div>\
            <div class="modal-body">\
              <div class="alert alert-danger collapse" role="alert"></div>\
              <p>\
                <span class="msg"></span>\
              </p>\
            </div>\
            <div class="modal-footer">\
              <button class="btn btn-primary ignore" data-dismiss="modal">ignore</button>\
              <button class="btn btn-danger reload">reload page (may cause data loss)</button>\
            </div>\
          </div>\
        </div>\
      </div>';

    var API = {};

    API.dbv = undefined;  // current data base version

    API.encoder = function(s) {
        // s. https://github.com/mitsuhiko/werkzeug/issues/797
        if (typeof s === 'string') {
            return encodeURIComponent(encodeURIComponent(s));
        } else {
            return s;
        }
    };

    API.startCB = null;
    API.stopCB = null;

    API.Helper = "Helper" in API ? API.Helper : {};
    API.Helper.CBOptions = function(options) {
        if (options == null) { options = {} }
        if (options.success == false) { options.success = function () {}}
        if (typeof options.success !== "function") { options.success = null }
        if (options.error == false) { options.error = function () {}}
        if (typeof options.error !== "function") { options.error = null }
        if (options.start == false) { options.start = function () {}}
        if (typeof options.start !== "function") { options.start = API.startCB }
        if (options.stop == false) { options.stop = function () {}}
        if (typeof options.stop !== "function") { options.stop = API.stopCB }
        if (typeof options.no_dbv !== "boolean") { options.no_dbv = false }
        if (typeof options.headers !== "object") { options.headers = {} }
        if (typeof options.plain !== "boolean") { options.plain = false }
        if (typeof options.allowFail !== "boolean") { options.allowFail = false }
        if (typeof options.retries !== "number") { options.retries = 5 }
        return options;
    };

    API.Helper.alertOpen = false;
    API.Helper.alert = function (msg) {
        if (API.Helper.alertOpen) return;
        $('.modal-load-overlay').removeClass('active');
        var modal = $(errorModalTemplate);
        modal.find('.msg').text(msg);
        modal.on('hidden.bs.modal', function () {
            $(this).remove();
            API.Helper.alertOpen = false;
        });
        modal.find('button.reload').click(function () {
            window.location.reload();
        });
        $(modal).modal();
        API.Helper.alertOpen = true;
    };

    API.baseUrl = "/";

    API.relativeUrls = "relativeUrls" in API ? API.relativeUrls : {};
    API.relativeUrls.category = function(cat) {
        cat = API.encoder(cat);
        return 'api/category/'+cat
    };
    API.relativeUrls.categories = function() { return 'api/categories' };

    API.relativeUrls.element = function(cat, elem) {
        cat = API.encoder(cat);
        elem = API.encoder(elem);
        return 'api/category/' + cat + "/" + elem;
    };

    API.relativeUrls.link = function(e1_cat, e1_elem, e2_cat, e2_elem) {
        e1_cat = API.encoder(e1_cat);
        e1_elem = API.encoder(e1_elem);
        e2_cat = API.encoder(e2_cat);
        e2_elem = API.encoder(e2_elem);
        return 'api/link/between/'+e1_cat+'/'+e1_elem+'/and/'+e2_cat+'/'+e2_elem;
    };
    API.relativeUrls.links = function(e1_cat, e1_elem) {
        e1_cat = API.encoder(e1_cat);
        e1_elem = API.encoder(e1_elem);
        if (e1_cat && e1_elem) { return 'api/links/of/'+e1_cat+'/'+e1_elem;}
        return 'api/links';
    };
    API.relativeUrls.poll = function() {
        return 'api/poll';
    };
    API.relativeUrls.userCount = function() {
        return 'api/user_count';
    };
    API.relativeUrls.diffQuery = function() {
        return 'api/diff/query';
    };
    API.relativeUrls.renameCategory = function(old_name, new_name) {
        old_name = API.encoder(old_name);
        new_name = API.encoder(new_name);
        return 'api/rename/'+old_name+'/to/'+new_name;
    };
    API.relativeUrls.renameElement = function(category, old_name, new_name) {
        category = API.encoder(category);
        old_name = API.encoder(old_name);
        new_name = API.encoder(new_name);
        return 'api/rename/'+category+'/'+old_name+'/to/'+new_name;
    };
    API.relativeUrls.renameElementOverCategory = function (old_cat, old_name, new_cat, new_name) {
        old_cat = API.encoder(old_cat);
        old_name = API.encoder(old_name);
        new_cat = API.encoder(new_cat);
        new_name = API.encoder(new_name);
        return 'api/rename/'+old_cat+'/'+old_name+'/to/'+new_cat+'/'+new_name;
    };
    API.relativeUrls.bulkaction = function() {
        return 'api/bulkaction';
    };
    API.relativeUrls.settings = function() {
        return 'api/settings';
    };
    API.relativeUrls.allTimestamps = function() {
        return 'api/all_timestamps';
    };
    API.relativeUrls.similarities = function() {
        return 'api/similarities';
    };
    API.relativeUrls.search = function(query, cat, forcefuzzy, fuzzthresh, fuzzlimit) {
        var url = 'api/search?';
        if (query == null) query = '';
        url += 'q=' + encodeURIComponent(query);
        if (cat != null) { url += '&cat=' + encodeURIComponent(cat) }
        if (forcefuzzy != null && ! forcefuzzy) { url += '&forcefuzzy=false' }
        if (fuzzthresh != null) { url += '&fuzzthresh=' + fuzzthresh }
        if (fuzzlimit!= null) { url += '&fuzzlimit=' + fuzzlimit }
        return url
    };
    API.relativeUrls.recentlyChanged = function() {
        return 'api/recently_changed';
    };
    API.relativeUrls.token = function() {
        return 'api/token';
    };
    API.relativeUrls.authStatus = function() {
        return 'api/auth_status';
    };


    API._call = function(baseUrl, relativeUrl, options, method, data, _fails) {
        if (_fails == null) { _fails = 0; }
        options = API.Helper.CBOptions(options);
        method = typeof method === 'undefined' ? 'GET' : method;
        var url = baseUrl + relativeUrl;
        if (options.start != null) {
            options.start();
        }
        var headers = "headers" in options ? options.headers : {};
        if (typeof API.dbv !== 'undefined' && !options.no_dbv) {
            headers['ETag'] = API.dbv;
        }
        $.ajax({url: url,
                accepts: 'application/json',
                dataType: options.plain ? 'text' : 'json',
                method: method.toUpperCase(),
                data: data == null ? {} : data,
                headers: headers,
                success: function(data, textStatus, response) {
                    if ( typeof API.dbv === 'undefined' ) {
                        API.dbv = response.getResponseHeader('ETag');
                    }
                    if (options.stop != null) {
                        options.stop();
                    }
                    if (options.success != null) {
                        options.success(data, textStatus, response, options.callback_data);
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {
                    _fails++;
                    if (_fails < options.retries) {
                        window.setTimeout(function() {
                            API._call(baseUrl, relativeUrl, options, method, data, _fails)
                        }, 100);
                        return;
                    }
                    if (options.stop != null) {
                        options.stop();
                    }
                    if (!options.allowFail) console.error(jqXHR, textStatus, errorThrown);
                    var msg = "???";
                    if (typeof jqXHR.responseJSON == "object" &&
                        typeof jqXHR.responseJSON.message == "string") {
                        msg = jqXHR.responseJSON.message;
                    } else if (typeof jqXHR.responseJSON == "object" &&
                        typeof jqXHR.responseJSON.description == "string") {
                        msg = jqXHR.responseJSON.description;
                    } else {
                        if (jqXHR.status != 0) {
                            msg = "Ooops. Something went wrong ("+jqXHR.status+": "+jqXHR.statusText+")";
                        } else {
                            msg = "Ooops. Something went wrong. It might well be that the server is not reachable. Please make sure your connection is fine.";
                        }
                    }
                    if (options.error != null) {
                        var cb_res = options.error(msg, jqXHR, textStatus, errorThrown, options.callback_data);
                    }
                    if ((options.error != null && cb_res === false) || (options.error == null && !options.allowFail)) {
                        API.Helper.alert(msg);
                    }
                }
            }
        );
    };


    API.get = "get" in API ? API.get : {};
    API.get.categories = function(options) {
        API._call(API.baseUrl, API.relativeUrls.categories(), options);
    };
    API.get.category = function(cat, options) {
        API._call(API.baseUrl, API.relativeUrls.category(cat), options);
    };
    API.get.member = function(cat, elem, options) {
        API._call(API.baseUrl, API.relativeUrls.element(cat, elem), options);
    };
    API.get.links = function(e1_cat, e1_elem, options) {
        if (typeof e1_cat === "object") {  // allow to be called with options only
            API._call(API.baseUrl, API.relativeUrls.links(), e1_cat);
        } else {
            API._call(API.baseUrl, API.relativeUrls.links(e1_cat, e1_elem), options);
        }
    };
    API.get.poll = function(options) {
        API._call(API.baseUrl, API.relativeUrls.poll(), options, "GET");
    };
    API.get.userCount = function(options) {
        API._call(API.baseUrl, API.relativeUrls.userCount(), options, "GET");
    };
    API.get.diffQuery = function(options) {
        var data = {};
        if (options == null) { options = {} }
        if (options.start != null) { data.start = options.start; delete options.start; }
        if (options.end != null) { data.end = options.end; delete options.end; }
        if (options.no_dbv == null) options.no_dbv = true;
        API._call(API.baseUrl, API.relativeUrls.diffQuery(), options, "GET", data);
    };
    API.get.diffQueryFromCurrent = function(to_version, options) {
        if (options == null) { options = {} }
        options.no_dbv = true;
        var data = {start: API.dbv, end: to_version};
        API._call(API.baseUrl, API.relativeUrls.diffQuery(), options, "GET", data);
    };
    API.get.settings = function(options){
        API._call(API.baseUrl, API.relativeUrls.settings(), options, "GET");
    };
    API.get.allTimestamps = function(options) {
        API._call(API.baseUrl, API.relativeUrls.allTimestamps(), options, "GET");
    };
    API.get.similarities = function(options) {
        API._call(API.baseUrl, API.relativeUrls.similarities(), options, "GET");
    };
    API.get.search = function (query, options) {
        // cat, forcefuzzy, fuzzthresh, fuzzlimit
        var cat = options.cat; delete options.cat;
        var forcefuzzy = options.forcefuzzy; delete options.forcefuzzy;
        var fuzzthresh = options.fuzzthresh; delete options.fuzzthresh;
        var fuzzlimit = options.fuzzlimit; delete options.fuzzlimit;
        var url = API.relativeUrls.search(query, cat, forcefuzzy, fuzzthresh, fuzzlimit);
        API._call(API.baseUrl, url, options, "GET");
    };
    API.get.recentlyChanged = function(options) {
        API._call(API.baseUrl, API.relativeUrls.recentlyChanged(), options, "GET");
    };
    API.get.token = function (options) {
        API._call(API.baseUrl, API.relativeUrls.token(), options, "GET");
    };
    API.get.authStatus = function (options) {
        API._call(API.baseUrl, API.relativeUrls.authStatus(), options, "GET");
    };

    API.post = "post" in API ? API.post : {};
    API.post.category = function(cat, options) {
        API._call(API.baseUrl, API.relativeUrls.category(cat), options, "POST");
    };
    API.post.element = function(cat, elem, options) {
        API._call(API.baseUrl, API.relativeUrls.element(cat, elem), options, "POST");
    };
    API.post.link = function(e1_cat, e1_elem, e2_cat, e2_elem, options) {
        API._call(API.baseUrl,
            API.relativeUrls.link(e1_cat, e1_elem, e2_cat, e2_elem), options, "POST");
    };
    API.post.bulkaction = function(diff, options) {
        API._call(API.baseUrl, API.relativeUrls.bulkaction(), options, "POST",
            { "diff": JSON.stringify(diff) }
        );
    };

    API.delete = "delete" in API ? API.delete : {};
    API.delete.category = function(cat, options) {
        API._call(API.baseUrl, API.relativeUrls.category(cat), options, "DELETE");
    };
    API.delete.element = function(cat, elem, options) {
        API._call(API.baseUrl, API.relativeUrls.element(cat, elem), options, "DELETE");
    };
    API.delete.link = function(e1_cat, e1_elem, e2_cat, e2_elem, options) {
        API._call(API.baseUrl,
            API.relativeUrls.link(e1_cat, e1_elem, e2_cat, e2_elem), options, "DELETE");
    };

    API.put = "put" in API ? API.put : {};
    API.put.renameCategory = function(old_name, new_name, options) {
        API._call(API.baseUrl, API.relativeUrls.renameCategory(old_name, new_name),
                  options, "PUT");
    };
    API.put.renameElement = function(cat, old_name, new_name, options) {
        API._call(API.baseUrl, API.relativeUrls.renameElement(cat, old_name, new_name),
                  options, "PUT");
    };
    API.put.renameElementOverCategory = function(old_cat, old_name, new_cat, new_name, options) {
        API._call(API.baseUrl, API.relativeUrls.renameElementOverCategory(old_cat, old_name, new_cat, new_name),
                  options, "PUT");
    };
    API.put.settings = function(settings, options){
        if (options == null) { options = {} }
        if ("headers" in options) {
            options.headers['Content-Type'] = 'application/json';
        } else {
            options.headers = {'Content-Type': 'application/json'}
        }
        settings = JSON.stringify(settings);
        API._call(API.baseUrl, API.relativeUrls.settings(), options, "PUT", settings);
    };

    API._newID = function() {
        return 'xxxxxxxxxxxxxxxxxxx'.replace(/[x]/g, function(c) {
            var r = Math.random()*16|0;
            return r.toString(16);
        });
    };

    API.Helper.POLL_METHODS = ['diff', 'user_count', 'settings'];
    API._pollCBs = {};
    API._pollID = null;

    API._registerPollCBForSingleMethod = function(func, method) {
        method = method.toLowerCase();
        if (API._pollCBs.hasOwnProperty(method)) {
            API._pollCBs[method].push(func);
        } else {
            API._pollCBs[method] = [func];
        }
    };

    API._unRegisterPollCBForSingleMethod = function(func, method) {
        method = method.toLowerCase();
        if (API._pollCBs.hasOwnProperty(method)) {
            var idx = API._pollCBs[method].indexOf(func);
            if (idx >= 0) {
                API._pollCBs[method].splice(idx, 1);
                return;
            }
        }
        throw func+' was never bound to '+method;
    };

    API.registerPollCB = function(func, method) {
        if (method == null) { method = 'all' }
        method = method.toLowerCase();
        if ($.inArray(method, API.Helper.POLL_METHODS) >= 0) {
            API._registerPollCBForSingleMethod(func, method);
        } else if ( method == 'all') {
            for (var i = 0; i < API.Helper.POLL_METHODS.length; i++) {
                API._registerPollCBForSingleMethod(func, API.Helper.POLL_METHODS[i]);
            }
        } else {
            throw 'Only accepting methods "'+ API.Helper.POLL_METHODS.join('", "') +'" and "all" (default)';
        }
    };
    API.unregisterPollCB = function(func, method) {
        if (method == null) { method = 'all' }
        method = method.toLowerCase();
        if ($.inArray(method, API.Helper.POLL_METHODS) >= 0) {
            API._unRegisterPollCBForSingleMethod(func, method)
        } else if (method == 'all' ) {
            for (var i = 0; i < API.Helper.POLL_METHODS.length; i++) {
                API._unregisterPollCBForSingleMethod(func, API.Helper.POLL_METHODS[i]);
            }
        } else {
            throw 'Only accepting methods "'+ API.Helper.POLL_METHODS.join('", "') +'" and "all" (default)';
        }
    };
    API.unregisterAllPollCBs = function() {
        API._pollCBs = {};
    };


    API._ws = null;

    API._detachedMode = false;
    API.detachPoll = function() {
        API._detachedMode = true;
    };
    API.attachPoll = function() {
        API._detachedMode = false;
    };

    API.startPollLoop = function () {
        API._ws_init();
    };
    API.stopPollLoop = function () {
        API._wsClose();
    };

    API._pollToLatestDBV = function (data_cb, done_cb) {
        var init = 0;
        // initial 'poll'
        API.get.diffQuery({'success': function(data) {
            if (data_cb != null) data_cb({'diff': data});
            if(++init == 2 && done_cb != null) done_cb();
        }, start:API.dbv});
        API.get.userCount({'success': function(data) {
            if (data_cb != null) data_cb({'user_count': data});
            if(++init == 2) done_cb();
        }});
    };

    API._wsCb = function(data, skippable, forceUpdate) {
        console.log('ws data cb');
        if (skippable == null) skippable = true;
        if (forceUpdate == null) forceUpdate = false;
        if (API._ws == null && skippable) {
            console.log('ws data cb skipped');
            return;
        }
        if ((data.hasOwnProperty('diff') && data.diff != null &&
             data.diff.to != null && !API._detachedMode) || forceUpdate) {
            API.dbv = data.diff.to;
            console.log(data.diff);
            console.log('ws update dbv to '+API.dbv);
        }
        for (var j=0; j < API.Helper.POLL_METHODS.length; j++) {
            var pollMethod = API.Helper.POLL_METHODS[j];
            if (data.hasOwnProperty(pollMethod) &&
                    data[pollMethod] != null &&
                    API._pollCBs.hasOwnProperty(pollMethod)) {
                for (var i = 0; i < API._pollCBs[pollMethod].length; i++) {
                    API._pollCBs[pollMethod][i](data[pollMethod]);
                }
            }
        }
    };

    API._wsOpen = function () {
        console.log('ws open');
        if (API._ws != null) {
            console.error('Websocket still open.');
            return;
        }
        API._ws = socketio("/", { query: "db_version="+API.dbv });
        API._ws.on('connect', function () {
            console.log('ws connected');
            for (var i=0; i < API.Helper.POLL_METHODS.length; i++) {
                var event = API.Helper.POLL_METHODS[i];
                (function (event) {
                    API._ws.on(event, function (data) {
                        console.log('ws ' + event);
                        var cbData = {};
                        cbData[event] = JSON.parse(data);
                        API._wsCb(cbData);
                    });
                })(event);
            }
        });
    };

    API._wsClose = function () {
        console.log('ws close');
        if (API._ws == null) return;
        API._ws.disconnect();
        API._ws = null;
    };

    API._ws_init = function () {
        console.log('ws init');
        API._pollToLatestDBV(function(data) {
            API._wsCb(data, false);
        }, API._wsOpen.bind(API));
    };

    return API;
});
