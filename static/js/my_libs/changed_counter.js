/**
 * Created by rouven on 03.05.17.
 */

DEFAULT_POLL_INTERVAL = 30*1000;

define(['jquery', 'api'], function ($, API) {
    var ChangedCounter = $('#recently-changed-counter');

    ChangedCounter.pollInterval = DEFAULT_POLL_INTERVAL;

    ChangedCounter.updateChangedCounter = function(count) {
        var changedCountDomElement = $('#recently-changed-count');
        changedCountDomElement.text(count);
        if (count === 0) {
            ChangedCounter.hide();
        } else {
            ChangedCounter.show();
        }
    };

    ChangedCounter._CBs = [];

    ChangedCounter.registerUpdateCB = function (func) {
        if (ChangedCounter._CBs.indexOf(func) < 0) {
            ChangedCounter._CBs.push(func);
        }
    };

    var loopRunning = false;

    ChangedCounter.updateRecentlyChanged = function() {
        API.get.recentlyChanged({
            success: function (data) {
                if (!loopRunning) return;
                ChangedCounter.recentlyChanged = data;
                ChangedCounter.updateChangedCounter(ChangedCounter.recentlyChanged.length);
                for (var i=0; i < ChangedCounter._CBs.length; i++) {
                    ChangedCounter._CBs[i](data);
                }
            },
            start: false,
            stop: false,
            error: false
        });
    };

    ChangedCounter.recentlyChanged = [];
    ChangedCounter.recentlyChangedTimer = null;
    ChangedCounter.recentlyChangedLoop= function () {
        ChangedCounter.updateRecentlyChanged();
        if (ChangedCounter.recentlyChangedTimer != null) {
            clearTimeout(ChangedCounter.recentlyChangedTimer);
        }
        ChangedCounter.recentlyChangedTimer = setTimeout(
            ChangedCounter.recentlyChangedLoop.bind(ChangedCounter), ChangedCounter.pollInterval);
    };

    ChangedCounter.startRecentlyChangedLoop = function () {
        if (loopRunning) {
            console.warn('ChangedCounter update already bound before attempt to bind.');
            return;
        }
        ChangedCounter.recentlyChangedLoop();
        loopRunning = true;
    };
    ChangedCounter.startRecentlyChangedLoop();

    ChangedCounter.stopRecentlyChangedLoop = function () {
        if (!loopRunning) {
            console.warn('ChangedCounter update already bound before attempt to bind.');
            return;
        }
        if (ChangedCounter.recentlyChangedTimer != null) {
            clearTimeout(ChangedCounter.recentlyChangedTimer);
        }
        loopRunning = false;
    };

    return ChangedCounter;
});
