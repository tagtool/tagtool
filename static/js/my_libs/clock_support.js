/**
 * Created by rouven on 12.09.16.
 */

require(['jquery', 'clock'], function ($, clock) {
    clock('#clock-wrapper', {toggleColon: false});
});
