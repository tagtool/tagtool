define(function () {
    /*
    * http://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
    * Converts an HSL color value to RGB. Conversion formula
    * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
    * Assumes h, s, and l are contained in the set [0, 1] and
    * returns r, g, and b in the set [0, 255].
    *
    * @param   Number  h       The hue
    * @param   Number  s       The saturation
    * @param   Number  l       The lightness
    * @return  Array           The RGB representation
    */
    function hslToRgb(h, s, l) {
        var r, g, b;

        if (arguments.length === 1) {
            s = h.s;
            l = h.l;
            h = h.h;
        }
        if (h > 1) {
            h = h / 360;
        }
        if (s > 1) {
            s = s / 100;
        }
        if (l > 1) {
            l = l / 100;
        }

        if (s === 0) {
            r = g = b = l; // achromatic
        } else {
            var hue2rgb = function hue2rgb(p, q, t) {
                if (t < 0) t += 1;
                if (t > 1) t -= 1;
                if (t < 1 / 6) return p + (q - p) * 6 * t;
                if (t < 1 / 2) return q;
                if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
                return p;
            };

            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = hue2rgb(p, q, h + 1 / 3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1 / 3);
        }

        return {
            r: Math.round(r * 255),
            g: Math.round(g * 255),
            b: Math.round(b * 255)
        };
    }

    return function (num_colors, saturation, lightness) {
        // choose darken [0, 1]
        // assumes hue [0, 360), saturation [0, 100), lightness [0, 100)
        if (!saturation) saturation = .9;
        if (!lightness) lightness = .5;
        var res = [];
        var step = 360 / num_colors;
        for (var i = 0; i < num_colors; i++) {
            var c = {};
            c.h = i * step; // hue
            c.s = 100*saturation; // saturation
            c.l = 100*lightness; // lightness

            var rgb = hslToRgb(c);
            res.push("rgb(" + rgb.r + ", " + rgb.g + ", " + rgb.b + ")");
        }
        return res;
    };
});
