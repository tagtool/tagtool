define([], function () {
    var CustomFoces = {};

    CustomFoces.BoundingBoxForce = function (options) {
        if (options == null) options = {};
        var _opts = {};
        _opts.x = options.x || 0;
        _opts.y = options.y || 0;
        _opts.w = options.w || options.width || 300;
        _opts.h = options.h || options.height || 300;
        _opts.padding = options.padding || 0;

        var force = function (alpha) {
            for (var i=0; i < force._nodes.length; i++) {
                var node = force._nodes[i];
                var next_x = node.x+node.vx;
                var next_y = node.y+node.vy;
                var bounds = {right: _opts.x+_opts.w-_opts.padding,
                              left: _opts.x+_opts.padding,
                              top: _opts.y+_opts.padding,
                              bottom: _opts.y+_opts.h-_opts.padding};
                if (next_x > bounds.right) {
                    node.x = bounds.right;
                    node.vx = 0;
                } else if (next_x < bounds.left) {
                    node.x = bounds.left;
                    node.vx = 0;
                }
                if (next_y > bounds.bottom) {
                    node.y = bounds.bottom;
                    node.vy = 0;
                } else if (next_y < bounds.top) {
                    node.y = bounds.top;
                    node.vy = 0;
                }
            }
        };

        force.options = function (options) {
            if (options == null) return _opts;
            _opts.x = options.x || _opts.x;
            _opts.y = options.y || _opts.y;
            _opts.w = options.w || options.width || _opts.w;
            _opts.h = options.h || options.height || _opts.h;
            _opts.padding = options.padding || _opts.padding;
            return force;
        };

        force.initialize = function (nodes) {
            force._nodes = nodes;
        };

        return force;
    };

    CustomFoces.IncreasingForce = function (options) {
        if (options == null) options = {};
        var _opts = {};
        _opts.func = options.func || function (alpha) {
            return 1-alpha;
        };
        _opts.modFunc = options.modFunc || function (force, value) {
            return;
        };
        _opts.force = options.force || null;

        var _nodes;

        var force = function (alpha) {
            if (_opts.force == null) return;
            var newAlpha = _opts.func(alpha)*alpha;
            _opts.modFunc(_opts.force, alpha);
            _opts.force(newAlpha);
        };

        force.options = function (options) {
            if (options == null) return _opts;
            _opts.func = options.func || _opts.func;
            _opts.modFunc = options.modFunc || _opts.modFunc;
            if (options.force) {
                _opts.force = options.force;
                _opts.force.initialize(_nodes);
            }
            return force;
        };

        force.func = function (func) {
            if (func == null) return _opts.func;
            return force.options({'func': func});
        };

        force.modFunc = function (modFunc) {
            if (modFunc == null) return _opts.modFunc;
            return force.options({'modFunc': modFunc});
        };

        force.force = function (_force) {
            if (_force == null) return _opts.force;
            return force.options({'force': _force});
        };

        force.initialize = function (nodes) {
            _nodes = nodes;
            if (_opts.force) _opts.force.initialize(nodes);
        };

        return force;
    };

    return CustomFoces;
});
