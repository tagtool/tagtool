define(['jquery'], function($) {
    $.Widget.prototype["_on"] = function (suppressDisabledCheck, element, handlers) {
        var delegateElement;
        var instance = this;

        // No suppressDisabledCheck flag, shuffle arguments
        if (typeof suppressDisabledCheck !== "boolean") {
            handlers = element;
            element = suppressDisabledCheck;
            suppressDisabledCheck = false;
        }

        // No element argument, shuffle and use this.element
        if (!handlers) {
            handlers = element;
            element = this.element;
            delegateElement = this.widget();
        } else {
            element = delegateElement = $(element);
            if (this.bindings.length !== this.bindings.not(element).length) {
                this.bindings = this.bindings.add(element);
            }
        }

        if (instance._registeredUIHanlders == null) {
            instance._registeredUIHanlders = {};
        }

        $.each(handlers, function (event, handler) {
            if (!instance._registeredUIHanlders.hasOwnProperty(event)) {
                instance._registeredUIHanlders[event] = [];
            }
            if ($.inArray(handler, instance._registeredUIHanlders[event]) !== -1) {
                return;
            }
            instance._registeredUIHanlders[event].push(handler);

            function handlerProxy() {

                // Allow widgets to customize the disabled handling
                // - disabled as an array instead of boolean
                // - disabled class as method for disabling individual parts
                if (!suppressDisabledCheck &&
                    (instance.options.disabled === true ||
                        $(this).hasClass("ui-state-disabled"))) {
                    return;
                }
                return (typeof handler === "string" ? instance[handler] : handler)
                    .apply(instance, arguments);
            }

            // Copy the guid so direct unbinding works
            if (typeof handler !== "string") {
                handlerProxy.guid = handler.guid =
                    handler.guid || handlerProxy.guid || $.guid++;
            }

            var match = event.match(/^([\w:-]*)\s*(.*)$/);
            var eventName = match[1] + instance.eventNamespace;
            var selector = match[2];

            if (selector) {
                delegateElement.on(eventName, selector, handlerProxy);
            } else {
                element.on(eventName, handlerProxy);
            }
        });
    };

    $.Widget.prototype["_off"] = function (element, eventName) {
        eventName = (eventName || "").split(" ").join(this.eventNamespace + " ") +
            this.eventNamespace;
        element.off(eventName).off(eventName);

        // Clear the stack to avoid memory leaks (#10056)
        if (this._registeredUIHanlders != null) {
            this._registeredUIHanlders[eventName] = [];
        }
        this.bindings = $(this.bindings.not(element).get());
        this.focusable = $(this.focusable.not(element).get());
        this.hoverable = $(this.hoverable.not(element).get());
    };
});
