// Simple patching of jquery-ui to support touch events (e.g. for draggable)
define(['jquery-ui.raw'], function (jqueryUI) {
    require(['jquery-ui.touch-punch']);
    require(['jquery-ui-memfix']);
    return jqueryUI
});

