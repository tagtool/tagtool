require(['mathjax_raw'], function (MathJax) {
    MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
});
