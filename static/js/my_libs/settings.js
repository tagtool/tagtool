/**
 * Created by rouven on 03.03.16.
 */

define(['api', 'cookies'], function (API, Cookies) {
    var Settings = {};

    Settings.serverSettings = null;

    Settings.latesMergedSettings = null;

    Settings.COOKIE_NAME = "settings";

    Settings.callbacks = [];

    // Increase by one whenever the structure of the settings cookie changes.
    // Also write a migration below.
    Settings.VERSION = 1;

    Settings.migrate = function (settings) {
        var v = settings.settingsVersion;
        if (v > Settings.VERSION) {
            throw 'Cookie is on an ahead version.';
        }
        while (v != Settings.VERSION) {
            switch (v) {
                case null:
                case undefined:
                    // version null -> 0
                    // adding versioning and purging local settings for all clients
                    settings = {"sizing": {}};
                    settings.settingsVersion = 0;
                    break;
                case 0:
                    settings['display-type'] = {};
                    settings.settingsVersion = 1;
                //further migrations go here
            }
            console.info('Migrated settings cookie from ' + v + ' ' +
                         'to ' + settings.settingsVersion);
            v = settings.settingsVersion;
        }
        // persists migration
        Settings.setLocalSettings(settings);
    };

    Settings.getLocalSettings = function () {
        var localSettings = Cookies.getJSON(Settings.COOKIE_NAME);
        if (localSettings != null && typeof localSettings != 'object') {
            console.error('Settings cookie was invalid. It was purged.');
            Cookies.remove(Settings.COOKIE_NAME);
            localSettings = null;
        }
        if (localSettings != null &&
            (!('settingsVersion' in localSettings) ||
             localSettings.settingsVersion != Settings.VERSION)) {
            console.info('Settings cookie has an outdated version.');
            Settings.migrate(localSettings);
            return Settings.getLocalSettings();
        }
        if (localSettings == null) {
            localSettings = {"sizing": {}, "display-type": {}}
        } else {
            delete localSettings.settingsVersion;
        }
        return localSettings;
    };
    Settings.setLocalSettings = function (settings) {
        if ('settingsVersion' in settings &&
            settings.settingsVersion != Settings.VERSION) {
            throw 'Trying to save an old settings version.'
        }
        if (!('settingsVersion' in settings)) {
            settings['settingsVersion'] = Settings.VERSION;
        }
        Cookies.set(Settings.COOKIE_NAME, settings);
        Settings._localSettingsCb(settings);
    };
    Settings.updateLocalSettings = function (partialSettings) {
        var localSettings = Settings.getLocalSettings();
        for (var setting in partialSettings) {
            for (var key in partialSettings[setting]) {
                if (partialSettings[setting][key] != null) {
                    localSettings[setting][key] = partialSettings[setting][key];
                } else {
                    delete localSettings[setting][key];
                }
            }
        }
        Settings.setLocalSettings(localSettings);
    };
    Settings.updateServerSettings = function (partialSettings) {
        API.put.settings(partialSettings, {
            start: null,
            stop: null,
            success: function () {
                Settings._serverSettings = null;
            }
        });
    };

    Settings.mergeSettings = function (localSettings, serverSettings) {
        // merge local_settings with serverSettings (local will overwrite server)
        var result = serverSettings;
        for (var setting in localSettings) {
            if (localSettings.hasOwnProperty(setting)) {
                if (result.hasOwnProperty(setting)) {
                    // merge
                    for (var key in localSettings[setting]) {
                        if (localSettings[setting].hasOwnProperty(key)) {
                            result[setting][key] = localSettings[setting][key];
                        }
                    }
                } else {
                    // copy local to result
                    result[setting] = localSettings[setting];
                }
            }
        }
        return result;
    };

    Settings._get_cb = function (cb, serverSettings) {
        Settings.serverSettings = jQuery.extend(true, {}, serverSettings); // deep copy;
        var local_settings = Settings.getLocalSettings();
        if (cb != null) {
            Settings.latesMergedSettings = Settings.mergeSettings(local_settings, serverSettings);
            cb(Settings.latesMergedSettings);
        }
    };

    Settings.registerCB = function (func) {
        if (Settings.callbacks.indexOf(func) < 0) {
            Settings.callbacks.push(func);
        }
    };

    Settings.unregisterCB = function (func) {
        if (func === 'all') {
            Settings.unregisterAllCBs();
        } else {
            var idx = Settings.callbacks.indexOf(func);
            if (idx >= 0) {
                Settings.callbacks.slice(idx, 1);
            } else {
                console.warn("Unbound callback that wasn't bound:", func);
            }
        }
    };

    Settings.unregisterAllCBs = function () { Settings.callbacks = [] };

    Settings._serverSettingsCB = function (settings) {
        Settings._get_cb(function (mergerdSettings) {
            for (var i=0; i < Settings.callbacks.length; i++) {
                Settings.callbacks[i](mergerdSettings);
            }
        }, settings);

    };

    Settings._localSettingsCb = function (settings) {
        Settings.latesMergedSettings = Settings.mergeSettings(settings, Settings.serverSettings);
        for (var i=0; i < Settings.callbacks.length; i++) {
            Settings.callbacks[i](Settings.latesMergedSettings);
        }
    };

    Settings.fetch = function (cb) {
        var own_cb = Settings._get_cb.bind(Settings, cb);
        if (Settings._serverSettings == null) {
            API.get.settings({
                success: own_cb,
                start: null,
                stop: null
            });
        } else {
            var local_settings = Settings.getLocalSettings();
            cb(Settings.mergeSettings(local_settings, Settings._serverSettings));
        }
    };

    Settings.save = function (local, setting, args) {
        /*
         * args can be a tuple or an array of tuples
         * currently supported:
         * setting              args-tuple(s)
         * "sizing"             [cat, sizing_opt] - sizing_opt may be one of ["none", "links", "recency", "null", null]
         * "display-type"       [cat, dt_opt] - dt_opt may be one of ["cloud", "links", "null", null]
         * "display-priority"   [cat, dp_opt] - dp_opt may be one of [<integer>, "null", null]
         */
        var partialSettings = {};
        switch (setting) {
            case "sizing":
            case "display-type":
            case "display-priority":
                partialSettings[setting] = {};
                var key, val;
                if (args[0].constructor === Array) {
                    for (var i = 0; i < args.length; i++) {
                        key = args[i][0];
                        val = args[i][1];
                        if (val == "null") {
                            val = null
                        }
                        partialSettings[setting][key] = val;
                    }
                } else {
                    key = args[0];
                    val = args[1];
                    if (val == "null") {
                        val = null
                    }
                    partialSettings[setting][key] = val;
                }
                break;
            default:
                throw "Setting " + setting + " is unknown!";
        }
        if (local) {
            Settings.updateLocalSettings(partialSettings);
        } else {
            Settings.updateServerSettings(partialSettings);
        }
    };

    Settings.saveOrder = function (local, categories) {
        var settings = [];
        for (var i = 0; i < categories.length; i++) {
            settings.push([categories[i], categories.length - i])
        }
        Settings.save(local, "display-priority", settings);
    };

    Settings.saveOrderLocal = Settings.saveOrder.bind(Settings, true);
    Settings.saveOrderServer = Settings.saveOrder.bind(Settings, false);
    Settings.saveLocal = Settings.save.bind(Settings, true);
    Settings.saveServer = Settings.save.bind(Settings, false);

    Settings.orderedCategories = function (allCategories) {
        if (Settings.latesMergedSettings == null) return allCategories;
        var res = [];
        var prior;
        for (var i = 0; i < allCategories.length; i++) {
            if (Settings.latesMergedSettings["display-priority"][allCategories[i]] != null) {
                prior = Settings.latesMergedSettings["display-priority"][allCategories[i]]
            } else {
                prior = 0;  // default prior
            }
            res.push([-prior, i, allCategories[i]])
        }
        res.sort(function (a, b) {
            // assuming a and b are arrays of same length
            // same positions must have same type
            for (var i = 0; i < a.length; i++) {
                // sorting asc.
                if (a[i] != b[i]) return -1 ? a[i] > b[i] : 1
            }
        });
        return res.map(function (elem) {
            return elem[elem.length - 1]
        });
    };

    // setup callback on WS settings event
    API.registerPollCB(Settings._serverSettingsCB.bind(Settings), 'settings');

    return Settings;

});
