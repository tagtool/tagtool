/**
 * Created by rouven on 03.05.17.
 */

define(['jquery', 'api'], function ($, API) {
    var UserCounter = $('#user-counter');

    UserCounter.updateUserCounter = function(count) {
        $('#user-count').text(count);
    };

    var registered = false;

    UserCounter.startUpdating = function () {
        if (registered) {
            console.warn('UserCounter update already bound before attempt to bind.');
            return;
        }
        API.registerPollCB(UserCounter.updateUserCounter.bind(UserCounter), 'user_count');
        registered = true;
    };
    UserCounter.startUpdating();

    UserCounter.stopUpdating = function () {
        if (!registered) {
            console.warn('UserCounter not bound before attempt to unbind.');
            return;
        }
        API.unregisterPollCB(UserCounter.updateUserCounter.bind(UserCounter), 'user_count');
        registered = false;
    };

    return UserCounter;
});
