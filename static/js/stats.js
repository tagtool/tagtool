require(
    ['jquery', 'api', 'd3', 'd3pie', 'color_generator'],
    function ($, api, d3, d3pie, ColorGenerator) {

    $('.modal-load-overlay').addClass('active');

    var apiCallsDone = 0;
    var API_CATS, API_LINKS;

    api.get.categories({
        success: function (data) {
            API_CATS = data;
            tryInit();
        }
    });
    api.get.links(false, false, {
        success: function (data) {
            API_LINKS = data;
            tryInit();
        }
    });

    function drawElemsPie(data) {
        var colors = ColorGenerator(data.length, .35, .42);
        var total = 0;
        for(var i=0; i < data.length; i++) {
            data[i]['color'] = colors[i];
            total += data[i]['value'];
        }
        new d3pie("elems-pie-chart", {
            "footer": {
                "text": "Distribution of the elements",
                "color": "#4e4e4e",
                "fontSize": 11,
                "font": "open sans",
                "location": "bottom-center"
            },
            "size": {
                "canvasHeight": 300,
                "canvasWidth": 400,
                "pieOuterRadius": "100%"
            },
            "data": {
                "content": data
            },
            "labels": {
                "outer": {
                    "pieDistance": 32
                },
                "inner": {
                    "format": "percentage",
                    "hideWhenLessThanPercentage": 10,
                    "centerDistance": 60
                },
                "mainLabel": {
                    "font": "verdana"
                },
                "percentage": {
                    "color": "#e1e1e1",
                    "font": "verdana",
                    "decimalPlaces": 0
                },
                "value": {
                    "style": "straight",
                    // "color": "#e1e1e1",
                    "color": "segment",
                    "font": "verdana"
                },
                "lines": {
                    "enabled": true,
                    "color": "#cccccc",
                    "style": "straight"
                },
                "truncation": {
                    "enabled": true
                }
            },
            "tooltips": {
                "enabled": true,
                "type": "placeholder",
                "string": "{label}: {value}, {percentage}%",
                "styles": {
                    "backgroundOpacity": 0.7
                }
            },
            "effects": {
                "pullOutSegmentOnClick": {
                    "effect": "linear",
                    "speed": 400,
                    "size": 8
                },
                "highlightSegmentOnMouseover": false
            },
            "misc": {
                "gradient": {
                    "enabled": false,
                    "percentage": 53
                }
            }
        });
    }

    var Graph = (function () {
        function catElemToId(cat, elem) {
            return btoa(cat)+"_"+btoa(elem);
        }
        function idToCatElem(id) {
            return id.split("_").map(function(e) { return atob(e); });
        }
        function calculateDegrees(w) {
            var degrees = [];
            var i, j;
            for (i = 0; i < w.length; i++) {
                var degree = 0;
                for (j = 0; j < w.length; j++) {
                    degree += w[i][j] < Infinity;
                }
                degrees.push(degree);
            }
            return degrees;
        }
        function calculateDistances(w) {
            var nodeCount = w.length;
            var dist = $.extend(true, [], w);
            var i, j, k;
            for (i=0; i < nodeCount; i++) {
                dist[i][i] = 0;
            }
            for (k=0; k < nodeCount; k++) {
                for (j=0; j < nodeCount; j++) {
                    for (i=0; i < nodeCount; i++) {
                        if (dist[i][j] > dist[i][k] + dist[k][j])
                            dist[i][j] = dist[i][k] + dist[k][j];
                    }
                }
            }
            return dist;
        }
        function calculateEccentricities(dist) {
            var eccentricities = [];
            for (var i=0; i < dist.length; i++) {
                eccentricities.push(Math.max.apply(null, dist[i]));
            }
            return eccentricities;
        }
        function calculateConnectedComponents(dist) {
            var visited = {};
            var components = [];
            for (var i=0; i < dist.length; i++) {
                if (visited.hasOwnProperty(i)) continue;
                var component = [];
                for (var j=0; j < dist.length; j++) {
                    if (dist[i][j] < Infinity) {
                        component.push(j);
                        visited[j] = 1;
                    }
                }
                components.push(component);
                if (Object.keys(visited).length === dist.length) break;
            }
            return components;
        }
        function calculateRadius(eccentricities) {
            return Math.min.apply(null, eccentricities);
        }
        function calculateDiameter(eccentricities) {
            return Math.max.apply(null, eccentricities);
        }

        function Graph(cats, links) {
            var that = this;
            var _vertices = [];
            var _idToIdx = {};
            for (var cat in cats) {
                if (!cats.hasOwnProperty(cat)) continue;
                var elems = cats[cat];
                for (var elem in elems) {
                    if (!elems.hasOwnProperty(elem)) continue;
                    var id = catElemToId(cat, elem);
                    _idToIdx[id] = _vertices.length;
                    _vertices.push(id);
                }
            }
            var nodeCount = _vertices.length;
            var _addMat = new Array(nodeCount).fill(Infinity).map(function () { return new Array(nodeCount).fill(Infinity); });
            for (var i=0; i < links.length; i++) {
                var idx1 = _idToIdx[catElemToId(links[i][0][0], links[i][0][1])];
                var idx2 = _idToIdx[catElemToId(links[i][1][0], links[i][1][1])];
                _addMat[idx1][idx2] = _addMat[idx2][idx1] = 1;
            }
            this.countVertices = function () {
                return _vertices.length;
            };
            this.countEdges = function () {
                return links.length;
            };
            var _degrees;
            this.degrees = function () {
                if (_degrees != null) return _degrees;
                return _degrees = calculateDegrees(_addMat);
            };
            var _distMat;
            this.distMat = function () {
                if (_distMat != null) return _distMat;
                return _distMat = calculateDistances(_addMat);
            };
            var _eccentricities;
            this.eccentricities = function () {
                if (_eccentricities != null) return _eccentricities;
                return _eccentricities = calculateEccentricities(that.distMat());
            };
            var _components;
            this.components = function () {
                if (_components != null) return _components;
                return _components = calculateConnectedComponents(that.distMat());
            };
            var _radius;
            this.radius = function () {
                if (_radius != null) return _radius;
                return _radius = calculateRadius(that.eccentricities());
            };
            var _diameter;
            this.diameter = function () {
                if (_diameter != null) return _diameter;
                return _diameter = calculateDiameter(that.eccentricities());
            };

            this.biggestCCGraph = function () {
                var biggestCC = that.components().sort(function (a, b) { return b.length - a.length })[0];
                var i;
                var ccCats = {};
                var usedIds = {};
                for (i=0; i < biggestCC.length; i++) {
                    var catelem = idToCatElem(_vertices[biggestCC[i]]);
                    if (!ccCats.hasOwnProperty(catelem[0])) {
                        ccCats[catelem[0]] = {};
                    }
                    ccCats[catelem[0]][catelem[1]] = 1;
                    usedIds[_vertices[biggestCC[i]]] = 1;
                }
                var ccLinks = [];
                for (i=0; i < links.length; i++) {
                    if (usedIds.hasOwnProperty(catElemToId(links[i][0][0], links[i][0][1])) &&
                        usedIds.hasOwnProperty(catElemToId(links[i][1][0], links[i][1][1]))) {
                        ccLinks.push(links[i]);
                    }
                }
                return new Graph(ccCats, ccLinks);
            };
        }

        Graph.prototype.density = function () {
            var v = this.countVertices();
            var e = this.countEdges();
            return 2*e/(v*(v-1));
        };

        return Graph;
    })();


    function linspace(start, stop, nsteps) {
        if (nsteps >= 2) {
            var delta = (stop-start)/(nsteps-1);
            return d3.range(nsteps).map(function(i) { return start + i * delta; });
        } else if (nsteps === 1) {
            return [(start + stop) / 2];
        } else {
            return [];
        }
    }

    function intTicks(start, stop, maxTicks) {
        var nsteps = Math.min(maxTicks, stop - start + 1);
        return linspace(start, stop, nsteps).map(function(i) { return Math.floor(i) });
    }

    function drawDegreeHistogram(selector, degrees) {
        var i;
        var maxDegree = d3.max(degrees);
        var data = new Array(maxDegree + 1);
        for(i=0; i < degrees.length; i++) {
            if (data[degrees[i]] != null) data[degrees[i]]["value"] += 1;
            else data[degrees[i]] = {"id": degrees[i], "value": 1};
        }
        for (i=0; i < data.length; i++) {
            if (data[i] == null) data[i] = {"id": i, "value": 0};
        }
        var maxCount = d3.max(data, function (d) { return d.value; });

        // var colorGradient = d3.scaleLinear().range(["#1c42ff", "#338dff"]).domain([0, 1]);
        var colorGradient = d3.interpolate("#914646", "#338dff");
        var height = 300;
        var width = 400;
        var paddingTop = 40;  // needed for tooltip
        var paddingRight = 70;  // needed for tooltip
        var paddingLeft = 40;  // needed for axis labels (excluded from padding)
        var paddingBottom = 30;  // needed for axis labels (excluded from padding)

        var scalePaddingLeft = 20;
        var scalePaddingBottom = 20;

        var bandwidth = (width - scalePaddingLeft - paddingLeft - paddingRight) / data.length - 1.5;
        var xScale = d3.scaleLinear()
            .domain([0, data.length - 1])
            .range([scalePaddingLeft+paddingLeft+bandwidth/2, width-paddingRight-bandwidth/2]);
        var xAxis = d3.axisBottom(xScale)
            .tickValues(intTicks(0, data.length - 1, 5))
            .tickFormat(d3.format("d"));
        function customXAxis(g) {
            g.call(xAxis);
            g.select(".domain").remove();
        }

        var yScale = d3.scaleLinear()
            .domain([0, maxCount])
            .range([height, paddingTop + scalePaddingBottom + paddingBottom]);
        var yAxis = d3.axisRight(yScale)
            .tickSize(width - paddingRight - paddingLeft)
            .tickValues(intTicks(0, maxCount, 13))
            .tickFormat(d3.format("d"));
        function customYAxis(g) {
            g.call(yAxis);
            g.select(".domain").remove();
            g.selectAll(".tick:not(:first-of-type) line").attr("stroke", "#777").attr("stroke-dasharray", "2,2");
            g.selectAll(".tick text").attr("x", 4).attr("dy", -4);
        }

        var svg = d3.select(selector).append('svg')
            .style('width', width + 'px')
            .style('height', height + 'px');

        svg.append("g")
            .attr("transform", "translate(" + paddingLeft + ", " + (-scalePaddingBottom - paddingBottom) +")")
            .call(customYAxis)
            .append("text")
            .text("Count")
            .attr("fill", "#474747")
            .style("text-anchor", "middle")
            .attr("transform", "translate(-13, " + ((scalePaddingBottom + paddingBottom + paddingTop + height) / 2) + ") rotate(-90)");
        svg.append("g")
            .attr("transform", "translate(" + 0 + ", " + (height - scalePaddingBottom - paddingBottom) + ")")
            .call(customXAxis)
            .append("text")
            .text("Degree")
            .attr("fill", "#474747")
            .style("text-anchor", "middle")
            .attr("transform", "translate(" + (paddingLeft + scalePaddingLeft + width - paddingRight) / 2 + ", 30)");

        function getDimensions(id) {
            var el = document.getElementById(id);
            var w = 0, h = 0;
            if (el) {
                var dimensions = el.getBBox();
                w = dimensions.width;
                h = dimensions.height;
            } else {
                console.log("error: getDimensions() " + id + " not found.");
            }
            return {w: w, h: h};
        }

        var barGroup = svg.selectAll(".degree-histogram-bar")
            .data(data)
            .enter()
            .append("g")
            .attr("class", "degree-histogram-bar");

        function showTooltip(data) {
            var fadeInSpeed = 250;
            var tt = d3.select("#degree-hist-tooltip");
            tt.select("#degree-hist-tootlip-degree")
                .text("Degree: " + data.id);
            tt.select("#degree-hist-tootlip-count")
                .text("Count: " + data.value);
            tt.select("rect")
                .attr("height", 0)
                .attr("width", 0);
            var dims = getDimensions("degree-hist-tooltip");
            tt.style("opacity", 0)
                .transition()
                .duration(fadeInSpeed)
                .style("opacity", 1);
            tt.select("rect")
                .attr("height", dims.h + 4)
                .attr("width", dims.w + 8);

            moveTooltip();
        }

        function moveTooltip () {
            d3.select("#degree-hist-tooltip")
                .attr("transform", function (d) {
                    var mouseCoords = d3.mouse(this.parentNode);
                    var x = mouseCoords[0] + 4 + 2;
                    var y = mouseCoords[1] - (2 * 4) - 2;
                    return "translate(" + x + "," + y + ")";
                });
        }

        function hideTooltip(index) {
            d3.select("#degree-hist-tooltip")
                .style("opacity", function () {
                    return 0;
                });

            // move the tooltip offscreen. This ensures that when the user next mouseovers the segment the hidden
            // element won't interfere
            d3.select("#degree-hist-tooltip")
                .attr("transform", function (d, i) {
                    // klutzy, but it accounts for tooltip padding which could push it onscreen
                    var x = width + 1000;
                    var y = height + 1000;
                    return "translate(" + x + "," + y + ")";
                });
        }

        barGroup.append('rect')
            .attr('class', 'bar')
            .on("mouseover", function (d) {
                showTooltip(d);
            })
            .on("mousemove", function () {
                moveTooltip();
            })
            .on("mouseout", function (d) {
                hideTooltip();
            })
            .attr("x", function (d, i) {
                return xScale(d.id) - bandwidth/2;
            })
            .attr("y", function (d, i) {
                return height - (scalePaddingBottom + paddingBottom);
            })
            .attr("width", function (d, i) {
                return bandwidth
            })
            .attr("fill", function (d, i) {
                // "#3549cc"
                // return 'rgb(255, ' + Math.round(i / 2) + ', ' + i + ')'
                return colorGradient(i/data.length);
            })
            .attr("height", 0)
            .transition()
            .duration(2500)
            .delay(function (d, i) {
                return i * 1000 / data.length;
            })
            .attr("y", function (d, i) {
                return height - (scalePaddingBottom + paddingBottom) - (height - yScale(d.value));
            })
            .attr("height", function (d, i) {
                return height - yScale(d.value);
            });

        var tooltip = svg.append("g")
            .attr("id", "degree-hist-tooltip");
        tooltip.append("rect")
            .attr("rx", 2)
            .attr("ry", 2)
            .attr("y", -12)
            .attr("opacity", 0.7)
            .style("fill", "#000");
        tooltip.append("text")
            .attr("fill", "#efefef")
            .style("font-size", "11")
            .style("font-family", "arial")
            .attr("dx", 4)
            .attr("id", "degree-hist-tootlip-degree");
        tooltip.append("text")
            .attr("fill", "#efefef")
            .style("font-size", "11")
            .style("font-family", "arial")
            .attr("dx", 4)
            .attr("dy", 12)
            .attr("id", "degree-hist-tootlip-count");
    }

    function tryInit() {
        if (++apiCallsDone < 2) return;
        console.log('API_CATS', API_CATS);
        console.log('API_LINKS', API_LINKS);
        var $elemStatsTable = $('table.elem-stats');
        var $elemStatsSampleRow = $elemStatsTable.find('tr.sample-row');
        var totalElemCount = 0;
        var pieData = [];
        $('.cat-count').text(Object.keys(API_CATS).length);
        for(var key in API_CATS) {
            if (!API_CATS.hasOwnProperty(key)) continue;
            var elemCount = Object.keys(API_CATS[key]).length;
            pieData.push({'label': key, 'value': elemCount});
            totalElemCount += elemCount;
            var $newRow = $elemStatsSampleRow.clone().removeClass('hidden');
            $newRow.find('.cat-name').text(key);
            $newRow.find('.elem-cat-total').text(elemCount);
            $elemStatsSampleRow.after($newRow);
        }
        $('.elem-total').text(totalElemCount);
        var graph = new Graph(API_CATS, API_LINKS);
        console.log(graph);
        $('.link-total').text(API_LINKS.length);
        var components = graph.components();
        $('.graph-cc-count').text(components.length);
        if (components.length === 0) {
            $('.graph-density, .graph-radius, .graph-diameter, .graph-radius-biggest-cc,' +
              '.graph-diameter-biggest-cc, .graph-elem-count-cc').parents('tr').remove();
            $('.graph-container').remove();
        }
        else {
            if (totalElemCount >= 2) {
                $('.graph-density').text(graph.density().toFixed(4));
            } else {
                $('.graph-density').parents('tr').remove();
            }
            if (components.length === 1) {
                $('.graph-radius').text(graph.radius());
                $('.graph-diameter').text(graph.diameter());
                $('.graph-radius-biggest-cc, .graph-diameter-biggest-cc, .graph-elem-count-cc').parents('tr').remove();
            } else {
                var biggestCCGraph = graph.biggestCCGraph();
                $('.graph-radius-biggest-cc').text(biggestCCGraph.radius());
                $('.graph-diameter-biggest-cc').text(biggestCCGraph.diameter());
                $('.graph-radius, .graph-diameter').parents('tr').remove();
                $('.graph-elem-count-cc').text(graph.components().map(function (cc) {
                    return cc.length;
                }).join(', '));
            }
        }

        $('.modal-load-overlay').removeClass('active');
        $('#main').show();
        if (components.length !== 0) {
            drawDegreeHistogram("#degree-histogram", graph.degrees());
            drawElemsPie(pieData);
        }
    }
});
