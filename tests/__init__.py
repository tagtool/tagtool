import sys
import os
import logging
logging.basicConfig(level=logging.DEBUG)

# tests need to import stuff from the root dir
sys.path.insert(0,
                os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
