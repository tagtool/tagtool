import pytest
from copy import deepcopy

import os
import time
import re
import threading
from operator import itemgetter

import auth_util
from models.db_diff import DB_Diff
from bots.chat_bot_base import ChatBotBase
from logging import getLogger
log = getLogger(__name__)

from tests.models_settings_test import settings  # settings fixture (needed)
from .util_store import store  # store fixture (needed)


EXAMPLE_URL = 'http://tags.example.com/test'
BOT_NAME = 'bot'
DIGEST_MINUTES = 1/400.0


class DummyBot(ChatBotBase):
    def __init__(self, *args, **kwargs):
        log.debug("%s %s Init DummyBot %s" % (os.getpid(),
                                              threading.current_thread(),
                                              self))
        self.out_buffer = []
        self.out_buffer_md = []
        if 'digest_minutes' in kwargs:
            self.timeout = kwargs['digest_minutes']*60
        else:
            self.timeout = 0
        super().__init__(*args, **kwargs)
        log.debug('DummyBot._listener: %s', self._listener)

    def emit(self, message, md_message, *args, **kwargs):
        log.debug('%s %s Bot sending msg %s' % (os.getpid(),
                                                threading.current_thread(),
                                                message))
        log.debug('%s %s Bot sending md_msg %s' % (os.getpid(),
                                                   threading.current_thread(),
                                                   md_message))
        if not (args or kwargs):
            self.out_buffer.append(message)
            self.out_buffer_md.append(md_message)
        else:
            self.out_buffer.append((message, args, kwargs))
            self.out_buffer_md.append((md_message, args, kwargs))

    def reset_buffer(self, wait=True):
        if wait:
            self.wait()
        log.debug('%s %s Bot message buffer cleared.' %
                  (os.getpid(), threading.current_thread()))
        self.out_buffer = []
        self.out_buffer_md = []

    def wait(self, simulate_by_force_flush=True):
        if self.timeout:
            if simulate_by_force_flush:
                self._listener.force_digest_flush()
            # Check if digest timer is running. If not: nothing to wait for!
            elif self._listener._digest_timer is not None:
                time.sleep(self.timeout*1.8)

    def wait_too_short(self):
        if self.timeout:
            # Check if digest timer is running. If not: nothing to wait for!
            if self._listener._digest_timer is not None:
                time.sleep(self.timeout*.25)


@pytest.fixture(params=[True, False])
def bot(store, settings, request):
    return DummyBot(store, settings, BOT_NAME, EXAMPLE_URL,
                    allow_cat_actions=request.param)


@pytest.fixture
def digest_bot(store, settings):
    return DummyBot(store, settings, BOT_NAME, EXAMPLE_URL,
                    digest_minutes=DIGEST_MINUTES)


@pytest.fixture
def password_bot(store, settings):
    return DummyBot(store, settings, BOT_NAME, EXAMPLE_URL,
                    allow_cat_actions=True,
                    password='password', salt='salt')


class TestChatBotBase(object):
    CATEGORIES = {"foo": list(map(str, range(1, 7))),
                  "bar": list(map(str, range(4, 10)))}

    LINKS = [[["foo", "4"], ["bar", "4"]],
             [["foo", "5"], ["bar", "5"]],
             [["foo", "6"], ["bar", "6"]],
             [["foo", "1"], ["foo", "2"]],
             [["foo", "2"], ["bar", "8"]]]

    UPDATE_MSG = ChatBotBase._MESSAGES['updates'].format(name=BOT_NAME,
                                                         url=EXAMPLE_URL)

    def _fill_db(self, store):
        """Fills the db with categories "foo" and "bar".

        foo -> "1","2","3","4","5","6"
        bar -> "4","5","6","7","8","9"

        links: foo.4 - bar.4
               foo.5 - bar.5
               foo.6 - bar.6
               foo.1 - foo.2
               foo.2 - bar.8"""
        diff = DB_Diff()
        for cat in self.CATEGORIES:
            store.add_category(cat, flush=False, diff=diff)
        for cat, elems in self.CATEGORIES.items():
            for elem in elems:
                store.add_to_category(cat, elem, flush=False, diff=diff)
        for l in self.LINKS:
            store.add_link(*l, flush=False, diff=diff)
        store.flush_db(force=True, high_level_diff=diff)

    @pytest.mark.parametrize('private', [True, False])
    def test_help_msg(self, bot, private):
        msg = 'help'
        if not private:
            msg = '@' + BOT_NAME + ' ' + msg
        bot.receive(msg, private=private)
        assert len(bot.out_buffer) == 1
        assert ChatBotBase.HELP_STRING.format(
            BOT_NAME, url=EXAMPLE_URL) in bot.out_buffer[0]
        assert TestChatBotBase.UPDATE_MSG not in bot.out_buffer[0]

    def test_ignores_messages_without_mention(self, bot):
        bot.receive('help')
        assert not bot.out_buffer

    @pytest.mark.parametrize('exists', [True, False])
    def test_add_cloud(self, bot, exists):
        if exists:
            bot.store.add_category('foo')
            bot.reset_buffer()
        bot.receive('add cloud foo', private=True)
        cat_enabled = bot.cat_enabled
        if cat_enabled and not exists:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].count(
                ChatBotBase._format_msg('new_cat', ['foo'], None)) == 1
            assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1
            assert bot.store.get_all_categories() == ['foo']
        else:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].lower().count('sorry') >= 0
            if not cat_enabled:
                assert bot.out_buffer[0] ==\
                       ChatBotBase.DISABLED_CMD_STRING.format(url=EXAMPLE_URL)
            else:
                assert bot.out_buffer[0] !=\
                       ChatBotBase.DISABLED_CMD_STRING.format(url=EXAMPLE_URL)
            if not exists:
                assert not bot.store.get_all_categories()

    @pytest.mark.parametrize(['key', 'arg_num'], [
        ['new_cat', 1],
        ['new_elem', 2],
        ['new_link', 4],
        ['remove_cat', 1],
        ['remove_elem', 2],
        ['remove_link', 4],
        ['rename_elem_in_cat', 3],
        ['rename_elem', 4],
        ['rename_cat', 2]
    ])
    @pytest.mark.parametrize('new', [True, False])
    def test_new_fragment_is_used(self, key, arg_num, new):
        impl_creation_idxs = ChatBotBase._MESSAGES[key + '_impl_creation_idxs']
        msg = ChatBotBase._format_msg(key, list(map(str, range(arg_num))),
                                      [new] * arg_num)
        for i in range(arg_num):
            assert '"%i"' % i in msg
        if impl_creation_idxs is not None and new:
            assert msg.count(ChatBotBase._FRAGMENTS['new']) ==\
                len(impl_creation_idxs)
            for i in impl_creation_idxs:
                assert '"%i"' % i+ChatBotBase._FRAGMENTS['new'] in msg
        else:
            assert ChatBotBase._FRAGMENTS['new'] not in msg

    @pytest.mark.parametrize('force', [True, False])
    @pytest.mark.parametrize(['cat_exists', 'elem_exists'],
                             [[True, False],
                              [True, True],
                              [False, False]])
    def test_add_elem(self, bot, cat_exists, elem_exists, force):
        cat_enabled = bot.cat_enabled
        if cat_exists:
            bot.store.add_category('foo')
        if elem_exists:
            bot.store.add_to_category('foo', 'bar')
        bot.reset_buffer()
        msg = 'add foo bar'
        if force:
            msg += "!"
        bot.receive(msg, private=True)
        if ((cat_enabled and force) or cat_exists) and not elem_exists:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].count(
                ChatBotBase._format_msg('new_elem', ['bar', 'foo'],
                                        [False, not cat_exists])) == 1
            assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1
            assert bot.store.get_all_categories() == ['foo']
            assert bot.store.get_all_category_members('foo') == [['foo', 'bar']]
        else:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].lower().count('sorry') > 0
            if not elem_exists:
                assert not bot.store.get_all_categories()

    @pytest.mark.parametrize('force', [True, False])
    @pytest.mark.parametrize(['cat_exists', 'elem_exists', 'link_exists'],
                             [[True, False, False],
                              [True, True, True],
                              [True, True, False],
                              [False, False, False]])
    def test_add_link(self, bot, cat_exists, elem_exists, link_exists, force):
        # linking ['a', 'a'] to ['foo', 'bar'].
        # ['a', 'a'] always exists.
        link = [['a', 'a'], ['foo', 'bar']]
        bot.store.add_to_category('a', 'a')
        cat_enabled = bot.cat_enabled
        if cat_exists:
            bot.store.add_category('foo')
        if elem_exists:
            bot.store.add_to_category('foo', 'bar')
        if link_exists:
            bot.store.add_link(*link)
        bot.reset_buffer()
        db_before = deepcopy(bot.store.db)
        msg = 'link a a and foo bar'
        if force:
            msg += "!"
        bot.receive(msg, private=True)
        if (link_exists or
                not cat_enabled and not cat_exists or
                not force and not elem_exists):
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].lower().count('sorry') > 0
            # don't tell the users the action is forbidden. Tell them the
            # actual problem.
            assert bot.out_buffer[0] !=\
                ChatBotBase.DISABLED_CMD_STRING.format(url=EXAMPLE_URL)
            assert bot.store.db == db_before
        else:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].count(
                ChatBotBase._format_msg('new_link', ['a', 'a', 'foo', 'bar'],
                                        [False, False, not cat_exists,
                                         not elem_exists])) == 1
            assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1
            assert bot.store.get_all_links(*link) == [link]

    @pytest.mark.parametrize('as_diff_action', [True, False])
    def test_listen_to_add_cloud(self, bot, as_diff_action):
        if as_diff_action:
            bot.store.diff_action(
                {'insertions': {'categories': ['foo']}})
        else:
            bot.store.add_category('foo')
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('new_cat', ['foo'], False)) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    @pytest.mark.parametrize('as_diff_action', [True, False])
    def test_listen_to_add_elem(self, bot, as_diff_action):
        if as_diff_action:
            bot.store.diff_action(
                {'insertions': {'elements': [['foo', 'bar']]}})
        else:
            bot.store.add_to_category('foo', 'bar')
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('new_elem', ['bar', 'foo'],
                                    [False, True])) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    @pytest.mark.parametrize('as_diff_action', [True, False])
    def test_listen_to_add_link(self, bot, as_diff_action):
        if as_diff_action:
            bot.store.diff_action(
                {'insertions': {'links': [[['a', 'b'], ['c', 'd']]]}})
        else:
            bot.store.add_link(['a', 'b'], ['c', 'd'])
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(ChatBotBase._format_msg(
            'new_link', ['a', 'b', 'c', 'd'], True)) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    @pytest.mark.parametrize('exists', [True, False])
    def test_remove_cloud(self, bot, exists):
        if exists:
            bot.store.add_category('foo')
            bot.reset_buffer()
        bot.receive('delete foo', private=True)
        cat_enabled = bot.cat_enabled
        if cat_enabled and exists:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].count(
                ChatBotBase._format_msg('remove_cat', ['foo'], False)) == 1
            assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1
            assert not bot.store.get_all_categories()
        else:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].lower().count('sorry') >= 0
            if not cat_enabled:
                assert bot.out_buffer[0] == \
                       ChatBotBase.DISABLED_CMD_STRING.format(
                           url=EXAMPLE_URL)
            else:
                assert bot.out_buffer[0] != \
                       ChatBotBase.DISABLED_CMD_STRING.format(
                           url=EXAMPLE_URL)
            if exists:
                assert bot.store.get_all_categories() == ['foo']
            else:
                assert not bot.store.get_all_categories()

    @pytest.mark.parametrize(['cat_exists', 'elem_exists'],
                             [[True, False],
                              [True, True],
                              [False, False]])
    def test_remove_elem(self, bot, cat_exists, elem_exists):
        if cat_exists:
            bot.store.add_category('foo')
        if elem_exists:
            bot.store.add_to_category('foo', 'bar')
        bot.reset_buffer()
        bot.receive('delete foo bar', private=True)
        if cat_exists:
            assert bot.store.get_all_categories() == ['foo']
        else:
            assert not bot.store.get_all_categories()
        if cat_exists and elem_exists:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].count(
                ChatBotBase._format_msg('remove_elem', ['bar', 'foo'], False)
            ) == 1
            assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1
            assert not bot.store.get_all_category_members('foo')
        else:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].lower().count('sorry') > 0
            if cat_exists and elem_exists:
                assert bot.store.get_all_category_members('foo') == [
                    ['foo', 'bar']]
            elif cat_exists:
                assert not bot.store.get_all_category_members('foo')

    @pytest.mark.parametrize(['cat_exists', 'elem_exists', 'link_exists'],
                             [[True, True, True],
                              [True, True, False],
                              [True, False, False],
                              [False, False, False]])
    def test_remove_link(self, bot, cat_exists, elem_exists, link_exists):
        # unlinking ['a', 'a'] from ['foo', 'bar'].
        # ['a', 'a'] always exists.
        link = [['a', 'a'], ['foo', 'bar']]
        bot.store.add_to_category('a', 'a')
        if cat_exists:
            bot.store.add_category('foo')
        if elem_exists:
            bot.store.add_to_category('foo', 'bar')
        if link_exists:
            bot.store.add_link(*link)
        bot.reset_buffer()
        db_before = deepcopy(bot.store.db)
        bot.receive('unlink a a and foo bar', private=True)
        if cat_exists:
            assert sorted(bot.store.get_all_categories()) == ['a', 'foo']
        else:
            assert bot.store.get_all_categories() == ['a']
        if elem_exists:
            assert bot.store.get_all_category_members('foo') == [['foo', 'bar']]
        elif cat_exists:
            assert not bot.store.get_all_category_members('foo')
        assert bot.store.get_all_category_members('a') == [['a', 'a']]
        if not (cat_exists and elem_exists and link_exists):
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].lower().count('sorry') > 0
            # don't tell the users the action is forbidden. Tell them the
            # actual problem.
            assert bot.out_buffer[0] != \
                ChatBotBase.DISABLED_CMD_STRING.format(url=EXAMPLE_URL)
            assert bot.store.db == db_before
        else:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].count(
                ChatBotBase._format_msg('remove_link',
                                        ['a', 'a', 'foo', 'bar'], False)) == 1
            assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1
            assert bot.store.get_all_links() == []

    @pytest.mark.parametrize('in_cat', [True, False])
    def test_rename_elem(self, bot, in_cat):
        bot.store.add_to_category('foo', 'a')
        bot.store.add_category('bar')
        bot.reset_buffer()
        if in_cat:
            bot.receive('rename foo a to b', private=True)
            assert bot.store.get_all_category_members('foo') == [['foo', 'b']]
        else:
            bot.receive('rename foo a to bar b', private=True)
            assert bot.store.get_all_category_members('foo') == []
            assert bot.store.get_all_category_members('bar') == [['bar', 'b']]

    def test_rename_cat(self, bot):
        bot.store.add_to_category('foo', 'a')
        bot.reset_buffer()
        bot.receive('rename foo to bar', private=True)
        if bot.cat_enabled:
            assert bot.store.get_all_category_members('bar') == [['bar', 'a']]
        else:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].lower().count('sorry') >= 0

    @pytest.mark.parametrize('with_elems', [True, False])
    @pytest.mark.parametrize('as_diff_action', [True, False])
    def test_listen_to_remove_cloud(self, bot, with_elems, as_diff_action):
        bot.store.add_category('foo')
        if with_elems:
            bot.store.add_to_category('foo', 'bar')
        bot.reset_buffer()
        if as_diff_action:
            bot.store.diff_action(
                {'deletions': {'categories': ['foo']}})
        else:
            bot.store.remove_category('foo')
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('remove_cat', ['foo'], False)) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    @pytest.mark.parametrize('as_diff_action', [True, False])
    def test_listen_to_remove_elem(self, bot, as_diff_action):
        bot.store.add_to_category('foo', 'bar')
        bot.reset_buffer()
        if as_diff_action:
            bot.store.diff_action(
                {'deletions': {'elements': [['foo', 'bar']]}})
        else:
            bot.store.remove_from_category('foo', 'bar')
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('remove_elem', ['bar', 'foo'], False)) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    @pytest.mark.parametrize('as_diff_action', [True, False])
    def test_listen_to_remove_link(self, bot, as_diff_action):
        bot.store.add_link(['a', 'b'], ['c', 'd'])
        bot.reset_buffer()
        if as_diff_action:
            bot.store.diff_action(
                {'deletions': {'links': [[['a', 'b'], ['c', 'd']]]}})
        else:
            bot.store.remove_link(['a', 'b'], ['c', 'd'])
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('remove_link', ['a', 'b', 'c', 'd'],
                                    False)) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    @pytest.mark.parametrize('in_cat', [True, False])
    @pytest.mark.parametrize('as_diff_action', [True, False])
    def test_listen_to_rename_element(self, bot, in_cat, as_diff_action):
        bot.store.add_to_category('a', 'b')
        if not in_cat:
            bot.store.add_category('c')
        to_cat = 'a' if in_cat else 'c'
        bot.reset_buffer()
        if as_diff_action:
            bot.store.diff_action(
                {'renamings': {'elements': [[['a', 'b'], [to_cat, 'd']]]}})
        else:
            bot.store.rename_element('a', 'b', to_cat, 'd')
        assert len(bot.out_buffer) == 1
        if in_cat:
            assert bot.out_buffer[0].count(
                ChatBotBase._format_msg('rename_elem_in_cat', ['a', 'b', 'd'],
                                        False)) == 1
        else:
            assert bot.out_buffer[0].count(
                ChatBotBase._format_msg('rename_elem', ['a', 'b', to_cat, 'd'],
                                        False)) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    @pytest.mark.parametrize('as_diff_action', [True, False])
    def test_listen_to_rename_category(self, bot, as_diff_action):
        bot.store.add_category('foo')
        bot.reset_buffer()
        if as_diff_action:
            bot.store.diff_action(
                {'renamings': {'categories': [['foo', 'bar']]}})
        else:
            bot.store.rename_category('foo', 'bar')
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('rename_cat', ['foo', 'bar'], False)) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    def test_listen_to_diff_action(self, bot):
        bot.store.diff_action(
            {'insertions': {'categories': ['foo', 'bar']}})
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('new_cat', ['foo'], False)) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('new_cat', ['bar'], False)) == 1
        assert bot.out_buffer[0].count(TestChatBotBase.UPDATE_MSG) == 1

    def test_show_categories(self, bot):
        self._fill_db(bot.store)
        bot.reset_buffer()
        bot.receive('list all categories', private=True)
        assert len(bot.out_buffer) == 1
        m = bot.out_buffer[0]
        assert m.find('"foo"') > 0
        assert m.find('"bar"') > 0
        assert m == bot._nice_print_list(None, [])
        assert TestChatBotBase.UPDATE_MSG not in m

    def test_show_elements(self, bot):
        self._fill_db(bot.store)
        bot.reset_buffer()
        bot.receive('list all elements of foo', private=True)
        assert len(bot.out_buffer) == 1
        m = bot.out_buffer[0]
        for e in TestChatBotBase.CATEGORIES['foo']:
            assert m.find('"' + str(e) + '"') > 0
        assert m == bot._nice_print_list('cat', 'foo')
        assert TestChatBotBase.UPDATE_MSG not in m

    def test_show_links(self, bot):
        self._fill_db(bot.store)
        bot.reset_buffer()
        bot.receive('list all links of foo 2', private=True)
        assert len(bot.out_buffer) == 1
        m = bot.out_buffer[0]
        for e in ('"foo" "1"', '"bar" "8"'):
            assert m.find(e) > 0
        assert m == bot._nice_print_list('elem', ['foo', '2'])
        assert TestChatBotBase.UPDATE_MSG not in m

    @pytest.mark.parametrize(('query', 'lines', 'contains'), (
        ('HTML5', 3, ('"Person" "Marco Polo"',
                      '"Tag" "HTML5"',
                      '"Tag" "JavaScript"')),
        ('html5', 3, ('"Person" "Marco Polo"',
                      '"Tag" "HTML5"',
                      '"Tag" "JavaScript"')),
        ('banana', 1, ('sorry',)),
        ('JavaScript', 3, ('"Tag" "JavaScript"',
                           '"Language" "JavaScript"')),
    ))
    def test_search(self, bot, query, lines, contains):
        bot.store.diff_action(
            {'insertions': {
                'categories': ['Tag', 'Person'],
                'elements': [['Tag', 'HTML5'],
                             ['Tag', 'XML'],
                             ['Tag', 'JavaScript'],
                             ['Person', 'Hans Peter'],
                             ['Person', 'Marco Polo'],
                             ['Language', 'JavaScript']],
                'links': [[['Tag', 'HTML5'], ['Tag', 'JavaScript']],
                          [['Tag', 'HTML5'], ['Person', 'Marco Polo']],
                          [['Tag', 'XML'], ['Person', 'Marco Polo']],
                          [['Tag', 'XML'], ['Person', 'Hans Peter']]]
            }}
        )
        bot.reset_buffer()
        bot.receive('search ' + query, private=True)
        assert len(bot.out_buffer) == 1
        res = bot.out_buffer[0]
        assert len(res.splitlines()) == lines
        res_lower = res.lower()
        for substring in contains:
            assert substring.lower() in res_lower

    def test_no_digest(self, bot):
        bot.store.add_category('a')
        bot.store.add_category('b')
        assert len(bot.out_buffer) == 2

    @pytest.mark.parametrize('real_wait', [True, False])
    def test_digest(self, digest_bot, real_wait):
        digest_bot.store.add_category('a')
        digest_bot.store.add_category('b')
        assert len(digest_bot.out_buffer) == 0
        digest_bot.wait(simulate_by_force_flush=not real_wait)
        assert len(digest_bot.out_buffer) == 1

    @pytest.mark.parametrize('real_wait', [True, False])
    def test_single_after_double_digest(self, digest_bot, real_wait):
        digest_bot.store.add_category('a')
        digest_bot.store.add_category('b')
        digest_bot.reset_buffer()
        digest_bot.store.add_category('c')
        digest_bot.wait(simulate_by_force_flush=not real_wait)
        assert len(digest_bot.out_buffer) == 1

    @pytest.mark.parametrize('real_wait', [True, False])
    def test_cascaded_digest(self, digest_bot, real_wait):
        digest_bot.store.add_category('a')
        digest_bot.wait_too_short()
        digest_bot.store.add_category('b')
        digest_bot.wait_too_short()
        digest_bot.store.add_category('c')
        assert len(digest_bot.out_buffer) == 0
        digest_bot.wait(simulate_by_force_flush=not real_wait)
        assert len(digest_bot.out_buffer) == 1

    def test_chat_msg_gives_instant_result(self, digest_bot):
        digest_bot.store.add_category('a')
        digest_bot.reset_buffer()
        assert len(digest_bot.out_buffer) == 0
        digest_bot.receive("add a b", private=True)
        assert len(digest_bot.out_buffer) == 1
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1

    @pytest.mark.parametrize(['as_diff', 'as_digest'], [
        [True, False],
        [False, True],
        [True, True]
    ])
    def test_links_in_digest_diff_msg_are_sorted(self, bot, digest_bot,
                                                 as_diff, as_digest):
        if as_digest:
            # ugly workaround https://github.com/pytest-dev/pytest/issues/349
            bot = digest_bot
        for i in range(1, 4):
            bot.store.add_to_category('foo', str(i))
        bot.reset_buffer()

        linkings = [
            [[['foo', '1'], ['foo', '2']],
             [['foo', '2'], ['foo', '3']]],
            [[['foo', '2'], ['foo', '3']],
             [['foo', '1'], ['foo', '2']]],
            [[['foo', '2'], ['foo', '1']],
             [['foo', '3'], ['foo', '2']]],
            [[['foo', '3'], ['foo', '2']],
             [['foo', '2'], ['foo', '1']]],
        ]
        ins_msgs = []
        del_msgs = []
        for linking in linkings:
            bot.reset_buffer()
            if as_diff:
                bot.store.diff_action({'insertions': {'links': linking}})
            else:
                for link in linking:
                    bot.store.add_link(*link)
            bot.wait()
            assert len(bot.out_buffer) == 1
            ins_msgs.append(bot.out_buffer[0])

            bot.reset_buffer()
            if as_diff:
                bot.store.diff_action({'deletions': {'links': linking}})
            else:
                for link in linking:
                    bot.store.remove_link(*link)
            bot.wait()
            assert len(bot.out_buffer) == 1
            del_msgs.append(bot.out_buffer[0])
        if not as_digest or as_diff:
            for i in range(1, len(linkings)):
                assert ins_msgs[0] == ins_msgs[i]
                assert del_msgs[0] == del_msgs[i]
        else:  # actions are ordered in digest mode
            l1 = re.match('\D*(\d)\D*(\d)\D*\d\D*\d.*\D*', ins_msgs[0]).groups()
            l2 = re.match('\D*\d\D*\d.*\D*(\d)\D*(\d)\D*', ins_msgs[0]).groups()
            order12 = '\D*' + '\D*'.join(l1 + l2) + '\D*'
            order21 = '\D*' + '\D*'.join(l2 + l1) + '\D*'
            for i in range(1, len(linkings)):
                if tuple(map(itemgetter(1), linkings[i][0])) in\
                        (l1, tuple(reversed(l1))):
                    order = order12
                else:
                    order = order21
                ins_msg = ins_msgs[i]
                del_msg = del_msgs[i]
                assert re.match(order, ins_msg) is not None
                assert re.match(order, del_msg) is not None

    AUTH_BACK_LINK_RE =\
        r'(https?://[^/]+/[^?]*?[^=]+=([0-9a-fA-F]+))'

    def test_back_link_includes_auth_with_password(self, password_bot):
        password_bot.store.add_category('foo')
        assert len(password_bot.out_buffer) == 1
        msg = password_bot.out_buffer[0]
        m = re.search(TestChatBotBase.AUTH_BACK_LINK_RE, msg)
        assert m is not None
        hashed_auth = auth_util.auth_token(password_bot.password,
                                           password_bot.salt)
        assert m.groups()[1].lower() == hashed_auth.lower()

    def test_back_link_includes_no_auth_without_password(self, bot):
        bot.store.add_category('foo')
        assert len(bot.out_buffer) == 1
        m = re.search(TestChatBotBase.AUTH_BACK_LINK_RE,
                      bot.out_buffer[0])
        assert m is None

    def test_help_in_digest_mode_does_not_send_digest(self, digest_bot):
        assert len(digest_bot.out_buffer) == 0
        digest_bot.store.add_category('foo')
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1
        digest_bot.reset_buffer(wait=False)
        assert len(digest_bot.out_buffer) == 0
        digest_bot.receive('help', private=True)
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1

    def test_digest_bot_handles_restart_1(self, digest_bot):
        assert len(digest_bot.out_buffer) == 0
        digest_bot.store.add_category('foo123')
        digest_bot.store.add_category('bar123')
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1
        assert 'foo123' in digest_bot.out_buffer[0]
        assert 'bar123' in digest_bot.out_buffer[0]
        digest_bot.reset_buffer(wait=False)
        digest_bot = DummyBot(digest_bot.store, digest_bot.settings, BOT_NAME,
                              EXAMPLE_URL, digest_minutes=DIGEST_MINUTES)
        assert len(digest_bot.out_buffer) == 0
        digest_bot.store.add_category('foobar321')
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1
        assert 'foo123' not in digest_bot.out_buffer[0]
        assert 'bar123' not in digest_bot.out_buffer[0]
        assert 'foobar321' in digest_bot.out_buffer[0]

    def test_digest_bot_handles_restart_2(self, digest_bot):
        assert len(digest_bot.out_buffer) == 0
        digest_bot.store.add_category('foo123')
        digest_bot.store.add_category('bar123')
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1
        assert 'foo123' in digest_bot.out_buffer[0]
        assert 'bar123' in digest_bot.out_buffer[0]
        digest_bot.reset_buffer(wait=False)
        digest_bot = DummyBot(digest_bot.store, digest_bot.settings, BOT_NAME,
                              EXAMPLE_URL, digest_minutes=DIGEST_MINUTES)
        assert len(digest_bot.out_buffer) == 0
        digest_bot.receive('help', private=True)
        assert len(digest_bot.out_buffer) == 1
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1
        assert 'foo123' not in digest_bot.out_buffer[0]
        assert 'bar123' not in digest_bot.out_buffer[0]

    def test_bot_matches_query_templates_iff_msg_not_addressed_to_bot(
            self, store, settings):
        self._fill_db(store)
        bot = DummyBot(store, settings, BOT_NAME, EXAMPLE_URL,
                       allow_cat_actions=True,
                       query_templates=[('foo', 'add{{bar}}'),
                                        ('foo', 'keyword{{bar}}')])
        # understood private message
        bot.receive('add 4', private=True)
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('new_cat', ['4'], False)) == 1
        store.remove_category('4')
        bot.reset_buffer()
        # understood non-private message
        bot.receive('@' + BOT_NAME + ': add 4')
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._format_msg('new_cat', ['4'], False)) == 1
        store.remove_category('4')
        bot.reset_buffer()
        # not understood private message
        bot.receive('keyword 4', private=True)
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].lower().count('sorry') >= 0
        bot.reset_buffer()
        # not understood non-private message
        bot.receive('keyword 4')
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._LIST_MESSAGES['head']['template']) == 1
        bot.reset_buffer()
        bot.receive('add 4')
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._LIST_MESSAGES['head']['template']) == 1

    @pytest.mark.parametrize(['ex_cat1', 'ex_cat2', 'ex_elem'], [
        [True, True, False], [True, False, False], [False, True, True],
        [False, True, False], [False, False, False]
    ])
    def test_query_templates_with_non_existing_cats(self, store, settings,
                                                    ex_cat1, ex_cat2, ex_elem):
        if ex_cat1:
            store.add_to_category('foo', 'foo_elem')
        if ex_cat2 and not ex_elem:
            store.add_category('bar')
        if ex_elem:
            assert ex_cat2
            store.add_to_category('bar', 'bar_elem')
        bot = DummyBot(store, settings, BOT_NAME, EXAMPLE_URL,
                       allow_cat_actions=True,
                       query_templates=[('foo', 'keyword{{bar}}')])
        bot.receive('keyword bar_elem')
        assert len(bot.out_buffer) == 0

    def test_query_template_with_empty_cat2(self, store, settings):
        store.add_link(['foo', 'foo_elem'], ['bar', 'bar_elem'])
        bot = DummyBot(store, settings, BOT_NAME, EXAMPLE_URL,
                       allow_cat_actions=True,
                       query_templates=[('foo', 'keyword{{bar}}')])
        bot.receive('keyword')
        assert len(bot.out_buffer) == 0

    def test_invalid_template(self, store, settings):
        store.add_link(['foo', 'foo_elem'], ['bar', 'bar_elem'])
        bot = DummyBot(store, settings, BOT_NAME,
                       EXAMPLE_URL, allow_cat_actions=True,
                       query_templates=[('foo', 'keyword{{bar}')])
        bot.receive('keyword bar_elem')
        assert len(bot.out_buffer) == 0

    @pytest.mark.parametrize('fuzzy', [True, False])
    def test_query_templates_match(self, store, settings, fuzzy):
        store.add_link(['Person', 'Hans'], ['Skill', 'Python'])
        store.add_link(['Person', 'Hans'], ['Skill', 'JavaScript'])
        store.add_link(['Person', 'Peter'], ['Skill', 'JavaScript'])
        store.add_to_category('Skill', 'HTML5')
        store.add_to_category('Person', 'Dieter')
        template_matcher = '{{Skill|.7}}' if fuzzy else '{{Skill}}'
        bot = DummyBot(store, settings, BOT_NAME, EXAMPLE_URL,
                       allow_cat_actions=True,
                       query_templates=[('Person', 'knows?'+template_matcher)])
        bot.receive('who knows Python3?')
        if fuzzy:
            assert len(bot.out_buffer) == 1
            assert bot.out_buffer[0].count(
                ChatBotBase._LIST_MESSAGES['head']['template']) == 1
            assert bot.out_buffer[0].count('Hans') == 1
            assert bot.out_buffer[0].count('Peter') == 0
            assert bot.out_buffer[0].count('Dieter') == 0
            bot.reset_buffer()
        else:
            assert len(bot.out_buffer) == 0
        bot.receive("I'm looking for people that know JavaScript.")
        assert len(bot.out_buffer) == 1
        assert bot.out_buffer[0].count(
            ChatBotBase._LIST_MESSAGES['head']['template']) == 1
        assert bot.out_buffer[0].count('Hans') == 1
        assert bot.out_buffer[0].count('Peter') == 1
        assert bot.out_buffer[0].count('Dieter') == 0

    def test_digest_prefers_shorter_diff(self, store, digest_bot):
        store.add_to_category('foo', '1')
        store.add_to_category('foo', '2')
        digest_bot.reset_buffer()
        store.rename_element('foo', '1', 'foo', '3')
        store.add_link(['foo', '3'], ['foo', '2'])
        store.rename_element('foo', '3', 'foo', '1')
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1
        assert 'rename' not in digest_bot.out_buffer[0].lower()

    @pytest.mark.parametrize('order', [1, 2])
    def test_digest_handles_element_renames(self, store, digest_bot, order):
        for i in range(1, 5):
            store.add_to_category('foo', 'e' + str(i))
        digest_bot.reset_buffer()
        if order == 1:
            store.rename_category('foo', 'bar')
            store.rename_element('bar', 'e1', 'bar', 'e5')
        if order == 2:
            store.rename_element('foo', 'e1', 'foo', 'e5')
            store.rename_category('foo', 'bar')
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1
        if order == 1:
            assert re.match(r'.*foo.*bar.*bar.*e1.*e5.*',
                            digest_bot.out_buffer[0], re.DOTALL)
            assert re.match(r'.*bar.*e1.*e5.*foo.*bar.*',
                            digest_bot.out_buffer[0].lower(), re.DOTALL) is None
            assert re.match(r'.*foo.*e1.*e5.*foo.*bar.*',
                            digest_bot.out_buffer[0].lower(), re.DOTALL) is None
        elif order == 2:
            assert re.match(r'.*foo.*e1.*e5.*foo.*bar.*',
                            digest_bot.out_buffer[0], re.DOTALL)
            assert re.match(r'.*foo.*bar.*bar.*e1.*e5.*',
                            digest_bot.out_buffer[0].lower(), re.DOTALL) is None
            assert re.match(r'.*foo.*bar.*foo.*e1.*e5.*',
                            digest_bot.out_buffer[0].lower(), re.DOTALL) is None
        for i in range(2, 5):
            assert re.match(r'.*(foo|bar).*e' + str(i) + '.*',
                            digest_bot.out_buffer[0].lower(), re.DOTALL) is None

    @pytest.mark.parametrize('order', [1, 2])
    def test_digest_handles_category_renames(self, store, digest_bot, order):
        for i in range(1, 5):
            for j in range(1, 5):
                store.add_to_category('cat' + str(i), 'elem' + str(i))
        digest_bot.reset_buffer()
        if order == 1:
            store.rename_category('cat1', 'cta1')
        store.rename_category('cat3', 'cta3')
        if order == 2:
            store.rename_category('cat1', 'cta1')
        digest_bot.wait()
        assert len(digest_bot.out_buffer) == 1
        if order == 1:
            assert re.match(r'.*cat1.*cta1.*cat3.*cta3.*',
                            digest_bot.out_buffer[0], re.DOTALL)
            assert re.match(r'.*cat3.*cta3.*cat1.*cta1.*',
                            digest_bot.out_buffer[0].lower(), re.DOTALL) is None
        elif order == 2:
            assert re.match(r'.*cat3.*cta3.*cat1.*cta1.*',
                            digest_bot.out_buffer[0], re.DOTALL)
            assert re.match(r'.*cat1.*cta1.*cat3.*cta3.*',
                            digest_bot.out_buffer[0].lower(), re.DOTALL) is None
        assert re.match(r'elem\d', digest_bot.out_buffer[0].lower(),
                        re.DOTALL) is None

    @pytest.mark.parametrize(['raw', 'escaped'], [
        ['*em*', r'\*em\*'],
        ['\*em escaped\*', r'\\\*em escaped\\\*'],
        ['**strong**', r'\*\*strong\*\*'],
        ['_em_', r'\_em\_'],
        ['__strong__', r'\_\_strong\_\_'],
        ['~~strikethrough~~', r'\~\~strikethrough\~\~'],
        ['# Headline 1', r'\# Headline 1'],
        ['## Headline 2', r'\#\# Headline 2'],
        ['### Headline 3', r'\#\#\# Headline 3'],
        ['#### Headline 4', r'\#\#\#\# Headline 4'],
        ['##### Headline 5', r'\#\#\#\#\# Headline 5'],
        ['###### Headline 6', r'\#\#\#\#\#\# Headline 6'],
        ['> Quote', r'\> Quote'],
        ['> > Nested Quote', r'\> \> Nested Quote'],
        ['`code`', r'\`code\`'],
        ['`code`', r'\`code\`'],
        ['``code escaped ` ``', r'\`\`code escaped \` \`\`'],
        ['+ list', r'\+ list'],
        ['  + list', r'  \+ list'],
        ['- list', r'\- list'],
        ['  - list', r'  \- list'],
        ['* list', r'\* list'],
        ['  * list', r'  \* list'],
        ['1. enumeration', r'1\. enumeration'],
        ['  1. enumeration', r'  1\. enumeration'],
        ['---------------', r'\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-'],
        ['[]', r'\[\]'],
        ['{}', r'\{\}'],
        ['()', r'\(\)'],
        ['[text "title"](http://example.com)',
         r'\[text "title"\]\(http://example\.com\)'],
    ])
    def test_md_escape_add_cat2(self, bot, raw, escaped):
        bot.store.add_category(raw)
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert escaped in bot.out_buffer_md[0]
        if raw not in escaped:
            assert raw not in bot.out_buffer_md[0]
        assert raw in bot.out_buffer[0]

    @pytest.mark.parametrize('digest', [True, False])
    @pytest.mark.parametrize('as_diff', [True, False])
    def test_md_escape_add_cat(self, bot, digest_bot, digest, as_diff):
        bot = digest_bot if digest else bot
        if not as_diff:
            bot.store.add_category('*italic*')
        else:
            bot.store.diff_action({'insertions': {'categories': ['*italic*']}})
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert r"\*italic\*" in bot.out_buffer_md[0]
        assert r"*italic*" in bot.out_buffer[0]
        if not digest:
            digest_bot.wait()  # wait for digest callback either way

    @pytest.mark.parametrize('digest', [True, False])
    @pytest.mark.parametrize('as_diff', [True, False])
    def test_md_escape_del_cat(self, bot, digest_bot, digest, as_diff):
        bot = digest_bot if digest else bot
        bot.store.add_category('*italic*')
        bot.reset_buffer()
        if not as_diff:
            bot.store.remove_category('*italic*')
        else:
            bot.store.diff_action({'deletions': {'categories': ['*italic*']}})
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert r"\*italic\*" in bot.out_buffer_md[0]
        assert r"*italic*" in bot.out_buffer[0]
        if not digest:
            digest_bot.wait()  # wait for digest callback either way

    @pytest.mark.parametrize('digest', [True, False])
    @pytest.mark.parametrize('as_diff', [True, False])
    @pytest.mark.parametrize('link_style', [True, False])
    @pytest.mark.parametrize('link_elem', [True, False])
    def test_md_escape_add_elem(self, bot, digest_bot, digest, as_diff,
                                link_style, link_elem):
        if link_elem:
            elem = '[**emph**](https://example.com "Title")'
            if link_style:
                elem_esc = r'[\*\*emph\*\*](https://example.com%20"Title")'
            else:
                elem_esc = r'\[\*\*emph\*\*\]\(https://example\.com "Title"\)'
        else:
            elem = '**emph**'
            elem_esc = r'\*\*emph\*\*'
        bot = digest_bot if digest else bot
        if link_style:
            bot.settings.update({'display-type': {'*italic*': 'links'}})
        bot.store.add_category('*italic*')
        bot.reset_buffer()
        if not as_diff:
            bot.store.add_to_category('*italic*', elem)
        else:
            bot.store.diff_action(
                {'insertions': {'elements': [['*italic*', elem]]}})
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert r"\*italic\*" in bot.out_buffer_md[0]
        assert elem_esc in bot.out_buffer_md[0]
        assert r"*italic*" in bot.out_buffer[0]
        assert elem in bot.out_buffer[0]
        if not digest:
            digest_bot.wait()  # wait for digest callback either way

    @pytest.mark.parametrize('digest', [True, False])
    @pytest.mark.parametrize('as_diff', [True, False])
    @pytest.mark.parametrize('link_style', [True, False])
    @pytest.mark.parametrize('link_elem', [True, False])
    def test_md_escape_del_elem(self, bot, digest_bot, digest, as_diff,
                                link_style, link_elem):
        if link_elem:
            elem = '[**emph**](https://example.com "Title")'
            if link_style:
                elem_esc = r'[\*\*emph\*\*](https://example.com%20"Title")'
            else:
                elem_esc = r'\[\*\*emph\*\*\]\(https://example\.com "Title"\)'
        else:
            elem = '**emph**'
            elem_esc = r'\*\*emph\*\*'
        bot = digest_bot if digest else bot
        if link_style:
            bot.settings.update({'display-type': {'*italic*': 'links'}})
        bot.store.add_to_category('*italic*', elem)
        bot.reset_buffer()
        if not as_diff:
            bot.store.remove_from_category('*italic*', elem)
        else:
            bot.store.diff_action(
                {'deletions': {'elements': [['*italic*', elem]]}})
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert r"\*italic\*" in bot.out_buffer_md[0]
        assert elem_esc in bot.out_buffer_md[0]
        assert r"*italic*" in bot.out_buffer[0]
        assert elem in bot.out_buffer[0]
        if not digest:
            digest_bot.wait()  # wait for digest callback either way

    @pytest.mark.parametrize('digest', [True, False])
    @pytest.mark.parametrize('as_diff', [True, False])
    @pytest.mark.parametrize('link_style', [True, False])
    @pytest.mark.parametrize('link_elem', [True, False])
    def test_md_escape_add_link(self, bot, digest_bot, digest, as_diff,
                                link_style, link_elem):
        cat = '*italic*'
        cat_esc = r'\*italic\*'
        if link_elem:
            elem = '[**emph**](https://example.com "Title")'
            if link_style:
                elem_esc = r'[\*\*emph\*\*](https://example.com%20"Title")'
            else:
                elem_esc = r'\[\*\*emph\*\*\]\(https://example\.com "Title"\)'
        else:
            elem = '**emph**'
            elem_esc = r'\*\*emph\*\*'
        bot = digest_bot if digest else bot
        if link_style:
            bot.settings.update({'display-type': {cat: 'links'}})
        bot.store.add_to_category(cat, 'a')
        bot.store.add_to_category(cat, elem)
        bot.reset_buffer()
        if not as_diff:
            bot.store.add_link([cat, 'a'], [cat, elem])
        else:
            bot.store.diff_action(
                {'insertions': {'links': [[[cat, 'a'], [cat, elem]]]}})
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert cat_esc in bot.out_buffer_md[0]
        assert elem_esc in bot.out_buffer_md[0]
        assert cat not in bot.out_buffer_md[0]
        assert elem not in bot.out_buffer_md[0]
        assert cat in bot.out_buffer[0]
        assert elem in bot.out_buffer[0]
        if not digest:
            digest_bot.wait()  # wait for digest callback either way

    @pytest.mark.parametrize('digest', [True, False])
    @pytest.mark.parametrize('as_diff', [True, False])
    @pytest.mark.parametrize('link_style', [True, False])
    @pytest.mark.parametrize('link_elem', [True, False])
    def test_md_escape_del_link(self, bot, digest_bot, digest, as_diff,
                                link_style, link_elem):
        cat = '*italic*'
        cat_esc = '\*italic\*'
        if link_elem:
            elem = '[**emph**](https://example.com "Title")'
            if link_style:
                elem_esc = r'[\*\*emph\*\*](https://example.com%20"Title")'
            else:
                elem_esc = r'\[\*\*emph\*\*\]\(https://example\.com "Title"\)'
        else:
            elem = '**emph**'
            elem_esc = r'\*\*emph\*\*'
        bot = digest_bot if digest else bot
        if link_style:
            bot.settings.update({'display-type': {cat: 'links'}})
        bot.store.add_link([cat, 'a'], [cat, elem])
        bot.reset_buffer()
        if not as_diff:
            bot.store.remove_link([cat, 'a'], [cat, elem])
        else:
            bot.store.diff_action(
                {'deletions': {'links': [[[cat, 'a'], [cat, elem]]]}})
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert cat_esc in bot.out_buffer_md[0]
        assert elem_esc in bot.out_buffer_md[0]
        assert cat not in bot.out_buffer_md[0]
        assert elem not in bot.out_buffer_md[0]
        assert cat in bot.out_buffer[0]
        assert elem in bot.out_buffer[0]
        if not digest:
            digest_bot.wait()  # wait for digest callback either way

    @pytest.mark.parametrize('digest', [True, False])
    @pytest.mark.parametrize('as_diff', [True, False])
    def test_md_rename_cat(self, bot, digest_bot, digest, as_diff):
        cat_old = '*italic*'
        cat_old_esc = '\*italic\*'
        cat_new = '~~stroke~~'
        cat_new_esc = '\~\~stroke\~\~'

        bot = digest_bot if digest else bot
        bot.store.add_category(cat_old)
        bot.reset_buffer()
        if not as_diff:
            bot.store.rename_category(cat_old, cat_new)
        else:
            bot.store.diff_action(
                {'renamings': {'categories': [[cat_old, cat_new]]}})
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert cat_old_esc in bot.out_buffer_md[0]
        assert cat_new_esc in bot.out_buffer_md[0]
        assert cat_old not in bot.out_buffer_md[0]
        assert cat_new not in bot.out_buffer_md[0]
        assert cat_old in bot.out_buffer[0]
        assert cat_new in bot.out_buffer[0]
        if not digest:
            digest_bot.wait()  # wait for digest callback either way

    @pytest.mark.parametrize('digest', [True, False])
    @pytest.mark.parametrize('as_diff', [True, False])
    @pytest.mark.parametrize('link_style_old', [True, False])
    @pytest.mark.parametrize('link_elem_old', [True, False])
    @pytest.mark.parametrize('link_style_new', [True, False])
    @pytest.mark.parametrize('link_elem_new', [True, False])
    def test_md_escape_del_link(self, bot, digest_bot, digest, as_diff,
                                link_style_old, link_elem_old,
                                link_style_new, link_elem_new):
        cat_old = '*italic*'
        cat_old_esc = '\*italic\*'
        cat_new = '~~stroke~~'
        cat_new_esc = '\~\~stroke\~\~'
        if link_elem_old:
            elem_old = '[**emph**](https://example.com "Title")'
            if link_style_old:
                elem_old_esc = r'[\*\*emph\*\*](https://example.com%20"Title")'
            else:
                elem_old_esc = r'\[\*\*emph\*\*\]' \
                               r'\(https://example\.com "Title"\)'
        else:
            elem_old = '**emph**'
            elem_old_esc = r'\*\*emph\*\*'
        if link_elem_new:
            elem_new = '[~~stroke~~](https://example.com "Title")'
            if link_style_new:
                elem_new_esc = r'[\~\~stroke\~\~]' \
                               r'(https://example.com%20"Title")'
            else:
                elem_new_esc = r'\[\~\~stroke\~\~\]' \
                               r'\(https://example\.com "Title"\)'
        else:
            elem_new = '~~stroke~~'
            elem_new_esc = r'\~\~stroke\~\~'
        bot = digest_bot if digest else bot
        if link_style_old:
            bot.settings.update({'display-type': {cat_old: 'links'}})
        if link_style_new:
            bot.settings.update({'display-type': {cat_new: 'links'}})
        bot.store.add_to_category(cat_old, elem_old)
        bot.store.add_category(cat_new)
        bot.reset_buffer()
        if not as_diff:
            bot.store.rename_element(cat_old, elem_old, cat_new, elem_new)
        else:
            bot.store.diff_action(
                {'renamings': {'elements': [[[cat_old, elem_old],
                                             [cat_new, elem_new]]]}})
        bot.wait()
        assert len(bot.out_buffer_md) == 1
        assert cat_old_esc in bot.out_buffer_md[0]
        assert elem_old_esc in bot.out_buffer_md[0]
        assert cat_new_esc in bot.out_buffer_md[0]
        assert elem_new_esc in bot.out_buffer_md[0]
        assert cat_old not in bot.out_buffer_md[0]
        assert elem_old not in bot.out_buffer_md[0]
        assert cat_new not in bot.out_buffer_md[0]
        assert elem_new not in bot.out_buffer_md[0]
        assert cat_old in bot.out_buffer[0]
        assert elem_old in bot.out_buffer[0]
        assert cat_new in bot.out_buffer[0]
        assert elem_new in bot.out_buffer[0]
        if not digest:
            digest_bot.wait()  # wait for digest callback either way
