import pytest

from bots.chat_bot_util import split_msg


class TestChatBotUtil(object):
    @pytest.mark.parametrize(['msg', 'max_length', 'expected'], [
        ['a'*10000, None, ['a'*10000]],
        ['abc', None, ['abc']],
        ['abc', 3, ['abc']],
        ['abcdef', 3, ['abc', 'def']],
        ['abcdefg', 3, ['abc', 'def', 'g']],
        ['abc\ndef\ng', 3, ['abc', 'def', 'g']],
        ['ab\ncde', 3, ['ab', 'cde']],
        ['a\nb\nde', 3, ['a\nb', 'de']],
    ])
    def test_split_msg_none_max_length(self, msg, max_length, expected):
        res = split_msg(msg, max_length=max_length)
        assert res == expected
