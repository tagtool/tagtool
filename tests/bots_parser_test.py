import pytest

from bots.parser import understand


class TestBotMessageParser(object):
    @pytest.mark.parametrize(('msg', 'cmd', 'target', 'args'), (
        # link commands
        ('add link from "a1" "b" to "c" "d"', 'add', 'link',
         [['a1', 'b'], ['c', 'd']]),
        ('add link from "a1" "b" to c "d"', 'add', 'link',
         [['a1', 'b'], ['c', 'd']]),
        ('add link from "a1" b to c "d"', 'add', 'link',
         [['a1', 'b'], ['c', 'd']]),
        ('ADD link froM "a 1" "B" to "c" "d"', 'add', 'link',
         [['a 1', 'B'], ['c', 'd']]),
        ('link from "a1" "b" to "c" "d"', 'add', 'link',
         [['a1', 'b'], ['c', 'd']]),
        ('link from a1 "t est" to c d', 'add', 'link',
         [['a1', 't est'], ['c', 'd']]),
        ('link from a"1 t to c d', 'add', 'link',
         [['a"1', 't'], ['c', 'd']]),
        ('link a b and c d', 'add', 'link',
         [['a', 'b'], ['c', 'd']]),
        ('link a b c d', 'add', 'link',
         [['a', 'b'], ['c', 'd']]),
        ('link from a\\"1 "t\\"est" to c d', 'add', 'link',
         [[r'a\"1', 't"est'], ['c', 'd']]),
        ('remove link from a b to c d', 'remove', 'link',
         [['a', 'b'], ['c', 'd']]),
        ('delete link between a b and c d', 'remove', 'link',
         [['a', 'b'], ['c', 'd']]),
        ('drop link a b c d', 'remove', 'link',
         [['a', 'b'], ['c', 'd']]),
        ('unlink a b and c d', 'remove', 'link',
         [['a', 'b'], ['c', 'd']]),
        ('unlink a b from c d', 'remove', 'link',
         [['a', 'b'], ['c', 'd']]),
        # element commands
        ('add a b', 'add', 'elem', [['a', 'b']]),
        ('add a "b!"', 'add', 'elem', [['a', 'b!']]),
        ('new element a c', 'add', 'elem', [['a', 'c']]),
        ('add new element a b', 'add', 'elem', [['a', 'b']]),
        ('add "new" a', 'add', 'elem', [['new', 'a']]),
        ('remove element a b', 'remove', 'elem', [['a', 'b']]),
        ('delete a b', 'remove', 'elem', [['a', 'b']]),
        (' drop c\ta   ', 'remove', 'elem', [['c', 'a']]),
        ('change a b into c', 'rename', 'elem', [['a', 'b'], 'c']),
        ('please change a b in c', 'rename', 'elem', [['a', 'b'], 'c']),
        ('rename a b to d', 'rename', 'elem', [['a', 'b'], 'd']),
        ('rename foo a to b', 'rename', 'elem', [['foo', 'a'], 'b']),
        ('rename a b to c d', 'rename', 'elem', [['a', 'b'], ['c', 'd']]),
        ('rename a b to c d please', 'rename', 'elem',
         [['a', 'b'], ['c', 'd']]),
        ('move a c to d', 'rename', 'elem', [['a', 'c'], 'd']),
        ('mv a b to c please', 'rename', 'elem', [['a', 'b'], 'c']),
        ('rename element a b to c', 'rename', 'elem', [['a', 'b'], 'c']),
        ('list all links of a b', 'list', 'elem', [['a', 'b']]),
        ('dump a b', 'list', 'elem', [['a', 'b']]),
        ('what\'s linked with a b', 'list', 'elem', [['a', 'b']]),
        ('what is linked with a b', 'list', 'elem', [['a', 'b']]),
        ('what are the links of a b', 'list', 'elem', [['a', 'b']]),
        ('what are links of a b', 'list', 'elem', [['a', 'b']]),
        ('show me all links of a b', 'list', 'elem', [['a', 'b']]),
        # category commands
        ('list', 'list', None, []),
        ('show', 'list', None, []),
        ('show me everything', 'list', None, []),
        ('ls', 'list', None, []),
        ('dump all the categories', 'list', None, []),
        ('list all categories', 'list', None, []),
        ('show all clouds', 'list', None, []),
        ('show the whole', 'list', None, []),
        ('what\'s inside a', 'list', 'cat', ['a']),
        ('what elements are in a', 'list', 'cat', ['a']),
        ('what elements in a', 'list', 'cat', ['a']),
        ('list elements inside a', 'list', 'cat', ['a']),
        ('show everything in a', 'list', 'cat', ['a']),
        ('show the whole a', 'list', 'cat', ['a']),
        ('show whole a', 'list', 'cat', ['a']),
        ('show a', 'list', 'cat', ['a']),
        ('list all elements of a', 'list', 'cat', ['a']),
        ('list all elements in a', 'list', 'cat', ['a']),
        ('list "all"', 'list', 'cat', ["all"]),
        ('add a', 'add', 'cat', ['a']),
        ('new category a', 'add', 'cat', ['a']),
        ('new cloud a', 'add', 'cat', ['a']),
        ('create "a"', 'add', 'cat', ['a']),
        ('drop a', 'remove', 'cat', ['a']),
        ('rename a b', 'rename', 'cat', ['a', 'b']),
        ('rename a to b', 'rename', 'cat', ['a', 'b']),
        ('rename a b please', 'rename', 'cat', ['a', 'b']),
        ('rename a to b please', 'rename', 'cat', ['a', 'b']),
        # help commands
        ('help me', 'help', None, []),
        ('please help', 'help', None, []),
        ('help', 'help', None, []),
        # search commands
        ('please find HTML5', 'search', 'id', ['HTML5']),
        ('search for HTML5', 'search', 'id', ['HTML5']),
        ('search HTML5', 'search', 'id', ['HTML5']),
        ('find "More Bananas" please', 'search', 'id', ['More Bananas']),
        ('find "More Bananas please"', 'search', 'id', ['More Bananas please']),
        # invalid commands
        ('hello world', -1, None, None)
    ))
    @pytest.mark.parametrize('force', [True, False])
    def test_understand(self, force, msg, cmd, target, args):
        if force:
            msg += '!'
        r_cmd, r_target, r_args, r_force = understand(msg)
        assert cmd == r_cmd
        assert target == r_target
        assert args == r_args
        assert r_force == (force if cmd != -1 else None)
