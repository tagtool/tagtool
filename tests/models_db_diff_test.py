import pytest
from copy import deepcopy
from .util import recursive_dict_merge
from .util_store import store  # store fixture
from .util_store import time_previous  # fixture
from .util_store import time_now
from .util_store import time_now_func
from models.db_diff import DB_Diff
from models.db_diff import HighLevelDB_Diff


class TestDB_Diff(object):
    def _get_diff_dict(self, high_level, changes=()):
        diff = {'insertions': {'categories': [], 'elements': [],
                               'links': []},
                'deletions':  {'categories': [], 'elements': [],
                               'links': []},
                'from': None,
                'to': None}
        if high_level:
            diff['renamings'] = {'categories': [], 'elements': []}
        for key, val in changes:
            diff[key] = val
        return diff

    def _diff_cls(self, high_level):
        return HighLevelDB_Diff if high_level else DB_Diff

    def _diff_init(self, high_level, *args, **kwargs):
        return self._diff_cls(high_level)(*args, **kwargs)

    @pytest.mark.parametrize('high_level', [True, False])
    def test_empty_init(self, high_level):
        assert self._diff_init(high_level) == self._get_diff_dict(high_level)

    @pytest.mark.parametrize('high_level', [True, False])
    def test_init_with_store(self, store, high_level):
        diff = self._diff_init(high_level, to_dbv=store)
        expected_dict = self._get_diff_dict(high_level,
                                            (('to', str(store.timestamp)),))
        assert diff == expected_dict

        diff = self._diff_init(high_level, from_dbv=store)
        expected_dict = self._get_diff_dict(high_level,
                                            (('from', str(store.timestamp)),))
        assert diff == expected_dict

    @pytest.mark.parametrize('high_level', [True, False])
    def test_init_with_timestamp(self, time_now, time_previous, high_level):
        diff = self._diff_init(high_level, from_dbv=time_previous,
                               to_dbv=time_now)
        expected_dict = self._get_diff_dict(high_level,
                                            (('from', str(time_previous)),
                                             ('to', str(time_now))))
        assert diff == expected_dict

    @pytest.mark.parametrize('high_level', [True, False])
    def test_init_with_string(self, time_now, time_previous, high_level):
        diff = self._diff_init(high_level, from_dbv=str(time_previous),
                               to_dbv=str(time_now))
        expected_dict = self._get_diff_dict(high_level,
                                            (('from', str(time_previous)),
                                             ('to', str(time_now))))
        assert diff == expected_dict

    @pytest.mark.parametrize('high_level', [True, False])
    @pytest.mark.parametrize(['key', 'value', 'value_is_fixture',
                              'expected_value_func'],
                             [('from', time_now_func(), False, str),
                              ('to', time_now_func(), False, str),
                              ('from', str(time_now_func()), False, lambda x: x),
                              ('to', str(time_now_func()), False, lambda x: x),
                              ('to', 'store', True, lambda x: str(x.timestamp)),
                              ('from', 'store', True,
                               lambda x: str(x.timestamp))
                              ])
    def test_set_item(self, key, value, value_is_fixture, expected_value_func,
                      high_level, request):
        if value_is_fixture:
            value = request.getfixturevalue(value)
        diff = self._diff_init(high_level)
        diff[key] = value
        assert diff[key] == expected_value_func(value)

    def _copy_diff(self, diff):
        cls = diff.__class__
        dict_ = dict(diff)
        from_ = dict_.pop('from')
        to = dict_.pop('to')
        return cls(from_dbv=from_, to_dbv=to, raw_diff=dict_)

    @pytest.mark.parametrize('use_class', [True, False])
    @pytest.mark.parametrize(['method', 'value', 'expected_diff'],
                             [('add_cat', 'bar',
                               {'insertions': {'categories': ['bar']}}),
                              ('add_cats', ['bar', 'barr'],
                               {'insertions': {'categories': ['bar', 'barr']}}),
                              ('add_elem', ['foo', 'bar'],
                               {'insertions': {'elements': [['foo', 'bar']]}}),
                              ('add_elems', [['foo', 'bar'], ['fooo', 'barr']],
                               {'insertions': {'elements':
                                               [['foo', 'bar'],
                                                ['fooo', 'barr']]}}),
                              ('add_link', [['foo', 'bar'], ['fooo', 'barr']],
                               {'insertions': {'links':
                                               [[['foo', 'bar'],
                                                 ['fooo', 'barr']]]}}),
                              ('add_links', [[['foo', 'bar'], ['fooo', 'barr']],
                                             [['1', '2'], ['3', '4']]],
                               {'insertions': {'links':
                                               [[['foo', 'bar'],
                                                 ['fooo', 'barr']],
                                                [['1', '2'],
                                                 ['3', '4']]]}}),
                              ('remove_cat', 'bar',
                               {'deletions': {'categories': ['bar']}}),
                              ('remove_cats', ['bar', 'barr'],
                               {'deletions': {'categories': ['bar', 'barr']}}),
                              ('remove_elem', ['foo', 'bar'],
                               {'deletions': {'elements': [['foo', 'bar']]}}),
                              ('remove_elems', [['foo', 'bar'], ['fooo', 'barr']],
                               {'deletions': {'elements':
                                               [['foo', 'bar'],
                                                ['fooo', 'barr']]}}),
                              ('remove_link', [['foo', 'bar'], ['fooo', 'barr']],
                               {'deletions': {'links':
                                              [[['foo', 'bar'],
                                                ['fooo', 'barr']]]}}),
                              ('remove_links', [[['foo', 'bar'], ['fooo', 'barr']],
                                                [['1', '2'], ['3', '4']]],
                               {'deletions': {'links':
                                              [[['foo', 'bar'],
                                                ['fooo', 'barr']],
                                               [['1', '2'],
                                                ['3', '4']]]}}),
                              ('rename_elem', [['c1', 'e1'], ['c2', 'e2']],
                               {'renamings': {'elements':
                                              [[['c1', 'e1'], ['c2', 'e2']]]}}),
                              ('rename_elems', [[['c1', 'e1'], ['c2', 'e2']],
                                                [['c1', 'e2'], ['c1', 'e4']]],
                               {'renamings': {'elements':
                                              [[['c1', 'e1'],
                                                ['c2', 'e2']],
                                               [['c1', 'e2'],
                                                ['c1', 'e4']]]}}),
                              ('rename_cat', ['c1', 'c2'],
                               {'renamings': {'categories': [['c1', 'c2']]}}),
                              ('rename_cats', [['c1', 'c2'], ['c3', 'c4']],
                               {'renamings': {'categories':
                                              [['c1', 'c2'],
                                               ['c3', 'c4']]}}),
                              ])
    @pytest.mark.parametrize('high_level', [True, False])
    @pytest.mark.parametrize('copy_diff', [True, False])
    def test_initializers(self, use_class, method, value, expected_diff,
                          high_level, copy_diff):
        if use_class:
            base = self._diff_cls(high_level)
        else:
            base = self._diff_init(high_level)
        if not high_level and method.startswith('rename'):
            with pytest.raises(AttributeError):
                getattr(base, method)
        else:
            diff = getattr(base, method)(value)
            if not use_class:
                diff = base

            if copy_diff:
                diff = self._copy_diff(diff)

            merge_diff = recursive_dict_merge(self._diff_init(high_level),
                                              expected_diff)
            assert diff == merge_diff

    def _diff_x_value_error(self, key, value, action, use_class, high_level,
                            error=ValueError):
        # type: (object, object, object, bool, bool, Exception) -> None
        cls = self._diff_cls(high_level)
        with pytest.raises(error):
            if use_class:
                cls._diff_x(action, key, value)
            else:
                cls()._diff_x(action, key, value)

    @pytest.mark.parametrize('action', ['insertions', 'deletions', 'renamings'])
    @pytest.mark.parametrize('use_class', [True, False])
    @pytest.mark.parametrize(['key', 'value'],
                             [('categories', ('test',)),
                              ('categories', 1),
                              ('categories', None),
                              ('categories', 'test'),
                              (['categories'], ['test']),
                              (('categories',), ['test']),
                              (None, ['test']),
                              (1, ['test'])])
    @pytest.mark.parametrize('high_level', [True, False])
    def test_diff_x_wrong_type(self, key, value, action, use_class, high_level):
        if action == 'renamings' and not high_level:
            return
        self._diff_x_value_error(key, value, action, use_class, high_level,
                                 error=TypeError)

    @pytest.mark.parametrize('use_class', [True, False])
    @pytest.mark.parametrize('high_level', [True, False])
    @pytest.mark.parametrize('action', [['insertions'], ('insertions',), 1])
    def test_diff_x_wrong_action_type(self, action, use_class, high_level):
        self._diff_x_value_error('elements', ['e1'], action, use_class,
                                 high_level, error=TypeError)

    @pytest.mark.parametrize('action', ['insertions', 'deletions', 'renamings'])
    @pytest.mark.parametrize('use_class', [True, False])
    @pytest.mark.parametrize('high_level', [True, False])
    def test_diff_x_wrong_key(self, action, use_class, high_level):
        if action == 'renamings' and not high_level:
            return
        self._diff_x_value_error('key', [['val1', 'val2']], action, use_class,
                                 high_level, error=ValueError)

    @pytest.mark.parametrize('action', ['derp', 'renamings'])
    @pytest.mark.parametrize('use_class', [True, False])
    @pytest.mark.parametrize(['key', 'value'],
                             [('elements', ['foo', 'bar']),
                              ('categories', ['foo', 'bar']),
                              ('links', [['foo', 'bar'], ['foo', 'foobar']])])
    @pytest.mark.parametrize('high_level', [True, False])
    def test_diff_x_wrong_action(self, key, value, action, use_class,
                                 high_level):
        if action == 'renamings' and high_level:
            return
        self._diff_x_value_error(key, value, action, use_class, high_level,
                                 error=ValueError)

    @pytest.mark.parametrize('high_level', [True, False])
    def test_action_count_0(self, high_level):
        diff = self._diff_init(high_level)
        assert diff.action_count() == 0

    @pytest.mark.parametrize(['action', 'args', 'count'], [
        ['add_elem', 'test', 1],
        ['add_cat', 'test', 1],
        ['remove_elem', 'test', 1],
        ['remove_cat', 'test', 1],
        ['add_elems', ['test1', 'test2'], 2],
        ['add_cats', ['test1', 'test2'], 2],
        ['remove_elems', ['test1', 'test2'], 2],
        ['remove_cats', ['test1', 'test2'], 2],
    ])
    @pytest.mark.parametrize('high_level', [True, False])
    def test_action_count_simple(self, action, args, count, high_level):
        cls = self._diff_cls(high_level)
        diff = getattr(cls, action)(args)
        assert diff.action_count() == count

    def test_action_count_low_level(self):
        diff = DB_Diff(
            raw_diff={'insertions': {'elements': ['e1', 'e2'],
                                     'categories': ['c1']},
                      'deletions':  {'links': [[['c1', 'e2'], ['c2', 'e2']]]}})
        assert diff.action_count() == 4

    def test_action_count_high_level(self):
        diff = HighLevelDB_Diff(
            raw_diff={'insertions': {'elements': ['e1', 'e2'],
                                     'categories': ['c1']},
                      'deletions':  {'links': [[['c1', 'e2'], ['c2', 'e2']]]},
                      'renamings': {'categories': [['c2', 'c4']]}})
        assert diff.action_count() == 5

    @pytest.mark.parametrize('high_level', [True, False])
    def test_is_empty(self, high_level):
        diff = self._diff_init(high_level)
        assert diff.is_empty()

    @pytest.mark.parametrize('high_level', [True, False])
    def test_is_not_empty(self, high_level):
        diff = self._diff_init(high_level,
                               raw_diff={'insertions': {'elements': ['e1']}})
        assert not diff.is_empty()
