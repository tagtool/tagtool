import pytest
import os
import errno
import shutil
import json

from models.settings import Settings
from models.settings import InvalidSettingsError

from .util_store import store  # store fixture

FN = Settings.DEFAULT_FILE_NAME


def _create_settings(path, store=None):
    return Settings(path=path, fn=FN, store=store)


def _delete_settings(path):
    try:
        shutil.rmtree(path)
    except OSError as e:
        if e.errno == errno.ENOENT:
            pass
        else:
            raise


@pytest.fixture()
def settings(tmpdir, store):
    return _create_settings(path=str(tmpdir), store=store)


class TestSettings(object):
    def _fill_sample_settings(self, settings, flush=True):
        settings.update({"sizing": {"foo": "links"},
                         "display-priority": {"foo": 2},
                         "display-type": {"foo": "cloud"}}, flush=flush)

    def _write_settings_file(self, path, fn, data):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno == errno.EEXIST:
                pass
            else:
                raise
        with open(os.path.join(path, fn), "w") as f:
            json.dump(data, f)

    def test_file_existence(self, settings):
        assert os.path.exists(os.path.join(settings.path, settings.fn))

    def test_persistence(self, settings):
        self._fill_sample_settings(settings)
        settings_data = settings.get()
        # reload from file should keep data
        assert _create_settings(settings.path).get() == settings_data

    def test_setup_with_existing_folder(self, tmpdir):
        path = str(tmpdir)
        _create_settings(path)
        assert os.path.exists(os.path.join(path, Settings.DEFAULT_FILE_NAME))
        _delete_settings(path)

    def test_flush_to_given_fn_no_path(self, settings):
        self._fill_sample_settings(settings)
        settings.flush('foobar.json')
        assert os.path.exists(os.path.join(settings.path, 'foobar.json'))

    def test_flush_to_given_fn_with_path(self, settings, tmpdir):
        path = str(tmpdir)
        self._fill_sample_settings(settings)
        abs_path = os.path.join(path, 'foobar.json')
        settings.flush(abs_path)
        assert os.path.exists(abs_path)

    def test_load_from_given_fn(self, tmpdir):
        tmpdir.mkdir('old')
        tmpdir.mkdir('new')
        new_path = os.path.join(str(tmpdir), 'new')
        old_path = os.path.join(str(tmpdir), 'old')
        settings_ = _create_settings(old_path)
        self._fill_sample_settings(settings_)
        data = settings_.get()
        abs_path = os.path.join(new_path, 'foobar.json')
        settings_.flush(abs_path)
        _delete_settings(old_path)
        assert Settings(path=new_path, fn='foobar.json').get() == data

    def test_update_no_flush(self, settings):
        settings_data_empty = settings.get()
        self._fill_sample_settings(settings, flush=False)
        settings_sample_data = settings.get()
        assert settings_sample_data != settings_data_empty
        settings_data_loaded = _create_settings(settings.path).get()
        assert settings_data_loaded == settings_data_empty

    def test_settings_data_is_immutable(self, settings):
        self._fill_sample_settings(settings)
        settings.get()['invalid_setting'] = 'Oh no...'
        assert 'invalid_setting' not in settings.get()

    @pytest.mark.parametrize("update",
                             [{'invalid-setting': 'Oh no...'},
                              ['sizing'],
                              'sizing',
                              None,
                              1])
    def test_update_with_invalid_setting(self, settings, update):
        with pytest.raises(InvalidSettingsError):
            settings.update(update)

    @pytest.mark.parametrize("sizing", Settings.ALLOWED_SIZINGS+[None])
    def test_update_sizing(self, settings, sizing):
        settings.update({"sizing": {"foo": sizing}})

    @pytest.mark.parametrize("sizing", ["null", "None", 0, "invalid", ["links"],
                                        {"links": "links"}])
    def test_update_invalid_sizing(self, settings, sizing):
        with pytest.raises(InvalidSettingsError):
            settings.update({"sizing": {"foo": sizing}})

    @pytest.mark.parametrize("display_priority", [None, -1, 0, 1, 999])
    def test_update_display_order(self, settings, display_priority):
        settings.update({"display-priority": {"foo": display_priority}})

    @pytest.mark.parametrize("display_priority", ["1", "None", [1], {1: 2},
                                                  1.2, "null"])
    def test_update_invalid_display_order(self, settings, display_priority):
        with pytest.raises(InvalidSettingsError):
            settings.update({"display-priority": {"foo": display_priority}})

    @pytest.mark.parametrize("display_type", [None, "cloud", "links"])
    def test_update_display_type(self, settings, display_type):
        settings.update({"display-type": {"foo": display_type}})

    @pytest.mark.parametrize("display_type", ["1", "None", [1], {1: 2},
                                              1.2, "null", "something"])
    def test_update_invalid_display_type(self, settings, display_type):
        with pytest.raises(InvalidSettingsError):
            settings.update({"display-type": {"foo": display_type}})

    @pytest.mark.parametrize(["setting", "value"],
                             [("sizing", "links"),
                              ("display-priority", 1),
                              ("display-type", "cloud")])
    @pytest.mark.parametrize(["flush", "load_from_disc"], [
                             [True, True],
                             [True, False],
                             [False, False]])
    def test_none_resets_setting(self, settings, setting, value, flush,
                                 load_from_disc):
        settings.update({setting: {"foo": value}}, flush=flush)
        if load_from_disc:
            settings._load_from_file()
        assert settings.get()[setting]["foo"] == value
        settings.update({setting: {"foo": None}}, flush=flush)
        if load_from_disc:
            settings._load_from_file()
        assert "foo" not in settings.get()[setting]

    @pytest.mark.parametrize(["flush", "load_from_disc"], [
                             [True, True],
                             [True, False],
                             [False, False]])
    def test_clone_category(self, settings, flush, load_from_disc):
        self._fill_sample_settings(settings)
        data_orig = settings.get()
        settings.clone_category("foo", "bar", flush=flush)
        if load_from_disc:
            settings._load_from_file()
        data = settings.get()
        for setting_key in ("sizing", "display-type", "display-type"):
            assert "bar" not in data_orig[setting_key]
            assert data[setting_key]["bar"] == data_orig[setting_key]["foo"]
            assert data[setting_key]["foo"] == data_orig[setting_key]["foo"]

    @pytest.mark.parametrize(["flush", "load_from_disc"], [
                             [True, True],
                             [True, False],
                             [False, False]])
    def test_update_with_json_string(self, settings, flush, load_from_disc):
        data = {"sizing": {"foo": "links"},
                "display-priority": {"foo": 2},
                "display-type": {"foo": "links"}}
        json_data = json.dumps(data)
        settings.update(json_data, flush=flush)
        if load_from_disc:
            settings._load_from_file()
        assert settings.get() == data

    @pytest.mark.parametrize("settings_dict",
                             [{'sizing': {'foo': 'links'}},
                              {'display-priority': {'foo': 1}},
                              {'display-type': {'foo': 'links'}},
                              {}])
    def test_migration(self, settings_dict, tmpdir):
        path = str(tmpdir)
        self._write_settings_file(path, FN, settings_dict)
        loaded_settings = _create_settings(path)
        assert sorted(Settings.EMPTY_SETTINGS.keys()) ==\
            sorted(loaded_settings.get().keys())

    def test_invalid_settings_file(self, tmpdir):
        path = str(tmpdir)
        self._write_settings_file(path, FN, {'invalid-setting': 'Oh no...'})
        with pytest.raises(InvalidSettingsError):
            _create_settings(path)

    def test_db_add_cat_sets_display_priority(self, settings):
        assert settings.get()['display-priority'] == {}
        settings.store.add_category('foo')
        assert settings.get()['display-priority'] == {'foo': 1}
        settings.store.add_category('bar')
        assert settings.get()['display-priority'] == {'foo': 1, 'bar': 2}
        settings.store.remove_category('foo')
        assert settings.get()['display-priority'] == {'foo': 1, 'bar': 2}
        settings.update({'display-priority': {'foo': None}})
        assert settings.get()['display-priority'] == {'bar': 2}
        settings.store.add_category('foobar')
        assert settings.get()['display-priority'] == {'foobar': 3, 'bar': 2}
        settings.store.remove_category('foobar')
        assert settings.get()['display-priority'] == {'foobar': 3, 'bar': 2}
        settings.store.add_category('foobar')
        assert settings.get()['display-priority'] == {'foobar': 3, 'bar': 2}
        settings.update({'display-priority': {'foobar': 1}})
        settings.store.remove_category('foobar')
        assert settings.get()['display-priority'] == {'foobar': 1, 'bar': 2}
        settings.store.add_category('foobar')
        assert settings.get()['display-priority'] == {'foobar': 1, 'bar': 2}
        settings.store.remove_category('foobar')
        settings.update({'display-priority':
                         {'bar': 2, 'foobar': 2, 'foo': 3, 'baz': 5}})
        assert settings.get()['display-priority'] == \
            {'bar': 2, 'foobar': 2, 'foo': 3, 'baz': 5}
        settings.store.add_category('foobar')
        assert settings.get()['display-priority'] == \
            {'bar': 2, 'foobar': 4, 'foo': 3, 'baz': 5}
