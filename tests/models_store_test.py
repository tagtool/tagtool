import pytest
import os
import io
import errno
import shutil
import csv
import datetime
from copy import deepcopy
import json
import freezegun

from .util import assert_link_lists_equal

from models.store import Store
from models.store import DB_Diff
from models.store import HighLevelDB_Diff
from models import errors

from .util_store import store  # store fixture
from .util_store import time_now  # fixture
from .util_store import _destroy_store
from .util import assert_same_elems


class TestStore(object):
    CATEGORIES = {"foo": list(map(str, range(1, 7))),
                  "bar": list(map(str, range(4, 10)))}

    LINKS = [[["foo", "4"], ["bar", "4"]],
             [["foo", "5"], ["bar", "5"]],
             [["foo", "6"], ["bar", "6"]],
             [["foo", "1"], ["foo", "2"]],
             [["foo", "2"], ["bar", "8"]]]

    def _fill_db(self, store):
        """Fills the db with categories "foo" and "bar".

        foo -> "1","2","3","4","5","6"
        bar -> "4","5","6","7","8","9"

        links: foo.4 - bar.4
               foo.5 - bar.5
               foo.6 - bar.6
               foo.1 - foo.2
               foo.2 - bar.8"""
        diff = DB_Diff()
        for cat in self.CATEGORIES:
            store.add_category(cat, flush=False, diff=diff)
        for cat, elems in self.CATEGORIES.items():
            for elem in elems:
                store.add_to_category(cat, elem, flush=False, diff=diff)
        for l in self.LINKS:
            store.add_link(*l, flush=False, diff=diff)
        store.flush_db(force=True, high_level_diff=diff)

    def test_get_all_categories(self, store):
        self._fill_db(store)
        assert_same_elems(store.get_all_categories(),
                          TestStore.CATEGORIES.keys())

    def test_get_all_categories_immutability(self, store):
        self._fill_db(store)
        cats = store.get_all_categories()
        cats += ["bar"]
        assert_same_elems(store.get_all_categories(),
                          TestStore.CATEGORIES.keys())

    def test_get_all_categories_with_elements(self, store):
        self._fill_db(store)
        assert_same_elems(store.get_all_categories_with_elements(),
                          TestStore.CATEGORIES)

    def test_get_all_categories_with_elements_immutability(self, store):
        self._fill_db(store)
        cats = store.get_all_categories_with_elements()
        cats['bar'] = ["foobar"]
        assert_same_elems(store.get_all_categories_with_elements(),
                          TestStore.CATEGORIES)

    def test_get_all_elements(self, store):
        self._fill_db(store)
        assert_same_elems(store.get_all_elements(),
                          [[c, e]
                           for c in TestStore.CATEGORIES
                           for e in TestStore.CATEGORIES[c]])

    def test_get_all_elements_immutability(self, store):
        self._fill_db(store)
        elems = store.get_all_categories()
        elems += ["bar", "1"]
        assert_same_elems(store.get_all_elements(),
                          [[c, e]
                           for c in TestStore.CATEGORIES
                           for e in TestStore.CATEGORIES[c]])

    def test_get_all_categories_with_elements_and_weights(self, store):
        self._fill_db(store)
        elems = store.get_all_categories_with_elements()
        res = {k: {e: 0 for e in elems[k]} for k in elems}
        for l in TestStore.LINKS:
            for i in (0, 1):
                c, e = l[i]
                res[c][e] += 1
        assert store.get_all_categories_with_elements_and_weights() == res

    def test_cannot_load_by_timestamp_in_empty_dir(self, time_now, tmpdir):
        path = str(tmpdir)
        with pytest.raises(IOError):
            Store(timestamp=time_now, path=path)
        _destroy_store(path)

    def test_db_changed(self, store):
        store.add_category('foo', flush=False)
        assert store.db_changed()
        store.reset_changes()
        assert not store.db_changed()
        self._fill_db(store)
        assert not store.db_changed()
        store.add_to_category('foo', 'barr')
        assert not store.db_changed()
        store.add_to_category('foo', 'barr', flush=False)
        assert not store.db_changed()
        store.add_to_category('foo', 'barrr', flush=False)
        assert store.db_changed()

    @pytest.mark.parametrize('flush', [True, False])
    def test_flush_on_read_only(self, store, flush):
        timestamp = store.timestamp
        self._fill_db(store)
        old_store = Store(path=store.path, timestamp=timestamp)
        with pytest.raises(IOError):
            old_store.add_category('foo', flush=flush)

    def test_flush_no_change_force(self, store):
        assert len(store.all_available_timestamps()) == 1
        assert store.flush_db(force=True)
        assert len(store.all_available_timestamps()) == 2

    def test_flush_no_change(self, store):
        assert len(store.all_available_timestamps()) == 1
        assert not store.flush_db()
        assert len(store.all_available_timestamps()) == 1

    def test_get_missing_cat(self, store):
        self._fill_db(store)
        with pytest.raises(ValueError):
            store.get_all_category_members('missing')

    @pytest.mark.parametrize('strict', [True, False])
    @pytest.mark.parametrize(['method', 'args', 'has_strict'],
                             [('add_category', ['foo'], True),
                              ('add_to_category', ['foo', '1'], True),
                              ('get_category', ['foobar'], False),
                              ('get_all_category_members', ['foobar'], False),
                              ('remove_from_category', ['foobar', 'bar'], True),
                              ('remove_from_category', ['foo', 'foobar'], True),
                              ('remove_category', ['foobar'], True)])
    def test_missing_or_existing_cats_or_elems(self, store, strict, method,
                                               args, has_strict):
        self._fill_db(store)
        data = deepcopy(store.db)
        if not has_strict or strict:
            with pytest.raises(ValueError):
                if has_strict:
                    getattr(store, method)(*args, strict=True)
                else:
                    getattr(store, method)(*args)
        else:
            getattr(store, method)(*args)
        assert data == store.db

    def test_cannot_add_empty_category(self, store):
        with pytest.raises(ValueError):
            store.add_category('')

    def test_cannot_add_empty_element(self, store):
        store.add_category('foo')
        with pytest.raises(ValueError):
            store.add_to_category('foo', '')

    def test_add_to_missing_category_not_creative(self, store):
        with pytest.raises(ValueError):
            store.add_to_category('foo', 'bar', strict=True)

    def test_get_link_between(self, store):
        self._fill_db(store)
        assert len(store.get_all_links(["foo", "4"], ["bar", "4"])) == 1
        assert len(store.get_all_links(("foo", "4"), ("bar", "4"))) == 1
        assert len(store.get_all_links(["foo", "1"], ["foo", "2"])) == 1
        assert len(store.get_all_links(["foo", "2"], ["bar", "6"])) == 0
        with pytest.raises(ValueError):
            store.get_all_links(["fooo", "2"], ["barr", "-123"])

    def test_get_all_links(self, store):
        self._fill_db(store)
        assert_link_lists_equal(store.get_all_links(), TestStore.LINKS)

    def test_get_all_links_of(self, store):
        self._fill_db(store)
        assert len(store.get_all_links(["foo", "2"])) == 2
        with pytest.raises(ValueError):
            store.get_all_links(["fooo", "2"])

    def test_get_all_links_of_e2(self, store):
        self._fill_db(store)
        with pytest.raises(ValueError):
            store.get_all_links(e2=["foo", "2"])
        with pytest.raises(ValueError):
            store.get_all_links(e2=["fooo", "2"])

    def test_get_all_links_of_invalid_elements(self, store):
        self._fill_db(store)
        with pytest.raises(ValueError):
            store.get_all_links(["fooo", "2"])  # not existing category
        with pytest.raises(ValueError):
            store.get_all_links(["foo", "200"])  # not existing element
        with pytest.raises(ValueError):
            store.get_all_links(["foo"])  # only category as list
        with pytest.raises(TypeError):
            store.get_all_links("foo")  # only category as string
        with pytest.raises(ValueError):
            store.get_all_links([None, "2"])  # None as category
        with pytest.raises(ValueError):
            store.get_all_links(["foo", None])  # None as element

    def test_elem_validation_without_must_exist(self, store):
        # this test is for the sake of code coverage... don't look at it ;)
        self._fill_db(store)
        assert store._elem_validation(["foo", "200"], must_exist=False) == False
        assert store._elem_validation(["fooo", "2"], must_exist=False) == False
        assert store._elem_validation(["foo", "2"], must_exist=False) == True
        with pytest.raises(ValueError):
            store._elem_validation(["fooo", "2"], must_exist=True)
        with pytest.raises(ValueError):
            store._elem_validation(["foo", "200"], must_exist=True)
        assert store._elem_validation(["foo", "2"], must_exist=True) is None

    def test_add_link_non_creative_store(self, store):
        self._fill_db(store)
        store.add_link(["foo", "1"], ["bar", "9"])
        with pytest.raises(ValueError):
            store.add_link(["foo", "100"], ["bar", "9"], strict=True)
        with pytest.raises(ValueError):
            store.add_link(["foo", "1"], ["bar", "900"], strict=True)
        with pytest.raises(ValueError):
            store.add_link(["fooo", "1"], ["bar", "9"], strict=True)
        with pytest.raises(ValueError):
            store.add_link(["foo", "1"], ["barr", "9"], strict=True)

    def test_add_existing_links(self, store):
        self._fill_db(store)
        store.add_link(["foo", "4"], ["bar", "4"])
        store.add_link(["bar", "4"], ["foo", "4"])
        assert len(store.get_all_links(["foo", "4"], ["bar", "4"])) == 1
        assert len([l for l in store.get_all_links() if
                   l in [[["foo", "4"], ["bar", "4"]],
                         [["bar", "4"], ["foo", "4"]]]]) == 1
        with pytest.raises(ValueError):
            store.add_link(["foo", "4"], ["bar", "4"], strict=True)
        with pytest.raises(ValueError):
            store.add_link(["bar", "4"], ["foo", "4"], strict=True)

    @pytest.mark.parametrize(["e1", "e2"],
                             [[["foo", "4"], ["bar", "4"]],
                              [["bar", "4"], ["foo", "4"]]])
    def test_remove_links(self, store, e1, e2):
        self._fill_db(store)
        store.remove_link(e1, e2)
        assert_link_lists_equal(store.get_all_links(),
                                [l for l in TestStore.LINKS if
                                 (l[0] != e1 or l[1] != e2) and
                                 (l[0] != e2 or l[1] != e1)])

    def test_remove_missing_link(self, store):
        self._fill_db(store)
        links = [[["foo", "1"],  ["bar", "1"]],
                 [["bar", "1"],  ["foo", "1"]],
                 [["fooo", "1"], ["bar", "1"]],
                 [["foo", "2"],  ["bar", "1"]]]
        for l in links:
            store.remove_link(*l)
        assert len(store.get_all_links()) == len(TestStore.LINKS)
        for l in links:
            with pytest.raises(ValueError):
                store.remove_link(*l, strict=True)

    @pytest.mark.parametrize(["e1", "exception"],
                             [["foo", TypeError],
                              [["foo"], ValueError],
                              [None, TypeError],
                              [123, TypeError]])
    @pytest.mark.parametrize("strict", [True, False])
    def test_remove_invalid_links(self, store, e1, strict, exception):
        self._fill_db(store)
        with pytest.raises(exception):
            store.remove_link(e1, ["bar", "2"], strict=strict)

    @pytest.mark.parametrize("as_diff_action", [True, False])
    def test_rename_category(self, store, as_diff_action):
        self._fill_db(store)
        if not as_diff_action:
            store.rename_category('foo', 'fooo')
        else:
            store.diff_action({'renamings': {'categories': [['foo', 'fooo']]}})
        assert sorted(store.get_all_categories()) == sorted(["fooo", "bar"])
        assert (sorted(store.get_all_category_members('fooo')) ==
                sorted([['fooo', e] for e in TestStore.CATEGORIES['foo']]))
        l = [[['fooo' if e[0] == 'foo' else e[0], e[1]]
              for e in l]
             for l in TestStore.LINKS]
        assert_link_lists_equal(store.get_all_links(), l)

    @pytest.mark.parametrize(["cat", "elem"],
                             [['foobar', 'foo'],  # from missing category
                              ['bar', 'foo']])  # to existing category
    def test_rename_error_category(self, store, cat, elem):
        self._fill_db(store)
        with pytest.raises(ValueError):
            store.rename_category(cat, elem)

    @pytest.mark.parametrize("as_diff_action", [True, False])
    def test_rename_element_in_cat(self, store, as_diff_action):
        self._fill_db(store)
        if not as_diff_action:
            store.rename_element('foo', '2', 'foo', '22')
        else:
            store.diff_action(
                {'renamings': {'elements': [[['foo', '2'], ['foo', '22']]]}})
        assert sorted(store.get_all_categories()) == sorted(['foo', 'bar'])
        assert (sorted(store.get_all_categories_with_elements()) ==
                sorted({c: [e if (c != 'foo' or e != '2') else '22' for e in es]
                        for c, es in TestStore.CATEGORIES.items()}))
        l = [[[e[0], '22' if e == ['foo', '2'] else e[1]]
              for e in l]
             for l in TestStore.LINKS]
        assert_link_lists_equal(store.get_all_links(), l)

    @pytest.mark.parametrize(["cat", "from_e", "to_e"],
                             [['foob', '2', '22'],  # from missing category
                              ['foo', '99', '22'],  # from missing element
                              ['foo', '2', '3']])  # to existing element
    def test_rename_error_element_in_cat(self, store, cat, from_e, to_e):
        self._fill_db(store)
        with pytest.raises(ValueError):
            store.rename_element(cat, from_e, cat, to_e)

    @pytest.mark.parametrize("as_diff_action", [True, False])
    def test_rename_element(self, store, as_diff_action):
        self._fill_db(store)
        if not as_diff_action:
            store.rename_element('foo', '2', 'bar', '22')
        else:
            store.diff_action(
                {'renamings': {'elements': [[['foo', '2'], ['bar', '22']]]}})
        assert sorted(store.get_all_categories()) == sorted(['foo', 'bar'])
        assert (sorted(store.get_all_categories_with_elements()) ==
                sorted({c: [e if (c != 'bar' or e != '2') else '22' for e in es
                            if (c != 'foo' or e != '2')]
                        for c, es in TestStore.CATEGORIES.items()}))
        l = [[e if e != ['foo', '2'] else ['bar', '22']
              for e in l]
             for l in TestStore.LINKS]
        assert_link_lists_equal(store.get_all_links(), l)

    @pytest.mark.parametrize(["from_cat", "from_e", "to_cat", "to_e"],
                             # from missing category
                             [['foob', '2', 'bar', '22'],
                              #  to missing category
                              ['foo', '2', 'barrr', '22'],
                              # from missing element
                              ['foo', '99', 'bar', '22'],
                              # to existing element
                              ['foo', '2', 'bar', '4'],
                              # inside missing category
                              ['foob', '2', 'foob', '22'],
                              # inside category missing element
                              ['foo', '22', 'foo', '23'],
                              # inside category to existing element
                              ['foo', '2', 'foo', '3']])
    def test_rename_error_element(self, store, from_cat, from_e, to_cat, to_e):
        self._fill_db(store)
        with pytest.raises(ValueError):
            store.rename_element(from_cat, from_e, to_cat, to_e)

    @pytest.mark.parametrize("as_json", [True, False])
    def test_diff_action(self, store, as_json):
        self._fill_db(store)
        diff = {
            'insertions': {
                'categories': ['fooo'],
                'elements': [['fooo', 'br']],
                'links': [[['foo', '1'], ['fooo', 'br']],
                          [['foo', '2'], ['foo', '3']]]
            },
            'deletions': {
                'categories': ['bar'],
                'elements': [['foo', '6']],
                'links': [[['foo', '1'], ['foo', '2']]]
            }
        }
        diff = json.dumps(diff) if as_json else diff
        store.diff_action(diff)
        expected = {'categories': {'foo': ['1', '2', '3', '4', '5'],
                                   'fooo': ['br']},
                    'links': [[['foo', '2'], ['foo', '3']],
                              [['foo', '1'], ['fooo', 'br']]]}
        for cat in store.db['categories']:
            assert (sorted(store.db['categories'][cat]) ==
                    sorted(expected['categories'][cat]))
        assert_link_lists_equal(store.db['links'], expected['links'])

    def test_diff_action_only_element_insertion(self, store):
        self._fill_db(store)
        store.diff_action({
            'insertions': {
                'elements': [['fooo', 'br']],
            },
        })
        expected = {'categories': {'foo': ['1', '2', '3', '4', '5', '6'],
                                   'bar': ['4', '5', '6', '7', '8', '9'],
                                   'fooo': ['br']},
                    'links': [[['foo', '4'], ['bar', '4']],
                              [['foo', '5'], ['bar', '5']],
                              [['foo', '6'], ['bar', '6']],
                              [['foo', '1'], ['foo', '2']],
                              [['foo', '2'], ['bar', '8']]]}
        for cat in store.db['categories']:
            assert_same_elems(store.db['categories'][cat],
                              expected['categories'][cat])
        assert_link_lists_equal(store.db['links'], expected['links'])

    @pytest.mark.parametrize("diff",
                             [{'inserare': {'categories': ['boom']}},
                              {'insertions': {'whatsthat?': ['boom']}},
                              {'insertions': ['derp']},
                              {'insertions': {'categories': 'no_list'}},
                              {'insertions': {'categories': [1, 2]}},
                              {'insertions': {'categories': 123}},
                              {'insertions': {'elements': 'no_list'}},
                              {'insertions': {'elements': ['foo', 1]}},
                              {'insertions': {'elements': [2, '1']}},
                              {'insertions': {'elements': [['foo'], '1']}},
                              {'insertions': {'links': ['foo', '1']}},
                              {'insertions': {'links': [['foo', '1'],
                                                        ['bar', 6]]}},
                              {'insertions': {'links': [[['foo'], '1'],
                                                        ['bar', '6']]}},
                              {'insertions': {'links': 'no_list'}},
                              {1: {'category': ['boom']}},
                              {'insertions': {1: ['boom']}},
                              {'renamings': {1: ['foo', '2', '22']}},
                              {'renamings': {'categories': [{'foo': '2'}]}},
                              {'renamings': {'categories': [['foo', 2]]}},
                              {'renamings': {'categories': ['foo']}},
                              {'renamings': {'categories': 'foo'}},
                              {'renamings': {'elements': 'foo'}},
                              {'renamings': {'elements': [['foo', '2', 22]]}},
                              {'renamings': {'elements': ['foo', '2', '22']}},
                              {'renamings': {'elements': ['foo']}},
                              {'renamings': {'links': ['wontwork']}},
                              {'deletions': {'whatsthat?': ['boom']}},
                              {'deletions': ['derp']},
                              {'deletions': {'categories': 'no_list'}},
                              {'deletions': {'categories': [1, 2]}},
                              {'deletions': {'categories': 123}},
                              {'deletions': {'elements': 'no_list'}},
                              {'deletions': {'elements': ['foo', 1]}},
                              {'deletions': {'elements': [2, '1']}},
                              {'deletions': {'elements': [['foo'], '1']}},
                              {'deletions': {'links': ['foo', '1']}},
                              {'deletions': {'links': [['foo', '1'],
                                                       ['bar', 6]]}},
                              {'deletions': {'links': [[['foo'], '1'],
                                                       ['bar', '6']]}},
                              {'deletions': {'links': 'no_list'}},
                              {1: {'category': ['boom']}},
                              {'insertions': {1: ['boom']}},
                              '["derp"]',
                              '"nope"',
                              'nopee'])
    def test_diff_action_invalid_diff(self, store, diff):
        with pytest.raises(ValueError):
            store.diff_action(diff)

    @pytest.mark.parametrize(('query', 'type', 'expected'), (
        ('Hans Peter', '==', [['Person', 'Hans Peter'],
                              ['Tag', 'Hans Peter']]),
        ('Max Musterman', '==', [['Person', 'Max Mustermann']]),
        ('Thomas Mayer', '==', [['Person', 'Thomas Mayer']]),
        ('Thomas Maier', '~=', [['Person', 'Thomas Mayer'],
                                ['Person', 'Thomas Meier']]),
        ('Thomas', '~=', [['Person', 'Thomas Mayer'],
                          ['Person', 'Thomas Meier']]),
        ('Gregor', '==', [['Person', 'Gregor Adam']]),
        ('Dieter Water', '==', [['Person', 'Dieter Walter'],
                                ['Person', 'Dieter Wahlter']]),
        ('JavaScript', '==', [['Tag', 'JavaScript']]),
        ('Java Script', '==', [['Tag', 'JavaScript']]),
        ('javascript', '==', [['Tag', 'JavaScript']]),
        ('Random Stuff', '==', [])
    ))
    def test_search_elem(self, store, query, type, expected):
        for p in ('Hans Peter', 'Max Mustermann', 'Gregor Adam',
                  'Jan Halberstett', 'Thomas Mayer', 'Thomas Meier',
                  'Dieter Walter', 'Dieter Wahlter'):
            store.add_to_category('Person', p)
        for t in ('JavaScript', 'HTML5', 'Python', 'Deep Learning', 'Docker',
                  'Hans Peter'):
            store.add_to_category('Tag', t)

        res = store.search_element(query)
        if type == '==':
            assert expected == res
        if type == '~=':
            assert_same_elems(expected, res)
        if type == '>=':
            for r in expected:
                assert r in res

    def test_search_elem_force_fuzzy_perfect_match(self, store):
        for p in ('Hans Peter', 'Max Mustermann', 'Gregor Adam',
                  'Jan Halberstett', 'Thomas Mayer', 'Thomas Meier',
                  'Dieter Walter', 'Dieter Wahlter'):
            store.add_to_category('Person', p)
        for t in ('JavaScript', 'HTML5', 'Python', 'Deep Learning', 'Docker',
                  'Hans Peter'):
            store.add_to_category('Tag', t)

        res = store.search_element(
            'Gregor Adam', force_fuzzy=True, fuzzy_limit=1)

        assert res == [['Person', 'Gregor Adam']]

    @pytest.mark.parametrize("idx_offset", [-1, 0, 1])
    @pytest.mark.parametrize(["idx", "time_offset", "expected_idx"],
                             [[0, datetime.timedelta(0, 0, -1), 0],
                              [0, datetime.timedelta(0       ), 0],
                              [0, datetime.timedelta(0, 0,  1), 0],
                              [1, datetime.timedelta(0, 0, -1), 0],
                              [1, datetime.timedelta(0       ), 1],
                              [1, datetime.timedelta(0, 0,  1), 1]])
    def test_find_timestamp(self, store, idx, idx_offset, time_offset,
                            expected_idx):
        self._fill_db(store)
        available_timestamps = store.all_available_timestamps()
        assert len(available_timestamps) == 2
        timestamp = available_timestamps[idx] + time_offset
        res = store.find_timestamp(timestamp, idx_offset)
        string_res = store.find_timestamp(str(timestamp), idx_offset)
        assert res == string_res
        if 0 <= expected_idx + idx_offset < 2:
            assert available_timestamps.index(res) == expected_idx + idx_offset
        else:
            assert res is False
        if res:
            db = store.get_db_by_timestamp(timestamp, idx_offset)
            assert db.timestamp == res
        else:
            with pytest.raises(ValueError):
                store.get_db_by_timestamp(timestamp, idx_offset)

    @pytest.mark.parametrize('from_idx', [0, 1, None])
    @pytest.mark.parametrize('to_idx', [0, 1, None])
    @pytest.mark.parametrize('timestamp_as_string', [True, False])
    def test_calc_db_diff(self, store, from_idx, to_idx, timestamp_as_string):
        self._fill_db(store)
        available_timestamps = store.all_available_timestamps()
        arg1 = from_timestamp = (available_timestamps[from_idx]
                                 if from_idx is not None else None)
        to_timestamp = (available_timestamps[to_idx]
                        if to_idx is not None else available_timestamps[-1])
        arg2 = to_timestamp if to_idx is not None else None
        if timestamp_as_string:
            arg1, arg2 = map(lambda x: str(x) if x is not None else None,
                             (arg1, arg2))
        diff = store.calc_db_diff(arg1, arg2)
        if (from_timestamp is None and to_idx == 0 or
                from_timestamp is not None and to_timestamp == from_timestamp):
            expected = DB_Diff(from_timestamp, to_timestamp)
        else:
            action = 'insertions'
            if from_timestamp is not None and to_timestamp < from_timestamp:
                action = 'deletions'
            expected = DB_Diff(from_timestamp, to_timestamp)
            expected._diff_x(action, 'categories',
                             list(TestStore.CATEGORIES.keys()))
            expected._diff_x(action, 'elements',
                             [[cat, e]
                              for cat in TestStore.CATEGORIES
                              for e in TestStore.CATEGORIES[cat]])
            expected._diff_x(action, 'links', TestStore.LINKS)
        assert diff == expected

    @pytest.mark.parametrize("timestamp_idx", [0, 1, 2, None])
    def test_full_db_as_diff(self, store, timestamp_idx):
        self._fill_db(store)
        store.add_to_category('fou', 'baz')
        all_times_stamps = store.all_available_timestamps()
        arg = (all_times_stamps[timestamp_idx]
               if timestamp_idx is not None else None)
        timestamp_to = (all_times_stamps[timestamp_idx]
                        if timestamp_idx is not None else all_times_stamps[-1])
        expected = DB_Diff(to_dbv=timestamp_to)
        if timestamp_idx is None or timestamp_idx >= 1:
            expected.add_cats(list(TestStore.CATEGORIES.keys()))
            expected.add_elems([[cat, e]
                               for cat in TestStore.CATEGORIES
                               for e in TestStore.CATEGORIES[cat]])
            expected.add_links(TestStore.LINKS)
        if timestamp_idx is None or timestamp_idx >= 2:
            expected.add_cat('fou')
            expected.add_elem(['fou', 'baz'])
        diff = store.full_db_as_diff(arg)
        assert expected == diff

    def test_delete_elem_keeps_order(self, store):
        self._fill_db(store)
        store.remove_from_category('foo', '4')
        assert (store.db['categories']['foo'] ==
                [str(i) for i in range(1, 7) if i != 4])
        assert (store.db['links'] ==
                [l for l in TestStore.LINKS if ['foo', '4'] not in l])

    @pytest.mark.parametrize(["from_cat", "from_elem", "to_cat", "to_elem"],
                             [['foo', '4', 'foo', '44'],
                              ['foo', '4', 'bar', '44']])
    def test_rename_elem_keeps_order(self, store,
                                     from_cat, from_elem, to_cat, to_elem):
        self._fill_db(store)
        old_db = deepcopy(store.db)
        store.rename_element(from_cat, from_elem, to_cat, to_elem)
        if from_cat == to_cat:  # preserves order if in same cat
            assert (store.db['categories'][from_cat] ==
                    [e if e != from_elem else to_elem
                     for e in old_db['categories'][from_cat]])
        assert (store.db['links'] ==
                [[e if e != [from_cat, from_elem] else [to_cat, to_elem]
                  for e in l]
                 for l in TestStore.LINKS])

    def test_adds_elem_at_end(self, store):
        self._fill_db(store)
        store.add_to_category('foo', '44')
        assert store.db['categories']['foo'] == [*map(str, range(1, 7)), '44']

    def test_delete_cat_keeps_order(self, store):
        self._fill_db(store)
        store.add_category('foobar')
        store.remove_category('bar')
        assert_same_elems(store.db['categories'].keys(), ['foo', 'foobar'])
        assert_link_lists_equal(
            store.db['links'],
            [l for l in TestStore.LINKS if 'bar' not in (l[0][0], l[1][0])]
        )

    def test_rename_cat_keeps_order(self, store):
        self._fill_db(store)
        store.rename_category('foo', 'foobar')
        assert_same_elems(store.db['categories'].keys(), ['foobar', 'bar'])
        assert (store.db['categories']['foobar'] ==
                [str(i) for i in range(1, 7)])
        assert (store.db['links'] ==
                [[e if e[0] != 'foo' else ['foobar', e[1]] for e in l]
                 for l in TestStore.LINKS])

    def test_adds_cat_at_end(self, store):
        self._fill_db(store)
        store.add_category('foobar')
        assert_same_elems(store.db['categories'].keys(),
                          ['foo', 'bar', 'foobar'])
        assert_link_lists_equal(store.db['links'], TestStore.LINKS)

    @pytest.mark.parametrize('twist', [True, False])
    def test_delete_link_keeps_order(self, store, twist):
        self._fill_db(store)
        if twist:
            store.remove_link(["bar", "6"], ["foo", "6"])
        else:
            store.remove_link(["foo", "6"], ["bar", "6"])
        assert_same_elems(store.db['categories'].keys(), ['foo', 'bar'])
        assert_link_lists_equal(
            store.db['links'],
            [l for l in TestStore.LINKS
             if (l != [["foo", "6"], ["bar", "6"]] and
                 l != [["bar", "6"], ["foo", "6"]])]
        )

    def test_adds_link_at_end(self, store):
        self._fill_db(store)
        store.add_link(["foo", "1"], ["bar", "9"])
        assert_link_lists_equal(
            store.db['links'],
            TestStore.LINKS + [[["foo", "1"], ["bar", "9"]]]
        )

    @pytest.mark.parametrize(
        ['bulk_action', 'changed'], [
            [{'insertions': {'links': [[['bar', '9'], ['bar', '7']]]}},
             [['bar', '9'], ['bar', '7']]],
            [{'insertions': {'links': [[['bar', '9'], ['bar', '8']]]}},
             [['bar', '9'], ['bar', '8']]],
            [{'insertions': {'elements': [['foo', '22']]}},
             [['foo', '22']]],
            [{'insertions': {'categories': ['foobar']}},
             ['foobar']],
            [{'deletions': {'links': [[['foo', '1'], ['foo', '2']]]}},
             [['foo', '1'], ['foo', '2']]],
            [{'deletions': {'elements': [['foo', '1']]}},
             [['foo', '2']]],  # because foo 1 is removed from foo 2's links
            [{'deletions': {'elements': [['bar', '9']]}},
             []],
            [{'deletions': {'categories': ['bar']}},
             [['foo', '4'], ['foo', '5'], ['foo', '6'], ['foo', '2']]],
        ])
    @pytest.mark.parametrize('recency', ['older', 'exact', 'newer'])
    def test_recently_changed(self, store, bulk_action, changed, recency):
        now = datetime.datetime.utcnow()
        with freezegun.freeze_time(now):
            self._fill_db(store)
        with freezegun.freeze_time(now + datetime.timedelta(seconds=2)):
            store.diff_action(bulk_action)
        if recency == 'older':
            with freezegun.freeze_time(now + datetime.timedelta(seconds=3)):
                res = store.recently_changed(datetime.timedelta(
                    milliseconds=999))
                assert res == tuple()
                return
        elif recency == 'exact':
            with freezegun.freeze_time(now + datetime.timedelta(seconds=3)):
                res = store.recently_changed(datetime.timedelta(seconds=1))
            assert_same_elems(res, changed)
        elif recency == 'newer':
            with freezegun.freeze_time(now + datetime.timedelta(seconds=3)):
                res = store.recently_changed(datetime.timedelta(seconds=2))
            assert_same_elems(res, changed)

    def test_empty_diff_after_thinning_db(self, store):
        now = datetime.datetime.utcnow()
        t = []
        for sec in range(4):
            t.append(now + datetime.timedelta(seconds=sec))

        with freezegun.freeze_time(t[0]):
            store.add_category('foo')
        with freezegun.freeze_time(t[1]):
            store.add_category('bar')
        with freezegun.freeze_time(t[2]):
            store.flush_db(force=True)
        with freezegun.freeze_time(t[3]):
            store.add_category('baz')
        diff = store.calc_db_diff(old_timestamp=t[0], new_timestamp=t[1])
        assert diff == DB_Diff.add_cat('bar', from_dbv=t[0], to_dbv=t[1])
        diff = store.calc_db_diff(old_timestamp=t[0], new_timestamp=t[2])
        assert diff == DB_Diff.add_cat('bar', from_dbv=t[0], to_dbv=t[2])
        diff = store.calc_db_diff(old_timestamp=t[1], new_timestamp=t[2])
        assert diff == DB_Diff(from_dbv=t[1], to_dbv=t[2])
        diff = store.calc_db_diff(old_timestamp=t[2], new_timestamp=t[3])
        assert diff == DB_Diff.add_cat('baz', from_dbv=t[2], to_dbv=t[3])
        diff = store.calc_db_diff(old_timestamp=t[0], new_timestamp=t[3])
        should_diff = DB_Diff(from_dbv=t[0], to_dbv=t[3])
        should_diff.add_cat('bar')
        should_diff.add_cat('baz')
        assert diff == should_diff

    @pytest.mark.parametrize('fill_db', [True, False])
    def test_similarities(self, store, fill_db):
        if fill_db:
            self._fill_db(store)
            store.diff_action({'insertions': {'links': [
                [['foo', '5'], ['bar', '6']],
                [['bar', '5'], ['bar', '6']],
                [['bar', '7'], ['bar', '9']],
                [['foo', '4'], ['bar', '9']],
                [['bar', '7'], ['bar', '4']],
                [['bar', '7'], ['foo', '3']]
            ]}})
            expected = [[['foo', '1'], ['bar', '8'], 1],
                        [['bar', '4'], ['bar', '9'], 1],
                        [['foo', '4'], ['bar', '7'], 2 / 2.5],
                        [['bar', '5'], ['foo', '5'], 1 / 2.0],
                        [['foo', '5'], ['foo', '6'], 1 / 1.5],
                        [['foo', '3'], ['bar', '4'], 1 / 1.5],
                        [['foo', '3'], ['bar', '9'], 1 / 1.5],
                        [['bar', '5'], ['foo', '6'], 1 / 1.5],
                        [['bar', '6'], ['foo', '5'], 1 / 2.5],
                        [['bar', '5'], ['bar', '6'], 1 / 2.5]]
            received = store.similarities()
            expected = [(r, sorted([e1, e2])) for e1, e2, r in expected]
            received = [(r, sorted([e1, e2])) for e1, e2, r in received]
            assert_same_elems(expected, received)
        else:
            assert store.similarities() == []

    @pytest.mark.parametrize('strict', [True, False])
    def test_md_link_title_id_collision(self, store, strict):
        store.add_category('foo')
        store.add_category('bar')
        store.add_to_category('foo', 'bar', strict=strict)
        if strict:
            with pytest.raises(errors.ElementExistsError):
                store.add_to_category('foo', '[bar](http://example.com)',
                                      strict=strict)
        else:
            store.add_to_category('foo', '[bar](http://example.com)',
                                  strict=strict)
        store.add_to_category('foo', '[barr](http://example.com)',
                              strict=strict)
        store.add_to_category('bar', '[bar](http://example.com)', strict=strict)
        if strict:
            with pytest.raises(errors.ElementExistsError):
                store.add_to_category('bar', 'bar',
                                      strict=strict)
        else:
            store.add_to_category('foo', 'bar',
                                  strict=strict)
        expected = [['foo', 'bar'],
                    ['foo', '[barr](http://example.com)'],
                    ['bar', '[bar](http://example.com)']]
        assert_same_elems(expected, store.get_all_elements())

    @pytest.mark.parametrize('bracket', [']', '['])
    @pytest.mark.parametrize('strict', [True, False])
    def test_md_link_bracket_escape(self, store, bracket, strict):
        store.add_to_category('foo',
                              '[foo\\' + bracket + 'bar](http://example.com)')
        store.add_to_category('foo',
                              '[foo' + bracket + 'bar](http://example.com)',
                              strict=True)
        store.add_to_category('foo', 'foo\\' + bracket + 'bar', strict=True)
        # 2nd and 3rd are not treated as links
        if strict:
            with pytest.raises(ValueError):
                store.add_to_category('foo', 'foo' + bracket + 'bar',
                                      strict=strict)
        else:
            store.add_to_category('foo', 'foo' + bracket + 'bar',
                                  strict=strict)
        assert_same_elems(
            store.db['categories']['foo'],
            ['[foo\\' + bracket + 'bar](http://example.com)',
             '[foo' + bracket + 'bar](http://example.com)',
             'foo\\' + bracket + 'bar']
        )

    def test_md_link_linking(self, store):
        store.add_to_category('foo', 'foo')
        store.add_to_category('foo', '[bar](http://example.com)')
        store.add_link(['foo', 'foo'], ['foo', '[bar](http://example.com)'])
        assert_link_lists_equal(
            store.get_all_links(),
            [[['foo', 'foo'], ['foo', '[bar](http://example.com)']]]
        )
        store.remove_link(['foo', 'foo'], ['foo', 'bar'])
        assert len(store.get_all_links()) == 0
        store.add_link(['foo', 'foo'], ['foo', 'bar'])
        assert_link_lists_equal(
            store.get_all_links(),
            [[['foo', 'foo'], ['foo', '[bar](http://example.com)']]]
        )
        store.remove_link(['foo', 'foo'], ['foo', '[bar](http://example.com)'])
        assert len(store.get_all_links()) == 0

    @pytest.mark.parametrize(['diff', 'diff_effect'], [
        [{'insertions': {'categories': ['foo']}}, {}],
        [{'insertions': {'elements': [['foo', '1']]}}, {}],
        [{'insertions': {'links': [[['foo', '1'], ['foo', '2']]]}}, {}],
        [{'insertions': {'links': [[['foo', '1'], ['foo', '3']]],
                         'elements': [['foo', '1'], ['foo', '3']]}},
         {'insertions': {'links': [[['foo', '1'], ['foo', '3']]]}}],
        [{'deletions': {'categories': ['foobarbaz']}}, {}],
        [{'deletions': {'elements': [['foo', '-1']]}}, {}],
        [{'deletions': {'elements': [['foobar', '4']]}}, {}],
        [{'deletions': {'links': [[['foo', '1'], ['foo', '3']]]}}, {}],
        [{'deletions': {'links': [[['foo', '1'], ['foo', '3']]]},
          'insertions': {'elements': [['foo', '-1']]}},
         {'insertions': {'elements': [['foo', '-1']]}}],
        [{'renamings': {'categories': [['foo', 'fooo']]}},
         {'renamings': {'categories': [['foo', 'fooo']]}}],
        [{'renamings': {'elements': [[['foo', '1'], ['foo', 'one']]]}},
         {'renamings': {'elements': [[['foo', '1'], ['foo', 'one']]]}}],
    ])
    def test_high_level_diff_is_effective_diff(self, store, diff, diff_effect):
        """Fills the db with categories "foo" and "bar".

        foo -> "1","2","3","4","5","6"
        bar -> "4","5","6","7","8","9"

        links: foo.4 - bar.4
               foo.5 - bar.5
               foo.6 - bar.6
               foo.1 - foo.2
               foo.2 - bar.8"""
        self._fill_db(store)
        all_dbvs = store.all_available_timestamps()
        store.diff_action(diff)
        if diff_effect:
            # check for equality modulo time stamps
            diff_effect = HighLevelDB_Diff(
                from_dbv=store.high_level_diff['from'],
                to_dbv=store.high_level_diff['to'], raw_diff=diff_effect)
            assert store.high_level_diff == diff_effect
        else:
            # check that nothing happened
            assert all_dbvs == store.all_available_timestamps(nocache=True)

    @pytest.mark.parametrize(['actions', 'cancels'], [
        [[{'insertions': {'elements': [['foobar', 'foo']]}},
          {'deletions': {'categories': ['foobar']}}],
         True],
        [[{'insertions': {'elements': [['foobar', 'foo']]}},
          {'deletions': {'elements': [['foobar', 'foo']]}}],
         False],
        [[{'renamings': {'elements': [[['bar', '5'], ['bar', 'five']]]}},
          {'renamings': {'elements': [[['bar', 'five'], ['bar', '5']]]}}],
         True],
        [[{'renamings': {'categories': [['bar', 'barr']]}},
          {'renamings': {'categories': [['barr', 'bar']]}}],
         True],
    ])
    @pytest.mark.parametrize('start_idx', list(range(3)))
    def test_high_level_diff_collapses_adjacent_pairs(self, store, actions,
                                                      cancels, start_idx):
        self._fill_db(store)
        fixed_acts = [{'renamings': {'categories': [['foo', 'fooo']]}},
                      {'deletions': {'elements': [['fooo', '3']]}}]
        acts = fixed_acts[:start_idx] + actions + fixed_acts[start_idx:]
        for act in acts:
            store.diff_action(act)
        timestamps = store.all_available_timestamps()

        assert len(timestamps) == 1 + 4 + 1  # empty db + _fill_db + actions
        # _fill_db causes one extra offset
        hl_diffs = store.high_level_diffs(timestamps[1], timestamps[-1])
        print('\n'.join(map(str, hl_diffs)))
        if cancels:
            assert len(hl_diffs) == 2
            for i in range(2):
                assert hl_diffs[i] == \
                       HighLevelDB_Diff(from_dbv=hl_diffs[i]['from'],
                                        to_dbv=hl_diffs[i]['to'],
                                        raw_diff=fixed_acts[i])
        else:
            assert len(hl_diffs) == 4
            for i in range(4):
                assert hl_diffs[i] == \
                       HighLevelDB_Diff(from_dbv=hl_diffs[i]['from'],
                                        to_dbv=hl_diffs[i]['to'],
                                        raw_diff=acts[i])

    def test_high_level_diff_collapses_multiple_adjacent_pairs(self, store):
        self._fill_db(store)
        fixed_acts = [{'renamings': {'categories': [['foo', 'fooo']]}},
                      {'deletions': {'elements': [['fooo', '3']]}}]
        actions = [{'insertions': {'elements': [['bar', 'new']]}},
                   {'renamings': {'categories': [['bar', 'barr']]}},
                   {'renamings': {'categories': [['barr', 'bar']]}},
                   {'deletions': {'elements': [['bar', 'new']]}}]
        acts = fixed_acts[:1] + actions + fixed_acts[1:]
        for act in acts:
            store.diff_action(act)

        timestamps = store.all_available_timestamps()
        assert len(timestamps) == 1 + 6 + 1  # empty db + _fill_db + actions
        # _fill_db causes one extra offset
        hl_diffs = store.high_level_diffs(timestamps[1], timestamps[-1])
        assert len(hl_diffs) == 2
        for i in range(2):
            assert hl_diffs[i] == \
                   HighLevelDB_Diff(from_dbv=hl_diffs[i]['from'],
                                    to_dbv=hl_diffs[i]['to'],
                                    raw_diff=fixed_acts[i])

    @pytest.mark.parametrize(['csv_', 'links', 'elems', 'cats'], [
        ['bar\nfoo', [], [], ['bar', 'foo']],
        ['\xfc', [], [], ['\xfc']],  # all unicode in db
        ['', [], [], []],
        ['foo,1,bar,1',
         [[['foo', '1'], ['bar', '1']]],
         [['foo', '1'], ['bar', '1']],
         ['foo', 'bar']],
        ['foo', [], [], ['foo']],
        ['foo,', [], [], ['foo']],
        ['foo,,', [], [], ['foo']],
        ['foo,,,', [], [], ['foo']],
        ['bar\n,,foo', [], [], ['bar', 'foo']],
        ['bar\n,,foo,', [], [], ['bar', 'foo']],
        ['foo,,bar', [], [], ['foo', 'bar']],
        ['foo,,bar,', [], [], ['foo', 'bar']],
        ['foo,1,bar', [], [['foo', '1']], ['foo', 'bar']],
        ['foo,1,bar,2\nfoo,2',
         [[['foo', '1'], ['bar', '2']]],
         [['foo', '1'], ['foo', '2'], ['bar', '2']],
         ['foo', 'bar']],
        ['foo,\n,1', [], [['foo', '1']], ['foo']],
        ['foo,1,bar,1\n,2,,1\n,',
         [[['foo', '1'], ['bar', '1']], [['foo', '2'], ['bar', '1']]],
         [['foo', '1'], ['foo', '2'], ['bar', '1']],
         ['foo', 'bar']],
    ])
    @pytest.mark.parametrize('with_head', [True, False])
    def test_csv_import(self, store, csv_, links, elems, cats, with_head):
        if with_head:
            csv_ = "Category1,Element1,Category2,Element2\n" + csv_
        stream = io.StringIO(csv_)
        store.import_('dummy.csv', stream, format_='csv')
        assert sorted(store.get_all_categories()) == sorted(cats)
        assert sorted(store.get_all_elements()) == sorted(elems)
        assert_link_lists_equal(store.get_all_links(), links)

    @pytest.mark.parametrize('csv_', [',,foo', ',,foo,', ',,,'])
    def test_invalid_csv_import(self, store, csv_):
        stream = io.StringIO(csv_)
        with pytest.raises(ValueError):
            store.import_('dummy.csv', stream, format_='csv')
        assert store.get_all_categories() == []
        assert store.get_all_elements() == []
        assert_link_lists_equal(store.get_all_links(), [])

    @pytest.mark.parametrize('fill_store', [True, False])
    @pytest.mark.parametrize('extra_diff', [
        {}, {'insertions': {'categories': ['foobar']}}
    ])
    def test_csv_export(self, store, fill_store, extra_diff):
        if fill_store:
            self._fill_db(store)
        if extra_diff:
            store.diff_action(extra_diff)
        links = []
        elems = []
        cats = []
        buffer_, store_inst = store.export(format_='csv')
        assert store_inst.timestamp == store.timestamp
        for i, row in enumerate(csv.reader(buffer_)):
            assert len(row) == 4
            if i == 0:
                assert row == Store.IMPORT_HEADER
                continue
            filled = 0
            for i in range(4):
                if row[i]:
                    if filled == i:
                        filled += 1
                    else:
                        assert False and "Row was not continuously filled"
            assert filled in (1, 2, 4)
            if filled == 1:
                if row[0] not in cats:
                    cats.append(row[0])
            elif filled == 2:
                if row[0] not in cats:
                    cats.append(row[0])
                if row[:2] not in elems:
                    elems.append(row[:2])
            else:
                for i in range(2):
                    if row[i*2] not in cats:
                        cats.append(row[i*2])
                    if row[i*2:(i+1)*2] not in elems:
                        elems.append(row[i*2:(i+1)*2])
                link = [row[:2], row[2:4]]
                if link not in links:
                    links.append(link)

        assert_same_elems(store.get_all_categories(), cats)
        assert_same_elems(store.get_all_elements(), elems)
        assert_link_lists_equal(store.get_all_links(), links)
