# -*- coding: utf-8 -*-
import os
from copy import deepcopy
import pytest
import json
import re
import time
import datetime
import threading
from threading import Thread
from urllib.parse import quote as url_quote

import freezegun

from .util import recursive_dict_merge
from .util import assert_link_lists_equal
from .util import assert_same_elems

import auth_util

from .util_server import app
from .util_server import app_w_pw
from .util_server import app_all_pw_and_ro_combinations
from .util_server import add_webhook_bot_to_app
from .util_server import json_post
from .util_server import json_put
from .util_async_listener import async_listener
from . import util_server

from models.db_diff import DB_Diff

import server

from logging import getLogger
log = getLogger(__name__)

# this is a dirty hack to make sure polls return faster, so that the threaded
# listener terminates not too long after calling .stop()
import models

__author__ = 'rouven'


class TestServer(object):
    ##################
    # Helper methods #
    ##################
    CATEGORIES = {"foo": list(map(str, range(1, 7))),
                  "bar": list(map(str, range(4, 10)))}
    LINKS = [[["foo", "4"], ["bar", "4"]],
             [["foo", "5"], ["bar", "5"]],
             [["foo", "6"], ["bar", "6"]],
             [["foo", "1"], ["foo", "2"]],
             [["foo", "2"], ["bar", "8"]]]

    def _fill_db(self):
        """Fills the db with categories "foo" and "bar".

        foo -> "1","2","3","4","5","6"
        bar -> "4","5","6","7","8","9"

        links: foo.4 - bar.4
               foo.5 - bar.5
               foo.6 - bar.6
               foo.1 - foo.2
               foo.2 - bar.8"""
        s = server.db
        diff = DB_Diff()
        for cat in self.CATEGORIES:
            s.add_category(cat, flush=False, diff=diff)
        for cat, elems in self.CATEGORIES.items():
            for elem in elems:
                s.add_to_category(cat, elem, flush=False, diff=diff)
        for l in self.LINKS:
            s.add_link(*l, flush=False, diff=diff)
        s.flush_db(force=True, high_level_diff=diff)

    def _db_link_counts(self):
        return server.db.get_all_categories_with_elements_and_weights()

    ################
    # Test methods #
    ################
    def test_post_category(self, app):
        self._fill_db()
        res = app.post("/api/category/fooo")
        assert res.status_code == 204
        s = server.db
        assert_same_elems(s.get_all_categories(), ["foo", "bar", "fooo"])

    def test_delete_category(self, app):
        self._fill_db()
        res = app.delete("/api/category/bar")
        assert res.status_code == 204
        s = server.db
        assert s.get_all_categories() == ["foo"]

    def test_delete_missing_category_strict(self, app):
        self._fill_db()
        res = app.delete("/api/category/fooo", data={'strict': 'true'})
        assert res.status_code == 404

    def test_delete_category_with_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        res = app.delete("/api/category/foo", data={'db_version': str(dbv)})
        assert res.status_code == 409

    def test_get_category(self, app):
        self._fill_db()
        res = app.get("/api/category/bar")
        assert_same_elems(
            json.loads(res.data.decode('utf-8')),
            [["bar", str(i)] for i in range(4, 10)]
        )

    def test_get_missing_category(self, app):
        self._fill_db()
        res = app.get("/api/category/barr")
        assert res.status_code == 404

    def test_post_existing_category(self, app):
        self._fill_db()
        res = app.post("/api/category/foo")
        assert res.status_code == 204
        s = server.db
        assert_same_elems(s.get_all_categories(), ["foo", "bar"])
        assert_same_elems(
            s.get_all_category_members('foo'),
            [['foo', e] for e in TestServer.CATEGORIES['foo']]
        )

    def test_post_existing_category_strict(self, app):
        self._fill_db()
        res = app.post("/api/category/foo", data={'strict': 'true'})
        assert res.status_code == 400

    def test_post_category_with_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        res = app.post("/api/category/fooo", data={'db_version': str(dbv)})
        assert res.status_code == 409

    def test_get_categories(self, app):
        self._fill_db()
        res = app.get("/api/categories")
        data = json.loads(res.data.decode('utf-8'))
        counts = self._db_link_counts()
        assert sorted(data.keys()) == sorted(TestServer.CATEGORIES.keys())
        for cat in data.keys():
            assert (sorted(data[cat].keys()) ==
                    sorted(TestServer.CATEGORIES[cat]))
            for elem in data[cat].keys():
                assert data[cat][elem] == counts[cat][elem]

    def test_post_categories_fails_gently(self, app):
        res = app.post('/api/categories')
        assert res.status_code == 405

    def test_delete_categories_fails_gently(self, app):
        res = app.delete('/api/categories')
        assert res.status_code == 405

    def test_post_element(self, app):
        self._fill_db()
        res = app.post("/api/category/foo/bar")
        res = app.post("/api/category/foo/barrr")
        assert res.status_code == 204
        s = server.db
        assert (s.get_category("foo") ==
                self.CATEGORIES["foo"] + ["bar", "barrr"])

    def test_post_existing_element(self, app):
        self._fill_db()
        res = app.post("/api/category/foo/1")
        assert res.status_code == 204
        s = server.db
        assert s.get_category("foo") == self.CATEGORIES["foo"]

    def test_post_existing_element_strict(self, app):
        self._fill_db()
        res = app.post("/api/category/foo/1", data={'strict': 'true'})
        assert res.status_code == 400

    def test_post_element_strict_with_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        res = app.post("/api/category/foo/100", data={'db_version': str(dbv)})
        assert res.status_code == 409

    def test_delete_element(self, app):
        self._fill_db()
        res = app.delete("/api/category/foo/1")
        assert res.status_code == 204
        s = server.db
        assert s.get_category("foo") == [str(i) for i in range(2, 7)]

    def test_delete_missing_element(self, app):
        self._fill_db()
        res = app.delete("/api/category/foo/1337")
        assert res.status_code == 204

    def test_delete_missing_element_strict(self, app):
        self._fill_db()
        res = app.delete("/api/category/foo/1337", data={'strict': 'true'})
        assert res.status_code == 404

    def test_get_element(self, app):
        self._fill_db()
        res = app.get("/api/category/foo/2")
        assert res.status_code == 200
        assert (json.loads(res.data.decode('utf-8')) ==
                [[["foo", "1"], ["foo", "2"]], [["foo", "2"], ["bar", "8"]]])

    def test_get_elements(self, app):
        self._fill_db()
        res = app.get("/api/category/foo")
        assert res.status_code == 200
        assert (json.loads(res.data.decode('utf-8')) ==
                [['foo', e] for e in TestServer.CATEGORIES['foo']])

    def test_get_missing_element(self, app):
        self._fill_db()
        res = app.get("/api/category/foo/1337")
        assert res.status_code == 404

    def test_post_link_existing_elements(self, app):
        self._fill_db()
        res = app.post("/api/link/between/foo/3/and/bar/4")
        assert res.status_code == 204
        s = server.db
        assert [["foo", "3"], ["bar", "4"]] in s.get_all_links()

    def test_post_link_existing_elements_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        res = app.post("/api/link/between/foo/3/and/bar/4",
                       data={'db_version': str(dbv)})
        assert res.status_code == 409

    def test_post_existing_link(self, app):
        self._fill_db()
        res = app.post("/api/link/between/foo/4/and/bar/4")
        assert res.status_code == 204
        s = server.db
        assert [["foo", "4"], ["bar", "4"]] in s.get_all_links()

    def test_post_existing_link_strict(self, app):
        self._fill_db()
        res = app.post("/api/link/between/foo/4/and/bar/4",
                       data={'strict': 'true'})
        assert res.status_code == 400

    def test_post_link_not_existing_category(self, app):
        self._fill_db()
        res = app.post("/api/link/between/fooo/1/and/bar/2")
        assert res.status_code == 204
        s = server.db
        assert [["fooo", "1"], ["bar", "2"]] in s.get_all_links()
        assert "fooo" in s.get_all_categories()
        assert ["fooo", "1"] in s.get_all_category_members("fooo")
        assert ["bar", "2"] in s.get_all_category_members("bar")

    def test_post_link_not_existing_element(self, app):
        self._fill_db()
        res = app.post("/api/link/between/foo/20/and/bar/2")
        assert res.status_code == 204
        s = server.db
        assert [["foo", "20"], ["bar", "2"]] in s.get_all_links()
        assert "foo" in s.get_all_categories()
        assert "bar" in s.get_all_categories()
        assert ["foo", "20"] in s.get_all_category_members("foo")
        assert ["bar", "2"] in s.get_all_category_members("bar")

    def test_post_link_not_existing_category_strict(self, app):
        self._fill_db()
        db_before = deepcopy(server.db.db)
        res = app.post("/api/link/between/fooo/1/and/bar/2",
                       data={'strict': 'true'})
        assert res.status_code == 400
        assert db_before == server.db.db

    def test_get_links(self, app):
        self._fill_db()
        res = app.get("/api/links")
        assert res.status_code == 200
        assert json.loads(res.data.decode('utf-8')) == self.LINKS

    def test_get_links_of_elem(self, app):
        self._fill_db()
        res = app.get("/api/links/of/foo/2")
        assert res.status_code == 200
        assert_link_lists_equal(json.loads(res.data.decode('utf-8')),
                                [l for l in self.LINKS if ["foo", "2"] in l])

    def test_get_links_of_missing_elem(self, app):
        self._fill_db()
        res = app.get("/api/links/of/foo/22")
        assert res.status_code == 404

    def test_delete_link(self, app):
        self._fill_db()
        res = app.delete("/api/link/between/foo/2/and/bar/8")
        assert res.status_code == 204
        l = deepcopy(self.LINKS)
        l.remove([["foo", "2"], ["bar", "8"]])
        s = server.db
        assert s.get_all_links() == l

    def test_delete_link_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        res = app.delete("/api/link/between/foo/2/and/bar/8",
                         data={'db_version': str(dbv)})
        assert res.status_code == 409

    def test_delete_missing_link(self, app):
        self._fill_db()
        res = app.delete("/api/link/between/foo/3/and/bar/8")
        assert res.status_code == 204

    def test_delete_missing_link_strict(self, app):
        self._fill_db()
        res = app.delete("/api/link/between/foo/3/and/bar/8",
                         data={'strict': 'true'})
        assert res.status_code == 404

    def test_add_link_and_delete_elem_parallel(self, app, async_listener):
        self._fill_db()
        log.debug('%s %s Done filling the db.' % (os.getpid(),
                                                  threading.current_thread()))

        listener = async_listener(app)

        resw1 = [None]
        resw2 = [None]

        dbv = str(server.db.timestamp)

        def worker1(target=[None]):  # add the link
            target[0] = app.post("/api/link/between/foo/6/and/bar/8",
                                 data={'strict': 'true',
                                       'db_version': dbv})
            return target[0]

        def worker2(target=[None]):  # remove the element
            target[0] = app.delete("/api/category/bar/8",
                                   data={'strict': 'true',
                                         'db_version': dbv})
            return target[0]

        tw1 = Thread(target=worker1, args=(resw1,))
        tw2 = Thread(target=worker2, args=(resw2,))
        tw1.start()
        tw2.start()
        tw1.join()
        tw2.join()

        time.sleep(0.5)  # wait for polls to be up to date

        assert len(listener.results) >= 1
        assert all([r.status_code == 200 for r in listener.results])
        assert len(listener.diffs) == 1

        if resw2[0].status_code in (409, 400):
            # second worker was slower
            # => link is added, element wasn't deleted
            assert resw1[0].status_code == 204
            assert (len(listener.diffs[0]['insertions']['links']) == 1)
            assert '8' in server.db.get_category("bar")
            assert ([["foo", "2"], ["bar", "8"]] in
                    server.db.get_all_links())
        elif resw1[0].status_code == (409, 400):
            # first worker was slower
            # => element was deleted, link wasn't added
            assert resw2[0].status_code == 204
            assert (len(listener.diffs[0]['deletions']['elements']) == 1)
            assert '8' not in server.db.get_category("bar")
            assert ([["foo", "2"], ["bar", "8"]] not in
                    server.db.get_all_links())
        else:
            # this will fail because it shouldn't happen...
            assert (409 in [resw1[0].status_code, resw2[0].status_code] or
                    400 in [resw1[0].status_code, resw2[0].status_code])

        listener.cleanup()

    @pytest.mark.parametrize(("url", "method", "expected_diff"), [
        ("/api/category/fooo", "post",
         {"insertions": {"categories": ["fooo"]}}),
        ("/api/category/foo", "delete",
         {"deletions": {"categories": ["foo"],
                        "elements": [["foo", x] for x in CATEGORIES["foo"]],
                        "links": [l for l in LINKS
                                  if "foo" in (l[0][0], l[1][0])]}}),
        ("/api/category/foo/bar", "post",
         {"insertions": {"elements": [["foo", "bar"]]}}),
        ("/api/category/foo/1", "delete",
         {"deletions": {"elements": [["foo", "1"]],
                        "links": [l for l in LINKS
                                  if ["foo", "1"] in l]}}),
        ("/api/link/between/foo/3/and/bar/4", "post",
         {"insertions": {"links": [[["foo", "3"], ["bar", "4"]]]}}),
        ("/api/link/between/foo/2/and/bar/8", "delete",
         {"deletions": {"links": [[["foo", "2"], ["bar", "8"]]]}})
    ])
    def test_poll(self, app, url, method, expected_diff, async_listener):
        default_diff_dict = {
            "deletions": {
                "categories": [],
                "elements": [],
                "links": []
            },
            "insertions": {
                "categories": [],
                "elements": [],
                "links": []
            }
        }

        full_expected_diff = recursive_dict_merge(default_diff_dict,
                                                  expected_diff)

        self._fill_db()
        listener = async_listener(app)
        res = app.open(url, method=method)
        if method == 'GET':
            assert res.status_code == 200
        else:
            assert res.status_code == 204

        # wait for poll result
        i = 0
        while i <= 10:
            i += 1
            if len(listener.diffs) == 0:
                time.sleep(0.1)
            else:
                break
        if len(listener.diffs) == 0:
            assert False, "Poll took to long to return"
        diffs = listener.diffs
        assert len(diffs) == 1
        for ins_del in full_expected_diff:
            for cls in full_expected_diff[ins_del]:
                if cls != 'links':
                    assert (sorted(diffs[0][ins_del][cls]) ==
                            sorted(full_expected_diff[ins_del][cls]))
                else:
                    assert (sorted([sorted(x) for x in
                                    diffs[0][ins_del][cls]]) ==
                            sorted([sorted(x) for x in
                                    full_expected_diff[ins_del][cls]]))

        listener.cleanup()

    @pytest.mark.parametrize("fill_db", [True, False])
    def test_poll_no_change(self, app, fill_db, async_listener):
        if fill_db:
            self._fill_db()
        listener = async_listener(app)
        # wait for no poll result ;)
        i = 0
        while i < 30:
            i += 1
            if len(listener.results) < 3:
                time.sleep(0.1)
            else:
                break
        assert len(listener.diffs) == 0
        listener.cleanup()

    @pytest.mark.parametrize('sizing_given', [True, False])
    def test_rename_category(self, app, sizing_given):
        self._fill_db()
        if sizing_given:
            server.settings.update({'sizing': {'foo': 'links'},
                                    'display-type': {'foo': 'links'}})
        sizing_before = server.settings._data['sizing'].get('foo')
        display_type_before = server.settings._data['display-type'].get('foo')
        res = app.put('/api/rename/foo/to/fooo')
        assert res.status_code in (200, 204)
        assert sorted(server.db.get_all_categories()) == sorted(["fooo", "bar"])
        assert (sorted(server.db.get_all_category_members('fooo')) ==
                sorted([['fooo', e] for e in TestServer.CATEGORIES['foo']]))
        l = [[['fooo' if e[0] == 'foo' else e[0], e[1]]
              for e in l]
             for l in TestServer.LINKS]
        assert_link_lists_equal(server.db.get_all_links(), l)
        assert server.settings._data['sizing'].get('fooo') == sizing_before
        assert server.settings._data['display-type'].get('fooo') == \
               display_type_before

    def test_rename_missing_category(self, app):
        self._fill_db()
        res = app.put('/api/rename/foobar/to/fooo')
        assert res.status_code == 404

    def test_rename_categories_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        res = app.put('/api/rename/foo/to/fooo',
                      data={'db_version': str(dbv)})
        assert res.status_code == 409

    def test_rename_categories_invalid_dbv(self, app):
        self._fill_db()
        res = app.put('/api/rename/foo/to/fooo',
                      data={'db_version': 'this is no timestamp.'})
        assert res.status_code == 400

    def test_rename_category_to_existing(self, app):
        self._fill_db()
        res = app.put('/api/rename/foo/to/bar')
        assert res.status_code == 400

    def test_api_fail(self, app):
        # oh, I *love* this url
        res = app.get('api/fail')
        assert res.status_code == 500

    def test_fail(self, app):
        # oh, I *love* this url
        with pytest.raises(Exception):
            res = app.get('/fail')

    VALID_SETTINGS = [
        {'sizing': {'foo': 'links'},
         'display-priority': {'bar': 1},
         'display-type': {'bar': 'cloud'}},
        {'sizing': {'foo': 'none'},
         'display-priority': {'bar': 1},
         'display-type': {'bar': 'cloud'}},
        {'sizing': {'foo': 'recency'},
         'display-priority': {'bar': 1},
         'display-type': {'bar': 'cloud'}},
        {'sizing': {'foo': 'links'},
         'display-priority': {'bar': 1},
         'display-type': {'bar': 'links'}},
    ]

    @pytest.mark.parametrize('setting', VALID_SETTINGS)
    def test_get_settings(self, app, setting):
        server.settings.update(setting)
        res = app.get('/api/settings')
        assert res.status_code == 200
        assert json.loads(res.data.decode('utf-8')) == setting

    @pytest.mark.parametrize('setting', VALID_SETTINGS)
    @pytest.mark.parametrize('direct', [True, False])
    def test_get_settings_ws_update(self, app, async_listener, setting, direct):
        listener = async_listener(app)
        if direct:
            server.settings.update(setting)
        else:
            res = json_put(app, '/api/settings', setting)
            assert 200 <= res.status_code < 300
        assert all([r.status_code == 200 for r in listener.results])
        assert len(listener.settings) == 1
        assert listener.settings[0] == setting
        listener.cleanup()

    def test_get_settings_ws_rename_cat(self, app, async_listener):
        self._fill_db()
        server.settings.update({'display-type': {'foo': 'links'}})
        listener = async_listener(app)
        res = app.put('/api/rename/foo/to/foobar')
        assert 200 <= res.status_code < 300
        new_cats = list(TestServer.CATEGORIES.keys())
        new_cats[new_cats.index('foo')] = 'foobar'
        assert (sorted(server.db.get_all_categories()) == sorted(new_cats))
        assert all([r.status_code for r in listener.results])
        assert len(listener.settings) == 1
        assert 'display-type' in listener.settings[0]
        assert 'foobar' in listener.settings[0]['display-type']
        assert listener.settings[0]['display-type']['foobar'] == 'links'

    @pytest.mark.parametrize('setting', VALID_SETTINGS)
    def test_put_settings(self, app, setting):
        res = app.put('/api/settings',
                      headers={'Content-Type': 'application/json'},
                      data=json.dumps(setting))
        assert res.status_code == 204
        assert server.settings.get() == setting

    def test_put_settings_wrong_format(self, app):
        setting = {'sizing': {'foo': 'links'},
                   'display-priority': {'bar': 1},
                   'display-type': {'bar': 'cloud'}}
        res = app.put('/api/settings',
                      data=json.dumps(setting))
        assert res.status_code == 400

    @pytest.mark.parametrize('setting', [
        {'sizing': {'foo': 'whatsoever'},
         'display-priority': {'bar': 1},
         'display-type': {'bar': 'cloud'}},
        {'sizing': {'foo': 'links'},
         'display-priority': {'bar': 1},
         'display-type': {'bar': 'whatsoever'}},
        {'sizing': {'foo': 'links'},
         'display-priority': {'bar': 'whatsoever'},
         'display-type': {'bar': 'cloud'}}
    ])
    def test_put_invalid_settings(self, app, setting):
        res = app.put('/api/settings',
                      headers={'Content-Type': 'application/json'},
                      data=json.dumps(setting))
        assert res.status_code == 400

    def test_rename_element(self, app):
        self._fill_db()
        r1 = app.get('/api/links/of/foo/2')
        res = app.put('/api/rename/foo/2/to/40')
        r2 = app.get('/api/links/of/foo/40')
        assert res.status_code == 204
        assert ['foo', '40'] in server.db.get_all_category_members('foo')
        assert '2' not in server.db.get_all_category_members('foo')

    def test_rename_element_missing_category(self, app):
        self._fill_db()
        res = app.put('/api/rename/fooo/2/to/40')
        assert res.status_code == 404

    def test_rename_element_missing_element(self, app):
        self._fill_db()
        res = app.put('/api/rename/foo/20/to/40')
        assert res.status_code == 404

    def test_rename_element_existing_element(self, app):
        self._fill_db()
        res = app.put('/api/rename/foo/2/to/4')
        assert res.status_code == 400

    def test_rename_element_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        res = app.put('/api/rename/foo/2/to/40',
                      data={'db_version': str(dbv)})
        assert res.status_code == 409

    def test_rename_element_over_cat(self, app):
        self._fill_db()
        r1 = app.get('/api/links/of/foo/1')
        res = app.put('/api/rename/foo/1/to/bar/100')
        r2 = app.get('/api/links/of/bar/100')
        assert res.status_code == 204
        assert ['bar', '100'] in server.db.get_all_category_members('bar')
        assert '1' not in server.db.get_all_category_members('foo')

    def test_rename_element_over_cat_missing_category(self, app):
        self._fill_db()
        res = app.put('/api/rename/fooo/2/to/bar/40')
        assert res.status_code == 404

    def test_rename_element_over_cat_missing_element(self, app):
        self._fill_db()
        res = app.put('/api/rename/foo/20/to/bar/40')
        assert res.status_code == 404

    def test_rename_element_over_cat_existing_element(self, app):
        self._fill_db()
        res = app.put('/api/rename/foo/2/to/bar/4')
        assert res.status_code == 400

    def test_rename_element_over_cat_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        res = app.put('/api/rename/foo/2/to/40',
                      data={'db_version': str(dbv)})
        assert res.status_code == 409

    def test_bulk_action(self, app):
        self._fill_db()
        json_diff = json.dumps({
            'insertions': {
                'categories': ['foobar'],
                'elements': [['foobar', 'test']]
            }
        })
        res = app.post('/api/bulkaction',
                      data={'diff': json_diff})
        assert res.status_code == 204
        assert 'foobar' in server.db.get_all_categories()
        assert ([['foobar', 'test']] ==
                server.db.get_all_category_members('foobar'))

    def test_bulk_action_invalid_diff(self, app):
        self._fill_db()
        json_diff = json.dumps({
            'insertions': ['broken']
        })
        res = app.post('/api/bulkaction',
                      data={'diff': json_diff})
        assert res.status_code == 400

    def test_bulk_action_old_dbv(self, app):
        self._fill_db()
        dbv = server.db.all_available_timestamps()[0]
        json_diff = json.dumps({
            'insertions': {
                'categories': ['foobar'],
            }
        })
        res = app.post('/api/bulkaction',
                      data={'diff': json_diff,
                            'db_version': str(dbv)})
        assert res.status_code == 409

    def test_get_user_count(self, app):
        res = app.get('/api/user_count')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert isinstance(data, int)
        assert 0 <= data

    def test_get_user_count_ws(self, app, async_listener):
        listener1 = async_listener(app)
        listener2 = async_listener(app)
        assert all([r.status_code == 200 for r in listener1.results])
        assert all([r.status_code == 200 for r in listener2.results])
        assert 0< len(listener1.user_counts) <= 1
        assert listener1.user_counts[-1] == 2
        listener3 = async_listener(app)
        assert all([r.status_code == 200 for r in listener1.results])
        assert all([r.status_code == 200 for r in listener2.results])
        assert all([r.status_code == 200 for r in listener3.results])
        assert 1 < len(listener1.user_counts) <= 2
        assert 0 < len(listener2.user_counts) <= 1
        assert listener1.user_counts[-1] == 3
        assert listener2.user_counts[-1] == 3
        listener3.cleanup()
        assert all([r.status_code == 200 for r in listener1.results])
        assert all([r.status_code == 200 for r in listener2.results])
        assert 2 < len(listener1.user_counts) <= 3
        assert 1 < len(listener2.user_counts) <= 2
        assert listener1.user_counts[-1] == 2
        assert listener2.user_counts[-1] == 2
        listener1.cleanup()
        listener2.cleanup()

    def test_query_all_params(self, app):
        self._fill_db()
        dbvs = server.db.all_available_timestamps()
        res = app.get('/api/diff/query?start={}&end={}'.format(
            *map(url_quote, map(str, dbvs))
        ), headers={'ETag': str(dbvs[1])})
        assert res.status_code == 400

    @pytest.mark.parametrize('start_idx', [0, 1, 2, None])
    @pytest.mark.parametrize('end_idx', [0, 1, 2, None])
    @pytest.mark.parametrize('use_etag_for', ['start', 'end', None])
    def test_query(self, app, start_idx, end_idx, use_etag_for):
        self._fill_db()
        server.db.add_to_category('foobaz', 'hi')
        dbvs = server.db.all_available_timestamps()
        start_timestamp = dbvs[start_idx] if start_idx is not None else None
        end_timestamp = dbvs[end_idx] if end_idx is not None else None
        start, end = start_timestamp, end_timestamp
        headers = {}
        if use_etag_for == 'start' and start is not None:
            if end_idx is None:
                # can't use ETag for start if end is not given
                return
            headers['ETag'] = str(start)
            start = None
        elif use_etag_for == 'end' and end is not None:
            headers['ETag'] = str(end)
            end = None
        q = {k: str(v) for k, v in (('start', start), ('end', end))
             if v is not None}
        q = '&'.join(["%s=%s" % tuple(map(url_quote, p))
                      for p in q.items()])
        res = app.get('/api/diff/query?'+q, headers=headers)
        data = res.data.decode('utf-8')
        assert res.status_code == 200
        assert json.loads(data) == server.db.calc_db_diff(start_timestamp,
                                                          end_timestamp)

    @pytest.mark.parametrize('add_cat', [True, False])
    def test_get_all_timestamps(self, app, add_cat):
        self._fill_db()
        if add_cat:
            server.db.add_category('foobar')
        res = app.get('/api/all_timestamps')
        assert res.status_code == 200
        assert (json.loads(res.data.decode('utf-8')) ==
                list(map(str, server.db.all_available_timestamps())))

    def test_get_similarities_wraps_store_similarities(self, app):
        self._fill_db()
        res = app.get('/api/similarities')
        assert res.status_code == 200
        assert json.loads(res.data.decode('utf-8')) == server.db.similarities()

    @pytest.mark.parametrize('add_cat', [True, False])
    def test_get_recently_changed_default_seconds(self, app, add_cat):
        now = datetime.datetime.utcnow()
        with freezegun.freeze_time(now):
            self._fill_db()
        if add_cat:
            with freezegun.freeze_time(now+datetime.timedelta(seconds=1)):
                app.post('/api/category/bananaS')
        with freezegun.freeze_time(
                now +
                datetime.timedelta(seconds=server.conf['RECENT_SECONDS']+1)):
            res = app.get('/api/recently_changed')
        assert res.status_code == 200
        if add_cat:
            assert (json.loads(res.data.decode('utf-8')) == ['bananaS'])
        else:
            assert (json.loads(res.data.decode('utf-8')) == [])

    @pytest.mark.parametrize('add_cat', [True, False])
    def test_get_recently_changed_custom_seconds(self, app, add_cat):
        now = datetime.datetime.utcnow()
        with freezegun.freeze_time(now):
            self._fill_db()
        if add_cat:
            with freezegun.freeze_time(now+datetime.timedelta(seconds=1)):
                app.post('/api/category/bananaS')
        with freezegun.freeze_time(
                now +
                datetime.timedelta(seconds=42000001)):
            res = app.get('/api/recently_changed?seconds=42000000')
        assert res.status_code == 200
        if add_cat:
            assert (json.loads(res.data.decode('utf-8')) == ['bananaS'])
        else:
            assert (json.loads(res.data.decode('utf-8')) == [])

    def test_encoding(self, app):
        # create category
        res = app.post('/api/category/\xe4')
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert '\xe4' in data
        assert data['\xe4'] == {}
        # create element
        res = app.post('/api/category/\xe4/\xe6')
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert '\xe4' in data
        assert '\xe6' in data['\xe4']
        # delete element
        res = app.delete('/api/category/\xe4/\xe6')
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert '\xe4' in data
        assert data['\xe4'] == {}
        # delete category
        res = app.delete('/api/category/\xe4')
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert data == {}

        # create element AND category
        res = app.post('/api/category/\xe4/\xe6')
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert '\xe4' in data
        assert '\xe6' in data['\xe4']
        # delete category with element
        res = app.delete('/api/category/\xe4')
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert data == {}

    def test_unquoting(self, app):
        # create category
        t_ = url_quote('Ŧ')
        ae = url_quote('æ')
        res = app.post(url_quote('/api/category/' + t_))
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert 'Ŧ' in data
        assert data['Ŧ'] == {}
        # create element
        res = app.post('/api/category/' + t_ + '/' + ae)
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert 'Ŧ' in data
        assert 'æ' in data['Ŧ']
        # delete element
        res = app.delete('/api/category/' + t_ + '/' + ae)
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert 'Ŧ' in data
        assert data['Ŧ'] == {}
        # delete category
        res = app.delete('/api/category/' + t_)
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert data == {}

        # create element AND category
        res = app.post('/api/category/' + t_ + '/' + ae)
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert 'Ŧ' in data
        assert 'æ' in data['Ŧ']
        # delete category with element
        res = app.delete('/api/category/' + t_)
        assert res.status_code == 204
        res = app.get('/api/categories')
        assert res.status_code == 200
        data = json.loads(res.data.decode('utf-8'))
        assert data == {}


    ##################
    # password tests #
    ##################

    def _assert_allowed(self, app, url, method='get'):
        res = app.__getattribute__(method)(url)
        assert 200 <= res.status_code < 500 and res.status_code != 403
        if 300 <= res.status_code < 400:
            assert '/login.html' not in res.location

    def _assert_not_allowed(self, app, url, method='get'):
        res = app.__getattribute__(method)(url)
        if url.startswith('/api/'):
            assert res.status_code == 403
        else:
            assert 300 <= res.status_code < 400
            assert '/login.html' in res.location

    @pytest.mark.parametrize(('url', 'method', 'forced_auth'), (
        ('/', 'get', False),
        ('/clouds.html', 'get', False),
        ('/api.html', 'get', False),
        ('/api/all_timestamps', 'get', False),
        ('/api/category/foo', 'put', False),
        ('/api/category/foo', 'delete', False),
        ('/api/categories', 'get', False),
        ('/api/category/foo/bar', 'get', False),
        ('/api/category/foo/bar', 'put', False),
        ('/api/category/foo/bar', 'delete', False),
        ('/api/links', 'get', False),
        ('/api/links/of/foo/bar', 'get', False),
        ('/api/link/between/foo/bar/and/foo/baz', 'put', False),
        ('/api/link/between/foo/bar/and/foo/baz', 'delete', False),
        ('/api/diff/query', 'get', False),
        ('/api/rename/foo/to/bar', 'put', False),
        ('/api/rename/foo/bar/to/baz', 'put', False),
        ('/api/rename/foo/bar/to/fuh/barr', 'put', False),
        ('/api/bulkaction', 'post', False),
        ('/api/recently_changed', 'get', False),
        ('/api/user_count', 'get', False),
        ('/api/settings', 'get', False),
        ('/api/token', 'get', True),
        ('/api/settings', 'put', False)
    ))
    @pytest.mark.parametrize('login', ['auth', 'auth_pw', 'password',
                                       'cookie', None])
    def test_password_required(self, app_all_pw_and_ro_combinations,
                               url, method, login, forced_auth):
        app = app_all_pw_and_ro_combinations
        password = server.conf['PASSWORD'] is not None
        ro = server.conf['ALLOW_READ_ONLY']
        print(server.conf['PASSWORD'], server.conf['ALLOW_READ_ONLY'])
        if login is None:
            if not password or (method == 'get' and ro and not forced_auth):
                self._assert_allowed(app, url, method=method)
            else:
                self._assert_not_allowed(app, url, method=method)
        else:
            if login == 'auth':
                url = auth_util.add_auth_to_url(url,
                                                util_server.PASSWORD,
                                                util_server.SALT)
            elif login == 'auth_pw':
                url = auth_util.add_auth_to_url(url, util_server.PASSWORD)
            elif login == 'password':
                url = auth_util.add_password_to_url(url, util_server.PASSWORD)
            elif login == 'cookie':
                util_server.set_auth_cookie(app,
                                            util_server.PASSWORD,
                                            util_server.SALT)
            self._assert_allowed(app, url, method=method)

    @pytest.mark.parametrize(('url', 'method'), (
        ('/js/clouds.js', 'get'),
        ('/js/common.js', 'get'),
        ('/css/api.css', 'get'),
        ('/css/login.css', 'get'),
        ('/img/icon.png', 'get'),
        ('/login.html', 'get'),
        ('/api/auth_status', 'get'),
    ))
    def test_no_password_required(self, app_all_pw_and_ro_combinations,
                                  url, method):
        app = app_all_pw_and_ro_combinations
        self._assert_allowed(app, url, method=method)

    def test_case_insensitive_auth(self, app_w_pw):
        url = '/api/category/foobar'
        res = app_w_pw.post(url)
        assert res.status_code == 403
        auth_url = auth_util.add_auth_to_url(url,
                                             util_server.PASSWORD,
                                             util_server.SALT)
        res = app_w_pw.post(auth_url)
        assert res.status_code == 204
        upper_auth_url = re.sub(r'(?<=auth=)([0-9a-zA-Z]+)',
                                lambda m: m.group().upper(),
                                auth_url)
        server.db.remove_category('foobar')
        res = app_w_pw.post(upper_auth_url)
        assert res.status_code == 204
        mixed_up_auth_url = re.sub(
            r'(?<=auth=)([0-9A-Z]*[a-z][0-9A-Z]*)([a-z]+)',
            lambda m: m.groups()[0] + m.groups()[1].upper(),
            auth_url
        )
        server.db.remove_category('foobar')
        res = app_w_pw.post(mixed_up_auth_url)
        assert res.status_code == 204
        mixed_lo_auth_url = re.sub(
            r'(?<=auth=)([0-9a-z]*[A-Z][0-9a-z]*)([A-Z]+)',
            lambda m: m.groups()[0] + m.groups()[1].lower(),
            auth_url
        )
        server.db.remove_category('foobar')
        res = app_w_pw.post(mixed_lo_auth_url)
        assert res.status_code == 204

    def test_login(self, app_w_pw):
        res = app_w_pw.post('/api/category/foobar')
        assert res.status_code == 403
        res = app_w_pw.get('/login.html')
        assert res.status_code == 200
        res = app_w_pw.post('/login.html',
                            data={'password': util_server.PASSWORD})
        assert 200 <= res.status_code < 400
        res = app_w_pw.post('/api/category/foobar')
        assert res.status_code == 204

    def test_get_token_without_pw(self, app):
        res = app.get('/api/token')
        assert res.status_code == 204

    @pytest.mark.parametrize('login', ['auth', 'auth_pw', 'password', 'cookie'])
    def test_get_token_with_pw(self, app_w_pw, login):
        url = '/api/token'
        res = app_w_pw.get(url)
        assert res.status_code == 403
        auth_token = auth_util.auth_token(util_server.PASSWORD,
                                          util_server.SALT)
        if login == 'auth':
            url = auth_util.add_auth_to_url(url,
                                            util_server.PASSWORD,
                                            util_server.SALT)
        elif login == 'auth_pw':
            url = auth_util.add_auth_to_url(url, util_server.PASSWORD)
        elif login == 'password':
            url = auth_util.add_password_to_url(url, util_server.PASSWORD)
        elif login == 'cookie':
            util_server.set_auth_cookie(app_w_pw,
                                        util_server.PASSWORD,
                                        util_server.SALT)
        res = app_w_pw.get(url)
        assert res.status_code == 200
        assert json.loads(res.data.decode('utf-8')) == auth_token

    @pytest.mark.parametrize('login',
                             ['auth', 'auth_pw', 'password', 'cookie', None])
    def test_get_auth_status(self, app_all_pw_and_ro_combinations, login):
        app = app_all_pw_and_ro_combinations
        password = server.conf['PASSWORD'] is not None
        ro = server.conf['ALLOW_READ_ONLY'] and password
        url = '/api/auth_status'
        if login == 'auth':
            url = auth_util.add_auth_to_url(url,
                                            util_server.PASSWORD,
                                            util_server.SALT)
        elif login == 'auth_pw':
            url = auth_util.add_auth_to_url(url, util_server.PASSWORD)
        elif login == 'password':
            url = auth_util.add_password_to_url(url, util_server.PASSWORD)
        elif login == 'cookie':
            util_server.set_auth_cookie(app,
                                        util_server.PASSWORD,
                                        util_server.SALT)
        res = app.get(url)
        assert res.status_code == 200
        res = json.loads(res.data.decode('utf-8'))
        assert res == {'passwordSet': password, 'readOnly': ro,
                       'loggedIn': not password or login is not None}

    @add_webhook_bot_to_app
    def test_webhook_bot_without_auth(self, app):
        payload = {'token': util_server.WEBHOOK_PAIRS[0][0],
                   'text': 'will be ignored!'}
        res = json_post(app, '/api/chat/webhook', payload)
        assert 200 <= res.status_code < 300
        payload = {'token': 'wrong-token',
                   'text': 'will be ignored!'}
        res = json_post(app, '/api/chat/webhook', payload)
        assert res.status_code == 403
        payload = {'text': 'will be ignored!'}
        res = json_post(app, '/api/chat/webhook', payload)
        assert res.status_code == 403

    def test_webhook_bot_with_auth(self, app_w_pw):
        self.test_webhook_bot_without_auth(app_w_pw)
