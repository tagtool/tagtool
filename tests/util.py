from copy import deepcopy


def recursive_dict_merge(dict1, dict2):
    def _recursive_dict_merge(a, b, path=None):
        """merges b into a

        copied from http://stackoverflow.com/questions/7204805/dictionaries-of-dictionaries-merge
        with slight changes"""
        if path is None:
            path = []
        for key in b:
            if key in a:
                if isinstance(a[key], dict) and isinstance(b[key], dict):
                    _recursive_dict_merge(a[key], b[key],
                                          path + [str(key)])
                elif a[key] == b[key]:
                    pass  # same leaf value
                else:
                    try:
                        a[key] += b[key]
                    except TypeError:
                        raise Exception(
                            'Conflict at %s' % '.'.join(path + [str(key)]))
            else:
                a[key] = b[key]
        return a

    return _recursive_dict_merge(deepcopy(dict1), dict2)


def assert_link_lists_equal(link_list_a, link_list_b):
    assert sorted(map(sorted, link_list_a)) == sorted(map(sorted, link_list_b))


def asset_links_equal(link_a, link_b):
    assert sorted(link_a) == sorted(link_b)


def assert_same_elems(l1, l2):
    l1, l2 = map(list, [l1, l2])
    assert len(l1) == len(l2) and sorted(l1) == sorted(l2)
