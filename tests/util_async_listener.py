import time
import json
from threading import Thread
from collections import defaultdict
import pytest
from server import app
from server import socket_io


WS_EVENTS = ['diff', 'user_count', 'settings']


class DummyResponse(object):
    def __init__(self):
        self.status_code = 200


class WebsocketListener(object):
    def __init__(self, test_app):
        self.socket_io_client = socket_io.test_client(app)
        self.socket_io_client.get_received()
        self._results = []
        self._updates = defaultdict(list)
        self.app = test_app

    def _get_ws_updates(self):
        recvd = self.socket_io_client.get_received()
        self._results += len(recvd) * [DummyResponse()]
        for msg in recvd:
            name = msg['name']
            if name in WS_EVENTS:
                self._updates[name].append(json.loads(msg['args'][0]))

    def __getattr__(self, item):
        # allows to do things like listener.diffs
        if item.endswith('s') and item not in WS_EVENTS:
            item = item[:-1]
        if item in WS_EVENTS:
            self._get_ws_updates()
            return self._updates[item]
        raise AttributeError

    @property
    def results(self):
        self._get_ws_updates()
        return self._results

    def cleanup(self):
        self.socket_io_client.disconnect()

    def __del__(self):
        self.cleanup()


@pytest.fixture(params=[WebsocketListener])
def async_listener(request):
    return request.param
