import pytest

import datetime
import json
import errno
import shutil
import decorator

import auth_util

import server
from config_loader import load_config

server.main()


PASSWORD = 'MySecretTestPassword!'
SALT = 'EvenMoreSecret.'

WEBHOOK_BOT_NAME = 'tagtool'
WEBHOOK_PAIRS = [
    ('token', 'http://test.example.com/nicehook', False)
]


def patch_server_conf(data_dir, password=False, ro=False):
    def tear_down():
        try:
            shutil.rmtree(data_dir)
        except OSError as error:
            if error.errno == errno.ENOENT:
                pass
            else:
                raise

    server.app.config['TESTING'] = True
    server.conf, server.settings_found = load_config('default_settings.py')
    server.conf['PASSWORD'] = PASSWORD if password else None
    server.conf['SECRET_KEY'] = SALT
    server.conf['ALLOW_READ_ONLY'] = ro
    server.conf['DATA_DIR'] = data_dir
    tear_down()
    server.open_store()
    server.bind_ws_to_db_and_settings()
    return tear_down


def _generate_app(request, password=False, ro=False):
    data_dir = str(request.getfixturevalue('tmpdir'))
    tear_down = patch_server_conf(data_dir, password=password, ro=ro)
    request.addfinalizer(tear_down)
    return server.app.test_client()


@pytest.fixture()
def app(request):
    return _generate_app(request)


@pytest.fixture(params=(True, False))
def app_w_pw(request):
    return _generate_app(request, password=True, ro=request.param)


@pytest.fixture()
def app_w_pw_wo_ro(request):
    return _generate_app(request, password=True)


@pytest.fixture()
def app_w_pw_and_ro(request):
    return _generate_app(request, password=True, ro=True)


@pytest.fixture()
def app_wo_pw_and_ro(request):
    return _generate_app(request, password=False, ro=True)


@pytest.fixture(params=(0, 1, 2, 3))
def app_all_pw_and_ro_combinations(request):
    password = (request.param & 2) / 2
    ro = (request.param & 1)
    return _generate_app(request, password=password, ro=ro)


def add_webhook_bot_to_app(func):
    def inner(_func, *args, **kwargs):
        server.conf['DIGEST_MESSAGES_OVER_MINUTES'] = 0
        server.conf['WEBHOOK_PAIRS'] = WEBHOOK_PAIRS
        server.conf['WEBHOOK_BOT_NAME'] = WEBHOOK_BOT_NAME
        bot = server.register_webhook_bot()
        server.bots.append(bot)
        try:
            _func(*args, **kwargs)
        finally:
            bot.stop()
            server.bots.remove(bot)

    return decorator.decorate(func, inner)


def set_auth_cookie(app, password, salt):
    app.set_cookie(
        'localhost',
        'TagToolAuth', auth_util.auth_token(password, salt),
        expires=datetime.datetime.utcnow() + datetime.timedelta(days=365),
        httponly=True
    )


def json_action(app, url, action, data, *args, **kwargs):
    return getattr(app, action)(
        url, data=json.dumps(data), content_type='application/json',
        *args, **kwargs)


def json_post(app, url, data, *args, **kwargs):
    return json_action(app, url, 'post', data, *args, **kwargs)


def json_put(app, url, data, *args, **kwargs):
    return json_action(app, url, 'put', data, *args, **kwargs)
