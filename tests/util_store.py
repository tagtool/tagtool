import shutil
import errno
import pytest
import datetime
from models.store import Store


def _load_store(path):
    return Store(path=path)


def _destroy_store(path):
    try:
        shutil.rmtree(path)
    except OSError as e:
        if e.errno == errno.ENOENT:
            pass
        else:
            raise


@pytest.fixture()
def store(request, tmpdir):
    path = str(tmpdir)
    request.addfinalizer(lambda: _destroy_store(path))
    return _load_store(path)


def time_now_func():
    return datetime.datetime.now()


@pytest.fixture()
def time_now():
    return time_now_func()


@pytest.fixture()
def time_previous():
    return datetime.datetime.now() - datetime.timedelta(minutes=1)
