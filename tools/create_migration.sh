#!/usr/bin/env bash

template="from models.migration import MigrationBase

from logging import getLogger
log = getLogger(__name__)


class Migration(MigrationBase):
    DESCRIPTION = \"Give a short migration summary.\"

    def __init__(self, *args, **kwargs):
        super(Migration, self).__init__(*args, **kwargs)

    # uncomment if you want to migrate the db state files
    # def run(self, db, timestamp):
    #     return db

    # uncomment if you want to migrate the diff_log file
    # def run_diff_log(self, diff_log):
    #     return diff_log

    # uncomment if you want to migrate the settings file
    # def run_settings(self, settings):
    #     return settings"
timestamp=$(date +%s)
fn="${BASH_SOURCE%/*}/../models/migrations/t${timestamp}.py"
echo "Creating migration template at $fn"
echo "$template" > "$fn"
