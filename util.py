import os
from functools import wraps, update_wrapper
from datetime import datetime
import traceback
import collections
import json
import urllib.parse as urlparse
from urllib.parse import urlencode
from flask import make_response
from werkzeug.wrappers import Response as ResponseBase
from werkzeug.exceptions import HTTPException

from logging import getLogger
log = getLogger(__name__)


# https://stackoverflow.com/questions/2301789/read-a-file-in-reverse-order-using-python
def readlines_reverse(filename):
    with open(filename) as qfile:
        qfile.seek(0, os.SEEK_END)
        position = qfile.tell()
        line = ''
        while position >= 0:
            qfile.seek(position)
            next_char = qfile.read(1)
            if next_char == "\n":
                yield line[::-1]
                line = ''
            else:
                line += next_char
            position -= 1
        yield line[::-1]


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    # http://stackoverflow.com/questions/38987/how-to-merge-two-python-dictionaries-in-a-single-expression
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, str):
            for sub in flatten(el):
                yield sub
        else:
            yield el


# found at http://arusahni.net/blog/2014/03/flask-nocache.html
def nocache(view):
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.now()
        response.headers['Cache-Control'] = ('no-store, no-cache, '
                                             'must-revalidate, post-check=0, '
                                             'pre-check=0, max-age=0')
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return no_cache


def add_params_to_url(url, params):
    # http://stackoverflow.com/questions/2506379/add-params-to-given-url-in-python
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)


def api_route(jsonify=json.dumps, returns_str=False):
    def decorator(func):
        bad_result_error = ValueError(
            "Result wasn't a valid JSON-String nor was it a jsonifiable object "
            "nor was it a json response object.")

        @wraps(func)
        def inner_api_route(*args, **kwargs):
            response = ResponseBase(
                mimetype='application/json',
            )
            try:
                result = func(*args, **kwargs)
                log.debug('API return %s: %s', func, result)
                if result is None or result == "":
                    log.debug('Empty API result')
                    response.status_code = 204  # 204 - No Content
                elif not returns_str and isinstance(result, str):
                    try:
                        result = jsonify(json.loads(result))
                    except (ValueError, TypeError) as e:
                        log.warn('Api return was no json: %s', result)
                        log.error(e)
                        log.error(traceback.format_exc())
                        result = jsonify(result)
                    log.debug('API result is a string %s', result)
                    response.data = result
                elif isinstance(result, ResponseBase):
                    log.debug('API result is a flask response')
                    if result.mimetype != 'application/json':
                        log.error('flask responses must have mimetype "'
                                  '`application/json` for API methods.')
                        raise bad_result_error
                    response = result
                else:
                    log.debug('API other result type (try to jsonify w/ %s)',
                              jsonify)
                    try:
                        response.data = jsonify(result)
                        log.debug('API result jsonified: %s', response.data)
                    except (ValueError, TypeError) as e:
                        log.error('Failed to convert: %s', result)
                        log.error(e)
                        log.error(traceback.format_exc())
                        raise bad_result_error
            except HTTPException as e:
                response.data = jsonify(
                    {'code': e.code, 'description': e.description,
                     'message': str(e) or e.response}) + "\n"
                response.status_code = e.code
            except Exception as e:
                log.exception("api route server error")
                response.data = jsonify(
                    {'code': 500, 'error': e.__class__.__name__,
                     'message': str(e)}) + "\n"
                response.status_code = 500
            return response
        return inner_api_route
    return decorator
